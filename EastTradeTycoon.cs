#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2024- Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file (suppose its name is `${file}.cs`)
#   * and `utils.cs` in `steamapps` folder, open a terminal in the same folder,
#   * and execute:
#   *
#   * ```
#   *     chmod +x "${file}.cs" && ./"$_"
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro KDE), if such script do not work
#   * in your computer, you could just try the instructions below :

export CODE_START_LINE_NO=100                               # might be modified if the number mismatch.
export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="East Trade Tycoon Inheritance"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export VERSION="0.1.0"                                      # should be modified

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export GAME_BASE_DIR="common/$GAME_DIR"                     # this could be used for modifying the following 3 dir in a faster way. Should not be modified unless the mod is installed in another location.
export plugins_dir="${GAME_BASE_DIR}/BepInEx/plugins"
export BepInEx_dir="${GAME_BASE_DIR}/BepInEx/core"
export Managed_dir="${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed"
export plugins_name="${FILE_NAME%.*}"                       # change output dll name (avoid taiwu-like errors)
export BepInEx_dll='
    0Harmony.dll
    BepInEx.dll
    BepInEx.Harmony.dll
'
export Managed_dll=$([[ -e "${Managed_dir}/netstandard.dll" ]] && echo "netstandard.dll")'
    System.dll
    System.Core.dll
    UnityEngine.dll
    UnityEngine.AIModule.dll
    UnityEngine.CoreModule.dll
    UnityEngine.UI.dll
    mscorlib.dll
''
' # game specific dlls could be added here.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[[ -z "$DOTNET_CSC_DLL" ]] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll
export IFS=$'\n' # to disable the annoying space.
case $1 in
    V | v | VERBOSE | verbose )   EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
    D | d | DEBUG   | debug   )   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
    *)                            EXTRA_DEFINE=""                                               ;;
esac

# Pre-compile code
# Compile
( yes "" | head -n $(($CODE_START_LINE_NO - 1)) && tail $FILE_NAME -n+$CODE_START_LINE_NO) | sed -e s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g -e s/%%PLUGIN_ID%%/${PLUGIN_ID}/g -e s/%%VERSION%%/${VERSION}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
    $(for i in ${BepInEx_dll} ; do [[ -z "${i// }" ]] || echo -e "-r:\"${BepInEx_dir}/${i// }\"\n" ; done) \
    $(for i in ${Managed_dll} ; do [[ -z "${i// }" ]] || echo -e "-r:\"${Managed_dir}/${i// }\"\n" ; done) \
    $(for i in "${Managed_dir}/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
    -out:"${plugins_dir}/${plugins_name}".dll \
    -optimize $EXTRA_DEFINE \
    - $UTILS || exit
# Post-compile code
rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg"
if [[ -n "$2" ]] ; then
    git add ${FILE_NAME}
    case $2 in
        R | r | RANDOM | random | U | u | UPLOAD | upload ) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit





using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

using Neutron3529.Utils;
namespace %%NAMESPACE_ID%%;
[BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "%%VERSION%%")]
public class Cheat : /*Neutron3529.Utils.*/ModEntry {
    public Cheat() : base("%%PLUGIN_ID%%") {}
    public override void Awake() {
        base.Awake();
        // 主要逻辑放在`utils.cs`中，这里的Awake2只是为了以防万一
        // 目前来说，这个函数的唯一用途是用来“叮”……
        // 就像这样：
        logger("叮~修改器启动，请安心游戏");
    }
    class Weight : Entry {
        [Desc("载具负重乘数")]
        static int mul = 100;
        [HarmonyPatch(typeof(ZaijuWindowClass),"StatuToLoad")]
        public static int Postfix(int ret) => ret * mul;
    }
    class Speed : Entry {
        [Desc("载具负重乘数")]
        static float mul = 100;
        [HarmonyPatch(typeof(ZaijuWindowClass),"CalZaijuAveSpeed")]
        public static float Postfix(float ret) => ret * mul;
    }
    [Desc("不疲劳")]
    class playerMoveClass : Entry {
        [HarmonyPatch(typeof(playerMoveClass),"PilaoChange")]
        public static bool Prefix() => false;
    }
    class Fight : Entry {
        [HarmonyPatch(typeof(robWindowClass),"returnRobWinRate")]
        public static bool Prefix(ref int  __result) {
            __result = 100;
            return false;
        }
    }
}
