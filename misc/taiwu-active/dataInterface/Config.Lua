return {GameVersion="0.0.0.0-Signed-by-Neutron",
    Title = "中子的数据接口 - 抢先体验版",
    --workshop_only    FileId = 1,
    Version = "3.5.2.9", -- 2022<<32+0925,
    NeedRestartWhenSettingChanged = false,
    Dependencies = {},
    --BackendPlugins = {"../Backend.dll"},
    --BackendPatches = {},
    --FrontendPlugins = {"../NeutronFrontend-DataInterface.dll"},
    --workshop_only    Source = 1,
    HasArchive = false,
    TagList = {"Configurations","Display"},
    Author = "Neutron3529",
    Description = "中子的数据接口，目前可以用于处理「因Mod新增物品导致存档爆炸」的问题。\n或许有更多功能，但……请相信中子放鸽子的水准。",
    DefaultSettings = {
    }
}
