// echo aux function only, should not compile alone.
// cp Utils.CS .. && cd $_ && for i in `\ls` ; do \cp -f Utils.CS $i/ ; done ; rm Utils.CS && cd "$OLDPWD"
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using static Utils.Logger;
using Config.EventConfig;
namespace Utils;
public static class Event {
    public static void DynContent(this TaiwuEventOption opt, Func<string> x){
        opt.GetReplacedContent=()=>{
            opt.SetContent(x());
            return string.Empty;
        };
    }
    public static string ModId=>"1_3113001972";
    public static string WorkshopDir=>System.IO.Path.GetDirectoryName(GameData.Domains.DomainManager.Mod.GetModDirectory(ModId)).Replace('\\','/');
    public static bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logwarn("enable 出错，出错键值为\""+key+"\"，错误原因是");
            logex(ex);
            return false;
        }
    }
    private static bool _enable(string key){
        bool enabled=false;
        return GameData.Domains.DomainManager.Mod.GetSetting(Event.ModId, key, ref enabled) && enabled;
    }
}
