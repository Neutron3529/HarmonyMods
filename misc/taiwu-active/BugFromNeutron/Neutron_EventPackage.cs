// for i in '-define:CHAIN_TRACING -define:VERBOSE@-debug' '-optimize@' ; do $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Linq.dll" -r:"../../Backend/System.Linq.Expressions.dll" -r:"../../Backend/System.Runtime.dll" -r:"../../Backend/System.ComponentModel.Primitives.dll" -unsafe -debug ${i%@*} -deterministic Neutron_EventPackage.cs ../UTILS/*.CS *.event.cs -out:Neutron_EventPackage${i#*@}.dll -define:CharacterMod -define:Neili & done ; wait
// -r:"../../Backend/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法
// 这段编译命令会同时生成Debug与Optimize两个dll
//   Debug的运行速度稍慢但会显示更精确的错误信息
//   Optimize会丢掉部分信息（比如出错信息精确到函数就谢天谢地了），但因此运行速度或许会快若干微秒。
// 因此默认Optimize，如果希望切debug，需自行动手改Config切换

// -r:"../../The Scroll of Taiwu_Data/Managed/30.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
// #define NEUTRON_DEBUG

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using static Utils.Logger;
using static Utils.Event;

using Config.EventConfig;
using GameData.Domains.TaiwuEvent;
using GameData.Domains.TaiwuEvent.Enum;

using static Neutron3529.Events.Const;
namespace Neutron3529.Events;

public static class Const {
    public static readonly string BASE_EVENT="4e657574-726f-6e20-3335-323900000000";
    public static readonly string BYPASS_EVENT="4e657574-726f-6e20-3335-323900000001";
    public static readonly string TRIGGER_EVENT="4e657574-726f-6e20-3335-323900000002";
    public static readonly string BASE_TRIGGER_PREFIX="4e657574-726f-6e20-3335-32390001";// "4e657574-726f-6e20-3335-323900010000" - "4e657574-726f-6e20-3335-32390001FFFF"
}
public static class ArrayAdd {
    public static void AddTo<T>(this T val, ref T[] self){
        var len=self.Length;
        Array.Resize<T>(ref self, len+1);
        self[len]=val;
    }
}
public static class Details {
    #nullable enable
    static HashSet<Type> allow=new HashSet<Type>{};
    public static string ToDetailedString(this Expression? expression){
        if(expression is null){
            return "(null)";
        }else if(expression is BlockExpression b){
            return b.ToDetailedString();
        } else if (expression is TryExpression t){
            return t.ToDetailedString();
        } else if (expression is MethodCallExpression m){
            return m.ToDetailedString();
        } else if (expression is BinaryExpression bi){
            return bi.ToDetailedString();
        } else if (expression is ConditionalExpression c){
            return c.ToDetailedString();
        } else if (expression is UnaryExpression u){
            return u.ToDetailedString();
        } else if (expression is MemberExpression me){
            return me.ToDetailedString();
        } else if (expression is LambdaExpression l){
            return l.ToDetailedString();
        } else if (expression is ParameterExpression || expression is DefaultExpression || expression is ConstantExpression){
            return expression.ToString();
        } else if (expression is null){
            return string.Empty;
        } else if (allow.Add(expression.GetType())){
            logger($"TODO: 未定义 ({expression.GetType()}) {expression} 的ToDetailedString");
        }
        return expression.ToString();
    }
    public static string ToDetailedString(this UnaryExpression expression)=>$"({expression.Method} {expression.Operand.ToDetailedString()})";
    public static string ToDetailedString(this LambdaExpression expression)=>$"({string.Join(", ",expression.Parameters.Select(x=>x.ToDetailedString()))})=>{{ {expression.Body.ToDetailedString()} }}";
    public static string ToDetailedString(this MemberExpression expression)=>$"({expression.Expression.ToDetailedString()}.{expression.Member.Name})";
    public static string ToDetailedString(this BinaryExpression expression)=>$"({expression.Left.ToDetailedString()} {expression.NodeType} {expression.Right.ToDetailedString()})";
    public static string ToDetailedString(this ConditionalExpression expression)=>$"if ({expression.Test.ToDetailedString()}) {{ {expression.IfTrue.ToDetailedString()} }} else {{ {expression.IfFalse.ToDetailedString()} }}";
    public static string ToDetailedString(this MethodCallExpression expression)=>$"{(expression.Object==null?string.Empty:expression.Object.ToDetailedString()+".")}{expression.Method.Name}({string.Join(", ",expression.Arguments.Select(x=>x.ToDetailedString()))})";
    public static string ToDetailedString(this BlockExpression expression)=>$"{{ {String.Join("; ",expression.Expressions.Select((x)=>x.ToDetailedString()))} }}";
    public static string ToDetailedString(this TryExpression expression)=>$"try {expression.Body.ToDetailedString()}";//$"try {{ {String.Join("; ",expression.Body.ToDetailedString())} }} "+string.Join(" ",expression.Handlers.Select((x)=>$"catch ({x.Test} {x.Variable}){{{x.Body.ToDetailedString()}}}"))+(expression.Fault is null || (expression.Fault is BlockExpression b && (b.Expressions is null || b.Expressions.Count==0))?$" fault {{ {expression.Fault.ToDetailedString()} }}":string.Empty)+(expression.Finally is null || (expression.Finally is BlockExpression c && (c.Expressions is null || c.Expressions.Count==0))?$" finally {{ {expression.Finally.ToDetailedString()} }}":string.Empty);
    #nullable restore
}

public class Events : EventPackage {
    public Events() {
        // logger("into ChaosEvents");
        var w=System.Diagnostics.Stopwatch.StartNew();
        this.NameSpace = "Taiwu.Event.Neutron3529.Events";
        this.Author = "Various";
        this.Group = "Neutron3529.Events";
        try{
            ComplexTaiwuEvent.Instance = new ComplexTaiwuEvent("Events.Complex", BASE_EVENT);
            ComplexTaiwuEvent.Instance.EventList.Add(ComplexTaiwuEvent.Instance);
            this.EventList = ComplexTaiwuEvent.Instance.EventList;
        }catch(Exception ex){
            logwarn("中子的事件处理器：初始化事件有误，这个Mod多半是寄了。错误信息如下：",ex);
            this.EventList = new();
            w.Stop();
            logger($"中子的事件处理器：初始化用时 {w.ElapsedMilliseconds} ms");
            return;
        }
        // check valid
        var hs=new HashSet<string>();
        foreach(var e in this.EventList){
            // logwarn(((EventContainer)e).guid);
            foreach(var o in e.EventOptions){
                if(!hs.Contains(o.OptionKey)){
                    hs.Add(o.OptionKey);
                } else {
                    logwarn($"OptionKey冲突：当前事件中已有{o.OptionKey}，这会导致部分选项失效");
                }
            }
        }
        w.Stop();
        logger($"中子的事件处理器：初始化用时 {w.ElapsedMilliseconds} ms");
    }
}
public class EventContainer : TaiwuEventItem {
    public string guid;
    public EventContainer(string name, string guid){
        this.guid = guid;
        this.Guid = Guid.Parse(guid);
        this.IsHeadEvent = false;
        this.EventGroup = name;
        this.ForceSingle = false;
        this.EventType = EEventType.ModEvent;
        this.TriggerType = EventTrigger.None;
        this.EventSortingOrder = 500;
        this.MainRoleKey = "";
        this.TargetRoleKey = "";
        this.EventBackground = "";
        this.MaskControl = 0;
        this.MaskTweenTime = 0f;
        this.EscOptionKey = "";
        this.EventOptions = new TaiwuEventOption[0];
    }
    public virtual Func<string> Desc(int i){return ()=>string.Empty;}
    public override bool OnCheckEventCondition()=>true;
    public override void OnEventEnter(){}
    public override void OnEventExit(){}
    public override string GetReplacedContentString()=>string.Empty;
    public override List<string> GetExtraFormatLanguageKeys()=>null;
    public GameData.Common.DataContext context {
        get {
            return GameData.Domains.DomainManager.TaiwuEvent.MainThreadDataContext;
        }
    }
    public static Dictionary<string,int> event_token=new();
    public static List<AutoEvent> events=new();
    public static int AddEvent(AutoEvent e){
        var x=events.Count;
        event_token[e.token]=x;
        events.Add(e);
        return x;
    }
    public static void ClearEvent(){
        events.Clear();
        event_token.Clear();
    }
    public static GameData.Domains.TaiwuEvent.EventArgBox argBox=>AutoEvent.showingEvent.ArgBox;//GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetGlobalArgBox();

    public static void SetToken(GameData.Domains.TaiwuEvent.EventArgBox box, int idx, string token){
#if CHAIN_TRACING
        string x=string.Empty;
        box.Get("NeutronChain", ref x);
        box.Set("NeutronChain",x + token + " -> ");
#endif
        box.Set("NeutronIndex",idx);
    }
    public static void SetToken(GameData.Domains.TaiwuEvent.EventArgBox box, int idx)=>SetToken(box, idx, EventContainer.events[idx].token);
    public static void SetToken(GameData.Domains.TaiwuEvent.EventArgBox box, string token)=>SetToken(box, EventContainer.event_token[token], token);
    // token_lazy
    public static string Event(string token, GameData.Domains.TaiwuEvent.EventArgBox ArgBox=null){
        if(event_token.TryGetValue(token, out var x))
            return Event(x, ArgBox??AutoEvent.showingEvent.ArgBox);
        logwarn($"事件token {token} 不存在，当前事件将直接结束。请通知Mod作者检查事件链设置是否正确");
#if CHAIN_TRACING
        token=string.Empty;
        argBox.Get("NeutronChain",ref token);
        logwarn($"事件链：{token} (this Error)");
#endif
        return string.Empty;
    }
    public static void Enter(string token, GameData.Domains.TaiwuEvent.EventArgBox ArgBox=null){
        EventContainer.Event(token,ArgBox??AutoEvent.showingEvent.ArgBox);
        EventContainer.Bypass(); // self.ArgBox=null.
    }
    public static void Bypass(){
        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(BYPASS_EVENT);
    }
    public static string Event(int token, GameData.Domains.TaiwuEvent.EventArgBox ArgBox=null){
        SetToken(ArgBox??argBox, token);
        return BASE_EVENT;
    }
}

public class EventItem : EventContainer {
    public EventItem(string name, string guid, string content=null):base(name,guid){
        this.content=content==null?string.Empty:content;
    }
    public string content;
    public string display_content;
    public new virtual string EventContent()=>this.content;
    public override string GetReplacedContentString(){
        this.SetLanguage(new string[]{this.EventContent()});
        // this.EventContent=this.EventContent();
        return string.Empty;
    }
}
public class AutoEvent {
    public static readonly string[] DUMMY=new string[0];
    public readonly string mod_dir_name;
    public string token;
    public string content;
    public string[] option;
    public string[] jumpto;
    public int esc_index;
    public Action<ComplexTaiwuEvent> enter=(self)=>{};
    public Action<ComplexTaiwuEvent> exit=(self)=>{};
    public Func<ComplexTaiwuEvent, bool> condition=(self)=>true;
    public Func<ComplexTaiwuEvent, bool> visible=(self)=>true;
    public string MainRoleKey="RoleTaiwu";
    public string TargetRoleKey="CharacterId";
    public Func<ComplexTaiwuEvent, bool>[] conditions=null;
    public (string,Func<ComplexTaiwuEvent, string>)[] dynamic_string = new (string,Func<ComplexTaiwuEvent, string>)[0];
    // public bool condition(int i)=>i<this.Count && check_condition(i, condition_code[i]);
    // public string[] effect_code=null;
    // public string effect(int i)=>i<this.Count?execute_effect(i, effect_code[i]):string.Empty;
    public int Count=>option.Length;
    public string EscOptionKey => this.esc_index>99||this.esc_index<0?string.Empty:EscKey(esc_index);
    public static string EscKey(int esc_index)=>"Option_352900"+esc_index.ToString("02");
    //templates: token visible condition enter content option jumpto esc_index

    static Func<GameData.Domains.TaiwuEvent.TaiwuEventDomain, GameData.Domains.TaiwuEvent.TaiwuEvent> _showingEvent(){
        var param=Expression.Parameter(typeof(GameData.Domains.TaiwuEvent.TaiwuEventDomain));
        return Expression.Lambda<Func<GameData.Domains.TaiwuEvent.TaiwuEventDomain, GameData.Domains.TaiwuEvent.TaiwuEvent>>(
            Expression.Field(
                param,
                typeof(GameData.Domains.TaiwuEvent.TaiwuEventDomain).GetField("_showingEvent",(BindingFlags)(-1))
            ),
          param
        ).Compile();
    }
    static Func<GameData.Domains.TaiwuEvent.TaiwuEventDomain, GameData.Domains.TaiwuEvent.TaiwuEvent> __showingEvent=_showingEvent();
    public static GameData.Domains.TaiwuEvent.TaiwuEvent showingEvent=>__showingEvent(GameData.Domains.DomainManager.TaiwuEvent);

    public static bool False()=>false; // for built-in functions
    public static bool True()=>true; // for built-in functions
    public static bool IsNull(object nullable)=>nullable is null;
    public static string CharName(GameData.Domains.Character.Character ch)=>CharNameColor(GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetMonasticTitleOrDisplayName(ch,false)); // for built-in functions
    public static string CharNameColor(string s)=>$"<color=#B97D4BFF>{s}</color>";
    public static string Name(GameData.Domains.Character.Character ch, int color=0)=>$"<color=#{Colors(color)}FF>{GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetMonasticTitleOrDisplayName(ch,false)}</color>";
    public static string NameG(GameData.Domains.Character.Character ch)=>Name(ch, 9-ch.GetOrganizationInfo().Grade);
    public static string Grade(string str,int grade)=>$"<color=#{Colors(grade)}FF>{str}</color>";
    public static string Colors(int grade){ //1-9:品阶，0:普通人
        switch (grade) {
            case 0:return "B97D4B";
            case 1:return "E4504D";
            case 2:return "F28234";
            case 3:return "E3C66D";
            case 4:return "AE5AC8";
            case 5:return "63CED0";
            case 6:return "8FBAE7";
            case 7:return "6DB75F";
            case 8:return "FBFBFB";
            default:return "8E8E8E";
        }
    }
    public static string i2s(int i)=>i.ToString();
    public static string concat(string a, string b)=>a+b;
    public static void dispatcher(){
        ComplexTaiwuEvent self=ComplexTaiwuEvent.Instance;
        logger($"dispatcher called, token={self.Event.token}");
        for(int i=0;i<self.Event.conditions.Length-1;i++){
            if(self.Event.conditions[i](self)){
                EventContainer.Enter(self.Event.jumpto[i]);
                // currently this.ArgBox is null
                return;
            }
        }
        EventContainer.Enter(self.Event.jumpto[self.Event.jumpto.Length-1]);
    }
    public static string guid()=>BYPASS_EVENT;
    public static bool to_token(string token){
        logger($"to_token called, token={token}");
        if(EventContainer.event_token.TryGetValue(token, out var idx)){
            var box=AutoEvent.showingEvent.ArgBox;
            EventContainer.SetToken(box,idx,token);
            GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(BYPASS_EVENT);
            // var showing=AutoEvent.showingEvent;
            // var box=showing.ArgBox;
            // box.Set("NeutronIndex",idx);
            // GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(BYPASS_EVENT);
            // GameData.Domains.DomainManager.TaiwuEvent.GetEvent(BYPASS_EVENT).ArgBox=box;
            // showing.ArgBox=box; // Enter会切换showingEvent并将之前showingEvent的ArgBox置空，为了保证to_token不出问题，在这里恢复showingEvent的ArgBox
        }
        return false;
    }
    /// 直接触发token, 当token为空时，只结算已有trigger
    public static bool trigger_token(string token) {
        logger($"trigger_token called, token={token}");
        if(!string.IsNullOrWhiteSpace(token)) {
            TriggerEvent.Trigger(token); // 这里需要借助TriggerEvent.Trigger保证当前事件的优先级不会因被trigger影响而被覆盖
            // var e=GameData.Domains.DomainManager.TaiwuEvent.GetEvent(BYPASS_EVENT);
            // e.ArgBox=AutoEvent.showingEvent.ArgBox;
            // EventContainer.Event(token, e.ArgBox);
            // GameData.Domains.DomainManager.TaiwuEvent.AddTriggeredEvent(e);
        } else {
            TriggerEvent.Trigger();
        }
        return false;
    }
    /// 延后触发token，需要使用trigger_token '' 进行触发
    /// 理应仅用于越过梦回界限/小村界限
    public static bool prepare_token(string token) {
        logger($"trigger_token called, token={token}");
        TriggerEvent.Trigger(token, false); // 这里需要借助TriggerEvent.Trigger保证当前事件的优先级不会因被trigger影响而被覆盖
        return false;
    }
    public static bool try_trigger() {
        logger("try triggering list (has {TriggerEvent.ablst.Count} list now)");
        TriggerEvent.Trigger();
        return false;
    }
    public static bool to_guid(string guid){
#if CHAIN_TRACING
        string x=string.Empty;
        EventContainer.argBox.Get("NeutronChain",ref x);
        logger($"neutron chain finished since to_guid called, guid='{guid}', chain={x} (finished)");
        EventContainer.argBox.Remove<string>("NeutronChain");
#endif
        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(guid);
        return false;
    }
    public static void eat(bool whatever){} // 这个函数的用法是把to_token转换成process_enter指令
    public static void EventBackground_with_box(GameData.Domains.TaiwuEvent.EventArgBox ab, string background) {
        var file = WorkshopDir + "/" + background;
        if (System.IO.File.Exists(file)) {
            ManualSetEventBackgroundArgboxKeyInBox(ab, file);
        } else {
            file = WorkshopDir + "/" + ComplexTaiwuEvent.Instance.curr_mod_dir + "/" + background;
            ManualSetEventBackgroundArgboxKeyInBox(ab, System.IO.File.Exists(file) ? file : background);
        }
    }
    public static void EventBackground(string background) {
        var file = WorkshopDir + "/" + background;
        if (System.IO.File.Exists(file)) {
            ManualSetEventBackgroundArgboxKey(file);
        } else {
            file = WorkshopDir + "/" + ComplexTaiwuEvent.Instance.curr_mod_dir + "/" + background;
            ManualSetEventBackgroundArgboxKey(System.IO.File.Exists(file) ? file : background);
        }
    }
    public static void ModBackground(string mod_id, string background) {
        if(mod_id.StartsWith("1_")) {
            mod_id = mod_id[2..];
        }
        if(!mod_id.EndsWith("/")) {
            mod_id = mod_id+"/";
        }
        var file = WorkshopDir + "/" + mod_id + background;
        if (System.IO.File.Exists(file)) {
            ManualSetEventBackgroundArgboxKey(file);
        } else {
            logwarn($"\n在mod_dir={mod_id}下找不到图片文件{file}，将直接使用{background}作为背景，这可能会导致背景图片显示不正常。\n如果你希望亲自控制图片显示情况，请选择使用下列两种方法：\n  (a) 使用ManualSetEventBackgroundArgboxKey函数\n  (b) 在ArgBox中将设置{EventBackgroundArgboxKey}的值为希望读取的图片的位置");
            ManualSetEventBackgroundArgboxKey(background);
        }
    }
    public static void ManualSetEventBackgroundArgboxKey(string background) {
        // EventContainer.argBox.Set(EventBackgroundArgboxKey, background);
        ManualSetEventBackgroundArgboxKeyInBox(ComplexTaiwuEvent.Instance.ArgBox, background);
    }
    public static void ManualSetEventBackgroundVal(string background) {
        if (string.IsNullOrWhiteSpace(background)) {
            ComplexTaiwuEvent.Instance.EventBackground = "";
            return;
        }
        if(background.ToLower().EndsWith(".png")) {
            background = System.IO.Path.ChangeExtension(background, null);
        }
        if (System.IO.File.Exists(WorkshopDir + "/" + background+".png")) {
            ComplexTaiwuEvent.Instance.EventBackground = "../../../"+background;
        } else if (System.IO.File.Exists(WorkshopDir + "/" + ComplexTaiwuEvent.Instance.curr_mod_dir + "/" + background+".png")) {
            ComplexTaiwuEvent.Instance.EventBackground = "../../../" + ComplexTaiwuEvent.Instance.curr_mod_dir + "/" + background;
        } else {
            logwarn($"输入参数<color=#ffff00>{background}</color>有误，不满足以下三点的任意一点：\n1. 参数非空\n2. 找不到图片{WorkshopDir}/<color=#ffff00>{background}</color>.png\n3. 找不到图片{WorkshopDir}/{ComplexTaiwuEvent.Instance.curr_mod_dir}/<color=#ffff00>{background}</color>.png\n这大概是Modder那里出了问题，请联系调用此函数的Modder，检查参数设置是否正确（扩展名有误？）\n在大多数情况下，应该优先使用`EventBackground:`函数以支持读取.jpg扩展名图像。在大多数情况下，`EventBackground:`完全够用。");
        }
    }
    #nullable enable
    static string EventBackgroundArgboxKey = ((string?) typeof(GameData.Domains.TaiwuEvent.EventArgBox).GetField("SpecifyEventBackground", (BindingFlags)(-1))?.GetValue(null)) ?? EventArgBox.SpecifyEventBackground;
    #nullable restore
    public static void ManualSetEventBackgroundArgboxKeyInBox(GameData.Domains.TaiwuEvent.EventArgBox ab, string background) {
        ComplexTaiwuEvent.Instance.EventBackground = ""; // 防止其他人调用时没有清除默认background
        ab.Set(EventBackgroundArgboxKey, background);
    }
    // // TODO: 自动注册EventBackground_at_exit
    // // 游戏会自动移除string，因此不需要了
    // public static void EventBackground_at_exit(string background) {
    //     ComplexTaiwuEvent.Instance.EventBackground = "";
    // }
    public static GameData.Domains.Character.Character GetCharacter(GameData.Domains.TaiwuEvent.EventArgBox argBox, string character){
        if(argBox==null || character==null){
            throw new Exception($"GetCharacter 函数{(argBox==null?"argBox为空":"")} {(character==null?"character字符串为空":"")}请通知中子修正Mod框架");
        }
        var ch=argBox.GetCharacter(character);
        if(ch==null){
            throw new Exception($"character字符串{character}并未指向某个character");
        }
        return ch;
    }
    public static int GetFromIntBox(GameData.Domains.TaiwuEvent.EventArgBox argBox, string Key){
        if (Key == "RoleTaiwu") {
            return GameData.Domains.DomainManager.Taiwu.GetTaiwuCharId();
        } else if(argBox==null || Key==null){
            throw new Exception($"GetFromIntBox 函数{(argBox==null?"argBox为空":"")} {(Key==null?"ItemKey字符串为空":"")}请通知中子修正Mod框架");
        }
        return argBox.GetInt(Key);
    }
    public static GameData.Domains.Item.ItemKey GetItemKey(GameData.Domains.TaiwuEvent.EventArgBox argBox, string ItemKey){
        if(argBox==null || ItemKey==null){
            throw new Exception($"ItemKey 函数{(argBox==null?"argBox为空":"")} {(ItemKey==null?"ItemKey字符串为空":"")}请通知中子修正Mod框架");
        }
        if(argBox.Get(ItemKey, out GameData.Domains.Item.ItemKey item)){
            return item;
        } else {
            throw new Exception($"ItemKey字符串{ItemKey}并未指向某个ItemKey");
        }
    }
    public static GameData.Domains.TaiwuEvent.EventArgBox GetArgBox(ComplexTaiwuEvent e){
        return showingEvent?.ArgBox ?? e.ArgBox ?? throw new Exception("ComplexTaiwuEvent的argBox为空，请通知中子修正Mod框架");
    }
    public static Type obtain_box(string name, Assembly[] extra_dlls){
        if(name.Length==0){
            return null;
        }
        Type box=null;
        if(extra_dlls!=null){
            foreach(var ass in extra_dlls){
                box=ass.GetType(name);
                if(box!=null){
                    return box;
                }
            }
        }
        return typeof(GameData.Domains.DomainManager).Assembly.GetType(name)??Type.GetType(name);
    }
    public static (string,Func<ComplexTaiwuEvent,string>)[] replacer_array(string content, ParameterExpression param, string dir, (ParameterExpression[], List<ParameterExpression>) variables, Expression[] consts, string id, bool careful, (string,Func<ComplexTaiwuEvent,string>)[] old_array){
        var keys=new HashSet<string>();
        var lst=new List<(string,Func<ComplexTaiwuEvent,string>)>();
        foreach(var line in content.Split("\\n",StringSplitOptions.RemoveEmptyEntries)){
            var kv=line.Split(' ',StringSplitOptions.RemoveEmptyEntries);
            if(kv.Length<2){
                continue;
            }
            var (fun,l)=process_block(kv[1..],param, dir, variables, consts, id);
            if(l!=kv.Length-1){
                logwarn($"表达式不匹配，process_block(['{string.Join("','",kv[1..].Select(x=>x.ToString()))}'],{param}, {dir}, ['{string.Join("','",variables.Item1.Select(x=>x.ToDetailedString()))}'], ['{string.Join("','",consts.Select(x=>x.ToDetailedString()))}'], {line})使用了{l}个command，而共有{kv.Length-1}个command");
            }
            if(fun.Type!=typeof(string)){
                fun=Expression.Call(fun,fun.Type.GetMethod("ToString",new Type[0]));
            }
            var result=Expression.Lambda<Func<ComplexTaiwuEvent,string>>(variables.Item2.Count>0?Expression.Block(variables.Item2,fun):fun,param);
            if(careful){
                logger($"Replacer process_block(['{string.Join("','",kv[1..].Select(x=>x.ToString()))}'],{param}, {dir}, ['{string.Join("','",variables.Item1.Select(x=>x.ToDetailedString()))}'], ['{string.Join("','",consts.Select(x=>x.ToDetailedString()))}'], {line}) goes to {result.ToDetailedString()}, used_variable=[{string.Join(", ",variables.Item2.Select(x=>x.ToDetailedString()))}]");
            }
            if(keys.Add($"{{{kv[0]}}}")){
                lst.Add(($"{{{kv[0]}}}",result.Compile()));
            } else {
                throw new Exception($"{line}在替换字典中重复添加了'{{{kv[0]}}}'这个键");
            }
        }
        return (old_array is null ? lst : old_array.Where(x=>!keys.Contains(x.Item1))).Concat(lst).ToArray();
    }
    public AutoEvent(string tsvline,string dir, Assembly[] extra_dlls, bool careful){
        this.mod_dir_name = dir;
        try {
            var content=tsvline.Split('\t');
            if(content.Length<5){
                throw new Exception($"事件读取有误，预期长度至少为5，实际长度{content.Length}，原始事件行如下：\n{tsvline}");
            }

            var args=AutoEvent.DUMMY;
            if(content.Length>11){
                args=content[11..].Select((x)=>x.Replace("\\n","\n")).ToArray();
            }
            this.token=content[0].Trim();
            this.content=content[1].Replace("\\n","\n");
            if(content[2].Trim().StartsWith("as ") && EventContainer.event_token.TryGetValue(content[2].Trim()[3..].Trim(), out var idx)){
                this.option=EventContainer.events[idx].option;
                this.jumpto=EventContainer.events[idx].jumpto;
                this.esc_index=EventContainer.events[idx].esc_index;
            } else {
                this.option=content[2].Split("\\n");
                this.jumpto=content[3].Split("\\n").Select((x)=>x.Trim()).ToArray();
                if(this.option.Length!=this.jumpto.Length){
                    logwarn($"option.Length({this.option.Length})与jumpto.Length({this.jumpto.Length})不相等，将二者置空");
                    this.option=new string[]{"option.Length({this.option.Length})与jumpto.Length({this.jumpto.Length})不相等，将二者置空"};
                    this.jumpto=new string[]{string.Empty};
                    this.esc_index=100;
                }else{
                    if(!int.TryParse(content[4], out this.esc_index)){
                        logwarn("tsvline 的 content[4]不是int : content={content[4]}, tsvline={tsvline}");
                        this.esc_index=100;
                    }
                }
            }

            var (param, variables, consts)=content.Length>5?prepare_expr(args, extra_dlls):(null,(null,null),null);
            if(content.Length>5){
                // reversed for replaced content
                if(content[2].Trim().StartsWith("as ") && EventContainer.event_token.TryGetValue(content[2].Trim()[3..].Trim(), out idx)) {
                    this.dynamic_string=replacer_array(content[5], param, dir, variables, consts, content[0], careful, EventContainer.events[idx].dynamic_string);
                } else {
                    this.dynamic_string=replacer_array(content[5], param, dir, variables, consts, content[0], careful, null);
                }
            }
            if(content.Length>6&&!string.IsNullOrWhiteSpace(content[6])){
                this.enter=process_enter(content[6],dir,args, content[0], param,variables,consts,careful,"OnEventEnter");
                if(content[6].Trim().Split("\\n").Any((x)=>x=="dispatcher")){
                    this.conditions=content[2].Split("\\n").Select((cond)=>process_condition(cond,dir,args, content[0],param,variables,consts,careful,"dispatcherCheck")).ToArray();
                }
            }
            if(content.Length>7&&!string.IsNullOrWhiteSpace(content[7])){
                this.condition=process_condition(content[7],dir,args, content[0],param,variables,consts,careful,"CheckEventCondition"); // 仿照SAT，先算or后算and，懒得支持括号了
            }
            if(content.Length>8&&!string.IsNullOrWhiteSpace(content[8])){
                this.visible=process_condition(content[8],dir,args, content[0],param,variables,consts,careful,"CheckEventVisible");
            }
            if(content.Length>9&&!string.IsNullOrWhiteSpace(content[9])){
                this.exit=process_enter(content[9],dir,args, content[0], param,variables,consts,careful,"OnEventExit");
            }

            if(content.Length>11){
                this.MainRoleKey=content[11].Trim();
            }
            if(content.Length>12){
                this.TargetRoleKey=content[12].Trim();
            }
        } catch(Exception ex) {
            logwarn($"语句{tsvline}不能正常转化为tsv事件，错误如下：",ex);
            throw;
        }
    }
    internal static Dictionary<string, MethodInfo> MethodDict=new();
    internal static Dictionary<string, MethodInfo> MethodDictAtExit=new();
    internal static Dictionary<string, MethodInfo> MethodDictOnEnter=new();
    public static MethodInfo obtain_action(string method_name, string curr_mod=null,string prefix=null){
        string find=string.Empty;
        if(prefix!=null){
            if(MethodDict.TryGetValue(method_name.Contains('@')?prefix+'.'+method_name:$"{prefix+'.'+method_name}@{curr_mod}", out var ret)){
                return ret;
            } else if(MethodDict.TryGetValue(prefix+'.'+method_name, out var ret2)){
                return ret2;
            }
            find+=$"{(method_name.Contains('@')?prefix+'.'+method_name:$"{prefix+'.'+method_name}@{curr_mod}")}、{method_name}、";
        }
        {
            if(MethodDict.TryGetValue(method_name.Contains('@')?method_name:$"{method_name}@{curr_mod}", out var ret)){
                return ret;
            } else if(MethodDict.TryGetValue(method_name, out var ret2)){
                return ret2;
            }
            find+=$"{(method_name.Contains('@')?method_name:$"{method_name}@{curr_mod}")}、{method_name}、";
        }
        method_name=method_name.Split('.')[^1];
        {
            if(MethodDict.TryGetValue(method_name.Contains('@')?method_name:$"{method_name}@{curr_mod}", out var ret)){
                return ret;
            } else if(MethodDict.TryGetValue(method_name, out var ret2)){
                return ret2;
            }
            find+=$"{(method_name.Contains('@')?method_name:$"{method_name}@{curr_mod}")}与{method_name}";
        }
        throw new Exception($"obtain_action({method_name}, {curr_mod}) 尝试搜索了 {find}均未找到对应函数，请确保{method_name}的实现是在public static class的public static method之上，且不在嵌套类中");
        // if (method_name=="dispatcher"){
        //     return typeof(AutoEvent).GetMethod("dispatcher");
        // }
        // var method=box.GetMethod(method_name.Trim(), BindingFlags.Static | BindingFlags.Public );
        // if (method==null){
        //     method=box.GetMethod(method_name.Trim(), BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic );
        //     if (method==null){
        //         method=box.GetMethod(method_name.Trim(), BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic );
        //         if (method==null){
        //             method=box.GetMethod(method_name.Trim(), (BindingFlags)(-1));
        //             if (method==null){
        //                 throw new Exception($"在类{box}中找不到方法{method_name}");
        //             } else {
        //                 throw new Exception($"在类{box}中找到了一个神奇的方法{method}，被(BindingFlags)(-1)捕获。请使用public static修饰这个方法");
        //             }
        //         } else {
        //             throw new Exception($"类{box}中的{method}是绑定实例的方法，而目前Mod只支持static方法");
        //         }
        //     } else {
        //         logwarn($"类{box}中的{method}是私有方法，请更正你需要使用的函数，或者使用public修饰之，以避免出现这个黄字");
        //     }
        // }
        // return method;
    }
    public static (Expression, int) process_check(Expression item, int len, string[] command, Expression param, string dir, (ParameterExpression[], List<ParameterExpression>) variables, Expression[] consts, string id){
        var ex=Expression.Variable(typeof(Exception),"Exception");
        return (Expression.TryCatch(
            item,
            Expression.Catch(
                ex,
                Expression.Block(
                    Expression.Call(typeof(Utils.Logger).GetMethod("logwarn", new Type []{typeof(string),typeof(Exception)}),Expression.Constant($"出错函数代码['{string.Join("', '", command[..len])}']在mod文件夹{dir}下的条目{id}中：variables=['{string.Join("', '", variables.Item1.Select(x=>x.ToDetailedString()))}'], consts=['{string.Join("', '", consts.Select((x)=>x.ToDetailedString()))}'], param={param.ToDetailedString()}"),ex),
                    Expression.Rethrow(item.Type)
                )
            )
        ),len);
    }
    public static (Expression,int) process_block(string[] command, Expression param, string dir, (ParameterExpression[], List<ParameterExpression>) variables, Expression[] consts, string id){
#if VERBOSE
        var ret=process_block_inner(command, param, dir, variables, consts, id);
        logger($"process_block got {ret.Item1.ToDetailedString()},{ret.Item2}, item1 has type {ret.Item1.Type}, command=['{string.Join("', '", command)}']");
        return ret;
    }
    public static (Expression,int) process_block_inner(string[] command, Expression param, string dir, (ParameterExpression[], List<ParameterExpression>) variables, Expression[] consts, string id){
#endif
        List<Expression> lst=new(10);
        try{
            if(command[0]!="{"){
                var curr=process_expr(command,param,dir,variables,consts,id);
                var idx=curr.Item2;
                if(idx<command.Length){
                    /*if (command[idx].Trim()==";"){
                        lst.Add(curr.Item1);
                        while(idx<command.Length && command[idx].Trim()==";"){
                            idx++;
                            curr=process_block(command[idx..],param,dir,variables,consts,id);
                            lst.Add(curr.Item1);
                            idx+=curr.Item2;
                        }
                        return (Expression.Block(lst),idx);
                    } else */if(command[idx].Trim()=="?.") {
                        if(command.Length>idx+1){
                            try {
                                var method=obtain_action(command[idx+1],dir,curr.Item1.Type.Name);
                                return process_calls(curr.Item1, idx+2, method, command, param, dir, variables, consts, id);
                            } catch(Exception ex) {
                                logwarn($"?. 符号使用有误，找不到{curr.Item1.Type.Name+"."+command[idx+1]}或者{curr.Item1.Type.Name+"."+command[idx+1]}@{dir}",ex);
                                throw;
                            }
                        } else {
                            throw new Exception($"?. 符号使用有误，未标明调用函数");
                        }
                    }
                }
                return curr;
            } else {
                var curr=process_block(command[1..],param,dir,variables,consts,id);
                curr.Item2+=1;
                var idx=curr.Item2;
                if(idx<command.Length && command[idx].Trim()==";"){
                    lst.Add(curr.Item1);
                    while(idx<command.Length && command[idx].Trim()==";"){
                        idx++;
                        curr=process_block(command[idx..],param,dir,variables,consts,id);
                        lst.Add(curr.Item1);
                        idx+=curr.Item2;
                    }
                    if (command[idx]!="}"){
                        throw new Exception($"Mod{dir}的{id}项括号未闭合，command[{idx}]={command[idx]}, command=['{string.Join("', '", command)}'], param={param.ToDetailedString()}, dir={dir}, variables=['{string.Join("', '", variables.Item1.Select(x=>x.ToDetailedString()))}'], consts=['{string.Join("', '", consts.Select((x)=>x.ToDetailedString()))}']");
                    }
                    return (Expression.Block(lst),idx+1);
                } else {
                    if (command[idx]!="}"){
                        throw new Exception($"Mod{dir}的{id}项括号未闭合，idx={idx}, command=['{string.Join("', '", command)}'], param={param.ToDetailedString()}, dir={dir}, variables=['{string.Join("', '", variables.Item1.Select(x=>x.ToDetailedString()))}'], consts=['{string.Join("', '", consts.Select((x)=>x.ToDetailedString()))}']");
                    }
                    return (curr.Item1,curr.Item2+1);
                }
            }
        } catch(Exception ex) {
            logwarn($"process_block: command=['{string.Join("', '", command)}'], param={param.ToDetailedString()}, dir={dir}, variables=['{string.Join("', '", variables.Item1.Select(x=>x.ToDetailedString()))}'], consts=['{string.Join("', '", consts.Select((x)=>x.ToDetailedString()))}']",ex);
            throw;
        }
    }
    public static readonly Type[] NumberType=new Type[]{typeof(sbyte),typeof(byte),typeof(short),typeof(ushort),typeof(int),typeof(uint),typeof(long),typeof(ulong),typeof(float),typeof(double)};
    public static readonly Type[] IntType=new Type[]{typeof(sbyte),typeof(byte),typeof(short),typeof(ushort),typeof(int),typeof(uint),typeof(long),typeof(ulong)};
    public static (Expression,int) process_expr(string[] command, Expression param, string dir, (ParameterExpression[], List<ParameterExpression>) variables, Expression[] consts, string id) {
        try{
            if(command is null || command.Length==0){
                return (Expression.Empty(),0);
            }
            if(command[0].Trim()==string.Empty){
                // shift
                (var v, var t)=process_block(command[1..], param, dir, variables, consts, id);
                return (v,t+1);
            }
            // special built-in functions
            {
                Func<Expression,Expression,Expression> fun=null;
                switch (command[0].Trim()){
                    case "+":fun=Expression.Add;break;
                    case "-":fun=Expression.Subtract;break;
                    case "*":fun=Expression.Multiply;break;
                    case "/":fun=Expression.Divide;break;
                    case "%":fun=Expression.Modulo;break;
                    case "+=":fun=Expression.AddAssign;break;
                    case "-=":fun=Expression.SubtractAssign;break;
                    case "*=":fun=Expression.MultiplyAssign;break;
                    case "/=":fun=Expression.DivideAssign;break;
                    case "%=":fun=Expression.ModuloAssign;break;
                    case "^":fun=Expression.ExclusiveOr;break;
                    case "**":fun=Expression.Power;break;
                    case "^=":fun=Expression.ExclusiveOrAssign;break;
                    case "**=":fun=Expression.PowerAssign;break;
                    case ">>":fun=Expression.RightShift;break;
                    case ">>=":fun=Expression.RightShiftAssign;break;
                    case "<<":fun=Expression.LeftShift;break;
                    case "<<=":fun=Expression.LeftShiftAssign;break;
                    case "<":fun=Expression.LessThan;break;
                    case "<=":fun=Expression.LessThanOrEqual;break;
                    case ">":fun=Expression.GreaterThan;break;
                    case ">=":fun=Expression.GreaterThanOrEqual;break;
                    case "!=": // !可能与!=连锁反应
                    case "<>":
                    case "><":
                    case "~=":fun=Expression.NotEqual;break;
                    case "==":fun=Expression.Equal;break;
                    case ":=": // 用:=主要是防止=被识别成公式
                    case "=":fun=Expression.Assign;break;
                    case "?:":break;
                    case "&&":fun=Expression.AndAlso;break;
                    case "||":fun=Expression.OrElse;break;
                    case "&":fun=Expression.And;break;
                    case "|":fun=Expression.Or;break;
                    default:
                        // imm long
                        if(long.TryParse(command[0],out var imm)){
                            return (Expression.Constant(imm),1);
                        }
                        char z=command[0][0];
                        var x=command[0];
                        Func<Expression,Expression> fn=null;
                        switch (z) {
                            case '#':return (consts[int.Parse(command[0][1..])],1);
                            case '\'':return (Expression.Constant(command[0][1..^1]),1);
                            case '$':
                                var c01=int.Parse(command[0][1..]);
                                if(c01==0){
                                    return (Expression.Field(null,typeof(ComplexTaiwuEvent).GetField("SelectIdx")),1);
                                }else if(!(variables.Item2.Contains(variables.Item1[c01]))){
                                    variables.Item2.Add(variables.Item1[c01]);
                                    #if VERBOSE
                                    logger($"{id} uses {c01} with insert. 入参:command=['{string.Join("', '", command)}']");
                                    #endif
                                } else {
                                    #if VERBOSE
                                    logger($"{id} uses {c01} 入参:command=['{string.Join("', '", command)}']");
                                    #endif
                                }return (variables.Item1[c01],1);
                            case '-':fn=Expression.Negate;command[0]=command[0][1..];break;
                            case '!':fn=Expression.Not;command[0]=command[0][1..];break;
                            case ';':return (Expression.Empty(),1);
                            default:break;
                        }
                        #if VERBOSE
                        logger($"command[0]={command[0]} z={z} consts=['{string.Join("', '", consts.Select((x)=>x.ToDetailedString()))}'], variables=['{string.Join("', '", variables.Item1.Select(x=>x.ToDetailedString()))}']");
                        logger($"z=='#':{z=='#'}");
                        #endif
                        if(fn!=null){
                            (var v, var t) = process_block(command, param, dir, variables, consts, id);
                            return process_check(fn(v), t, command, param, dir, variables, consts, id);
                        }
                        command[0]=x;
                        break;
                }
                if(fun!=null){
                    (var left, var ll) = process_block(command[1..], param, dir, variables, consts, id);
                    (var right, var rl) = process_block(command[(1+ll)..], param, dir, variables, consts, id);
                    if(left.Type!=right.Type){
                        return process_check(fun(left,Expression.Convert(right,left.Type)), 1+ll+rl, command, param, dir, variables, consts, id);
                    }
                    return (fun(left,right),1+ll+rl);
                } else if(command[0].Trim()=="?:" || command[0].Trim()==":?"){
                    (var cond, var cl) = process_block(command[1..], param, dir, variables, consts, id);
                    if(command[(1+cl)].Trim()!=":T"){
                        throw new Exception("三目表达式的第一目有误");
                    }
                    (var left, var ll) = process_block(command[(2+cl)..], param, dir, variables, consts, id);
                    if(command[(2+cl+ll)].Trim()!=":F"){
                        if(command[(2+cl+ll)].Trim()==":;" ){
                            return process_check(Expression.IfThen(cond,left), 3+ll+cl, command, param, dir, variables, consts, id);
                        } else if(command[(2+cl+ll)].Trim()==":." ) {
                            logwarn($"检测到Mod{dir}的{id}条目中，IfThen表达式使用:.而非:;结尾，command=['{string.Join("', '", command)}']");
                            return process_check(Expression.IfThen(cond,left), 3+ll+cl, command, param, dir, variables, consts, id);
                        } else {
                            throw new Exception("三目表达式的第二目有误");
                        }
                    }
                    (var right, var rl) = process_block(command[(3+cl+ll)..], param, dir, variables, consts, id);
                    if(cond.Type!=typeof(bool)){
                        cond=Expression.Convert(cond,typeof(bool));
                    }
                    if(command[(3+cl+ll+rl)].Trim()==":."){
                        if(left.Type==typeof(void) || right.Type==typeof(void)){
                            logwarn($"检测到Mod{dir}的{id}条目中，三目表达式的:T或:F返回void，此时应以:;结尾，以表明这是IfThenElse语句，command=['{string.Join("', '", command)}']");
                            return process_check(Expression.IfThenElse(cond,left,right), 4+ll+rl+cl, command, param, dir, variables, consts, id);
                        } else if(left.Type!=right.Type){
                            right=Expression.Convert(right,left.Type);
                        }
                        return process_check(Expression.Condition(cond,left,Expression.Convert(right,left.Type)), 4+ll+rl+cl, command, param, dir, variables, consts, id);
                    } else if(command[(3+cl+ll+rl)].Trim()==":;"){
                        return process_check(Expression.IfThenElse(cond,left,right), 4+ll+rl+cl, command, param, dir, variables, consts, id);
                    } else {
                        throw new Exception("三目表达式的第三目有误");
                    }
                }
            }
            var method=obtain_action(command[0].Trim(), dir);
            if(method!=null){
                // calls
                if(method.DeclaringType.Name.EndsWith("Domain")){
                    return process_calls(Expression.Field(null, typeof(GameData.Domains.DomainManager).GetField(method.DeclaringType.Name.Split('.')[^1][..^6])??throw new Exception($"在GameData.Domains.DomainManager找不到{method}对应的{method.DeclaringType.Name.Split('.')[^1][..^6]}")), 1, method, command,param,dir,variables,consts,id);
                } else {
                    return process_calls(null, 1, method, command,param,dir,variables,consts,id);
                }
            } else {
                throw new Exception($"Mod {dir} 找不到 {command[0].Trim()}");
            }
        } catch(Exception ex) {
            logwarn($"process_expr: {id} 入参:command=['{string.Join("', '", command)}'], param={param.ToDetailedString()}, dir={dir}, variables=['{string.Join("', '", variables.Item1.Select(x=>x.ToDetailedString()))}'], consts=['{string.Join("', '", consts.Select((x)=>x.ToDetailedString()))}']",ex);
            throw;
        }
    }

    public static bool process_calls_verify_variables(ref Expression call_param, ParameterInfo mgp, Type mipty, int i, int score, MethodInfo method, string[] command, Expression param, string dir, (ParameterExpression[], List<ParameterExpression>) variables, Expression[] consts, string id){
        // #0的三种用法：作为普通TaiwuEventItem（或者作为中子的ComplexTaiwuEvent），作为ArgBox，以及作为context
        if(command[score].Trim()=="#0"){
            if(mipty.IsAssignableFrom(typeof(ComplexTaiwuEvent))){
                call_param=param;
            } else if(mipty.IsAssignableFrom(typeof(GameData.Domains.TaiwuEvent.EventArgBox))) {
                call_param=Expression.Call(typeof(AutoEvent).GetMethod("GetArgBox"),param);
            } else if(mipty.IsAssignableFrom(typeof(GameData.Common.DataContext))) {
                call_param=Expression.Property(param,typeof(ComplexTaiwuEvent).GetProperty("context"));
            } else {
                logwarn($"事件{id}处理出错：尚未实现以{mipty}为参数#0的call op");
                return true;
            }
        }
        if(mgp!=null && mgp.IsOut){
            return false;
            // if(call_param.Type.IsAssignableFrom(mipty)){
            //     return false;
            // } else {
            //     logwarn($"{mgp}是Out，但{mgp}不能向{call_param}({call_param.Type})赋值");
            //     return true;
            // }
        }
        var ret=false;
        if(!mipty.IsAssignableFrom(call_param.Type)){
            if(NumberType.Contains(call_param.Type) && NumberType.Contains(mipty)){
                // 数字随意转化
                call_param = Expression.Convert(call_param,mipty);
            } else if(mipty.IsAssignableFrom(typeof(GameData.Domains.Character.Character)) && typeof(string).IsAssignableFrom(call_param.Type)){
                // 可以向character传string
                if(call_param is System.Linq.Expressions.ConstantExpression expr && string.IsNullOrWhiteSpace((string)(expr.Value))){
                    logwarn($"事件`{id}`获取到的第{i}个变量({command[score]})指向Character却是空字符串");
                    return true;
                }
                call_param = Expression.Call(
                    typeof(AutoEvent).GetMethod("GetCharacter"),                          // GetCharacter(
                    Expression.Call(typeof(AutoEvent).GetMethod("GetArgBox"),param),      // param.ArgBox,
                    call_param                                                             // #arg)
                );
            } /*else if(NumberType.Contains(mipty.Type) && typeof(string).IsAssignableFrom(call_param.Type)){
                // 可以向int传常数string
                if(call_param is System.Linq.Expressions.ConstantExpression expr){
                    if(string.IsNullOrWhiteSpace((string)(expr.Value))){
                        logwarn($"事件`{id}`获取到的第{i}个变量({command[score]})指向IntBox却是空字符串");
                        return true;
                    } else {
                        call_param = Expression.Call(
                            typeof(AutoEvent).GetMethod("GetFromIntBox"),                          // GetFromIntBox(
                            Expression.Call(typeof(AutoEvent).GetMethod("GetArgBox"),param),      // param.ArgBox,
                            call_param                                                            // #arg)
                        );
                    }
                } else {
                    logwarn($"事件`{id}`获取到的第{i}个变量({command[score]})为变量字符串，为防止歧义，请用`GetFromIntBox '字符串' #0`显式取字符串");
                    return true;
                }
            } */else if(mipty.IsAssignableFrom(typeof(GameData.Domains.Item.ItemKey)) && typeof(string).IsAssignableFrom(call_param.Type)){
                // 可以向itemKey传string
                if(call_param is System.Linq.Expressions.ConstantExpression expr && string.IsNullOrWhiteSpace((string)(expr.Value))){
                    logwarn($"事件`{id}`获取到的第{i}个变量({command[score]})指向ItemKey却是空字符串");
                    return true;
                }
                call_param = Expression.Call(
                    typeof(AutoEvent).GetMethod("GetItemKey"),                            // GetItemKey(
                    Expression.Call(typeof(AutoEvent).GetMethod("GetArgBox"),param),      // param.ArgBox,
                    call_param                                                             // #arg)
                );
            } else if (IntType.Contains(call_param.Type) && typeof(bool).IsAssignableFrom(mipty)){
                if(call_param is System.Linq.Expressions.ConstantExpression expr){
                    call_param=Expression.Constant(Convert.ToBoolean(expr.Value));
                } else {
                    call_param=Expression.Convert(call_param,typeof(bool));
                }
            } else if(IntType.Contains(mipty) && typeof(string).IsAssignableFrom(call_param.Type)){
                // 可以向numberType传string
                if(call_param is System.Linq.Expressions.ConstantExpression expr){
                    if(expr.Value is string s && !string.IsNullOrWhiteSpace(s)){
                        if(s=="RoleTaiwu"){
                            call_param=Expression.Call(Expression.Field(null, typeof(GameData.Domains.DomainManager).GetField("Taiwu")), typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("GetTaiwuCharId"));
                        } else {
                            call_param = Expression.Call(
                                Expression.Call(typeof(AutoEvent).GetMethod("GetArgBox"),param),      // param.ArgBox,
                                typeof(GameData.Domains.TaiwuEvent.EventArgBox).GetMethod("GetInt"),  // GetInt(
                                call_param                                                             // #arg)
                            );
                        }
                    } else {
                        logwarn($"事件{id}获取到的第{i}个变量({command[score]})指向IntBox却是空字符串");
                    }
                } else {
                    call_param = Expression.Condition(
                        Expression.Equal(call_param, Expression.Constant("RoleTaiwu")),
                        Expression.Call(
                            Expression.Field(
                                null,
                                typeof(GameData.Domains.DomainManager).GetField("Taiwu")
                            ),
                            typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("GetTaiwuCharId")
                        ),
                        Expression.Call(
                            Expression.Call(typeof(AutoEvent).GetMethod("GetArgBox"),param),      // param.ArgBox,
                            typeof(GameData.Domains.TaiwuEvent.EventArgBox).GetMethod("GetInt"),  // GetInt(
                            call_param                                                             // #arg)
                        )
                    );
                }
            } else if(call_param is System.Linq.Expressions.ConstantExpression expr && mipty.IsEnum){
                if(NumberType.Contains(expr.Type)){
                    // 数字转枚举
                    call_param=Expression.Constant(Enum.ToObject(mipty, expr.Value),mipty);
                } else if(expr.Type == typeof(string) && Enum.TryParse(mipty, (string)(expr.Value), out var mval)){
                    call_param=Expression.Constant(mval,mipty);
                } else {
                    ret=true;
                }
            } else{
                ret=true;
            }
        }
        if(ret){
            logwarn($"{method}的{(i==-1?$"instance({mipty})":$"第{i}个参数{mgp}")}与入参{call_param.ToDetailedString()}({call_param.Type})不兼容, score={score}");
        }
        return ret;
    }
    public static (Expression,int) process_calls(Expression instance, int score, MethodInfo method, string[] command, Expression param, string dir, (ParameterExpression[], List<ParameterExpression>) variables, Expression[] consts, string id){
        var mgp=method.GetParameters();
        var call_param=new Expression[mgp.Length];
        bool exception=false;
        #if VERBOSE
        logger($"enter process_calls：command=['{string.Join("', '", command)}'], param={param.ToDetailedString()}, dir={dir}, variables=['{string.Join("', '", variables.Item1.Select(x=>x.ToDetailedString()))}'], consts=['{string.Join("', '", consts.Select((x)=>x.ToDetailedString()))}'], mgp=['{string.Join("', '", mgp.Select((x)=>x.ToString()))}']");
        #endif
        try {
            for(int i=0;i<mgp.Length;i++){
                if(command[score].Trim()=="#0"){
                    exception|=process_calls_verify_variables(ref call_param[i], mgp[i], mgp[i].ParameterType, i, score, method, command, param, dir, variables, consts, id);
                    score+=1;
                } else {
                    (call_param[i],var l)=process_block(command[score..], param, dir, variables, consts, id);
                    exception|=process_calls_verify_variables(ref call_param[i], mgp[i], mgp[i].ParameterType, i, score, method, command, param, dir, variables, consts, id);
                    score+=l;
                }
            }
            if(instance!=null){
                exception|=process_calls_verify_variables(ref instance, null, method.DeclaringType, -1, 0, method, command, param, dir, variables, consts, id);
            }
            if(exception){
                throw new Exception($"检测到上述{method}的生成参数存在不匹配的现象");
            }
            // if(instance == null && !method.IsStatic){
            //     var name=method.Name.Split('.')[0];
            //     if(name.EndsWith("Domain")){
            //         var field=typeof(GameData.Domains.DomainManager).GetField(name[..^6]);
            //         if(field==null){
            //             throw new Exception ($"GameData.Domains.DomainManager不存在名为{name[..^6]}的field");
            //         } else {
            //             process_check(Expression.Call(Expression.Field(null, field), method, call_param), score, command, param, dir, variables, consts, id);
            //         }
            //     } else {
            //         throw new Exception ($"只有针对Domain的call才可以省略instance");
            //     }
            // }
            return instance==null ? process_check(Expression.Call(method, call_param), score, command, param, dir, variables, consts, id)
                                    :process_check(Expression.Call(instance, method, call_param), score, command, param, dir, variables, consts, id);
        } catch(Exception ex) {
            logwarn($"process_calls参数：command=['{string.Join("', '", command)}'], score={score} param={param.ToDetailedString()}, dir={dir}, variables=['{string.Join("', '", variables.Item1.Select(x=>x.ToDetailedString()))}'], consts=['{string.Join("', '", consts.Select((x)=>x.ToDetailedString()))}'], mgp=['{string.Join("', '", mgp.Select((x)=>x.ToString()))}']",ex);
            throw;
        }
    }
    public static Type make_type(string tvc, Assembly[] extra_dlls){
        return tvc.Trim().Split(' ')[0] switch {
            "bool"=>typeof(bool),
            "void"=>typeof(void),
            "sbyte"=>typeof(sbyte),
            "byte"=>typeof(byte),
            "short"=>typeof(short),
            "ushort"=>typeof(ushort),
            "int"=>typeof(int),
            "uint"=>typeof(uint),
            "long"=>typeof(long),
            "ulong"=>typeof(ulong),
            "float"=>typeof(float),
            "double"=>typeof(double),
            "object"=>typeof(object),
            var x=>obtain_box(x,extra_dlls)??typeof(string)
        };
    }
    public static string make_rem(string tvc, int cntr){
        return $"{(string.IsNullOrWhiteSpace(tvc)?(tvc.Trim().Split(' ')[^1..][0]):(string.Empty))}_{cntr.ToString()}";
    }
    public static Expression make_const(string tvc){
        if(tvc.StartsWith("string ")){
            return Expression.Constant(tvc[7..]);
        }
        var res=tvc.Split(' ',StringSplitOptions.RemoveEmptyEntries);
        return res.Length==0?Expression.Constant(string.Empty):res[0] switch {
            "sbyte"=>Expression.Constant(sbyte.Parse(res[1])),
            "byte"=>Expression.Constant(byte.Parse(res[1])),
            "short"=>Expression.Constant(short.Parse(res[1])),
            "ushort"=>Expression.Constant(ushort.Parse(res[1])),
            "int"=>Expression.Constant(int.Parse(res[1])),
            "uint"=>Expression.Constant(uint.Parse(res[1])),
            "long"=>Expression.Constant(long.Parse(res[1])),
            "ulong"=>Expression.Constant(ulong.Parse(res[1])),
            "float"=>Expression.Constant(float.Parse(res[1])),
            "double"=>Expression.Constant(double.Parse(res[1])),
            _=>Expression.Constant(tvc.Trim())
        };
    }
    public static (ParameterExpression, (ParameterExpression[], List<ParameterExpression>), Expression[]) prepare_expr(string[] args, Assembly[] extra_dlls){
        ParameterExpression param = Expression.Parameter(typeof(ComplexTaiwuEvent));
        // logwarn($"args=['{string.Join("', '",args)}']");
        var variables=(args.Select(x=>{
            var z=x.Split(' ',StringSplitOptions.RemoveEmptyEntries);
            return z.Length>1?Expression.Variable(make_type(x, extra_dlls), z[^1]):Expression.Variable(make_type(x, extra_dlls));
        }).Prepend(
            null
        ).ToArray(),new List<ParameterExpression>());
        var consts=args.Select((x)=>make_const(x)).Prepend(
            Expression.Field(
                Expression.Field(null, typeof(GameData.Domains.DomainManager).GetField("TaiwuEvent")),
                typeof(GameData.Domains.TaiwuEvent.TaiwuEventDomain).GetField("MainThreadDataContext")
            ) as Expression
        ).ToArray();
        return (param, variables, consts);
    }
    public static Action<ComplexTaiwuEvent> process_enter(string code, string dir, string[] args, string id, ParameterExpression param, (ParameterExpression[], List<ParameterExpression>) variables, Expression[] consts, bool careful, string Enter="OnEnter"){
        code=code.Trim();
        if(string.IsNullOrWhiteSpace(code)){return (_)=>{};}
        if(code.StartsWith("#[complex]")){
            code=code[10..].Replace("\\n"," ").Trim();
        }
        if(code.Split("\\n").Length==1){
            var fun=process_block(code.Split(' ',StringSplitOptions.RemoveEmptyEntries), param, dir, variables, consts, id).Item1;
            var result=Expression.Lambda<Action<ComplexTaiwuEvent>>(
                variables.Item2.Count>0?Expression.Block(variables.Item2,fun):fun,param
            );
            if(careful){
                logger($"{Enter} {code} is {result.ToDetailedString()}, used_variable=[{string.Join(", ",variables.Item2.Select(x=>x.ToDetailedString()))}]");
            }
            return result.Compile();
        } else {
            var fun=code.Split("\\n").Select((x)=>{
                try{
                    return process_block(x.Split(' ',StringSplitOptions.RemoveEmptyEntries), param, dir, variables, consts, id).Item1;
                } catch(Exception ex) {
                    logwarn($"Line 288: mod {dir}找不到{x.Trim()}",ex);
                    throw;
                }
            }).ToArray();
            var result=Expression.Lambda<Action<ComplexTaiwuEvent>>(
                Expression.Block(variables.Item2,fun),
                param
            );
            if(careful){
                logger($"{Enter} {code} is {result.ToDetailedString()}, used_variable=[{string.Join(", ",variables.Item2.Select(x=>x.ToDetailedString()))}]");
            }
            return result.Compile();
        }
    }
    public static Func<ComplexTaiwuEvent,bool> process_condition(string code, string dir, string[] args, string id, ParameterExpression param, (ParameterExpression[], List<ParameterExpression>) variables, Expression[] consts,bool careful,string Condition="condition"){
        if(string.IsNullOrWhiteSpace(code)){return (_)=>true;}
        Expression fun=code.Split("&&",StringSplitOptions.RemoveEmptyEntries).Select(
            (ors)=>
            ors.Split("||",StringSplitOptions.RemoveEmptyEntries).Select(
                (func)=>process_block(func.Replace("\\n"," ").Split(' ',StringSplitOptions.RemoveEmptyEntries), param, dir, variables, consts, id).Item1
            ).Aggregate(
                (first,second)=>Expression.OrElse(first,second) as Expression
            )
        ).Aggregate(
            (first,second)=>Expression.AndAlso(first,second)
        ).Reduce();
        var result = Expression.Lambda<Func<ComplexTaiwuEvent, bool>>(
            variables.Item2.Count>0?Expression.Block(variables.Item2,fun):fun,
            param
        );
        if(careful){
            logger($"{Condition} {code} reduce to ({String.Join(",",result.Parameters)}) => {{ {result.Body.ToDetailedString()} }}, used_variable=[{string.Join(", ",variables.Item2.Select(x=>x.ToDetailedString()))}]");
        }
        return result.Compile();
    }
}

public class Trigger : EventContainer {
    string[] Events;
    List<int> triggering_events=new();
    public Trigger(string name, string guid, short trigger, string[] events):base(name,guid){
        this.TriggerType = trigger;
        this.Events = events;
        this.IsHeadEvent = true;
        this.ForceSingle = true;
        this.EventSortingOrder = -32767;
        this.EventOptions = new TaiwuEventOption[]{
            new TaiwuEventOption {
                OptionKey = $"Option_NOT_AVAILABLE-{trigger}",
                OnOptionSelect = null,
                OnOptionVisibleCheck = ()=>true,
                OnOptionAvailableCheck = ()=>true,
                GetExtraFormatLanguageKeys = ()=>null
            }
        };
        this.EventOptions[0].SetContent($"{name} with guid {guid} is triggered accidently!");
    }
    public override void OnEventEnter(){
#if NEUTRON_DEBUG
        logwarn($"trigger {this.TriggerType} triggered");
#endif
        foreach(var idx in triggering_events){
            // 不必trycatch，因为这玩意能出错多半是Mod底层寄了，必然是要修复的
            TriggerEvent.Trigger(idx,false);
        }
        triggering_events.Clear();
        // TriggerEvent.Trigger();
        // GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(string.Empty);
        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(TRIGGER_EVENT);
    }
    public override bool OnCheckEventCondition(){
#if NEUTRON_DEBUG
        logwarn($"checking condition with trigger {this.TriggerType}");
#endif
        // logwarn($"checking {this.guid}, {this.TriggerType} , triggering_events.Count = {triggering_events.Count}");
        if(triggering_events.Count>0){
            return true;
        }
        var oldbox=ComplexTaiwuEvent.Instance.ArgBox;
        ComplexTaiwuEvent.Instance.ArgBox=this.ArgBox;
        foreach(var ae in this.Events){
            try{
                // logwarn($"triggering event {ae}");
                var idx=EventContainer.event_token[ae];
                if(EventContainer.events[idx].condition(ComplexTaiwuEvent.Instance)){
                    // logwarn($"condition of {idx} {ae} is true, triggering it");
                    triggering_events.Add(idx);
                }
            } catch(Exception ex) {
                logwarn("事件Trigger计算出错，错误如下：",ex);
            }
        }
        ComplexTaiwuEvent.Instance.ArgBox=oldbox;
        return triggering_events.Count>0;
        // if(triggered){
        //     TriggerEvent.Trigger();
        //     return true;
        // } else {
        //     return false;
        // }
    }
}
public class BypassEvent:EventContainer {
    public BypassEvent(string name, string guid):base(name,guid){
        this.EventSortingOrder = 32767;
    }
    public override void OnEventEnter(){
        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(BASE_EVENT);
    }
}
public class TriggerEvent:EventContainer {
    public TriggerEvent(string name, string guid):base(name,guid){
        this.EventSortingOrder = -32768;
        this.ForceSingle = true;
        this.EventOptions=new TaiwuEventOption[]{
            new TaiwuEventOption {
                OptionKey = "Option_NOT_AVAILABLE",
                OnOptionSelect = null,
                OnOptionVisibleCheck = ()=>true,
                OnOptionAvailableCheck = ()=>true,
                GetExtraFormatLanguageKeys = ()=>null
            }
        };
        this.EventOptions[0].SetContent("This should never be displayed!");
    }
    public static bool triggered=false;
    public static Stack<GameData.Domains.TaiwuEvent.EventArgBox> ablst=new();
    public static void Push(GameData.Domains.TaiwuEvent.EventArgBox ab)=>ablst.Push(ab);
    public static void Trigger(){
        if(!triggered){
            triggered=true;
            int idx=-1;
            string tkn=string.Empty;
            if(ablst.Count==0){
                logwarn("trying trigger something but failed");
                return;
            }
            if(ablst.Peek().Get("NeutronIndex", ref idx)){
                logger($"trying triggering {EventContainer.events[idx].token} with idx {idx}");
            } else if(ablst.Peek().Get("NeutronToken", ref tkn)){
                logger($"trying triggering {tkn}");
            } else {
                logwarn("don't know what to trigger");
            }
            GameData.Domains.DomainManager.TaiwuEvent.AddTriggeredEvent(GameData.Domains.DomainManager.TaiwuEvent.GetEvent(TRIGGER_EVENT));
        }
    }
    public static void Trigger(GameData.Domains.TaiwuEvent.EventArgBox ab, bool auto_trigger=true){
        ablst.Push(ab);
        if(auto_trigger){
            TriggerEvent.Trigger();
        }
    }
    public static void Trigger(int idx, bool auto_trigger=true){
        GameData.Domains.DomainManager.TaiwuEvent.GetEventArgBox(); // discard an old box;
        var tab=new GameData.Domains.TaiwuEvent.EventArgBox();
        EventContainer.SetToken(tab, idx);
        TriggerEvent.Trigger(tab, auto_trigger);
    }
    public static void Trigger(string token, bool auto_trigger=true)=>TriggerEvent.Trigger(EventContainer.event_token[token], auto_trigger);
    public override void OnEventEnter(){
#if NEUTRON_DEBUG
        int idx=0;
        if(ablst.Peek().Get("NeutronIndex", ref idx)){
            logwarn($"TriggerEvent OnEnter triggering {EventContainer.events[idx].token} with idx {idx}");
        } else {
            logwarn($"TriggerEvent OnEnter called but failed.");
        }
#endif
        triggered=false;
        // logwarn("enter trigger");
        if(ablst.Count>0){
            // if(this.ArgBox is not null){
            //     this.ArgBox=GameData.Domains.DomainManager.TaiwuEvent.GetEventArgBox();
            // }
            var e=GameData.Domains.DomainManager.TaiwuEvent.GetEvent(BASE_EVENT);
            e.ArgBox=ablst.Pop();
            GameData.Domains.DomainManager.TaiwuEvent.AddTriggeredEvent(e);
        }
        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(string.Empty);
    }
}


public class MyALC {
    Dictionary<string,(System.Runtime.Loader.AssemblyLoadContext,System.Reflection.Assembly)> loaded=new();
    public System.Reflection.Assembly LoadFrom(string s)=>loaded.TryGetValue(s, out var v)?v.Item2:load(s);
    public System.Reflection.Assembly load(string s){
        var alc=new System.Runtime.Loader.AssemblyLoadContext(s,true);
        var pdb=System.IO.Path.ChangeExtension(s,"pdb");
        var ass=alc.LoadFromStream(System.IO.File.OpenRead(s),System.IO.File.Exists(pdb)?System.IO.File.OpenRead(pdb):null);
        loaded[s]=(alc,ass);
        return ass;
    }
    public WeakReference[] unload(){
        var ret=loaded.Values.Select(x=>{x.Item1.Unload();return new WeakReference(x.Item1);}).ToArray();
        return ret;
    }
    // public Assembly LoadFrom(string path){
    //     // var pdb=System.IO.Path.ChangeExtension(path,"pdb");
    //     // return Assembly.LoadFrom(path, System.IO.File.Exists(pdb)?System.IO.File.OpenRead(pdb):null);
    //     return Assembly.LoadFrom(path);
    // }
}
public class ComplexTaiwuEvent : EventItem {
    public static string GetModId(string tsv_path)=>$"1_{System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(tsv_path))}";
    public static bool IsDisable(string modid, string data) {
        bool disable = false;
        if(GameData.Domains.DomainManager.Mod.GetSetting(modid, data, ref disable)) {
            return !(disable ^ data.ToLower().StartsWith("disable"));
        }
        string info = null;
        if(GameData.Domains.DomainManager.Mod.GetSetting(modid, data, ref info)) {
            return string.IsNullOrWhiteSpace(info);
        }
        int val = 0;
        if(GameData.Domains.DomainManager.Mod.GetSetting(modid, data, ref val)) {
            return val<=0;
        }
        return false;
    }
    public List<TaiwuEventItem> EventList;
    public new AutoEvent Event=null;
    public static ComplexTaiwuEvent Instance;
    public static BypassEvent BypassEvent;
    public static List<TaiwuEventOption> CustomOptions=new();
    public static MyALC alc=new();
    static Assembly LoadFile(string path, string mod_dir, bool careful=true){
        var dll=alc.LoadFrom(path);
        foreach (var ty in dll.GetTypes()){
            if(ty.IsPublic){
                logger($"搜索到{ty}");
                foreach(var m in ty.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.InvokeMethod)){
                    var dna = (System.ComponentModel.DisplayNameAttribute)Attribute.GetCustomAttribute(m,typeof(System.ComponentModel.DisplayNameAttribute));
                    if(dna!=null){
                        logger($"搜索到{ty}.{m} as {dna.DisplayName}");
                        if(!AutoEvent.MethodDict.TryAdd($"{dna.DisplayName}@{mod_dir}",m)){
                            throw new Exception("Mod {mod_dir} 正试图定义重名的方法，这可能会导致错误的调用，大概率会使Mod失效。请通知Mod作者检查。");
                        } else if(careful){
                            logger($"adding '{dna.DisplayName}@{mod_dir}' to dict");
                        }
                        if(!AutoEvent.MethodDict.TryAdd(dna.DisplayName,m)){
                            if(careful){
                                logwarn("Mod {mod_dir} 定义了一个与其他Mod重名的方法，望周知");
                            } else {
                                logger("Mod {mod_dir} 定义了一个与其他Mod重名的方法，望周知");
                            }
                        } else if(careful){
                            logger($"adding '{dna.DisplayName}' to dict");
                        }
                    } else if(careful) {
                        logger($"搜索到{ty}.{m} 但找不到DisplayNameAttribute");
                        var attrs = Attribute.GetCustomAttributes(m);
                        foreach(var attr in attrs){
                            logger($"Attribute on {ty} : {attr.GetType().Name}");
                        }
                    }
                }
            }
        }
        return dll;
    }
    public static int SelectIdx=-1;
    public static Dictionary<string,(bool, string, Assembly[])> ModDict=new();
    public static Dictionary<short, HashSet<string>> revdict=new();
    public ComplexTaiwuEvent(string name, string guid):base(name,guid){
        this.EscOptionKey = "Option_35290000";
        this.EventOptions=new TaiwuEventOption[100];
        for(int j=0;j<100;j++){
            int i=j;
            this.EventOptions[i]=new TaiwuEventOption{
                OptionKey = "Option_352900"+i.ToString("02"),
                OnOptionSelect = this.OnOptionSelect(i),
                OnOptionVisibleCheck = this.OnOptionVisibleCheck(i),
                OnOptionAvailableCheck = this.OnOptionAvailableCheck(i),
                GetReplacedContent = this.Desc(i),
                GetExtraFormatLanguageKeys = this.GetExtraFormatLanguageKeys
            };
        };
        this.content=string.Empty;
        LoadMethodDict();
        {
            var sb=new System.Text.StringBuilder();
            foreach(var kv in AutoEvent.MethodDict.OrderBy(kv=>kv.Key)){
                sb.Append($"{kv.Key} => {kv.Value.Name}({string.Join<ParameterInfo>(", ",kv.Value.GetParameters())})\n");
            }
            System.IO.File.WriteAllText(WorkshopDir+"/Builtin-list.txt",sb.ToString());
        }
        FullReload();

        this.EventList=new List<TaiwuEventItem>(){ new BypassEvent("Events.Bypass",BYPASS_EVENT) , new TriggerEvent("Events.Trigger",TRIGGER_EVENT)};
        foreach(var hs in revdict){
            logger($"trigger got type hs={hs.Key}, cnt={hs.Value.Count}");
            this.EventList.Add(new Trigger($"NeutronTrigger-{hs.Key:x4}", $"{BASE_TRIGGER_PREFIX}{hs.Key:x4}", hs.Key, hs.Value.ToArray()));
        }
    }
    public static void LoadMethodDict(){
        AutoEvent.MethodDict.Clear();
        AutoEvent.MethodDictAtExit.Clear();
        AutoEvent.MethodDictOnEnter.Clear();
        // foreach(var ty in new Type[]{
        //     // add more if necessary.
        //     typeof(GameData.Domains.Extra.ExtraDomain),
        //         typeof(GameData.Domains.Map.MapDomain),
        //         typeof(GameData.Domains.Map.Location),
        //         typeof(GameData.Domains.Organization.Settlement),
        //         typeof(GameData.Domains.Character.Character)
        // }){
        //     foreach(var x in ty.GetMethods((BindingFlags)(-1))){
        //         var pre=(x.IsStatic?string.Empty:((ty.Name.EndsWith("Domain")?ty.Name.Split('.')[^1]:ty.Name)+".")) + (x.IsPublic?"":"_") + x.Name;
        //         if(x.IsPublic){
        //             AutoEvent.MethodDict[prefix+x.Name]=x;
        //         } else {
        //             AutoEvent.MethodDict[prefix+"_"+x.Name]=x;
        //         }
        //     }
        // }
        foreach(var ty in new Type[]{
            // add more if necessary.
            typeof(GameData.Domains.Adventure.AdventureDomain),
                typeof(GameData.Domains.Building.BuildingDomain),
                typeof(GameData.Domains.Character.Character),
                typeof(GameData.Domains.Character.CharacterDomain),
                typeof(GameData.Domains.CombatSkill.CombatSkillDomain),
                typeof(GameData.Domains.Extra.ExtraDomain),
                typeof(GameData.Domains.Global.GlobalDomain),
                typeof(GameData.Domains.Item.ItemDomain),
                typeof(GameData.Domains.Item.Cricket),
                typeof(GameData.Domains.Information.InformationDomain),
                typeof(GameData.Domains.Information.Collection.SecretInformationCollection),
                typeof(GameData.Domains.LegendaryBook.LegendaryBookDomain),
                typeof(GameData.Domains.LifeRecord.LifeRecordDomain),
                typeof(GameData.Domains.Map.MapDomain),
                typeof(GameData.Domains.Map.Location),
                typeof(GameData.Domains.Merchant.MerchantDomain),
                typeof(GameData.Domains.Organization.Settlement),
                typeof(GameData.Domains.Organization.OrganizationDomain),
                typeof(GameData.Domains.Taiwu.TaiwuDomain),
                typeof(GameData.Domains.TaiwuEvent.TaiwuEventDomain),
                typeof(GameData.Domains.World.WorldDomain),
        }){
            foreach(var x in ty.GetMethods((BindingFlags)(-1))){
                var pre=ty.Name+ (x.IsStatic?"::":".")+(x.IsPublic?"":"_") + x.Name;
                AutoEvent.MethodDict[pre]=AutoEvent.MethodDict[pre+string.Join("",x.GetParameters().Select((x)=>'#'+x.Name))]
                    =AutoEvent.MethodDict[pre+string.Join("",x.GetParameters().Select((x)=>'#'+x.ParameterType.Name))]=x;
            }
        }
        foreach(var x in typeof(GameData.Domains.TaiwuEvent.EventHelper.EventHelper).GetMethods((BindingFlags)(-1))){
            var pre = (x.IsPublic ? "EventHelper." : "EventHelper._") + x.Name.Split('.')[^1];
            AutoEvent.MethodDict[pre]=AutoEvent.MethodDict[pre+string.Join("",x.GetParameters().Select((x)=>'#'+x.Name))]
                =AutoEvent.MethodDict[pre+string.Join("",x.GetParameters().Select((x)=>'$'+x.ParameterType.Name))]=x;
        }
        foreach(var x in typeof(GameData.Domains.TaiwuEvent.EventArgBox).GetMethods((BindingFlags)(-1))){
            var pre = (x.IsPublic ? "EventArgBox." : "EventArgBox._") + x.Name.Split('.')[^1];
            AutoEvent.MethodDict[pre]=AutoEvent.MethodDict[pre+string.Join("",x.GetParameters().Select((x)=>'#'+x.Name))]
                =AutoEvent.MethodDict[pre+string.Join("",x.GetParameters().Select((x)=>'$'+x.ParameterType.Name))]=x;
        }
        AutoEvent.MethodDict["logger"]=typeof(Utils.Logger).GetMethod("logger");
        AutoEvent.MethodDict["logwarn"]=typeof(Utils.Logger).GetMethod("logwarn",new Type[]{typeof(string)});
        AutoEvent.MethodDict["dispatcher"]=typeof(AutoEvent).GetMethod("dispatcher");
        AutoEvent.MethodDict["to_guid"]=typeof(AutoEvent).GetMethod("to_guid");
        AutoEvent.MethodDict["to_token"]=typeof(AutoEvent).GetMethod("to_token");
        AutoEvent.MethodDict["try_trigger"]=typeof(AutoEvent).GetMethod("try_trigger");
        AutoEvent.MethodDict["trigger_token"]=typeof(AutoEvent).GetMethod("trigger_token");
        AutoEvent.MethodDict["prepare_token"]=typeof(AutoEvent).GetMethod("prepare_token");
        AutoEvent.MethodDict["i2s"]=typeof(AutoEvent).GetMethod("i2s");
        AutoEvent.MethodDict["GetFromIntBox"]=typeof(AutoEvent).GetMethod("GetFromIntBox");
        // token_lazy 'token' #0
        AutoEvent.MethodDict["token_lazy"]=typeof(EventContainer).GetMethod("Event", new Type[]{typeof(string), typeof(GameData.Domains.TaiwuEvent.EventArgBox)});
        AutoEvent.MethodDict["concat"]=typeof(AutoEvent).GetMethod("concat");
        AutoEvent.MethodDict["eat"]=typeof(AutoEvent).GetMethod("eat");
        AutoEvent.MethodDict["guid"]=typeof(AutoEvent).GetMethod("guid");
        AutoEvent.MethodDict["name"]=typeof(AutoEvent).GetMethod("CharName");
        AutoEvent.MethodDict["namecolor"]=typeof(AutoEvent).GetMethod("CharNameColor");
        AutoEvent.MethodDict["false"]=typeof(AutoEvent).GetMethod("False");
        AutoEvent.MethodDict["true"]=typeof(AutoEvent).GetMethod("True");
        AutoEvent.MethodDict["isnull"]=typeof(AutoEvent).GetMethod("IsNull");
        AutoEvent.MethodDict["EventBackground:"]=typeof(AutoEvent).GetMethod("EventBackground");
        AutoEvent.MethodDict["EventBackground_with_box"]=typeof(AutoEvent).GetMethod("EventBackground_with_box");
        AutoEvent.MethodDict["ModBackground"]=typeof(AutoEvent).GetMethod("ModBackground");
        AutoEvent.MethodDict["ManualSetEventBackgroundArgboxKey"]=typeof(AutoEvent).GetMethod("ManualSetEventBackgroundArgboxKey");
        AutoEvent.MethodDict["ManualSetEventBackgroundVal"]=typeof(AutoEvent).GetMethod("ManualSetEventBackgroundVal");
        AutoEvent.MethodDict["ReloadEvents"]=typeof(ComplexTaiwuEvent).GetMethod("ReloadEvents");
        AutoEvent.MethodDict["ReloadEventWithPlugins"]=typeof(ComplexTaiwuEvent).GetMethod("ReloadEventWithPlugins");
        foreach(var kv in AutoEvent.MethodDict) {
            if (kv.Value is null) {
                logwarn($"AutoEvent.MethodDict[{kv.Key}] == null");
            }
        }
    }
    public static bool PrepareEvents(string directory, out bool _careful, out string _dir, out Assembly[] extra_dlls){
        extra_dlls=null;
        bool enable=false;
        var tsv=directory+"/Events.python.tsv";
        if(GameData.Domains.DomainManager.Mod.GetSetting(GetModId(tsv), "EnableNeutronEvents", ref enable)){
            if (enable){
                bool careful=false;
                GameData.Domains.DomainManager.Mod.GetSetting(GetModId(tsv), "NeutronEventDebug", ref careful);
                var dir=System.IO.Path.GetFileName(directory);
                var dll_str=string.Empty;
                if(GameData.Domains.DomainManager.Mod.GetSetting(GetModId(tsv), "NeutronEventPlugins", ref dll_str) && !string.IsNullOrWhiteSpace(dll_str)){
                    extra_dlls=dll_str.Split(',', StringSplitOptions.RemoveEmptyEntries).Select((x)=>{logger($"loading plugin {System.IO.Path.GetDirectoryName(tsv)+'/'+x}");return LoadFile(System.IO.Path.GetDirectoryName(tsv)+'/'+x, dir,careful);}).ToArray();
                }
                _dir=dir;
                _careful=careful;
                return true;
            } else {
                logger($"Events in {directory} are disabled");
            }
        } else {
            logger($"Found no events in {directory}");
        }
        _dir=string.Empty;
        _careful=false;
        return false;
    }
    public static void LoadEvents(string directory, bool careful, string dir, Assembly[] extra_dlls){
        logger($"LoadEvents entered {directory}");
        var tsv=directory+"/Events.python.tsv";
        if(System.IO.File.Exists(tsv)){
            foreach(var tsvline in System.IO.File.ReadAllLines(tsv)){
                if(string.IsNullOrWhiteSpace(tsvline) || tsvline.StartsWith('#')){continue;}
                if(careful){
                    logger($"reading {tsvline}");
                }
                try{
                    AddEvent(new AutoEvent(tsvline, dir, extra_dlls, careful));
                } catch (Exception ex){
                    logwarn($"mod id 为{dir}的事件行{tsvline}出现了错误：",ex);
                }
            }
        } else {
            logger($"cannot find {tsv}");
        }
    }
    public static void ReloadEvents(){
        ClearEvent();
        foreach(var kv in ModDict){
            LoadEvents(kv.Key,kv.Value.Item1,kv.Value.Item2,kv.Value.Item3);
        }
    }
    public static void InitModDict() {
        foreach (string directory in System.IO.Directory.GetDirectories(WorkshopDir, "*", System.IO.SearchOption.TopDirectoryOnly)) {
            try {
                if(PrepareEvents(directory, out bool careful, out string dir, out Assembly[] extra_dlls)){
                    logger($"loading events in {directory}");
                    ModDict[directory]=(careful,dir,extra_dlls);
                }
            } catch (Exception ex) {
                logwarn($"Mod {directory} 无法初始化，错误原因为",ex);
            }
        }
    }
    public static void ReloadEventWithPlugins(){
        LoadMethodDict();
        ModDict.Clear();
        var weak=ComplexTaiwuEvent.alc.unload();
        ComplexTaiwuEvent.alc=new();
        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();
        InitModDict();
        foreach (var (directory,(careful, dir, extra_dlls)) in ModDict){
            try {
                LoadEvents(directory, careful, dir, extra_dlls);
            } catch (Exception ex) {
                logwarn($"Mod {directory} 的事件部分加载出错，错误原因为",ex);
            }
        }
        {
            int i = 1;
            for (; weak.Any(x=>x.IsAlive); i++) {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                if (i>10){
                    logwarn("经过10次GC之后仍然有弱引用存活，这可能会导致内存泄漏，请重载Mod的modder记得重启太吾以防止内存爆炸");
                    return;
                }
            }
            logwarn($"经过{i}次GC之后成功释放了全部plugins");
        }
    }
    public static void FullReload(){
        ModDict.Clear();
        InitModDict();
        foreach (var (directory,(careful, dir, extra_dlls)) in ModDict){
            try {
                LoadEvents(directory, careful, dir, extra_dlls);
                LoadOptions(directory, careful, dir, extra_dlls);
                LoadTriggers(directory, careful, dir, extra_dlls, true);
            } catch (Exception ex) {
                logwarn($"Mod {directory} 加载出错，错误原因为",ex);
            }
        }
    }

    // public static void ReloadEvents(string directory){
    //     if(ModDict.TryGetValue(directory, our var Value)){
    //         LoadEvents(directory,Value.Item1,Value.Item2,Value.Item3);
    //     } else {
    //         logwarn($"游戏初始化时未曾加载{directory}文件夹，需");
    //     }
    // }
    public static void LoadOptions(string directory, bool careful, string dir, Assembly[] extra_dlls){
        logger($"LoadOptions entered {directory}");
        var tsv=directory+"/EventOptions.python.tsv";
        if(System.IO.File.Exists(tsv)){
            foreach(var tsvline in System.IO.File.ReadAllLines(tsv)){
                if(string.IsNullOrWhiteSpace(tsvline) || tsvline.StartsWith('#')){continue;}
                if(careful){
                    logger($"reading {tsvline}");
                }
                var opt=TsvOption(tsvline, dir, extra_dlls, careful);
                if(opt is not null){
                    CustomOptions.Add(opt);
                }
            }
        } else {
            logger($"cannot find {tsv}");
        }
    }
    public static void LoadTriggers(string directory, bool careful, string dir, Assembly[] extra_dlls, bool init=false){
        logger($"LoadTriggers entered {directory}");
        var tsv=directory+"/EventTriggers.python.tsv";
        if(System.IO.File.Exists(tsv)){
            foreach(var tsvline in System.IO.File.ReadAllLines(tsv)){
                if(string.IsNullOrWhiteSpace(tsvline) || tsvline.StartsWith('#')){continue;}
                if(careful){
                    logger($"reading {tsvline}");
                }
                try{
                    var content=tsvline.Split('\t');
                    if(content.Length<2){
                        logwarn($"{content} 不完整");
                        continue;
                    }
                    foreach(var key in content[1].Split(',')){
                        var k=((short?)(typeof(GameData.Domains.TaiwuEvent.EventTrigger).GetField(key)?.GetValue(null)))??short.Parse(key);
                        if(!revdict.TryGetValue(k, out var hs)){
                            if(init){
                                hs=new();
                            } else {
                                logwarn($"没有初始化触发类型为{k}的触发，修改需重启游戏方可生效");
                                continue;
                            }
                        }
                        if(content.Length>2 && IsDisable(GetModId(tsv), content[2].Trim())) {
                            return;
                        }
                        if(!hs.Add(content[0].Trim())){
                            logwarn($"事件{content[0].Trim()}被{tsv}重复添加");
                        }
                        revdict[k]=hs;
                    }
                } catch (Exception ex){
                    logwarn($"{tsvline}无法正常转化为trigger",ex);
                }
            }
        } else {
            logger($"cannot find {tsv}");
        }
    }
    #nullable enable
    public static AutoEvent? TsvGetEvent(string to){
        if(EventContainer.event_token is null || !EventContainer.event_token.TryGetValue(to,out var idx)){
            return null;
        }
        if(idx<EventContainer.events.Count){
            return EventContainer.events[idx];
        } else {
            logwarn($"the idx(={idx}) is larger than total event count {EventContainer.events.Count}");
            return null;
        }
    }
    #nullable restore
    public static TaiwuEventOption TsvOption(string tsvline, string dir, Assembly[] extra_dlls, bool careful){
        var data=tsvline.Split('\t').Select((x)=>x.Trim()).ToArray();
        if(data.Length<4){
            throw new Exception($"tsv 数据长度为 {data.Length}，小于4（数据为「{tsvline}」）");
        }
        var modid="1_"+dir.Split('/')[^1].Split('\\')[^1];
        if(data.Length>4 && IsDisable(modid, data[4].Trim())){
#if CHAIN_TRACING
            logger($"开关`{data[4]}`禁用了{modid}的{tsvline}");
#endif
            return null;
#if CHAIN_TRACING
        } else {
            logger($"开关`{data[4]}`未禁用{modid}的{tsvline}");
#endif
        }
        // var args=AutoEvent.DUMMY;
        // if(data.Length>7){
        //     args=data[7..];
        // }
        // #OptionKey（数字）, 目标事件, 选项描述, 插入事件，dir，可见条件，可选择条件
        // var box=data.Length>4?AutoEvent.obtain_box(data[4],extra_dlls):null;

        var opt= new TaiwuEventOption {
            OptionKey = "Option_"+data[0],
            OnOptionSelect = null,
            OnOptionVisibleCheck = ()=>true,
            OnOptionAvailableCheck = ()=>true, //ev.available(ComplexTaiwuEvent.Instance),
            GetExtraFormatLanguageKeys = ()=>null
        };
        { // 以此捕获闭包保证数据不出问题
            var to=data[1];
            TsvGetEvent(to);
            var content=" "+data[2];
            opt.SetContent(content);
            opt.OnOptionSelect = ()=>EventContainer.Event(to);
            opt.OnOptionVisibleCheck=()=>{
                var eve=TsvGetEvent(to);
                if(eve?.visible(ComplexTaiwuEvent.Instance)??false){
                    opt.SetContent(ComplexTaiwuEvent.Instance.replacer(content, eve.dynamic_string));
                    return true;
                } else {
                    return false;
                }
            };
            opt.OnOptionAvailableCheck=()=>TsvGetEvent(to)?.condition(ComplexTaiwuEvent.Instance)??false;
            opt.GetExtraFormatLanguageKeys = ()=>null;
        }

        var taiwuEvent = GameData.Domains.DomainManager.TaiwuEvent.GetEvent(data[3]);
        if(taiwuEvent is null) {
            logwarn($"Option{data[0]}的注入事件（guid：{data[3]}）不存在{(string.IsNullOrWhiteSpace(data[4])?"":$"，请打开开关`{data[4]}`以避免黄字出现")}");
            return null;
        }
        int count=taiwuEvent.EventConfig.EventOptions.Length;
        for(var i=0;i<count;i++){
            if(taiwuEvent.EventConfig.EventOptions[i].OptionKey==opt.OptionKey){
                taiwuEvent.EventConfig.EventOptions[i]=opt;
                return opt;
            }
        }
        opt.AddTo(ref taiwuEvent.EventConfig.EventOptions);
        return opt;
    }
    public Func<bool> OnOptionVisibleCheck(int i){
        return ()=>{
            try {
                return string.IsNullOrWhiteSpace(this.Event.jumpto[i])||EventContainer.events[EventContainer.event_token[this.Event.jumpto[i]]].visible(ComplexTaiwuEvent.Instance);
            } catch {
                return false;
            }
        };
    }
    public Func<bool> OnOptionAvailableCheck(int i){
        return ()=>{
            try{
                return string.IsNullOrWhiteSpace(this.Event.jumpto[i])||EventContainer.events[EventContainer.event_token[this.Event.jumpto[i]]].condition(ComplexTaiwuEvent.Instance);
            }catch{
                return false;
            }
        };
    }
    static int _depth=0;
    public string curr_mod_dir = "";
    public override void OnEventEnter(){
        // logwarn($"enter baseEvent-pre, depth={_depth}");
// #if CHAIN_TRACING
//         var boxes=this.ArgBox;
//         var xx=string.Empty;
//         boxes.Get("NeutronChain",ref xx);
//         var tokenx=string.Empty;
//         if(!boxes.Get("NeutronToken",ref tokenx)){
//             int idx=0;
//             boxes.Get("NeutronIndex",ref idx);
//             tokenx=EventContainer.events[idx].token;
//         }
//         logwarn($"OnEventEnter with _depth={_depth+1}. neutron chain cancalled with error: {xx} *'{tokenx}'(failed)");
// #endif
        if(_depth>0){
            return;
        }
        _depth++;
        // logwarn("enter baseEvent");
        var box=this.ArgBox;
        int index=0;
        if(box.Get("NeutronIndex",ref index)){
            box.Remove<int>("NeutronIndex");
            this.Event=EventContainer.events[index];
        } else {
            string token=string.Empty;
            if((!box.Get("NeutronToken",ref token))){
#if CHAIN_TRACING
                var x=string.Empty;
                box.Get("NeutronChain",ref x);
                logwarn($"OnEventEnter failed, neutron chain cancalled with error: {x} *'{token}'(failed)");
#else
                logwarn($"OnEventEnter failed with token '{token}'");
#endif
                return;
            }
            this.Event=EventContainer.events[EventContainer.event_token[token]];
        }
        curr_mod_dir = this.Event.mod_dir_name;
        if(this.Event.esc_index<0){
            var x=this.Event.esc_index;
            for(int idx=this.Event.option.Length-1;idx>=0;idx--){
                try {
                    var to=EventContainer.events[EventContainer.event_token[this.Event.jumpto[idx]]];
                    if(to.visible(ComplexTaiwuEvent.Instance)){
                        x++;
                        if(x==0){
                            this.EscOptionKey=AutoEvent.EscKey(idx);
                            break;
                        }
                    }
                } catch {this.EscOptionKey=string.Empty;}
            }
        } else {
            this.EscOptionKey=this.Event.EscOptionKey;
        }
        #if VERBOSE
        logger($"event token is {this.Event.token}");
        for(int idx=0;idx<this.Event.option.Length;idx++){
            try {
                if(string.IsNullOrWhiteSpace(this.Event.jumpto[idx])){
                    logger($"Option {idx}: end event.");
                } else {
                    var to=EventContainer.events[EventContainer.event_token[this.Event.jumpto[idx]]];
                    logger($"Option {idx}: visible:{to.visible(ComplexTaiwuEvent.Instance)} available:{to.condition(ComplexTaiwuEvent.Instance)}");
                }
            } catch (Exception ex){
                logwarn($"logging Option {idx} ({this.Event.jumpto[idx]}) failed",ex);
            }
        }
        #endif
        if(!string.IsNullOrWhiteSpace(this.Event.MainRoleKey)){this.MainRoleKey=this.Event.MainRoleKey;} else {this.TargetRoleKey=string.Empty;}
        if(!string.IsNullOrWhiteSpace(this.Event.TargetRoleKey)){this.TargetRoleKey=this.Event.TargetRoleKey;} else {this.TargetRoleKey=string.Empty;}
        try {
            this.Event.enter(ComplexTaiwuEvent.Instance);
        } catch (Exception ex) {
            logwarn($"事件 {this.Event.token} 的enter执行出错，错误原因为", ex);
        }
    }
    public override void OnEventExit(){
        // logwarn($"OnEventExit with _depth={_depth}.");
        try {
            this.Event.exit(ComplexTaiwuEvent.Instance);
        } catch (Exception ex) {
            logwarn($"事件 {this.Event.token} 的exit执行出错，错误原因为", ex);
        }
        SelectIdx=-1;
        _depth--;
        if(TriggerEvent.ablst.Count>0){
            TriggerEvent.Trigger();
        }
        // System.GC.Collect();
    }
    public System.Text.StringBuilder sb=new();
    public string replacer(string content, (string,Func<ComplexTaiwuEvent, string>)[] dynamic){
        lock(sb){ // 天知道螺舟会不会多线程，反正lock一下不会造成任何可感知的性能损失
            sb.Clear(); //防一手
            sb.Append(content);
            foreach(var (k,v) in dynamic){
                sb.Replace(k,v(this));
            }
            var s=sb.ToString();
            sb.Clear();
            return s;
        }
    }
    public override string EventContent()=>replacer(this.Event.content, this.Event.dynamic_string);
    new Func<string> Desc(int i)=>()=>{
        if(i<this.Event.option.Length){
            var tmp=SelectIdx;
            SelectIdx=i;
            this.EventOptions[i].SetContent(this.replacer(this.Event.option[i],this.Event.dynamic_string));
            SelectIdx=tmp;
        }
        return string.Empty;
    };
    Func<string> OnOptionSelect(int i){
        return ()=>{
            SelectIdx=i;
            var to=i<this.Event.jumpto.Length?this.Event.jumpto[i]:string.Empty;
            // NeutronChain: debug only, show the whole chain.
#if CHAIN_TRACING
            string x=string.Empty;
            EventContainer.argBox.Get("NeutronChain",ref x);
#endif
            if(EventContainer.event_token.ContainsKey(to)){
#if CHAIN_TRACING
                EventContainer.argBox.Set("NeutronChain",x + to + " -> ");
#endif
                to=Event(to);
            } else {
                if(TriggerEvent.ablst.Count>0 && !TriggerEvent.triggered){
                    TriggerEvent.Trigger();
#if CHAIN_TRACING
                    logger("neutron chain finished: "+x+(to==string.Empty?"(finish)":to)+", new trigger triggered.");
                } else {
                    logger("neutron chain finished: "+x+(to==string.Empty?"(finish)":to));
#endif
                }
#if CHAIN_TRACING
                EventContainer.argBox.Remove<string>("NeutronChain");
#endif
            }
            return to;
        };
    }
}
