#!/bin/python
from sys import argv
from csv import reader
import os
def cvrt(human_readable='Events.tsv',machine_friendly='Events.python.tsv',checks=('option','jumpto')):
    if not os.path.exists(human_readable):
        print(f"input file {human_readable} does not exist")
        return
    with open(human_readable) as f:r=[[r for r in r] for r in reader(f,delimiter='\t')]
    check_idx=[r[0].index(c) for c in checks]
    content_idx=r[0].index('content')
    token_idx=r[0].index('token')
    enter_idx=r[0].index('enter')
    esc_index=r[0].index('esc_index')
    dynamic_idx=r[0].index('dynamic')
    r=r[1:]
    for item in r:
        item[token_idx]=item[token_idx].strip()
        for idx in range(len(item)):
            item[idx]=item[idx].replace('\n','\\n').strip()
        cnt=[item[idx].count('\\n') for idx in check_idx]
        if len(cnt)>0 and max(cnt)!=min(cnt):
            print(f"checks failed for item: cnt={cnt}")
            print(item)
            return
        if item[token_idx][:8]=='CondJump' and item[enter_idx].lower().split(',').index('dispatcher')==-1 :
            print(f"checks failed for item: token=`{item[token_idx]}` starts with CondJump but event ({item[enter_idx]}) contains no `dispatcher`")
            print(item)
            return
        if item[content_idx]=='' and item[enter_idx]=='' :
            print(f"checks failed for item: both content and enter for token=`{item[token_idx]}` is empty.")
            print(item)
            return
        try:
            int(item[esc_index])
        except:
            item[esc_index]="-1"
    with open(machine_friendly,'w') as f:f.writelines("\t".join(r)+'\n' for r in r)

def cvrt_option(human_readable='EventOptions.tsv',machine_friendly='EventOptions.python.tsv'):
    if not os.path.exists(human_readable):
        print(f"input file {human_readable} does not exist")
        return
    with open(human_readable) as f:r=[[r for r in r] for r in reader(f,delimiter='\t')]
    r=r[1:]
    for item in r:
        item[0]=item[0].strip()
        for idx in range(len(item)):
            item[idx]=item[idx].replace('\n','\\n').strip()
    with open(machine_friendly,'w') as f:f.writelines("\t".join(r)+'\n' for r in r)

cvrt_advmonth=cvrt_option

if __name__=='__main__':
    if len(argv)>1:human_readable=argv[1]
    else:human_readable='Events.tsv'
    if len(argv)>2:machine_friendly=argv[2]
    else:machine_friendly='Events.python.tsv'
    cvrt(human_readable, machine_friendly)

    if len(argv)>3:human_readable=argv[3]
    else:human_readable='EventOptions.tsv'
    if len(argv)>4:machine_friendly=argv[4]
    else:machine_friendly='EventOptions.python.tsv'
    cvrt_option(human_readable, machine_friendly)

    if len(argv)>3:human_readable=argv[5]
    else:human_readable='EventTriggers.tsv'
    if len(argv)>4:machine_friendly=argv[6]
    else:machine_friendly='EventTriggers.python.tsv'
    cvrt_advmonth(human_readable, machine_friendly)

    if os.path.exists("../compile_all.py"):
        os.system('python ../compile_all.py pass')
