return {GameVersion="0.0.0.0-Signed-by-Neutron",
    Title = "都可以导：导入与导出太吾村布局",
    Version = "3.5.2.9", -- 2022<<32+0925,
    Author = "Neutron3529",
    NeedRestartWhenSettingChanged = true,
    Dependencies = { 3113001972, 3225725370 },
    HasArchive = false,
    TagList = {"Modifications", "Frameworks"},
    --workshop_only    Source = 1,
    --workshop_only    Cover = "cover.png",
    --workshop_only    FileId = 3225725509,
    Description = "这是一个简单的，纯tsv的demo（其实有dll部分，但dll部分被巧妙地切到3225725370里面了）\n这是简单的导出与导入太吾村布局的事件，默认注入到人物交流中，等我找到更好的注入位置之后，我会修改\n当然你们也可以直接改tsv，在EventOption.python.tsv里面，将注入GUID改成更合适的版本\n\n如果你们测得了问题，可以试着把出现问题的存档以提交BUG的形式传给螺舟（并标注是mod问题，请螺舟不要处理），然后把邮箱和问题追踪码给我",
    DefaultSettings = {
        {Key="EnableNeutronEvents", DisplayName="启用事件",  Description="这是一个启用tsv事件的开关，中子会强行查询EnableNeutronEvents的取值，如果为false或不存在，则事件Mod（包括Config里面的dll）不会被加载", SettingType="Toggle", DefaultValue=true},
        {Key="NeutronEventDebug", DisplayName="调试事件",  Description="这是一个启用tsv事件调试的开关，目前只针对各种函数。启用开关之后，会以warning而不是静默的形式输出「检测到重名函数」之类的BUG。Modder专用，如果不填写这个键，则这个键会被当作false进行处理\n<color=#lightgrey>（删掉这个东西不会对发布的mod产生任何功能性上的影响）</color>", SettingType="Toggle", DefaultValue=false},
    },
}

