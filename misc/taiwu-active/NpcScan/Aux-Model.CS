﻿// echo partial file
#if FRONTEND
using Config;
using GameData.Domains.Character;
using GameData.Domains.Character.Display;
using System;
using System.Collections.Generic;

namespace NpcScan
{
    public class Model
    {
        private static Model instance;

        public static Model Instance
        {
            get
            {
                if (instance == null)
                    instance = new Model();
                return instance;
            }
        }

        public List<CharacterData> AllDataList = new List<CharacterData>();
        public List<CharacterData> CurrentDataList = new List<CharacterData>();
        public static int updateidx=-1;
        public void Update(){
            var end=updateidx+1000;
            if(end>AllDataList.Count){
                end=AllDataList.Count;
            }
            for(int idx=updateidx;idx<end;idx++){
                AllDataList[idx].Fill();
            }
            if(end==AllDataList.Count){
                updateidx=-1;
            } else {
                updateidx=end;
            }
        }
        public void SetData(List<CharacterData> dataList)
        {
            if (dataList == null || dataList.Count == 0)
                return;
            dataList.Sort(CharacterDataById.Instance);
            AllDataList = dataList;
            Utils.update_map();

            if(View.ToggleDic[Model.Toggle.尽显丝滑].isOn){
                updateidx=0; // 之后借助Mono的Update调用异步方法
            } else {
                foreach(var data in dataList){
                    data.Fill();
                }
            }
            // System.Threading.Tasks.Parallel.ForEach(dataList, data=>data.Fill()); // 越TM并行越TM慢……
            // var ranges=System.Collections.Concurrent.Partitioner.Create(0,dataList.Count);
            // System.Threading.Tasks.Parallel.ForEach(ranges, (range, loopState) => {
            //     for (int i = range.Item1; i < range.Item2; i++) {
            //         dataList[i].Fill();
            //     }
            // }); // 越TM并行越TM慢……
        }

        public CharacterData GetCharacterData(int id)
        {
            return AllDataList.Find(data => data.id == id);
        }

        public enum Input
        {
            最低年龄,
            最大年龄,
            入魔值下限,
            入魔值上限,
            膂力,
            灵敏,
            定力,
            体质,
            根骨,
            悟性,
            魅力,
            健康,
            轮回次数,
            内功,
            身法,
            绝技,
            拳掌,
            指法,
            腿法,
            暗器,
            剑法,
            刀法,
            长兵,
            奇门,
            软兵,
            御射,
            乐器,
            音律,
            弈棋,
            诗书,
            绘画,
            术数,
            品鉴,
            锻造,
            制木,
            医术,
            毒术,
            织锦,
            巧匠,
            道法,
            佛学,
            厨艺,
            杂学,
            角色ID,
            姓名,
            归属,
            心仪,
            身份,
            特性,
            物品
        }
        public enum Toggle
        {
            全部性别,
            男性,
            女性,// 太吾以0为女性1为男性，与这里的顺序不同，因此需要特别小心地处理性别问题
            全部生相,
            相同,
            不同,
            全部取向,
            异性,
            双性,
            全世界,
            同区域,
            同地格,
            全部立场,
            刚正,
            仁善,
            中庸,
            叛逆,
            唯我,
            全部婚姻,
            未婚,
            已婚,
            丧偶,
            不论死活,
            已故,
            健在,
            全部归属,
            无门无派,
            门派,
            太吾村,
            外道,
            城镇,
            村庄,
            无关紧要,
            派系群众,
            派系领袖,
            跟随领队,
            带队前行,
            被拘禁,
            精确特性,
            村民灌顶,
            自动反选,
            同时刷新,
            尽显丝滑, // 之后借助Mono的Update调用异步方法
            // 顺序查询,
            Count, // auto generate count
            重置筛选,
            基础数值,
            后端并行,
        }

        public enum CharacterInfo
        {
            姓名,
            年龄,
            性别,
            位置,
            状态,
            魅力,
            归属,
            身份,
            立场,
            婚姻,
            精纯,
            心情,
            健康,
            好感,
            入邪,
            奇书,
            心仪,
            内力,
            战力,
            派系,
            领队,
            被拘,
            周天运转,
            膂力,
            灵敏,
            定力,
            体质,
            根骨,
            悟性,
            功法成长,
            内功,
            身法,
            绝技,
            拳掌,
            指法,
            腿法,
            暗器,
            剑法,
            刀法,
            长兵,
            奇门,
            软兵,
            御射,
            乐器,
            技艺成长,
            音律,
            弈棋,
            诗书,
            绘画,
            术数,
            品鉴,
            锻造,
            制木,
            医术,
            毒术,
            织锦,
            巧匠,
            道法,
            佛学,
            厨艺,
            杂学,
            前世,
            人物特性,
            物品,
            血亲父母,
            _END_,
            查找,
            更新数据,
            上一页,
            下一页
        }
    }
}
#endif
