﻿using System.Collections.Generic;

namespace NpcScan
{

    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
    public class Length : System.Attribute {
        public int method;
        public Length(int method) {
            this.method=method;
        }
    }
    public static class Const {
        public static int ROW_MAX=50;
        public static readonly int NameLength=new GameData.Domains.Character.Display.NameRelatedData().GetSerializedSize();
        public static int ToItem(sbyte grade, sbyte itemType,short templateId) => ((byte)grade<<item_grade_shift) | ((byte)itemType<<item_type_shift) | (ushort)templateId;
        public static Dictionary<int,string> NMdict=new Dictionary<int,string>();
        public const int item_id_mask=65535; // since it is short.
        public const int item_type_shift=16;
        public const int item_type_mask=255;
        public const int item_grade_shift=24;
    }
    partial class CharacterData
    {
        // [Length(0)]
        // public byte[] Name; // NameRelatedData, length=NameLength
        // public int isAlive;
        // public bool transgender;
        // public bool bisexual;
        // public byte[][] samsara; // preexistenceCharacter, NameRelatedData
        // public int templateId;
        //public LifeSkillShorts baseLifeSkillQualifications;
        //public CombatSkillShorts baseCombatSkillQualifications;
        // public string[] preexistenceCharacterNames;
        // public int[] preexistenceCharacterIds;
        //public List<LifeSkillItem> learnedLifeSkills;
        //public List<int> learnedCombatSkills;

        // using python to generate serialize and deserialize code.

        // working public int id;
        // working public int name;
        // working public int gender;// (isAlive<<24 | transgender << 16 | bisexual<< 8 | gender)
        // working public short curr;
        // working public short age;
        // working public short xiangshuInfection;
        // working public short attraction;
        // working public byte creatingType;
        // working public sbyte behaviorType;
        // working public short grid_offset;
        // working public short marriage;
        // working public short state; // ExternalRelationType 1:有工作 2:关押者 4:奇遇中 8:面壁呢 16:摊上大事了 32:自由活动受限 128:不正常
        // working public short FavorabilityToTaiwu;
        // working public short LegendaryBookOwnerState;
        // working public int mood; // sbyte mood, 上24bit可以直接使用
        // working public int power;
        // working public int extra_neili;
        // working public int max_neili;
        // working public int jingchun; // block_template_id << 16 | area_size << 8 | jingchun
        // working public int lifeSkillGrowthType;
        // working public int combatSkillGrowthType;
        // working public int health;
        // working public int maxLeftHealth;
        // working public int faction;
        // working public int kidnapper;
        // working public int leader;
        // working public int ideal; // zhoutian_yunzhuan<<16 | ideal
        // reorder to align results fit align
        // working public short[] lifeSkillQualifications; // length=16
        // working public short[] organizationInfo; // length=4
        // working public short[] combatSkillQualifications; // length=14
        // working public short[] location; // length=2
        // working public short[] maxMainAttributes; // length=6
        // dyn
        // working public short[] featureIds;
        // working public short[] potentialFeatureIds;
        // realign
        // working public int[] samsara; // preexistenceCharacter, NameRelatedData
        // working public int[] items;
        // working public int[] hidden_parents;
    }
}
