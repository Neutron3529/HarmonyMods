// echo partial file, should not compile along
#if MapEditor

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using GameData.Domains;
namespace Utils;

public static class MapEditor {
    public static unsafe void convert_to(this GameData.Domains.Map.MapBlockData mbd, GameData.Common.DataContext context, short templateId) {
        if(mbd.CanChangeBlockType() && mbd.TemplateId >38 && ((mbd.GroupBlockList?.Count??0)==0)){
            mbd.force_to(context,templateId);
        }
    }
    public static unsafe void force_to(this GameData.Domains.Map.MapBlockData mbd, GameData.Common.DataContext context, short templateId) {
        if(mbd.TemplateId!=templateId){
            mbd.TemplateId=templateId;
            DomainManager.Map.SetBlockData(context,mbd);
        }
    }
    public static unsafe void maximize_resource(this GameData.Domains.Map.MapBlockData mbd, GameData.Common.DataContext context) {
        MakeMaxResourceGrid(mbd);
        mbd.CurrResources=mbd.MaxResources;
        DomainManager.Map.SetBlockData(context,mbd);
        if(mbd.GroupBlockList!=null){
            foreach(var x in mbd.GroupBlockList){
                MakeMaxResourceGrid(x);
                x.CurrResources=x.MaxResources;
                DomainManager.Map.SetBlockData(context,x);
            }
        }
    }
    public static unsafe bool MakeMaxResourceGrid(GameData.Domains.Map.MapBlockData block){
        var configData=block.GetConfig();
        fixed(short*ptr=block.MaxResources.Items)for (sbyte resourceType = 0; resourceType < 6; resourceType ++) {
            short maxResource = configData.Resources[(int)resourceType];
            if (maxResource < 0) {
                maxResource = (short)(Math.Abs(maxResource)* 5);
            }
            else if (maxResource != 0) {
                maxResource = (short)(maxResource+25);
            }
            *(ptr + resourceType) = maxResource;
        }
        return block.CurrResources.GetSum()==block.MaxResources.GetSum();
    }
}
#endif
