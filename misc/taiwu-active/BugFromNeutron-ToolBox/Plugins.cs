// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Runtime.dll" -r:"../../Backend/System.Linq.dll" -r:"../../Backend/System.Linq.Expressions.dll" -r:"../../Backend/System.Text.Json.dll" -r:"../../Backend/System.Text.Encodings.Web.dll" -r:"../../Backend/System.ComponentModel.Primitives.dll" -unsafe -optimize -deterministic Plugins.cs reflection.CS ../UTILS/*.CS -out:Plugins.dll -debug -define:BACKEND -define:NO_HARMONY -define:MapEditor -define:Parse -define:Neili -define:CharacterMod -define:Expr

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法
//! 这是一个示例Mod，用途是展示事件Mod的标准写法

/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.ComponentModel;
using Utils;
using static Utils.Logger;
using static Utils.Expr.Checker;
using static Utils.CharacterMod;

using GameData.Domains;
using GameData.Domains.TaiwuEvent.EventHelper;
namespace Neutron3529.Bug;
public static class 事件结算 {
    [DisplayName("void_显示参数")] // 记得加 `using System.ComponentModel;`
    public static void Test(string a, string b, int c, Config.EventConfig.TaiwuEventItem d, GameData.Common.DataContext e, GameData.Domains.TaiwuEvent.EventArgBox f){
        logwarn($"获取参数'{a}' '{b}' '{c}' '{d}'={d.GetType()} '{e}'={e.GetType()} '{f}'={f.GetType()}");
    }

    [DisplayName("bool_Mod1启用开关2")]
    public static bool Mod1启用开关2(string mod_id, string key){
        bool flag=false;
        return DomainManager.Mod.GetSetting(mod_id, key, ref flag) && flag;
        // var res=DomainManager.Mod.GetSetting(mod_id, key, ref flag);
        // logwarn($"{mod_id}启用{key}的状态是{res} && {flag},");
        // return res && flag;
    }
    [DisplayName("bool_字符串1为空")]
    public static bool 字符串1为空(string str)=>string.IsNullOrEmpty(str);
    [DisplayName("string_Mod1字符串选项2")]
    public static string Mod1字符串选项2(string mod_id, string key){
        string flag="";
        DomainManager.Mod.GetSetting(mod_id, key, ref flag);
        return flag??"";
    }
    [DisplayName("sbyte_人物1性别")]
    public static sbyte Gender(GameData.Domains.Character.Character character)=>character.GetGender();
    [DisplayName("sbyte_男性")]
    public static sbyte Gender_Male()=>1;
    [DisplayName("sbyte_女性")]
    public static sbyte Gender_Female()=>0;
    [DisplayName("short_人物1基础魅力")]
    public static short BaseCharm(GameData.Domains.Character.Character character)=>character.GetAvatar().GetCharm(character.GetPhysiologicalAge(),0);
    [DisplayName("short_人物1内息")]
    public static short DisorderOfQi(GameData.Domains.Character.Character character)=>character.GetDisorderOfQi();
    [DisplayName("short_人物1月寿")]
    public static short MaxHealth(GameData.Domains.Character.Character character)=>character.GetMaxHealth();
    [DisplayName("int_人物1月龄")]
    public static int MaxMonth(GameData.Domains.Character.Character character)=>GameData.Domains.Character.CharacterDomain.GetLivedMonths(character.GetCurrAge(),character.GetBirthMonth());
    [DisplayName("int_人物1余寿")]
    public static int MaxLeftHealth(GameData.Domains.Character.Character character)=>character.GetLeftMaxHealth();
    [DisplayName("int_人物1健康")]
    public static int Health(GameData.Domains.Character.Character character)=>character.GetHealth();

    static ushort RelationType_Adored=((ushort?)typeof(GameData.Domains.Character.Relation.RelationType).GetField("Adored")?.GetValue(null))??GameData.Domains.Character.Relation.RelationType.Adored;
    [DisplayName("ushort_爱慕关系")]
    public static ushort _RelationType_Adored()=>RelationType_Adored;
    [DisplayName("ExtraDomain.DlcArgBox")]
    public static GameData.Domains.TaiwuEvent.EventArgBox ArgBox()=>DomainManager.Extra.DlcArgBox;
    [DisplayName("GlobalArgBox")]
    public static GameData.Domains.TaiwuEvent.EventArgBox GlobalArgBox()=>EventHelper.GetGlobalArgBox();

    // [DisplayName("测试GetEnumerator")]
    // public static void test_iterate(){
    //     logwarn($"test isnull:{Config.WugKing.Instance.GetType().GetField("_dataArray",(BindingFlags)(-1)).GetValue(Config.WugKing.Instance) is null }");
    //     logwarn($"test isnull:{Config.EnhancedRichTextTags.Instance.GetType().GetField("_dataArray",(BindingFlags)(-1)).GetValue(Config.EnhancedRichTextTags.Instance) is null }");
    // }
    [DisplayName("void_全同道离队")]
    public static void 全同道离队(){
        var twid=DomainManager.Taiwu.GetTaiwuCharId();
        foreach(var id in DomainManager.Taiwu.GetGroupCharIds().GetCollection().Where(id=>id!=twid)){
            DomainManager.Taiwu.LeaveGroup(context, id, true, false, false);
        }
    }
    [DisplayName("Character_取同道，无同道时返回太吾")]
    public static GameData.Domains.Character.Character 取同道或太吾() {
        var twid=DomainManager.Taiwu.GetTaiwuCharId();
        foreach(var id in DomainManager.Taiwu.GetGroupCharIds().GetCollection().Where(id=>id!=twid)){
            if (DomainManager.Character.TryGetElement_Objects(id, out var ch)) {
                return ch;
            }
        }
        return DomainManager.Taiwu.GetTaiwu();
    }
    [DisplayName("int_同道数")]
    public static int 同道数()=>DomainManager.Taiwu.GetGroupCharIds().GetCollection().Count-1;
    [DisplayName("void_人物1技艺成长设为2")]
    public static void lifeSkillQualificationGrowthType(GameData.Domains.Character.Character self, sbyte val){
        self.lifeSkillQualificationGrowthType(val,context);
    }
    [DisplayName("void_人物1武学成长设为2")]
    public static void combatSkillQualificationGrowthType(GameData.Domains.Character.Character self, sbyte val){
        self.combatSkillQualificationGrowthType(val,context);
    }
    [DisplayName("void_人物1真实年龄设为2")]
    public static void actualAge(GameData.Domains.Character.Character self, short val){
        self.actualAge(val,context);
    }
    [DisplayName("void_人物1获取名称为2的人物特性")]
    public static void obtainFeature(GameData.Domains.Character.Character self, string trick){
        foreach(var trickstr in trick.Split(',',StringSplitOptions.RemoveEmptyEntries).Select(x=>x.Trim())){
            if(Parser.Name2Id(trickstr,Config.CharacterFeature.Instance, out short type)){
                self.AddFeature(context, type, true);
            } else {
                logwarn($"不存在名为{trickstr}的特性");
            }
        }
    }
    [DisplayName("void_设定人物1临时特性2的剩余月份为3")]
    public static void SetupTempFeature(GameData.Domains.Character.Character ch, string trick, int month){
        foreach(var trickstr in trick.Split(',',StringSplitOptions.RemoveEmptyEntries).Select(x=>x.Trim())){
            if(Parser.Name2Id(trickstr,Config.CharacterFeature.Instance, out short fid)){
                if(ch.GetFeatureIds().Contains(fid)) {
                    DomainManager.Extra.RegisterCharacterTemporaryFeature(context, ch.GetId(), fid, DomainManager.World.GetCurrDate() + month);
                } else {
                    logwarn($"人物{ch.GetId()}不存在名为{trickstr}(id:{fid})的特性，略过注册步骤");
                }
            } else {
                logwarn($"不存在名为{trickstr}的特性");
            }
        }
    }
    [DisplayName("void_设定人物1全部临时特性的剩余月份为2")]
    public static void SetupTempFeature(GameData.Domains.Character.Character ch, int month){
        foreach(var fid in ch.GetFeatureIds()){
            var config = Config.CharacterFeature.Instance[fid];
            if (config.Duration > 0){
                DomainManager.Extra.RegisterCharacterTemporaryFeature(context, ch.GetId(), fid, DomainManager.World.GetCurrDate() + month);
            }
        }
    }

    [DisplayName("bool_移除对id1执行行动2的CD")]
    public static bool 移除对id1执行行动2的CD(int targetCharId, short optionTemplateId) => _executedOncePerMonthOptions.Remove(targetCharId, optionTemplateId);
    [DisplayName("bool_移除对ArgBox1中id2执行行动3的CD")]
    public static bool 移除对ArgBox1中id2执行行动3的CD(GameData.Domains.TaiwuEvent.EventArgBox ab, string target, short optionTemplateId) => 移除对id1执行行动2的CD(ab.GetInt(target), optionTemplateId);
    static short MourningTomb1 = (short)(typeof(Config.InteractionEventOption.DefKey).GetField("MourningTomb1",(BindingFlags)(-1)).GetValue(null) ?? Config.InteractionEventOption.DefKey.MourningTomb1);
    [DisplayName("int_盗墓行动ID")]
    public static short 盗墓行动ID()=>MourningTomb1;

    [DisplayName("bool_人物1可通过男媒女妁红字检查")]
    public static bool 人物1可通过男媒女妁红字检查(GameData.Domains.Character.Character spouseChar) {
        var spouseOrgInfo = spouseChar.GetOrganizationInfo();
        var spouseOrgMemberCfg = GameData.Domains.Organization.OrganizationDomain.GetOrgMemberConfig(spouseOrgInfo);
        return spouseOrgInfo.Principal && spouseOrgMemberCfg.ChildGrade >=0;
    }

    [DisplayName("Int_Array_人物1所在地格孕妇id")]
    public static int[] 人物1所在地格有孕妇(GameData.Domains.Character.Character character) {
        var loc = character.GetLocation();
        if(loc.IsValid() && GameData.Domains.DomainManager.Map.GetBlock(loc) is var blck and not null) {
            return (blck.CharacterSet?.Union(GameData.Domains.DomainManager.Taiwu.GetGroupCharIds().GetCollection()) ?? GameData.Domains.DomainManager.Taiwu.GetGroupCharIds().GetCollection()).Where(x=>
                GameData.Domains.DomainManager.Character.TryGetPregnantState(x, out var _)
            && !GameData.Domains.DomainManager.Building.TryGetElement_SamsaraPlatformBornDict(x, out var _)
            ).ToArray();
        } else {
            return new int[0];
        }
    }
    [DisplayName("Character_人物id列表1中首个人物")]
    public static GameData.Domains.Character.Character 人物id列表1中首个人物(IEnumerable<int> character) => character.Select(x=>DomainManager.Character.TryGetElement_Objects(x, out var ch)?ch:null).Where(x=>x is not null).FirstOrDefault();

    [DisplayName("void_人物1给人物2代孕")]
    public static void 人物1给人物2代孕(GameData.Domains.Character.Character ch1, GameData.Domains.Character.Character ch2) {
        if(ch1 is null) { logwarn("在函数`void_人物1给人物2代孕`的调用中，ch1为null");return;}
        if(ch2 is null) { logwarn("在函数`void_人物1给人物2代孕`的调用中，ch2为null");return;}
        if(DomainManager.Character.TryGetPregnantState(ch1.GetId(), out var p)) { logwarn("在函数`void_人物1给人物2代孕`的调用中，ch1已怀孕");return; }
        if(!DomainManager.Character.TryGetPregnantState(ch2.GetId(), out p)) { logwarn("在函数`void_人物1给人物2代孕`的调用中，ch2未怀孕");return; }
        if(check(PregnantStates.enable)) {
            PregnantStates.remove(DomainManager.Character, ch2.GetId(), context);
            PregnantStates.add(DomainManager.Character, ch1.GetId(), p, context);
            EventHelper.RemoveFeature(ch2, DefKey_Pregnant);
            EventHelper.AddFeature(ch1, DefKey_Pregnant);
        }
    }

    [DisplayName("short_获取名称为1的药品id")]
    public static short obtainMedicine(string trickstr){
        if(Parser.Name2Id(trickstr,Config.Medicine.Instance, out short type)){
            return type;
        } else {
            return -1;
        }
    }
    [DisplayName("short_获取名称为1的特性id")]
    public static short obtainCharacterFeature(string trickstr){
        if(Parser.Name2Id(trickstr,Config.CharacterFeature.Instance, out short type)){
            return type;
        } else {
            return -1;
        }
    }
    [DisplayName("short_获取名称为1的遗惠id")]
    public static short 获取名称为1的遗惠id(string trickstr){
        if(Parser.Name2Id(trickstr,Config.CharacterFeature.Instance, out short type)){
            return type;
        } else {
            return -1;
        }
    }
    [DisplayName("void_获取名称为1的遗惠2个")]
    public static void 获取名称为1的遗惠(string trick, int count){
        foreach(var trickstr in trick.Split(',',StringSplitOptions.RemoveEmptyEntries).Select(x=>x.Trim())){
            if(Parser.Name2Id(trickstr,Config.Legacy.Instance, out short type)){
                for(int i=0;i<count;i++){
                    DomainManager.Taiwu.AddAvailableLegacy(context, type);
                }
            } else {
                logwarn($"不存在名为{trickstr}的遗惠");
            }
        }
    }
    [DisplayName("void_允许重新刷新非剑冢独特遗惠")]
    public static void 允许重新刷新非剑冢独特遗惠(){
        if(check(SelectedUniqueLegacies.enable)){
            SelectedUniqueLegacies.set(DomainManager.Extra,SelectedUniqueLegacies.get(DomainManager.Extra).Where(x=>x<9).ToList(), context);
        }
    }

    [DisplayName("void_人物1获取名称为2的药品，数量为3")]
    public static void obtainMedicine(GameData.Domains.Character.Character self, string trick,int count){
        foreach(var trickstr in trick.Split(',',StringSplitOptions.RemoveEmptyEntries).Select(x=>x.Trim())){
            if(Parser.Name2Id(trickstr,Config.Medicine.Instance, out short type)){
                var key=DomainManager.Item.CreateItem(context, 8, type);
                self.AddInventoryItem(context, key, count);
            } else {
                logwarn($"不存在名为{trickstr}的药品");
            }
        }
    }

    [DisplayName("void_人物1获取超过主属性上限2点的主属性")]
    public static void obtainmainattrib(GameData.Domains.Character.Character self,short extra){
        extra=(short)(-extra);
        self.SetCurrMainAttributes(self.GetMaxMainAttributes().Subtract(new(extra,extra,extra,extra,extra,extra)),context);
    }
    public static class 九毒圣僵 {
        internal static string item_str="万化灵符";
        internal static string feature_str="活死人";
        internal static short type=-1;
        internal static short ftype=-1;

        public static unsafe void make(GameData.Domains.Character.Character self){
            if(type==-1){
                if(Parser.Name2Id(item_str,Config.Medicine.Instance, out short typex)){
                    type=typex;
                } else {
                    logwarn("找不到 九毒圣僵.item_str (={item_str})对应的药品id，请调用 `void_九毒圣僵_设置胃格物品`修正这个BUG");
                    return;
                }
                if(Parser.Name2Id(feature_str,Config.CharacterFeature.Instance, out typex)){
                    九毒圣僵.ftype=typex;
                } else {
                    logwarn("找不到 九毒圣僵.feature_str (={feature_str})对应的特性id，请联系中子修正这个BUG");
                    type=-1;
                    return;
                }
            }
            if(!check(Medicine.enable,"九毒圣僵.make")){
                return;
            }
            var _id=self.GetId();
            var eatingitems=self.GetEatingItems();
            foreach(var (idx,pflag) in new (short,sbyte)[]{(0,19) , (1,28), (2,41), (3,11),  (4,44), (5,21),(6,37), (7,26),(8,42)}){
                var oriItemKey = (GameData.Domains.Item.ItemKey)eatingitems.ItemKeys[idx];
                if (oriItemKey.IsValid() || GameData.Domains.Character.EatingItems.IsWug(oriItemKey)) {
                    continue;
                }
                var item=制作淬毒药品(type,pflag);
                var itemKey=item.GetItemKey();
                DomainManager.Item.SetPoisonsIdentified(context, itemKey, isIdentified: true);
                DomainManager.Item.SetOwner(itemKey, GameData.Domains.Item.ItemOwnerType.CharacterEatingItem, _id);


                var config2 = Config.Medicine.Instance[itemKey.TemplateId];
                if (config2.InstantEffectType >= 0) {
                    Medicine.ApplyMedicineInstantEffect(self, context, config2);
                }
                Medicine.ApplySpecialMedicineEffect(self, context, itemKey.TemplateId);
                eatingitems.ItemKeys[idx] = (ulong)itemKey;
                eatingitems.Durations[idx] = 32000;
            }
            self.SetEatingItems(ref eatingitems,context);
            var x=new GameData.Domains.Character.PoisonInts(32000,32000,32000,32000,32000,32000);
            self.SetPoisoned(ref x,context);
            self.AddFeature(context, ftype, true);
            self.consummateLevel(18,context);
        }
    }
    [DisplayName("void_九毒圣僵_设置胃格物品")]
    public static void void_九毒圣僵_设置胃格物品(string str=null,string feature_str=null){
        if(str is not null){
            九毒圣僵.item_str=str;
            if(Parser.Name2Id(str,Config.Medicine.Instance, out short type)){
                九毒圣僵.type=type;
            } else {
                logwarn("找不到 九毒圣僵.item_str (={item_str})对应的药品id，请联系中子修正这个BUG");
                九毒圣僵.type=-1;
            }
        }
        if(feature_str is not null){
            九毒圣僵.feature_str=feature_str;
            if(Parser.Name2Id(str,Config.CharacterFeature.Instance, out short type)){
                九毒圣僵.ftype=type;
            } else {
                logwarn("找不到 九毒圣僵.feature_str (={feature_str})对应的特性id，请联系中子修正这个BUG");
                九毒圣僵.type=九毒圣僵.ftype=-1; // 清空`九毒圣僵.type`防止程序进入错误路径之后红字
            }
        }
    }

    [DisplayName("void_将人物1炼成九毒圣僵")]
    public static void obtainMedicine_impl(GameData.Domains.Character.Character self)=>九毒圣僵.make(self);

    static class obtainMedicine_mask{
        internal static (short,short)[] template=new (short,short)[]{(1,8),(2,17),(4,26),(8,35),(16,44),(32,53)};
    }

    [DisplayName("void_制作id为1的物品，并按flag2（烈.郁.寒.赤.腐.幻=1.2.4.8.16.32）进行120点淬毒")]
    public static GameData.Domains.Item.ItemBase 制作淬毒药品(short type,sbyte flag){
        var item=DomainManager.Item.GetBaseItem(DomainManager.Item.CreateItem(context, 8, type));
        foreach (var id in obtainMedicine_mask.template.Where(x=>(x.Item1&flag)!=0).Select(x=>x.Item2).Take(3)){
            item=DomainManager.Item.SetAttachedPoisons(context,item,id,true).Item1;
        }
        return item;
    }
    [DisplayName("void_人物1获取名称为2的药品，数量为3，并按flag4（烈.郁.寒.赤.腐.幻=1.2.4.8.16.32）进行120点淬毒")]
    public static void obtainMedicine_impl(GameData.Domains.Character.Character self, string trickstr,int count,sbyte flag){
        try {
            if(Parser.Name2Id(trickstr,Config.Medicine.Instance, out short type)){
                for(int i=0;i<count;i++){
                    var item=制作淬毒药品(type,flag);
                    self.AddInventoryItem(context, item.GetItemKey(), 1);
                }
            } else {
                logwarn($"不存在名为{trickstr}的药品");
            }
        } catch (Exception ex){
            logwarn($"获取{trickstr}时出错：",ex);
        }
    }
    [DisplayName("void_人物1单向爱慕人物2")]
    public static void EstablishLoveRelation(GameData.Domains.Character.Character self, GameData.Domains.Character.Character love_target){
        DomainManager.Character.AddRelation(context, self.GetId(), love_target.GetId(), RelationType_Adored, int.MinValue);
        DomainManager.TaiwuEvent.RecordCharacterRelationChanged(false, self.GetId(), love_target.GetId(), RelationType_Adored);
    }
    [DisplayName("void_人物1对人物2好感增加数值3")]
    public static void EnhanceRelation(GameData.Domains.Character.Character self, GameData.Domains.Character.Character love_target, short value){
        // DomainManager.Character.ChangeFavorabilityOptional(context, self, love_target, value);
        EventHelper.DirectlyChangeFavorabilityOptional(self, love_target, value, -1);
    }
    [DisplayName("void_人物1内息增加数值2")]
    public static void DisorderOfQi(GameData.Domains.Character.Character character, short val)=>character.SetDisorderOfQi((short)Math.Clamp(character.GetDisorderOfQi()+val,0,8000), context);

    [DisplayName("string_注册ArgBox1事件跳转token2")]
    public static string PrepareJump(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string value){
        if(ArgBox.Contains<string>("NeutronToken")){
            logwarn($"NeutronToken已被设置为「{ArgBox.GetString("value")}」,因此忽视当前注册");
            return string.Empty;
        } else {
            ArgBox.Set("NeutronToken", value);
            return "4e657574-726f-6e20-3335-323900000000";
        }
    }

    [DisplayName("void_设置ArgBox1中的整数键2为值3")]
    public static void SetArgBox(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key, int value)=>ArgBox.Set(key,value);
    [DisplayName("void_设置ArgBox1中的字符串键2为值3")]
    public static void SetArgBox(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key, string value)=>ArgBox.Set(key,value);
    [DisplayName("void_设置ArgBox1中的布尔键2为值3")]
    public static void SetArgBox(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key, bool value)=>ArgBox.Set(key,value);
    [DisplayName("int_获取ArgBox1中整数键2的值")]
    public static int GetArgBoxVal(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key)=>ArgBox.GetInt(key);
    [DisplayName("bool_ArgBox1中有整数键2")]
    public static bool GetArgBoxContainsVal(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key)=>ArgBox.Contains<int>(key);
    [DisplayName("bool_ArgBox1中有布尔键2")]
    public static bool GetArgBoxBool(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key)=>ArgBox.Contains<bool>(key);
    [DisplayName("bool_ArgBox1中布尔键2的值")]
    public static bool GetArgBoxBoolVal(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key)=>ArgBox.GetBool(key);
    [DisplayName("string_获取ArgBox1中字符串键2的值")]
    public static string GetArgBoxString(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key)=>ArgBox.GetString(key);
    [DisplayName("void_删除ArgBox1中的整数键2")]
    public static void RemoveArgBoxVal(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key)=>ArgBox.Remove<int>(key);
    [DisplayName("void_删除ArgBox1中的字符串键2")]
    public static void RemoveArgBoxString(GameData.Domains.TaiwuEvent.EventArgBox ArgBox, string key)=>ArgBox.Remove<string>(key);
    [DisplayName("EventArgBox_获取门派1剧情ArgBox")] // ExtraDomain.GetSectMainStoryEventArgBox
    public static GameData.Domains.TaiwuEvent.EventArgBox 获取门派1剧情ArgBox(sbyte key)=>DomainManager.Extra.GetSectMainStoryEventArgBox(key);

    static string mainRoleAdjustClothId=((string)typeof(GameData.Domains.TaiwuEvent.EventArgBox).GetField("MainRoleAdjustClothId")?.GetValue(null))??GameData.Domains.TaiwuEvent.EventArgBox.MainRoleAdjustClothId;
    static string targetRoleAdjustClothId=((string)typeof(GameData.Domains.TaiwuEvent.EventArgBox).GetField("TargetRoleAdjustClothId")?.GetValue(null))??GameData.Domains.TaiwuEvent.EventArgBox.TargetRoleAdjustClothId;
    [DisplayName("string_左侧人物显示服装键")]
    public static string MainRoleAdjustClothId()=>mainRoleAdjustClothId;
    [DisplayName("string_右侧人物显示服装键")]
    public static string TargetRoleAdjustClothId()=>targetRoleAdjustClothId;

    [DisplayName("void_人物1肖像显示皱纹")]
    public static void older(GameData.Domains.Character.Character character){
        var avatar=character.GetAvatar();
        avatar.SetGrowableElementShowingAbility(3,true);
        avatar.SetGrowableElementShowingAbility(4,true);
        avatar.SetGrowableElementShowingAbility(5,true);
        character.SetAvatar(avatar, context);
    }
    [DisplayName("void_人物1肖像隐藏皱纹并显示头发眉毛")]
    public static void younger(GameData.Domains.Character.Character character){
        var avatar=character.GetAvatar();
        avatar.SetGrowableElementShowingAbility(0,true); // 头发
        avatar.SetGrowableElementShowingAbility(3,false);
        avatar.SetGrowableElementShowingAbility(4,false);
        avatar.SetGrowableElementShowingAbility(5,false);
        avatar.SetGrowableElementShowingAbility(6,true); // 眉毛
        character.SetAvatar(avatar, context);
    }
    [DisplayName("void_人物1肖像不显示头发眉毛")]
    public static void older2(GameData.Domains.Character.Character character){
        var avatar=character.GetAvatar();
        avatar.SetGrowableElementShowingAbility(0,false); // 头发
        avatar.SetGrowableElementShowingAbility(6,false); // 眉毛
        character.SetAvatar(avatar, context);
    }
    [DisplayName("string_获取人物数组1的姓名")]
    public static string GetNames(IEnumerable<GameData.Domains.Character.Character> chars)=>string.Join('、', chars.Select(ch=>$"<color=#B97D4BFF>{GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetMonasticTitleOrDisplayName(ch,false)}</color>"));
    [DisplayName("string_性别1随机汉名")]
    public static string rnd_given_name(sbyte gender)=>EventHelper.GetRandomCharacterHanName(gender).Item2;
    [DisplayName("string_性别1随机汉姓")]
    public static string rnd_family_name(sbyte gender)=>EventHelper.GetRandomCharacterHanName(gender).Item1;
    [DisplayName("void_人物失去所有主属性")]
    public static void zeroing_mainattributes(GameData.Domains.Character.Character character){
        character.SetCurrMainAttributes(new GameData.Domains.Character.MainAttributes(0,0,0,0,0,0),context);
    }
    [DisplayName("string_人物1浑浊度2/3阴元")]
    public static string yuanyin_str(GameData.Domains.Character.Character character, int p, int q)=>$"<color=#{(170-170*p/q).ToString("X2")}{(170*p/q).ToString("X2")}{(255*p/q).ToString("X2")}>{(character.HasVirginity()?"元阴":"阴元")}</color>";
    [DisplayName("int_太吾阴元")]
    public static int taiwu_yin(){
        int ret=0;
        GlobalArgBox().Get("Neutron-TaiwuYin",ref ret);
        return Math.Max(1,ret);
    }
    [DisplayName("int_太吾阳元")]
    public static int taiwu_yang(){
        int ret=0;
        GlobalArgBox().Get("Neutron-TaiwuYang",ref ret);
        return ret;
    }
    [DisplayName("int_太吾阴元增加数量1")]
    public static int taiwu_yin(int amount){
        int ret=0;
        GlobalArgBox().Get("Neutron-TaiwuYin",ref ret);
        ret=Math.Clamp(ret+amount,0,10000000);
        GlobalArgBox().Set("Neutron-TaiwuYin",ret);
        return ret;
    }
    [DisplayName("int_太吾阳元增加数量1")]
    public static int taiwu_yang(int amount){
        int ret=0;
        GlobalArgBox().Get("Neutron-TaiwuYang",ref ret);
        ret=Math.Clamp(ret+amount,0,10000000);
        GlobalArgBox().Set("Neutron-TaiwuYang",ret);
        return ret;
    }
    class SetElement_PregnantStates {
        public static Action<GameData.Domains.Character.CharacterDomain, int, GameData.Domains.Character.PregnantState, GameData.Common.DataContext> act=Utils.FastExpr.Met<SetElement_PregnantStates>.get(out act);
    }
    [DisplayName("void_人物1怀孕月数增加2")] // 统计精华用
    public static void taiwu_preg_add_month(GameData.Domains.Character.Character ch, int month){
        if(DomainManager.Character.TryGetPregnantState(ch.GetId(), out var ps)){
            ps.ExpectedBirthDate+=month; // DomainManager.World.GetCurrDate()+1;
            SetElement_PregnantStates.act(DomainManager.Character, ch.GetId(), ps, context);
        }
    }
    [DisplayName("int_人物1腹内精华")]
    public static int taiwu_yang_count(GameData.Domains.Character.Character ch){
        if(DomainManager.Character.TryGetPregnantState(ch.GetId(), out var ps)){
            return Math.Max(0,ps.ExpectedBirthDate-DomainManager.World.GetCurrDate()-1);
        } else {
            return 0;
        }
    }
    [DisplayName("int_数值1到数值2之间的随机数")]
    public static int random(int min, int max){
        return context.Random.Next(min,max);
    }
    [DisplayName("int_小于数值1的随机自然数")]
    public static int random(int max){
        return context.Random.Next(0,max);
    }
    [DisplayName("string_太吾元阴")]
    public static string taiwu_yuanyin()=>yuanyin_str(DomainManager.Taiwu.GetTaiwu(), taiwu_yin(),taiwu_yin()+taiwu_yang());
    [DisplayName("void_复杂逻辑我选择直接写函数（获取所有书籍）")]
    public static void get_all_book(GameData.Domains.Character.Character character, short from, short to){
        for(var i=from;i<to;i++){
            EventHelper.AddItemToRole(character, EventHelper.CreateSkillBook(i), 1);
        }
    }
    [DisplayName("void_人物1真气五行变为2-5")]
    public static void get_all_book(GameData.Domains.Character.Character character,byte i1,byte i2,byte i3,byte i4){
        if(100-i1-i2-i3-i4>0)character.SetBaseNeiliProportionOfFiveElements(new GameData.Domains.Character.NeiliProportionOfFiveElements((sbyte)i1,(sbyte)i2,(sbyte)i3,(sbyte)i4,(sbyte)(100-i1-i2-i3-i4)), context);
    }
    [DisplayName("void_人物1真气变为天人一体")]
    public static void get_all_book(GameData.Domains.Character.Character character)=>character.SetBaseNeiliProportionOfFiveElements(new GameData.Domains.Character.NeiliProportionOfFiveElements(20,20,20,20,20), context);
    public static short DefKey_VirginityTrue = (short?)typeof(Config.CharacterFeature.DefKey).GetField("VirginityTrue",(BindingFlags)(-1))?.GetValue(null)??Config.CharacterFeature.DefKey.VirginityTrue;
    public static short DefKey_VirginityFalse = (short?)typeof(Config.CharacterFeature.DefKey).GetField("VirginityFalse",(BindingFlags)(-1))?.GetValue(null)??Config.CharacterFeature.DefKey.VirginityFalse;
    [DisplayName("void_人物1失贞")]
    public static void shizhen(GameData.Domains.Character.Character character){
        character.AddFeature(context, DefKey_VirginityFalse, true);
    }
    [DisplayName("void_人物1恢复童贞")]
    public static void inv_shizhen(GameData.Domains.Character.Character character){
        character.AddFeature(context, DefKey_VirginityTrue, true);
    }
    [DisplayName("void_人物1恢复六维")]
    public static void recover_six(GameData.Domains.Character.Character character){
        character.SetCurrMainAttributes(character.GetMaxMainAttributes(), context);
    }
    [DisplayName("void_人物1返老还童")]
    public static void become_child(GameData.Domains.Character.Character character){
        character.SetCurrMainAttributes(new GameData.Domains.Character.MainAttributes(1000, 1000, 1000, 1000, 1000, 1000), context);
        character.AddFeature(context, DefKey_VirginityTrue, true);
        var from=character.GetCurrAge();
        if (from > GlobalConfig.Instance.AgeBaby) {
            var needChildCloth = character.GetAgeGroup() != 1;
            character.SetCurrAge((short)GlobalConfig.Instance.AgeBaby, context);
            GameData.DomainEvents.Events.RaiseCharacterAgeChanged(context, character, from, GlobalConfig.Instance.AgeBaby);
            if (needChildCloth) {
                var itemKey = DomainManager.Item.CreateClothing(context, 65, character.GetGender());
                character.AddInventoryItem(context, itemKey, 1);
                character.ChangeEquipment(context, -1, 4, itemKey);
            }
        }
    }
    static int AgeAdult = (((int?)typeof(GlobalConfig).GetField("AgeAdult").GetValue(null))??GlobalConfig.AgeAdult);
    [DisplayName("void_人物1年轻2年")]
    public static void being_younger(GameData.Domains.Character.Character character, int year){
        var from = character.GetCurrAge();
        if(from-year>=AgeAdult){
            character.SetCurrAge((short)(from-year), context);
            GameData.DomainEvents.Events.RaiseCharacterAgeChanged(context, character, from, from-year);
        } else if (character.GetCurrAge() > GlobalConfig.Instance.AgeBaby) {
            var needChildCloth = character.GetAgeGroup() != 1;
            character.SetCurrAge((short)Math.Max(GlobalConfig.Instance.AgeBaby,(character.GetCurrAge()-year)), context);
            GameData.DomainEvents.Events.RaiseCharacterAgeChanged(context, character, from, Math.Max(GlobalConfig.Instance.AgeBaby,(character.GetCurrAge()-year)));
            if (needChildCloth) {
                var itemKey = DomainManager.Item.CreateClothing(context, 65, character.GetGender());
                character.AddInventoryItem(context, itemKey, 1);
                character.ChangeEquipment(context, -1, 4, itemKey);
            }
        }
    }
    [DisplayName("void_人物1年老2年")]
    public static void being_older(GameData.Domains.Character.Character character, int year){
        var from = character.GetCurrAge();
        if((character.GetCurrAge()+year>=AgeAdult && character.GetCurrAge()>=AgeAdult) || (character.GetCurrAge()+year<AgeAdult && character.GetCurrAge()>=GlobalConfig.Instance.AgeBaby)){
            character.SetCurrAge((short)(character.GetCurrAge()+year), context);
            GameData.DomainEvents.Events.RaiseCharacterAgeChanged(context, character, from, from+year);
        } else {
            logwarn("懒得写跨年龄阶段的逻辑了，就这样罢");
        }
    }

    [DisplayName("void_人物1年龄改变2年")] // 年龄改变可以为负数
    public static void 人物1年龄改变2年(GameData.Domains.Character.Character character, int year) {
        var from = character.GetCurrAge();
        var to = from + year;
        if (from<16) {
            var pf = character.GetPotentialFeatureIds();
            int oriCount = pf.Count * from / 16;
            int currCount = pf.Count * Math.Min(16, to) / 16;
            if (oriCount < currCount) {
                var dict = new Dictionary<short, sbyte>();
                foreach(var currFeatureId in character.GetFeatureIds()){
                    var currFeature = Config.CharacterFeature.Instance[currFeatureId];
                    if(dict.TryGetValue(currFeature.MutexGroupId, out var level)) {
                        dict[currFeature.MutexGroupId] = Math.Max(level, currFeature.Level);
                    } else {
                        dict[currFeature.MutexGroupId] = currFeature.Level;
                    }
                }
                for (int i = oriCount; i < currCount-1; i++) {
                    var currFeature = Config.CharacterFeature.Instance[pf[i]];
                    if ((dict.TryGetValue(currFeature.MutexGroupId, out var level) ? level: -128) < currFeature.Level) {
                        character.AddFeature(context, pf[i], removeMutexFeature: true);
                        dict[currFeature.MutexGroupId] = currFeature.Level;
                    }
                }
            }
        } else { // 清除potential_id，防止potential id反复注册
            var pf = character.GetPotentialFeatureIds();
            if(pf.Count>0) {
                pf.Clear();
                character.SetPotentialFeatureIds(pf, context);
            }
        }
        character.ChangeCurrAge(context, year);
    }
    [DisplayName("void_人物1所处地格中所有人产生春宵欲望")]
    public static void chunxiao(GameData.Domains.Character.Character character){
        var blk=DomainManager.Map.GetBlock(character.GetLocation());
        if(blk?.CharacterSet is not null){
            人物id列表1中所有人产生春宵欲望(blk.CharacterSet);
        }
    }

    [DisplayName("void_人物id列表1中所有人产生春宵欲望")]
    public static void 人物id列表1中所有人产生春宵欲望(IEnumerable<int> character){
        var chs=character.ToArray();
        foreach(var cid in chs) {
            if(DomainManager.Character.TryGetElement_Objects(cid,out var ch)) {
                foreach(var cid2 in chs){
                    if (cid2!=cid && DomainManager.Character.TryGetElement_Objects(cid2,out var ch2)) {
                        AddPNeed(ch, 99, NeedChunXiao(ch2));
                    }
                }
            }
        }
    }
    [DisplayName("void_人物列表1中全部人物成长至成年")]
    public static void 人物列表1中全部人物成长至成年(IEnumerable<GameData.Domains.Character.Character> characters){
        foreach(var ch in characters) {
            if (ch.GetCurrAge()<16) {
                人物1年龄改变2年(ch, 16-ch.GetCurrAge());
            }
        }
    }
    [DisplayName("void_人物1开无遮大会，魅惑周围距离2格之内人物来此，魅惑好感增加值为3")]
    public static void wuzhe_dahui(GameData.Domains.Character.Character character, int distance, short meihuo){
        // context;
        var location = character.GetLocation();
        if (location.IsValid()){
            var mapBlockList = GameData.Utilities.ObjectPool<List<GameData.Domains.Map.MapBlockData>>.Instance.Get();
            var blk=DomainManager.Map.GetBlock(location);
            DomainManager.Map.GetNeighborBlocks(location.AreaId, location.BlockId, mapBlockList, distance);
            var mapBlockList2 = GameData.Utilities.ObjectPool<List<GameData.Domains.Map.MapBlockData>>.Instance.Get();
            mapBlockList2.Clear();
            mapBlockList.Add(blk);
            foreach(var b in mapBlockList){
                if(b.GetRootBlock().GroupBlockList is not null){
                    mapBlockList2.AddRange(b.GetRootBlock().GroupBlockList);
                }
                mapBlockList2.Add(b.GetRootBlock());
            }
            mapBlockList.Clear();
            GameData.Utilities.ObjectPool<List<GameData.Domains.Map.MapBlockData>>.Instance.Return(mapBlockList);

            foreach(var block in mapBlockList2){
                if(block?.CharacterSet is not null){
                    var chs=block.CharacterSet.ToArray();
                    foreach(var cid in chs){
                        if(DomainManager.Character.TryGetElement_Objects(cid,out var ch)){
                            if(ch.GetLocation()!=location){
                                DomainManager.Character.GroupMove(context, ch, location);
                            }
                        }
                    }
                }
            }
            if(blk?.CharacterSet is not null){
                foreach(var cid in blk.CharacterSet){
                    if(DomainManager.Character.TryGetElement_Objects(cid,out var ch)){
                        EnhanceRelation(ch, character, meihuo);
                        support_taiwu(ch);
                    }
                }
            }
            GameData.Utilities.ObjectPool<List<GameData.Domains.Map.MapBlockData>>.Instance.Return(mapBlockList2);
        }
    }
    [DisplayName("int_人物1汲取当地所有性别大于2的人物的阳元")]
    public static int makelove(GameData.Domains.Character.Character character,sbyte gender_ge){
        var loc=character.GetLocation();
        var ret=0;
        if (loc.IsValid()){
            var cids=DomainManager.Map.GetBlock(loc)?.CharacterSet?.Select(x=>x)?.ToArray();
            if(cids is not null){
                foreach(var cid in cids){
                    if(DomainManager.Character.TryGetElement_Objects(cid,out var ch)){
                        if(ch.GetGender()>gender_ge){
                            older(ch);
                            zeroing_mainattributes(ch);
                            if(MaxLeftHealth(ch)>0){
                                ret+=MaxLeftHealth(ch);
                                ModifyHealth(ch,-MaxHealth(ch));
                                if(MaxLeftHealth(ch)==0){
                                    older2(ch);
                                }
                            }
                        }
                    }
                }
            }
        }
        if(ret>0){
            taiwu_preg_add_month(character, ret*100);
        }
        return ret;
    }
    [DisplayName("void_人物1获取内力2点")]
    public static void add_neili(GameData.Domains.Character.Character character, int neili) {
        character.AddNeili(context, neili);
    }
    [DisplayName("void_人物1主动发起与人物2春宵")]
    public static void makelove(GameData.Domains.Character.Character character, GameData.Domains.Character.Character character2){
        character.MakeLove(context,character2,false);
        DomainManager.LifeRecord.GetLifeRecordCollection().AddMakeLoveIllegal(character.GetId(), DomainManager.World.GetCurrDate(), character2.GetId(), character.GetLocation());
    }
    [DisplayName("void_人物1主动发起与人物2春宵并产生欺辱秘闻")]
    public static void 人物1主动发起与人物2春宵并产生秘闻(GameData.Domains.Character.Character character, GameData.Domains.Character.Character character2){
        character.MakeLove(context,character2,false);
        DomainManager.LifeRecord.GetLifeRecordCollection().AddRapeSucceed(character.GetId(), DomainManager.World.GetCurrDate(), character2.GetId(), character.GetLocation());
        DomainManager.Information.AddSecretInformationMetaData(context, DomainManager.Information.GetSecretInformationCollection().AddRape(character.GetId(), character2.GetId()));
    }
    [DisplayName("void_人物1主动发起与人物2春宵并公开欺辱秘闻")]
    public static void 人物1主动发起与人物2春宵并公开欺辱秘闻(GameData.Domains.Character.Character character, GameData.Domains.Character.Character character2){
        character.MakeLove(context,character2,false);
        DomainManager.LifeRecord.GetLifeRecordCollection().AddRapeSucceed(character.GetId(), DomainManager.World.GetCurrDate(), character2.GetId(), character.GetLocation());
        DomainManager.Information.MakeSecretInformationBroadcastEffect(context, DomainManager.Information.AddSecretInformationMetaData(context, DomainManager.Information.GetSecretInformationCollection().AddRape(character.GetId(), character2.GetId())), -1);
    }
    [DisplayName("void_卸下人物id列表1中人物的全部装备")]
    public static void 卸下人物id列表1中人物的全部装备(IEnumerable<int> 人物id列表1) {
        foreach(var id in 人物id列表1) {
            if(DomainManager.Character.TryGetElement_Objects(id, out var ch)) {
                var equips = ch.GetEquipment().ToArray();
                for(int curr = equips.Length;curr-->0;){
                    var equip = equips[curr];
                    if(equip.IsValid() && DomainManager.Item.GetBaseEquipment(equip).GetDetachable()) {
                        equips[curr] = GameData.Domains.Item.ItemKey.Invalid;
                    }
                }
                ch.ChangeEquipment(context, equips);
            }
        }
    }
    [DisplayName("void_人物列表1中全部人物将物品转移至列表首个人物")]
    public static void 人物列表1中全部人物将物品转移至列表首个人物(IEnumerable<GameData.Domains.Character.Character> lst){
        GameData.Domains.Character.Character ch = null;
        foreach(var c in lst) {
            if (ch == null) {
                ch = c;
            } else {
                foreach(var (itemKey, amount) in c.GetInventory().Items.ToArray()) {
                    c.RemoveInventoryItem(context, itemKey, amount, deleteItem: false);
                    ch.AddInventoryItem(context, itemKey, amount);
                }
            }
        }
    }
    [DisplayName("void_人物列表1中全部人物死亡")]
    public static void 人物列表1中全部人物死亡(IEnumerable<GameData.Domains.Character.Character> 人物列表1) {
        foreach(var ch in 人物列表1) {
            DomainManager.Character.MakeCharacterDead(context, ch, 10);
        }
    }

    [DisplayName("bool_人物1所在地格可转化")]
    public static bool cancvrt(GameData.Domains.Character.Character character){
        var loc=character.GetLocation();
        if(loc.IsValid()){
            return DomainManager.Map.GetBlock(loc)?.CanChangeBlockType()??false;
        } else {
            return false;
        }
    }
    [DisplayName("bool_人物1所在地格可转化为2x2地形")]
    public static bool chkcvrt(GameData.Domains.Character.Character character){
        var loc=character.GetLocation();
        if(loc.IsValid()){
            var size=DomainManager.Map.GetAreaSize(loc.AreaId);
            if(loc.BlockId+size+1<size*size){
                // foreach(var x in new int[]{loc.BlockId,loc.BlockId+1,loc.BlockId+size,loc.BlockId+size+1}){
                //     var mbd = DomainManager.Map.GetBlock(loc.AreaId,(short)x);
                //     logwarn($"mbd {x} have type {mbd.TemplateId}");
                // }
                foreach(var x in new int[]{loc.BlockId,loc.BlockId+1,loc.BlockId+size,loc.BlockId+size+1}){
                    var mbd = DomainManager.Map.GetBlock(loc.AreaId,(short)x);
                    if(!mbd.CanChangeBlockType()){
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
    [DisplayName("void_人物1所在地格转化为大地形2")]
    public static void execcvrt(GameData.Domains.Character.Character character, short type){
        var loc=character.GetLocation();
        if(type%6>2 || type<60 || type>92){
            return;
        }
        if(loc.IsValid()){
            var size=DomainManager.Map.GetAreaSize(loc.AreaId);
            if(loc.BlockId+size+1<size*size){
                foreach(var x in new int[]{loc.BlockId,loc.BlockId+1,loc.BlockId+size,loc.BlockId+size+1}){
                    var mbd = DomainManager.Map.GetBlock(loc.AreaId,(short)x);
                    if(!mbd.CanChangeBlockType()){
                        return;
                    }
                }
                var rmbd = DomainManager.Map.GetBlock(loc);
                rmbd.TemplateId=type;
                rmbd.RootBlockId = -1;
                rmbd.BelongBlockId = -1;
                if (rmbd.GroupBlockList == null){
                    rmbd.GroupBlockList = new();
                } else {
                    rmbd.GroupBlockList.Clear();
                }
                foreach(var x in new int[]{loc.BlockId+1,loc.BlockId+size,loc.BlockId+size+1}){
                    var mbd = DomainManager.Map.GetBlock(loc.AreaId,(short)x);
                    mbd.RootBlockId = rmbd.BlockId;
                    mbd.BelongBlockId = rmbd.BlockId;
                    mbd.TemplateId = type;
                    rmbd.GroupBlockList.Add(mbd);
                    mbd.maximize_resource(context);
                    // DomainManager.Map.SetBlockData(,mbd);
                }
                rmbd.maximize_resource(context);
            }
        }
    }
    [DisplayName("void_将人物1蛊转移到人物2")]
    public static unsafe void absorb_qing_sui(GameData.Domains.Character.Character character, GameData.Domains.Character.Character character2){
        for(var i=0;i<9;i++){
            var itemKey=character.GetEatingItems().Get(i);
            if(GameData.Domains.Character.EatingItems.IsWug(itemKey)){
                character.RemoveWug(context, itemKey.TemplateId);
                character2.AddWug(context, itemKey.TemplateId);
            }
        }
    }
    [DisplayName("bool_人物1身上有青髓蛊")]
    public static unsafe bool have_qing_sui(GameData.Domains.Character.Character character)=>character.GetEatingItems().IndexOfWug(7)!=-1;
    [DisplayName("bool_人物1身上有蛊")]
    public static unsafe bool have_wug(GameData.Domains.Character.Character character){
        for(var i=0;i<9;i++){
            var itemKey=character.GetEatingItems().Get(i);
            if(GameData.Domains.Character.EatingItems.IsWug(itemKey)){
                return true;
            }
        }
        return false;
    }
    [DisplayName("void_人物1毒免")]
    public static void 人物1毒免(GameData.Domains.Character.Character character) {
        DomainManager.Extra.AddPoisonImmunities(context, character.GetId(), 63);
    }
    [DisplayName("void_太吾毒免")]
    public static void legacy_boost_poison_immue() {
        DomainManager.Extra.AddPoisonImmunities(context, DomainManager.Taiwu.GetTaiwuCharId(), 63);
    }
    [DisplayName("void_太吾半毒免")]
    public static void legacy_boost_poison_half_immue() {
        DomainManager.Extra.AddPoisonImmunities(context, DomainManager.Taiwu.GetTaiwuCharId(), 56);
    }
    [DisplayName("void_结算神女传剑")]
    public static void legacy_boost_poison_monv_immue() {
        if(!DomainManager.Extra.IsUniqueLegacySelected(0)){
            DomainManager.Extra.AddSelectedUniqueLegacies(context, 0);
        }
        DomainManager.Extra.AddPoisonImmunities(context, DomainManager.Taiwu.GetTaiwuCharId(), 7);
    }
    [DisplayName("void_保证全剑柄继承六维不小于数值1")]
    public static void legacy_boost_6(int value){
        foreach(var item in new[]{
            ECharacterPropertyReferencedType.Strength,
            ECharacterPropertyReferencedType.Dexterity,
            ECharacterPropertyReferencedType.Concentration,
            ECharacterPropertyReferencedType.Vitality,
            ECharacterPropertyReferencedType.Energy,
            ECharacterPropertyReferencedType.Intelligence
        }){
            var v=value-DomainManager.Extra.GetTaiwuPropertyPermanentBonus(item,GameData.Domains.Character.CharacterPropertyBonus.EPropertyBonusType.AddValue);
            if(v>0){
                DomainManager.Extra.AddTaiwuPropertyPermanentBonus(
                    context,
                    item,
                    GameData.Domains.Character.CharacterPropertyBonus.EPropertyBonusType.AddValue,
                    v
                );
            }
        }
    }
    [DisplayName("void_保证全剑柄继承功法资质不小于数值1")]
    public static void legacy_boost_14(int value){
        foreach(var item in new[]{
            ECharacterPropertyReferencedType.QualificationNeigong,
            ECharacterPropertyReferencedType.QualificationPosing,
            ECharacterPropertyReferencedType.QualificationStunt,
            ECharacterPropertyReferencedType.QualificationFistAndPalm,
            ECharacterPropertyReferencedType.QualificationFinger,
            ECharacterPropertyReferencedType.QualificationLeg,
            ECharacterPropertyReferencedType.QualificationThrow,
            ECharacterPropertyReferencedType.QualificationSword,
            ECharacterPropertyReferencedType.QualificationBlade,
            ECharacterPropertyReferencedType.QualificationPolearm,
            ECharacterPropertyReferencedType.QualificationSpecial,
            ECharacterPropertyReferencedType.QualificationWhip,
            ECharacterPropertyReferencedType.QualificationControllableShot,
            ECharacterPropertyReferencedType.QualificationCombatMusic
        }){
            var v=value-DomainManager.Extra.GetTaiwuPropertyPermanentBonus(item,GameData.Domains.Character.CharacterPropertyBonus.EPropertyBonusType.AddValue);
            if(v>0){
                DomainManager.Extra.AddTaiwuPropertyPermanentBonus(
                    context,
                    item,
                    GameData.Domains.Character.CharacterPropertyBonus.EPropertyBonusType.AddValue,
                    v
                );
            }
        }
    }
    [DisplayName("void_保证全剑柄继承技艺资质不小于数值1")]
    public static void legacy_boost_16(int value){
        foreach(var item in new[]{
            ECharacterPropertyReferencedType.QualificationMusic,
            ECharacterPropertyReferencedType.QualificationChess,
            ECharacterPropertyReferencedType.QualificationPoem,
            ECharacterPropertyReferencedType.QualificationPainting,
            ECharacterPropertyReferencedType.QualificationMath,
            ECharacterPropertyReferencedType.QualificationAppraisal,
            ECharacterPropertyReferencedType.QualificationForging,
            ECharacterPropertyReferencedType.QualificationWoodworking,
            ECharacterPropertyReferencedType.QualificationMedicine,
            ECharacterPropertyReferencedType.QualificationToxicology,
            ECharacterPropertyReferencedType.QualificationWeaving,
            ECharacterPropertyReferencedType.QualificationJade,
            ECharacterPropertyReferencedType.QualificationTaoism,
            ECharacterPropertyReferencedType.QualificationBuddhism,
            ECharacterPropertyReferencedType.QualificationCooking,
            ECharacterPropertyReferencedType.QualificationEclectic
        }){
            var v=value-DomainManager.Extra.GetTaiwuPropertyPermanentBonus(item,GameData.Domains.Character.CharacterPropertyBonus.EPropertyBonusType.AddValue);
            if(v>0){
                DomainManager.Extra.AddTaiwuPropertyPermanentBonus(
                    context,
                    item,
                    GameData.Domains.Character.CharacterPropertyBonus.EPropertyBonusType.AddValue,
                    v
                );
            }
        }
    }
    [DisplayName("void_全剑柄继承六维数值增加1")]
    public static void 全剑柄继承六维数值增加1(int value){
        foreach(var item in new[]{
            ECharacterPropertyReferencedType.Strength,
            ECharacterPropertyReferencedType.Dexterity,
            ECharacterPropertyReferencedType.Concentration,
            ECharacterPropertyReferencedType.Vitality,
            ECharacterPropertyReferencedType.Energy,
            ECharacterPropertyReferencedType.Intelligence
        }){
            DomainManager.Extra.AddTaiwuPropertyPermanentBonus(
                context,
                item,
                GameData.Domains.Character.CharacterPropertyBonus.EPropertyBonusType.AddValue,
                value
            );
        }
    }
    [DisplayName("void_全剑柄继承功法资质数值增加1")]
    public static void 全剑柄继承功法资质数值增加1(int value){
        foreach(var item in new[]{
            ECharacterPropertyReferencedType.QualificationNeigong,
            ECharacterPropertyReferencedType.QualificationPosing,
            ECharacterPropertyReferencedType.QualificationStunt,
            ECharacterPropertyReferencedType.QualificationFistAndPalm,
            ECharacterPropertyReferencedType.QualificationFinger,
            ECharacterPropertyReferencedType.QualificationLeg,
            ECharacterPropertyReferencedType.QualificationThrow,
            ECharacterPropertyReferencedType.QualificationSword,
            ECharacterPropertyReferencedType.QualificationBlade,
            ECharacterPropertyReferencedType.QualificationPolearm,
            ECharacterPropertyReferencedType.QualificationSpecial,
            ECharacterPropertyReferencedType.QualificationWhip,
            ECharacterPropertyReferencedType.QualificationControllableShot,
            ECharacterPropertyReferencedType.QualificationCombatMusic
        }){
            DomainManager.Extra.AddTaiwuPropertyPermanentBonus(
                context,
                item,
                GameData.Domains.Character.CharacterPropertyBonus.EPropertyBonusType.AddValue,
                value
            );
        }
    }
    [DisplayName("void_全剑柄继承技艺资质数值增加1")]
    public static void 全剑柄继承技艺资质数值增加1(int value){
        foreach(var item in new[]{
            ECharacterPropertyReferencedType.QualificationMusic,
            ECharacterPropertyReferencedType.QualificationChess,
            ECharacterPropertyReferencedType.QualificationPoem,
            ECharacterPropertyReferencedType.QualificationPainting,
            ECharacterPropertyReferencedType.QualificationMath,
            ECharacterPropertyReferencedType.QualificationAppraisal,
            ECharacterPropertyReferencedType.QualificationForging,
            ECharacterPropertyReferencedType.QualificationWoodworking,
            ECharacterPropertyReferencedType.QualificationMedicine,
            ECharacterPropertyReferencedType.QualificationToxicology,
            ECharacterPropertyReferencedType.QualificationWeaving,
            ECharacterPropertyReferencedType.QualificationJade,
            ECharacterPropertyReferencedType.QualificationTaoism,
            ECharacterPropertyReferencedType.QualificationBuddhism,
            ECharacterPropertyReferencedType.QualificationCooking,
            ECharacterPropertyReferencedType.QualificationEclectic
        }){
            DomainManager.Extra.AddTaiwuPropertyPermanentBonus(
                context,
                item,
                GameData.Domains.Character.CharacterPropertyBonus.EPropertyBonusType.AddValue,
                value
            );
        }
    }
    public static short DefKey_Pregnant = (short?)typeof(Config.CharacterFeature.DefKey).GetField("Pregnant",(BindingFlags)(-1))?.GetValue(null)??Config.CharacterFeature.DefKey.Pregnant;
    [DisplayName("void_人物1借人物2受孕")]
    public static void BeingPregnanted(GameData.Domains.Character.Character taiwuChar, GameData.Domains.Character.Character character){
        DomainManager.Character.CreatePregnantState(context, taiwuChar, character, false);
        taiwuChar.AddFeature(context, DefKey_Pregnant);
        if(DomainManager.Character.TryGetPregnantState(taiwuChar.GetId(), out var ps)) {
            ps.ExpectedBirthDate=DomainManager.World.GetCurrDate()+1;
            DomainManager.Taiwu.PrenatalEducateForCombatSkill(context, 30);
            DomainManager.Taiwu.PrenatalEducateForLifeSkill(context, 30);
            DomainManager.Taiwu.PrenatalEducateForMainAttribute(context, 30);
        } else {
            logwarn("<color=#FF0000>你可以认为是红字了……\n中子并没有处理这种明明创建了PregnantState但无法获取PregnantState的情况\n\n你大概需要备份一次存档，并在此之后原地过月检查是否有过月红字\n或许这也是徒劳，毕竟理论上你永远也不应该看见这行字\n……请自求多福好了……</color>");
            EventHelper.ToEvent(string.Empty);
        }
    }
    [DisplayName("void_人物1基础寿命增加月数2")]
    public static void ModifyHealth(GameData.Domains.Character.Character character, int month){
        character.SetBaseMaxHealth((short)Math.Clamp(character.GetBaseMaxHealth()+month,0,32767),context);
    }
    [DisplayName("void_人物1支持太吾")]
    public static void support_taiwu(GameData.Domains.Character.Character character){
        if(character.GetLocation().IsValid()){
            DomainManager.Extra.SetAreaSpiritualDebt(context, character.GetLocation().AreaId, 30000);
        }
        if(DomainManager.Organization.TryGetElement_SectCharacters(character.GetId(),out _)){
            EventHelper.SetSectCharApprovedTaiwu(character.GetId(), true);
            var set=DomainManager.Organization.GetSettlementByOrgTemplateId(character.GetOrganizationInfo().OrgTemplateId);
            if(set!=null){
                EventHelper.AddMaxApprovingRateBonus(set.GetId(), 100, 3);
            }
        }
    }
    class SetElement_BuildingBlocks {
        internal static readonly Action<GameData.Domains.Building.BuildingDomain, GameData.Domains.Building.BuildingBlockKey, GameData.Domains.Building.BuildingBlockData, GameData.Common.DataContext> Set=Utils.FastExpr.Met<SetElement_BuildingBlocks>.get(out Set);
    }


    [DisplayName("void_将太吾村建筑升满，并开启八方鼎力")]
    public static void character_based_grow(){
        foreach(var settlement in GetSettlements(DomainManager.Organization).Values){
            if(settlement.GetMaxSafety()>0){
                settlement.SetMaxSafety(250,context);
                settlement.SetMaxCulture(250,context);
                settlement.SetSafety(250,context);
                settlement.ChangeCulture(context,250);
            }
            var loc=settlement.GetLocation();
            if(settlement.GetMaxSafety()>0 && loc!=DomainManager.Taiwu.GetTaiwuVillageLocation()){
                character_based_ding_impl(settlement, 20, 120);
            } else {
                var settlementId = settlement.GetId();
                var lst=DomainManager.Building.GetBuildingBlockList(loc);
                if(lst==null){return;}
                foreach(var buildingData in lst){
                    if(buildingData!=null && Config.BuildingBlock.Instance[buildingData.TemplateId] != null && (
                        GameData.Domains.Building.BuildingBlockData.IsResource(Config.BuildingBlock.Instance[buildingData.TemplateId].Type)
                        || GameData.Domains.Building.BuildingBlockData.IsBuilding(Config.BuildingBlock.Instance[buildingData.TemplateId].Type)
                    )){
                        var key=new GameData.Domains.Building.BuildingBlockKey(loc.AreaId, loc.BlockId, buildingData.BlockIndex);
                        buildingData.Level=Math.Max(buildingData.Level, Config.BuildingBlock.Instance[buildingData.TemplateId].MaxLevel);
                        SetElement_BuildingBlocks.Set(DomainManager.Building, key, buildingData, context);
                    }
                }
            }
        }
    }

    [DisplayName("void_将字符串数据2保存到相对路径字符串1指向的文件之中")]
    public static void sl(string file, string content)=>System.IO.File.WriteAllText("../"+file,content);

    [DisplayName("string_读取相对路径字符串1指向的文件之中的内容")]
    public static string sl(string file)=>System.IO.File.ReadAllText("../"+file);

    public static readonly System.Text.Json.JsonSerializerOptions options = new (){IncludeFields = true, Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping};
    [DisplayName("string_导出太吾村数据")]
    public static string export_village(){
        var loc=DomainManager.Taiwu.GetTaiwuVillageLocation();
        var lst=DomainManager.Building.GetBuildingBlockList(loc);
        if(lst==null){return string.Empty;}
        return System.Text.Json.JsonSerializer.Serialize((new uint[]{2,1,1},DomainManager.Building.GetBuildingAreaData(loc),DomainManager.Building.GetBuildingBlockList(loc)), options);
    }

    [DisplayName("void_导入太吾村数据")]
    public static void import_village(string json){

        var loc=DomainManager.Taiwu.GetTaiwuVillageLocation();
        var lst=DomainManager.Building.GetBuildingBlockList(loc);

        if(lst.All(x=>x.TemplateId!=44)){
            logwarn($"敢问……您太吾村呢？");
            return;
        }
        var(modify,area,blocklist)=System.Text.Json.JsonSerializer.Deserialize<(uint[],GameData.Domains.Building.BuildingAreaData,GameData.Domains.Building.BuildingBlockData[])>(json,options);
        IEnumerable<GameData.Domains.Building.BuildingBlockData> ie=blocklist;
        if(lst.All(x=>x.TemplateId!=45)){
            ie=ie.Where(x=>x.TemplateId!=45);
        }
        modify[0]=modify[1]=modify[2]=0;//目前modify无效，因为不知道为什么前端只能接受256个buildingblockkey
        if(modify[0]+modify[1]+modify[2]!=0){
            ie=ie.Select(x=>{
                recalculate_block_index(ref x.BlockIndex,area.Width,modify);
                recalculate_block_index(ref x.RootBlockIndex,area.Width,modify);
                return x;
            });
        }
        foreach(var x in lst.Where(x=>x.TemplateId!=-1)){
            remove_building(new(loc.AreaId,loc.BlockId,x.BlockIndex));
        }
        // foreach(var x in DomainManager.Building.GetBuildingBlockList(loc)){
        //     RemoveElement_BuildingBlocks.Invoke(DomainManager.Building,new(loc.AreaId,loc.BlockId,x.BlockIndex),context);
        // }
        if(modify[0]>0){
            var from=(short)(DomainManager.Building.GetBuildingBlockList(loc).Count);
            area.Width+=(sbyte)modify[0];
            var to=area.Width*area.Width;
            SetElement_BuildingAreas.Invoke(DomainManager.Building,loc,area,context);
            for(;from<to;from++){
                AddElement_BuildingBlocks.Invoke(DomainManager.Building,new(loc.AreaId,loc.BlockId,from),new(from,0,-1,-1),context);
            }
        }
        var lsts=DomainManager.Building.GetBuildingBlockList(loc);
        // logwarn($"Count={lsts.Count}, lsts0={System.Text.Json.JsonSerializer.Serialize(lsts[0], options)}, lsts[{lsts.Count-1}]={System.Text.Json.JsonSerializer.Serialize(lsts[lsts.Count-1], options)}");
        EventHelper.SetWorldFunctionsStatus(10);
        foreach(var x in ie.Where(x=>x.TemplateId!=-1 && ((Config.BuildingBlock.Instance[x.TemplateId]?.MaxLevel??0)>0)).ToArray()){
            build_building(new(loc.AreaId,loc.BlockId,x.BlockIndex), x);
        }
    }
    class SetElement_BuildingAreas {
        public static readonly Action<GameData.Domains.Building.BuildingDomain,GameData.Domains.Map.Location,GameData.Domains.Building.BuildingAreaData,GameData.Common.DataContext> Invoke=Utils.FastExpr.Met<SetElement_BuildingAreas>.get(out Invoke);
    }
    class AddElement_BuildingBlocks {
        public static readonly Action<GameData.Domains.Building.BuildingDomain,GameData.Domains.Building.BuildingBlockKey,GameData.Domains.Building.BuildingBlockData,GameData.Common.DataContext> Invoke=Utils.FastExpr.Met<AddElement_BuildingBlocks>.get(out Invoke);
    }
    class RemoveElement_BuildingBlocks {
        public static readonly Action<GameData.Domains.Building.BuildingDomain,GameData.Domains.Building.BuildingBlockKey,GameData.Common.DataContext> Invoke=Utils.FastExpr.Met<RemoveElement_BuildingBlocks>.get(out Invoke);
    }
    public static void build_building(GameData.Domains.Building.BuildingBlockKey blockKey, GameData.Domains.Building.BuildingBlockData blockData){
        DomainManager.Building.GmCmd_BuildImmediately(context,blockData.TemplateId,blockKey,blockData.Level);
        SetElement_BuildingBlocks.Set(DomainManager.Building, blockKey, blockData, context);
    }
    [DisplayName("void_删除地格1")]
    public static void remove_building(GameData.Domains.Building.BuildingBlockKey blockKey){
        DomainManager.Building.GmCmd_RemoveBuildingImmediately(context,blockKey);
        // {for(int i=0;i<3;i++){DomainManager.Building.SetShopManager(context, blockKey, i,-1);}}
        // DomainManager.Building.ResetAllChildrenBlocks(context, blockKey, 0, -1);
        // DomainManager.Building.SetBuildingCustomName(context, blockKey, null);
        //
        // var autoExpandList = DomainManager.Extra.GetAutoExpandBlockIndexList();
        // if(autoExpandList.Remove(modification.BlockData.BlockIndex)){
        //     DomainManager.Extra.SetAutoExpandBlockIndexList(autoExpandList, context);
        // }
        //
        //
        // if (blockData.TemplateId == 46) {
        //     RemoveResidence(context, blockKey);
        // } else if (blockData.TemplateId == 47) {
        //     RemoveComfortableHouse(context, blockKey);
        // }
        // RemoveAllOperatorsInBuilding(context, blockKey);
        // RemoveAllManagersInBuilding(context, blockKey, blockData.TemplateId == 222);
        // ResetAllChildrenBlocks(context, blockKey, 23, 1);
        // blockData.ResetData(23, 1, -1);
    }
    static void recalculate_block_index(ref short old, int oldwidth, uint[] modify){
        if(old==-1){return;}
        var width=modify[0];
        var addx=modify[1];
        var addy=modify[2];
        int x=old/oldwidth;
        int y=old%oldwidth;
        old=(short)((x+addx)*width+(y+addy));
    }

    static class character_based_nest_expr {
        internal static class Wrapper {
            internal class SetElement_EnemyNestSites;
        }
        public static Action<GameData.Domains.Adventure.AdventureDomain, GameData.Domains.Map.Location, GameData.Domains.Adventure.EnemyNestSiteExtraData, GameData.Common.DataContext> SetElement_EnemyNestSites=Utils.FastExpr.Met<Wrapper.SetElement_EnemyNestSites>.get(out SetElement_EnemyNestSites);
    }
    [DisplayName("bool_人物1所在地格如果是巢穴，解锁巢穴升级潜能，并使巢穴过月升级")]
    public static bool character_based_nest(GameData.Domains.Character.Character character){
        var loc = character.GetLocation();
        if (loc.IsValid() && DomainManager.Adventure.TryGetElement_EnemyNestSites(loc,out var nestExtraData)){
            // return DomainManager.Adventure.TryUpgradeEnemyNest(context, loc.AreaId, loc.BlockId);
            nestExtraData.InitialNestTemplateId=(sbyte)GameData.Domains.Adventure.EnemyNestConstValues.HereticStrongholdNestIds[^1];
            nestExtraData.UpgradeChance=100;
            character_based_nest_expr.SetElement_EnemyNestSites(DomainManager.Adventure,loc,nestExtraData,context);
            return true;
        }
        return false;
    }

    [DisplayName("void_人物1所在地格如果是村镇，全资源等级设为参数2并开启等级为参数3的鼎力")]
    public static void character_based_ding(GameData.Domains.Character.Character character, sbyte building_lv, int ding_lv){
        var loc = character.GetLocation();
        if(loc.IsValid() && loc!=DomainManager.Taiwu.GetTaiwuVillageLocation()){
            character_based_ding_impl(EventHelper.GetSettlementByLocation(loc), building_lv, ding_lv);
        }
    }
    internal class _settlements;
    static Func<GameData.Domains.Organization.OrganizationDomain, Dictionary<short, GameData.Domains.Organization.Settlement>> GetSettlements=Utils.FastExpr.FP<_settlements>.field(out GetSettlements);
    static void character_based_ding_impl(GameData.Domains.Organization.Settlement settlement, sbyte building_lv, int ding_lv){
        if(settlement==null){return;}
        var loc=settlement.GetLocation();
        var settlementId = settlement.GetId();
        var modified=false;
        // var first=false;
        var lst=DomainManager.Building.GetBuildingBlockList(loc);
        if(lst==null){return;}
        if(!DomainManager.Extra.TryGetElement_SettlementExtraData(settlementId, out var data)){
            data=new();
            // first=true;
        }
        foreach(var buildingData in lst){
            if(buildingData!=null && Config.BuildingBlock.Instance[buildingData.TemplateId] != null && GameData.Domains.Building.BuildingBlockData.IsUsefulResource(Config.BuildingBlock.Instance[buildingData.TemplateId].Type)){
                var key=new GameData.Domains.Building.BuildingBlockKey(loc.AreaId, loc.BlockId, buildingData.BlockIndex);
                buildingData.Level=Math.Max(buildingData.Level, building_lv);
                SetElement_BuildingBlocks.Set(DomainManager.Building, key, buildingData, context);
                data.SupportedBlocks[key]=ding_lv;
                modified=true;
            }
        }
        // if(first){
        // data.DeactivateSupportedBlocks.Clear(); // 系统帮你ban的
        data.DisabledSupportedBlocks.Clear();  // 你自己ban的
        // }
        if(modified){
            DomainManager.Extra.SetSettlementExtraData(context, settlementId, data);
        }
    }
    [DisplayName("void_向人物1添加种类2id3的物品，数量为4")]
    public static void AddItemToRole(GameData.Domains.Character.Character character, sbyte ty, short id, int cnt){
        EventHelper.AddItemToRole(character,ty,id,cnt,-1);
    }
    [DisplayName("void_向人物1添加种名称为id2的杂物，数量为3")]
    public static void AddItemToRole(GameData.Domains.Character.Character character, string id, int cnt){
        if(Parser.Name2Id(id,Config.Misc.Instance, out short type)){
            EventHelper.AddItemToRole(character,12,type,cnt,-1);
        } else {
            logwarn($"不存在名为{id}的物品");
        }
    }
    [DisplayName("void_令蛟池1中蛟几种属性极大化")]
    public static void JiaoMax(int pool){
        if(DomainManager.Extra.TryGetJiaoFromPool(pool,out var jiao)){
            jiao.TamePoint=100;
            jiao.Behavior=DomainManager.Taiwu.GetTaiwu().GetBehaviorType();
            jiao.Properties.TravelTimeReduction=(Math.Max(1000000,jiao.Properties.TravelTimeReduction.Item1),Math.Max(1000000,jiao.Properties.TravelTimeReduction.Item2));
            jiao.Properties.MaxInventoryLoadBonus=(Math.Max(1000000,jiao.Properties.MaxInventoryLoadBonus.Item1),Math.Max(1000000,jiao.Properties.MaxInventoryLoadBonus.Item2));
            jiao.Properties.DropRateBonus=(Math.Max(1000000,jiao.Properties.DropRateBonus.Item1),Math.Max(1000000,jiao.Properties.DropRateBonus.Item2));
            jiao.Properties.CaptureRateBonus=(Math.Max(1000000,jiao.Properties.CaptureRateBonus.Item1),Math.Max(1000000,jiao.Properties.CaptureRateBonus.Item2));
            jiao.Properties.MaxKidnapSlotAbilityBonus=(Math.Max(1000000,jiao.Properties.MaxKidnapSlotAbilityBonus.Item1),Math.Max(1000000,jiao.Properties.MaxKidnapSlotAbilityBonus.Item2));
            jiao.Properties.Value=(Math.Max(1000000,jiao.Properties.Value.Item1),Math.Max(1000000,jiao.Properties.Value.Item2));
            jiao.Properties.Price=(Math.Max(1000000,jiao.Properties.Price.Item1),Math.Max(1000000,jiao.Properties.Price.Item2));
            jiao.Properties.HappinessChange=(Math.Max(1000000,jiao.Properties.HappinessChange.Item1),Math.Max(1000000,jiao.Properties.HappinessChange.Item2));
            jiao.Properties.FavorabilityChange=(Math.Max(1000000,jiao.Properties.FavorabilityChange.Item1),Math.Max(1000000,jiao.Properties.FavorabilityChange.Item2));
            jiao.LifeSpan=(Math.Max(30000,jiao.LifeSpan.Item1),Math.Max(30000,jiao.LifeSpan.Item2));
        }
    }
    [DisplayName("string_养龙说明")]
    public static string Jiaotest(){
        var IsMainStoryLineProgressMeetLoongDlc1=DomainManager.World.GetMainStoryLineProgress() >= 21;
        var IsMainStoryLineProgressMeetLoongDlc2=DomainManager.World.GetMainStoryLineProgress() < 27;

        var taiwuVillageLocation = DomainManager.Taiwu.GetTaiwuVillageLocation();
        var taiwuBuildingAreaData = DomainManager.Building.GetBuildingAreaData(taiwuVillageLocation);
        var buildingBlockData = GameData.Domains.Building.BuildingDomain.FindBuilding(taiwuVillageLocation, taiwuBuildingAreaData, 44);
        var IsTaiwuVillageLevelMeetFiveLoongDlc1=buildingBlockData!=null;
        var IsTaiwuVillageLevelMeetFiveLoongDlc2=IsTaiwuVillageLevelMeetFiveLoongDlc1 && buildingBlockData.Level >= 6;

        var loongDlcArgBox = DomainManager.Extra.GetOrCreateDlcArgBox(2764950uL, context);
        bool fiveLoongDlcBeginMonthlyEventTriggered = false;
        var boxget=loongDlcArgBox.Get("ConchShip_PresetKey_FiveLoongDlcBeginMonthlyEventTriggered", ref fiveLoongDlcBeginMonthlyEventTriggered);
        var trigger=fiveLoongDlcBeginMonthlyEventTriggered;
        return $"IsMainStoryLineProgressMeetLoongDlc: {IsMainStoryLineProgressMeetLoongDlc1} && {IsMainStoryLineProgressMeetLoongDlc2}\nIsTaiwuVillageLevelMeetFiveLoongDlc: {IsTaiwuVillageLevelMeetFiveLoongDlc1} && {IsTaiwuVillageLevelMeetFiveLoongDlc2}\n main:!({boxget}&&{trigger})";
    }
    [DisplayName("bool_可以触发养龙过月事件")]
    public static bool JiaoJudge(){
        var taiwuVillageLocation = DomainManager.Taiwu.GetTaiwuVillageLocation();
        var taiwuBuildingAreaData = DomainManager.Building.GetBuildingAreaData(taiwuVillageLocation);
        var buildingBlockData = GameData.Domains.Building.BuildingDomain.FindBuilding(taiwuVillageLocation, taiwuBuildingAreaData, 44);
        var IsTaiwuVillageLevelMeetFiveLoongDlc1=buildingBlockData!=null;
        var IsTaiwuVillageLevelMeetFiveLoongDlc2=IsTaiwuVillageLevelMeetFiveLoongDlc1 && buildingBlockData.Level >= 6;

        var loongDlcArgBox = DomainManager.Extra.GetOrCreateDlcArgBox(2764950uL, context);
        bool fiveLoongDlcBeginMonthlyEventTriggered = false;
        var boxget=loongDlcArgBox.Get("ConchShip_PresetKey_FiveLoongDlcBeginMonthlyEventTriggered", ref fiveLoongDlcBeginMonthlyEventTriggered);
        var trigger=fiveLoongDlcBeginMonthlyEventTriggered;
        return IsTaiwuVillageLevelMeetFiveLoongDlc1 && IsTaiwuVillageLevelMeetFiveLoongDlc2 && !(boxget && trigger);
    }
    [DisplayName("string_跳转到养龙过月事件")]
    public static string JiaoTrigger(){
            // return "05103440-477d-4eba-aeea-0c1ef3decc45";
        return "583308f4-2ade-4ec5-831e-dcfca90d4e98";
    }

    [DisplayName("void_人物1周围参数2格变成地形3")]
    public static void TreeChangeMapBlock(GameData.Domains.Character.Character character,sbyte range,short templateId){
        var loc=character.GetLocation();
        if(!loc.IsValid()){return;}
        var areaId=loc.AreaId;
        var blockId=loc.BlockId;
        var mbdata=new List<GameData.Domains.Map.MapBlockData>();
        DomainManager.Map.GetNeighborBlocks(areaId, blockId, mbdata, range);
        mbdata.Add(DomainManager.Map.GetBlock(areaId, blockId));
        foreach(var mbd in mbdata){
            mbd.convert_to(context, templateId);
            mbd.maximize_resource(context);
        }
    }

    [DisplayName("void_人物1周围参数2格变成人物所在格地形")]
    public static void TreeChangeMapBlock2(GameData.Domains.Character.Character character,sbyte range){
        var loc=character.GetLocation();
        if(!loc.IsValid()){
            return;
        }
        var areaId=loc.AreaId;
        var blockId=loc.BlockId;
        var mbdata=new List<GameData.Domains.Map.MapBlockData>();
        DomainManager.Map.GetNeighborBlocks(areaId, blockId, mbdata, range);
        var templateId = DomainManager.Map.GetBlock(areaId, blockId).TemplateId;
        foreach(var mbd in mbdata){
            mbd.convert_to(context, templateId);
            mbd.maximize_resource(context);
        }
    }
    [DisplayName("void_母亲1与父亲2生3个资质等级为4六道轮回为5性别为6的孩子")]
    public static IEnumerable<int> 母亲1与父亲2生3个资质等级为4六道轮回为5性别为6的孩子(GameData.Domains.Character.Character mother,GameData.Domains.Character.Character father, int count, sbyte grade, int destinyType, sbyte gender) => bornchild_门派模板为太吾村__如果母亲已怀孕_以父亲2为接盘侠_孩子的真实父亲由母亲怀孕状态决定(mother,father, count, grade, destinyType, gender);
    [DisplayName("Int_Array_母亲1与父亲2生3个资质等级为4六道轮回为5性别为6的孩子")]
    // 门派模板为太吾村
    public static int[] bornchild_门派模板为太吾村__如果母亲已怀孕_以父亲2为接盘侠_孩子的真实父亲由母亲怀孕状态决定(GameData.Domains.Character.Character mother,GameData.Domains.Character.Character father, int count, sbyte grade, int destinyType, sbyte gender){
        var newInfo=mother.GetOrganizationInfo();
        newInfo.Grade=0;
        var lst=new List<int>{};

        var waitingReincarnationChars = new HashSet<int>();
        DomainManager.Character.GetAllWaitingReincarnationCharIds(waitingReincarnationChars);
        var lst2=waitingReincarnationChars.Where(ch=>DomainManager.Character.TryGetDeadCharacter(ch,out var _)).OrderBy(ch=>-DomainManager.Character.GetDeadCharacter(ch).BaseLifeSkillQualifications.GetSum()).ToList();
        // logwarn($"lst2 have length {lst2.Count}");
        var create_relation=(false,false);
        var afather=father;
        if(DomainManager.Character.TryGetPregnantState(mother.GetId(), out var ps)){
            create_relation=(ps.CreateFatherRelation,ps.CreateMotherRelation);
            if(!DomainManager.Character.TryGetElement_Objects(ps.FatherId, out afather)){
                afather=father;
            }
        }
        for(;count>0;){
            if(ps is not null){
                (ps.CreateFatherRelation,ps.CreateMotherRelation)=(false,false);
            }
            var info=new GameData.Domains.Character.Creation.IntelligentCharacterCreationInfo(mother.GetLocation(), newInfo, 10); // 10=太吾传人，按村民模板进行生成

            count--;
            if(count<lst2.Count){
                info.ReincarnationCharId=lst2[count];
            }
            info.PregnantState = ps;
            info.GrowingSectId = 16;
            info.GrowingSectGrade = grade; // grade可以高于神一品，但似乎并无卵用……
            info.InitializeSectSkills = false;
            info.Gender=gender;
            info.Age=0;
            info.BirthMonth=DomainManager.World.GetCurrMonthInYear();
            info.Transgender=false;
            info.CombatSkillQualificationGrowthType=2;
            info.LifeSkillQualificationGrowthType=2;
            info.MotherCharId = mother.GetId();
            info.Mother = mother;
            info.FatherCharId = father.GetId();
            info.ActualFatherCharId = afather.GetId();
            info.Father = father;
            info.ActualFather = afather;
            info.DestinyType = destinyType;
            {
                var target=mother.GetId()==DomainManager.Taiwu.GetTaiwuCharId()?mother:father;
                var rfn=info.ReferenceFullName;
                rfn.CustomSurnameId=target.GetFullName().CustomSurnameId;
                rfn.Type=target.GetFullName().Type;
                info.ReferenceFullName=rfn;
            }
            // var character = DomainManager.Character.CreateIntelligentCharacter(context, ref info);
            var character= DomainManager.Character.ComplementCreateIntelligentCharacter(context, GameData.Domains.Character.CharacterDomain.ParallelCreateIntelligentCharacter(context, ref info, recordModification: false), autoComplement: false);

            int charId = character.GetId();
            int birthDate = character.GetBirthDate();
            if (GameData.Domains.Character.Relation.RelationType.AllowAddingBloodParentRelation(charId, father.GetId()) && GameData.Domains.Character.Relation.RelationType.AllowAddingBloodParentRelation(charId, mother.GetId())) {
                DomainManager.Character.AddBloodParentRelations(context, charId, father.GetId(), birthDate);
                DomainManager.Character.AddBloodParentRelations(context, charId, mother.GetId(), birthDate);
            } else {
                if (GameData.Domains.Character.Relation.RelationType.AllowAddingBloodParentRelation(charId, father.GetId())) {
                    DomainManager.Character.AddBloodParentRelations(context, charId, father.GetId(), birthDate);
                } else if (GameData.Domains.Character.Relation.RelationType.AllowAddingBloodParentRelation(charId, mother.GetId())) {
                    DomainManager.Character.AddBloodParentRelations(context, charId, mother.GetId(), birthDate);
                }
            }
            DomainManager.Character.CompleteCreatingCharacter(charId);
            character.SetIdealSect(-1,context);
            DomainManager.Taiwu.JoinGroup(context, charId);
            lst.Add(charId);
            // var fn=character.GetFullName();
            // fn.CustomSurnameId=mother.GetFullName().CustomSurnameId;
            // character.SetFullName(fn,context);
        }
        if(ps!=null){
            (ps.CreateFatherRelation,ps.CreateMotherRelation)=create_relation;
        }
        EventHelper.ShowGetItemPageForCharacters(lst,isVillager: false);
        return lst.ToArray();
    }
    [DisplayName("int_默认剑冢侵袭延长时间")]
    public static int QinXiExtendTime()=>DomainManager.Adventure.GetSwordTombAdventureMaxMonthCount()>0?DomainManager.Adventure.GetSwordTombAdventureMaxMonthCount():360;
    [DisplayName("bool_存在可延长侵袭时间剑冢")]
    public static bool ExtendQinXi()=>DomainManager.Adventure.GetAdventuresInArea(GameData.Domains.TaiwuEvent.EventArgBox.TaiwuVillageAreaId).AdventureSites.Values.Any(x=>x.RemainingMonths>0);
    [DisplayName("void_延长剑冢侵袭时间1月")]
    public static void ExtendQinXi(int month){
        var dom=DomainManager.Adventure;
        foreach(var kv in dom.GetAdventuresInArea(GameData.Domains.TaiwuEvent.EventArgBox.TaiwuVillageAreaId).AdventureSites){
            if(kv.Value.RemainingMonths>0){
                dom.SetAdventureRemainingMonths(context, GameData.Domains.TaiwuEvent.EventArgBox.TaiwuVillageAreaId, kv.Key, (short)Math.Clamp(kv.Value.RemainingMonths+month,0,32767));
            }
        }
    }
    [DisplayName("使人物1灵魂携带人物特性离体")]
    public static void ExtendQinXi(GameData.Domains.Character.Character self){
        var newInfo=self.GetOrganizationInfo();
        newInfo.Grade=0;
        var lst=new List<int>{};

        var info=new GameData.Domains.Character.Creation.IntelligentCharacterCreationInfo(self.GetLocation(), newInfo, 10); // 10=太吾传人，按村民模板进行生成
        info.ReincarnationCharId=-2;
        info.PregnantState = null;
        info.GrowingSectId = newInfo.OrgTemplateId;
        info.GrowingSectGrade = 0; // grade可以高于神一品，但似乎并无卵用……
        info.InitializeSectSkills = false;
        info.Gender=self.GetGender();
        info.Age=self.GetCurrAge();
        info.BirthMonth=self.GetBirthMonth();
        info.Transgender=false;
        info.CombatSkillQualificationGrowthType=2;
        info.LifeSkillQualificationGrowthType=2;
        info.MotherCharId = -1;
        info.Mother = null;
        info.FatherCharId = -1;
        info.ActualFatherCharId = -1;
        info.Father = null;
        info.ActualFather = null;
        info.DestinyType = -1;
        info.ReferenceFullName=self.GetFullName();
        var character= DomainManager.Character.ComplementCreateIntelligentCharacter(context, GameData.Domains.Character.CharacterDomain.ParallelCreateIntelligentCharacter(context, ref info, recordModification: false), autoComplement: false);


        character.consummateLevel(self.consummateLevel(),context);
        character.gender(self.gender(),context);
        character.transgender(self.transgender(),context);
        character.bisexual(self.bisexual(),context);
        character.haveLeftArm(self.haveLeftArm(),context);
        character.haveRightArm(self.haveRightArm(),context);
        character.haveLeftLeg(self.haveLeftLeg(),context);
        character.haveRightLeg(self.haveRightLeg(),context);
        character.happiness(self.happiness(),context);
        character.birthMonth(self.birthMonth(),context);
        character.actualAge(self.actualAge(),context);
        character.currAge(self.currAge(),context);
        character.baseMorality(self.baseMorality(),context);
        character.idealSect(self.idealSect(),context);
        character.lifeSkillTypeInterest(self.lifeSkillTypeInterest(),context);
        character.combatSkillTypeInterest(self.combatSkillTypeInterest(),context);
        character.mainAttributeInterest(self.mainAttributeInterest(),context);
        character.lifeSkillQualificationGrowthType(self.lifeSkillQualificationGrowthType(),context);
        character.combatSkillQualificationGrowthType(self.combatSkillQualificationGrowthType(),context);
        character.xiangshuInfection(self.xiangshuInfection(),context);
        character.health(self.health(),context);
        character.baseMaxHealth(self.baseMaxHealth(),context);
        character.disorderOfQi(self.disorderOfQi(),context);
        character.preexistenceCharIds(self.preexistenceCharIds(),context);
        character.baseMainAttributes(self.baseMainAttributes(),context);
        character.baseLifeSkillQualifications(self.baseLifeSkillQualifications(),context);
        character.baseCombatSkillQualifications(self.baseCombatSkillQualifications(),context);
        character.currMainAttributes(self.currMainAttributes(),context);

        self.preexistenceCharIds(new(),context);
        int charId = character.GetId();

        DomainManager.Character.CompleteCreatingCharacter(charId);

        DomainManager.Character.MakeCharacterDead(context, character, 0);
        if(DomainManager.Character.TryGetElement_Graves(charId, out var grave)){
            DomainManager.Character.RemoveGrave(context, grave);
        }

    }

    [DisplayName("void_使用字符串1设定太吾峨嵋黄点")]
    public static void 峨嵋黄点(string line) {
        // 目前使用的格式：
        // 技能:第一黄点,第二黄点,第三黄点;
        // with open('CombatSkill[auto].csv') as f:f=[f for f in [(f[0],[(f['TrickType'],f['NeedCount']-1) for f in eval(f[1].replace("，",",")) if f['NeedCount']>1]) for f in [f.split(',')[1:54:52] for f in f.read().split('\n')][1:-1]] if f[1]!=[]]
        // for f in f:print(f"{f[0]}:"+",".join(f for f in [[['掷','弹','御','劈','刺','撩','崩','点','拿','音','缠','咒','机','药','毒','扫','撞','抓','噬','杀','无','神'][f[0]]]*f[1] for f in f[1]] for f in f))
        // 一行却一切式：print(";".join(f[0]+":"+",".join('却'+f for f in [[['掷','弹','御','劈','刺','撩','崩','点','拿','音','缠','咒','机','药','毒','扫','撞','抓',' 噬','杀','无','神'][f[0]]]*f[1] for f in f[1]] for f in f) for f in [f for f in [(f[0],[(f['TrickType'],f['NeedCount']-1) for f in eval(f[1].replace("，",",")) if f['NeedCount']>1]) for f in [f.split(',')[1:54:52] for f in open('CombatSkill[auto].csv').read().split('\n')][1:-1]] if f[1]!=[]]))
        Dictionary<short,List<short>> YaN02=new();
        int cntr=0;
        foreach(var type in line.Split(';')){
            cntr++;
            var s=type.Split(':');
            if(s.Length<2){
                logwarn($"峨嵋黄点 第{cntr}段（{type}）不正确，已忽略");
                continue;
            }
            Parser.Name2Id(s[0],Config.CombatSkill.Instance, out short skill);
            if(skill<0){
                logwarn($"峨嵋黄点 第{cntr}段（{type}）的{s[0]}并不代表任何功法，已忽略");
                continue;
            }
            GameData.Domains.DomainManager.Extra.ClearEmeiSkillBreakBonus(context, skill);
            var lst=new List<short>();
            foreach(var grid in s[1].Split(',')){
                Parser.Name2Id(grid,Config.SkillBreakPlateGridBonusType.Instance, out short x);
                if(x<0){
                    logwarn($"峨嵋黄点 第{cntr}段（{type}）的{s[1]}部分中，{grid}并不代表任何突破格，已忽略");
                } else if(!GameData.Domains.DomainManager.Extra.AddEmeiSkillBreakBonusWithoutCost(context, skill, x)){
                    break;
                }
            }
        }
    }

    [DisplayName("void_人物1增加持续时间为2的个人需求3")] // # ch ?. Character.AddPersonalNeed #0 need
    public static void AddPNeed(GameData.Domains.Character.Character ch, sbyte dur, GameData.Domains.Character.Ai.PersonalNeed need){
        var lst = ch.GetPersonalNeeds();
        if(dur>0){need.RemainingMonths = dur;}
        lst.Add(need);
        ch.SetPersonalNeeds(lst, context);
    }
    public static sbyte DefKey_MakeLove=((sbyte?)typeof(Config.PersonalNeed.DefKey).GetField("MakeLove",(BindingFlags)(-1))?.GetValue(null))??Config.PersonalNeed.DefKey.MakeLove;
    [DisplayName("PersonalNeed_与人物1春宵")]
    public static GameData.Domains.Character.Ai.PersonalNeed NeedChunXiao(GameData.Domains.Character.Character ch){
        return GameData.Domains.Character.Ai.PersonalNeed.CreatePersonalNeed(DefKey_MakeLove, ch.GetId());
    }
    public static sbyte DefKey_GetRevenge=((sbyte?)typeof(Config.PersonalNeed.DefKey).GetField("GetRevenge",(BindingFlags)(-1))?.GetValue(null))??Config.PersonalNeed.DefKey.GetRevenge;
    [DisplayName("PersonalNeed_向人物1复仇")]
    public static GameData.Domains.Character.Ai.PersonalNeed NeedRevenge(GameData.Domains.Character.Character ch){
        return GameData.Domains.Character.Ai.PersonalNeed.CreatePersonalNeed(DefKey_GetRevenge, ch.GetId());
    }
    // 关系表
    // 父母	1 4 7
    // 结义	10
    // 夫妻	11
    // 子女	2 5 8
    // 派系
    // 仇敌	16
    // 朋友	14
    // 爱慕	15
    // 师承	12
    // 手足	3 6 9
    [DisplayName("PersonalNeed_随机结成关系1")]
    public static GameData.Domains.Character.Ai.PersonalNeed  NeedMakeRelation(ushort rel){
        return GameData.Domains.Character.Ai.PersonalNeed.CreatePersonalNeed(25, (ushort)(1<<rel));
    }
}
