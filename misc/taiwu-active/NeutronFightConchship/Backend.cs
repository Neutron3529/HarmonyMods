// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/System.Linq.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Console.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Runtime.dll" -unsafe -optimize -deterministic -debug Backend.cs ../UTILS/*.CS -out:Backend.dll
// -r:"System.Speech.dll"
// -define:DEBUG_TREESEED
// -r:"../../Backend/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;
using static Utils.Logger;

namespace NeutronFightsConchship;
[TaiwuModdingLib.Core.Plugin.PluginConfig("NeutronFightsConchship","Neutron3529","0.1.0")]
public partial class NeutronFightsConchship : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Initialize()=>this.HarmonyInstance = new RobustHarmonyInstance(this.GetGuid());
    public override void Dispose()=>this.HarmonyInstance.UnpatchSelf();
    public RobustHarmonyInstance HarmonyInstance;
    private bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logwarn("enable 出错，出错键值为\""+key+"\"，错误原因是"+ex.Message);
            return false;
        }
    }
    private bool _enable(string key){
        bool enabled=false;
        GameData.Domains.DomainManager.Mod.GetSetting(this.ModIdStr, key, ref enabled);
        return enabled;
    }
    public override void OnModSettingUpdate(){
        this.HarmonyInstance.UnpatchSelf();
        // if(enable("LP"))this.HarmonyInstance.PatchAll(typeof(ModDomainLoadAllEventPackages));
        if(enable("RY"))this.HarmonyInstance.PatchAll(typeof(TaiwuEventDomainGetEventDisplayData));
        if(enable("SS"))this.HarmonyInstance.PatchAll(typeof(StripSave));
        if(enable("BH"))this.HarmonyInstance.PatchAll(typeof(BaihuaProgress));
        // this.HarmonyInstance.PatchAll(typeof(SendDataFaster));
        // this.HarmonyInstance.PatchAll(typeof(WTF));
        #if DEBUG_AMO // move to AdvMonthOptim
            this.HarmonyInstance.PatchAll(typeof(Updates));
            this.HarmonyInstance.PatchAll(typeof(FAST));
        #endif
        #if DEBUG_TREESEED
            this.HarmonyInstance.PatchAll(typeof(TREESEED));
        #endif
        // this.HarmonyInstance.PatchAll(typeof(ToDisplayData));
    }
    // public static class ToDisplayData {
    //     static SpeechSynthesizer synthesizer = new SpeechSynthesizer();
    //     [HarmonyPostfix, HarmonyPatch(typeof(GameData.Domains.TaiwuEvent.TaiwuEvent),"ToDisplayData")]
    //     public static GameData.Domains.TaiwuEvent.DisplayEvent.TaiwuEventDisplayData Pfx1(GameData.Domains.TaiwuEvent.DisplayEvent.TaiwuEventDisplayData ret) {
    //         logwarn($"reading {ret.EventContent}");
    //         try {
    //             synthesizer.SetOutputToDefaultAudioDevice();
    //             synthesizer.Speak("All we need to do is to make sure we keep talking. 123. 中文. 456.");
    //         } catch (Exception ex) {
    //             logwarn($"cannot read", ex);
    //         }
    //         return ret;
    //     }
    // }
    #if DEBUG_TREESEED
    public static class TREESEED {
        [HarmonyPrefix, HarmonyPatch(typeof(GameData.Domains.TaiwuEvent.TaiwuEventDomain),"TaiwuCollectWudangHeavenlyTreeSeed")]
        public static void Pfx1(sbyte resourceType) {
            logwarn($"TaiwuCollectWudangHeavenlyTreeSeed called with resourceType {resourceType}.");
        }
        [HarmonyPrefix, HarmonyPatch(typeof(GameData.Domains.TaiwuEvent.EventManager.GlobalCommonEventManager),"OnEventTrigger_TaiwuCollectWudangHeavenlyTreeSeed")]
        public static void Pfx2(sbyte arg0, List<GameData.Domains.TaiwuEvent.TaiwuEvent> ____headEventList) {
            logwarn($"OnEventTrigger_TaiwuCollectWudangHeavenlyTreeSeed called with resourceType {arg0}.");
            var cnt=0;
            foreach(var taiwuEvent in ____headEventList) {
                if(taiwuEvent.EventConfig.TriggerType == GameData.Domains.TaiwuEvent.EventTrigger.TaiwuCollectWudangHeavenlyTreeSeed) {
                    logwarn($"found event with guid {taiwuEvent.EventConfig.Guid}");
                    cnt+=1;

                    var argBox = GameData.Domains.DomainManager.TaiwuEvent.GetEventArgBox();
                    argBox.Set("ResourceType", arg0);
                    taiwuEvent.ArgBox = argBox;
                    logwarn($"condition = {taiwuEvent.EventConfig.CheckCondition()}, Script = {GameData.Domains.DomainManager.TaiwuEvent.ScriptRuntime.CheckConditionList(taiwuEvent.EventConfig.Conditions, argBox)}, OnCheckEventCondition = {taiwuEvent.EventConfig.OnCheckEventCondition()}");
                    logwarn($"{(GameData.Domains.TaiwuEvent.EventHelper.EventHelper.IsProfessionalSkillUnlocked(2) && !GameData.Domains.TaiwuEvent.EventHelper.EventHelper.IsOneShotEventHandled(4))} || {(GameData.Domains.TaiwuEvent.EventHelper.EventHelper.IsProfessionalSkillUnlocked(1) && !GameData.Domains.TaiwuEvent.EventHelper.EventHelper.IsOneShotEventHandled(3))}");
                    taiwuEvent.ArgBox = null;
                    GameData.Domains.DomainManager.TaiwuEvent.ReturnArgBox(argBox);
                }
            }
            if(cnt == 0) {
                logwarn("found nothing");
            }
        }
    }
    #endif
    // [HarmonyPatch(typeof(GameData.GameDataBridge.GameDataBridge),"AdvanceFrame")]
    // public static class SendDataFaster {
    //     public static void Prefix(ref long ____averageSleepInterval) {
    //         ____averageSleepInterval = 100000000L;
    //     }
    // }
    // [HarmonyPatch(typeof(GameData.Domains.LifeRecord.LifeRecordDomain),"GetRecordRenderInfoArguments")]
    // public static class WTF {
    //     public static void Prefix(GameData.Domains.LifeRecord.GeneralRecord.RecordArgumentsRequest request){
    //         logwarn($"\nchars: {string.Join(",", request.Characters.Select(x=>x.ToString()))}; \nlocations:{string.Join(",", request.Locations.Select(x=>x.ToString()))}");
    //     }
    // }
    // [HarmonyPatch(typeof(GameData.GameDataBridge.DataMonitorManager),"CheckMonitoredData")]
    // public static class Updates {
    //     // static System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
    //     // static IEnumerable<MethodInfo> TargetMethods(){
    //     //     // foreach(var x in typeof(UI_Combat).GetMethods((BindingFlags)(-1))) {
    //     //     //     if (x.Name.ToLower().Contains("update")){
    //     //     //         yield return x;
    //     //     //     }
    //     //     // }
    //     //     yield return typeof(GameData.GameDataBridge.GameDataBridge).GetMethod("ProcessOperations", (BindingFlags)(-1));
    //     //     yield return typeof(GameData.Common.ParallelModificationsRecorder).GetMethod("ApplyAll", (BindingFlags)(-1));
    //     //     yield return typeof(GameData.GameDataBridge.DataMonitorManager).GetMethod("CheckMonitoredData", (BindingFlags)(-1));
    //     //     yield return typeof(GameData.GameDataBridge.GameDataBridge).GetMethod("TransferPendingNotifications", (BindingFlags)(-1));
    //     //     yield return typeof(GameData.DomainEvents.Events).GetMethod("RaiseBeforeSendRequestToArchiveModule", (BindingFlags)(-1));
    //     //     yield return typeof(GameData.ArchiveData.ArchiveDataManager).GetMethod("SendRequest", (BindingFlags)(-1));
    //     //     // foreach(var x in typeof(GameData.GameDataBridge.GameDataBridge).Assembly.GetTypes()) {
    //     //     //     if (x.Name.ToLower().Contains("domain") && x.GetMethod("OnUpdate",(BindingFlags)(-1)) is var y and not null){
    //     //     //         yield return y;
    //     //     //     }
    //     //     // }
    //     // }
    //     public static void Prefix(
    //         HashSet<GameData.Common.DataUid> ____monitoredData,
    //         HashSet<GameData.Common.DataUid> ____initialMonitoring,
    //         HashSet<GameData.Common.DataUid> ____dataWithPostModificationHandlers,
    //         List<GameData.Common.DataUid> ____handledData,
    //         HashSet<GameData.Common.DataUid> ____dataWithAllHandlersRemoved,
    //         HashSet<GameData.Common.DataUid> ____dataWithInitialHandler,
    //         out System.Diagnostics.Stopwatch __state
    //     ){
    //         logger($"Counts:{____monitoredData.Count} {____initialMonitoring.Count} {____dataWithPostModificationHandlers.Count} {____handledData.Count} {____dataWithAllHandlersRemoved.Count} {____dataWithInitialHandler.Count}");
    //         __state = System.Diagnostics.Stopwatch.StartNew();
    //         // sw.Stop();
    //         // if (sw.ElapsedMilliseconds<50) {
    //         //     logger($"{__originalMethod.DeclaringType}::{__originalMethod} ({__instance?.GetType()??typeof(void)}) called after {sw.ElapsedMilliseconds}ms");
    //         // } else {
    //         //     logwarn($"{__originalMethod.DeclaringType}::{__originalMethod} ({__instance?.GetType()??typeof(void)}) called after {sw.ElapsedMilliseconds}ms");
    //         // }
    //         // sw.Restart();
    //     }
    //     public static void Postfix(object __instance, MethodBase __originalMethod, System.Diagnostics.Stopwatch __state){
    //         __state.Stop();
    //         if (__state.ElapsedMilliseconds<50) {
    //             logger($"{__originalMethod.DeclaringType}::{__originalMethod} ({__instance?.GetType()??typeof(void)}) cost {__state.ElapsedMilliseconds}ms");
    //         } else {
    //             logwarn($"{__originalMethod.DeclaringType}::{__originalMethod} ({__instance?.GetType()??typeof(void)}) cost {__state.ElapsedMilliseconds}ms");
    //         }
    //     }
    // }
    #if DEBUG_AMO
    [HarmonyPatch(typeof(GameData.Domains.Character.Character), "GetCombatSkillCanAffect")]
    public static class FAST {
        public static bool Prefix(ref bool __result) {
            __result = true;
            return false;
        }
    }
    public static class Updates {
        static IEnumerable<MethodInfo> TargetMethods(){
            // yield return typeof(GameData.Domains.Character.Character).GetMethod("GetPropertyBonusOfEquipments", (BindingFlags)(-1));
            // yield return typeof(GameData.Domains.Character.EatingItems).GetMethod("GetCharacterPropertyBonus", (BindingFlags)(-1));
            yield return typeof(GameData.Domains.Character.Character).GetMethod("GetPropertyBonusOfCombatSkillEquippingAndBreakout", (BindingFlags)(-1));
        }
        public static void Prefix(ref System.Diagnostics.Stopwatch __state){
            __state = System.Diagnostics.Stopwatch.StartNew();
        }
        public static void Postfix(GameData.Domains.Character.Character __instance, MethodBase __originalMethod, System.Diagnostics.Stopwatch __state){
            __state.Stop();
            if (__state.ElapsedMilliseconds>0) {
                logwarn($"char {__instance.GetId()} cost {__state.ElapsedMilliseconds}ms");
            }
        }
    }
    #endif
    [HarmonyPatch(typeof(GameData.Domains.World.WorldDomain),"PostAdvanceMonth")]
    public static class StripSave {
        static FieldInfo _objects=typeof(GameData.Domains.Character.CharacterDomain).GetField("_objects",(BindingFlags)(-1)); // Dictionary<int, GameData.Domains.Character.Character>
        static FieldInfo _relatedCharIds=typeof(GameData.Domains.Character.CharacterDomain).GetField("_relatedCharIds",(BindingFlags)(-1)); // Dictionary<int, GameData.Domains.Character.Relation.RelatedCharacters>
        public static void Postfix(GameData.Common.DataContext context){
            var sw=System.Diagnostics.Stopwatch.StartNew();
            var alive=new HashSet<int>(((Dictionary<int, GameData.Domains.Character.Character>)(_objects.GetValue(GameData.Domains.DomainManager.Character))).Keys);
            var relatedCharIds=(Dictionary<int, GameData.Domains.Character.Relation.RelatedCharacters>)(_relatedCharIds.GetValue(GameData.Domains.DomainManager.Character));
            var deadKeys=(new HashSet<int>(relatedCharIds.Keys));
            deadKeys.ExceptWith(alive);
            sw.Stop();
            logger($"alive.Count={alive.Count} deadKeys.Count={deadKeys.Count} sw={sw.Elapsed.TotalMilliseconds}");
            sw.Restart();
            var hs=new HashSet<int>();
            var counter=0;
            foreach(var x in deadKeys){
                hs.Clear();
                relatedCharIds[x].GetAllRelatedCharIds(hs, true);
                hs.IntersectWith(deadKeys);
                foreach(var y in hs){
                    counter++;
                    GameData.Domains.DomainManager.Character.RemoveRelation(context, x, y);
                }
            }
            sw.Stop();
            logger($"counter={counter} sw={sw.Elapsed.TotalMilliseconds}");
        }
    }
    // [HarmonyPatch(typeof(GameData.Domains.Mod.ModDomain),"LoadAllEventPackages")]
    // public static class ModDomainLoadAllEventPackages {
    //     public static bool Prefix(List<GameData.Domains.Mod.ModId>___LoadedMods, Dictionary<string, GameData.Domains.Mod.ModInfo> ___ModInfoDict){
    //         foreach (var modId in ___LoadedMods) {
    //             var modInfo = ___ModInfoDict[modId.ToString()];
    //             string eventsDir = Path.Combine(modInfo.DirectoryName, "Events");
    //             foreach (string eventPackageName in modInfo.EventPackages) {
    //                 logger(" - Loading events from " + eventPackageName);
    //                 string packageDir = Path.Combine(eventsDir, "EventLib", eventPackageName);
    //                 string pdbDir = Path.Combine(eventsDir, "EventLib", eventPackageName.Replace(".dll",".pdb"));
    //                 string langDir = Path.Combine(eventsDir, "EventLanguages");
    //                 byte[] buffer = File.ReadAllBytes(packageDir);
    //                 byte[] buffer2 = File.Exists(pdbDir)?File.ReadAllBytes(pdbDir):null;
    //                 Assembly assembly = Assembly.Load(buffer, buffer2);
    //                 GameData.Domains.DomainManager.TaiwuEvent.LoadEventPackageFromAssembly(assembly, packageDir, langDir, modId.ToString());
    //             }
    //         }
    //         return false;
    //     }
    // }
    [HarmonyPatch(typeof(GameData.Domains.TaiwuEvent.TaiwuEventDomain),"GetEventDisplayData")]
    public static class TaiwuEventDomainGetEventDisplayData {
        public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            return instructions.MethodReplacer(typeof(System.Console).GetMethod("WriteLine",new Type[]{typeof(string)}), typeof(TaiwuEventDomainGetEventDisplayData).GetMethod("logwarn"));
        }
    }
    [HarmonyPatch(typeof(GameData.Domains.World.WorldDomain),"AdvanceMonth_SectMainStory_Baihua")]
    public static class BaihuaProgress {
        public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            return instructions.MethodReplacer(
                typeof(GameData.Domains.World.WorldDomain).GetMethod("GetMainStoryLineProgress"),
                typeof(NeutronFightsConchship).GetMethod("Return23")
            );
        }
    }
    public static short Return23(GameData.Domains.World.WorldDomain __instance){return 23;}
}

