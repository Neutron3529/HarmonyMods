// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../The Scroll of Taiwu_Data/Managed/System.dll" -r:"../../The Scroll of Taiwu_Data/Managed/netstandard.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Reflection.Emit.ILGeneration.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../The Scroll of Taiwu_Data/Managed/mscorlib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Assembly-CSharp.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Unity.TextMeshPro.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.CoreModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.UI.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.dll" -optimize -unsafe -deterministic NeutronFightsConchship.cs ../UTILS/*.CS -out:NeutronFightsConchship.dll -define:FRONTEND
//-define:DEBUG_TREESEED
// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll"
/**
 *  Neutron's Taiwu Collections
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;
using static Utils.Logger;

[assembly: AssemblyVersion("0.0.0.3529")]
namespace NeutronFightsConchship;
[TaiwuModdingLib.Core.Plugin.PluginConfig("NeutronFightsConchship","Neutron3529","0.1.0")]
public class NeutronFightsConchship : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Initialize(){
        this.HarmonyInstance = new RobustHarmonyInstance(this.GetGuid());

        SingletonObject.getInstance<GlobalSettings>().Resolution = new UnityEngine.Vector2Int(width, height);
    }
    public override void Dispose()=>this.HarmonyInstance.UnpatchSelf();
    public RobustHarmonyInstance HarmonyInstance;

    private bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logwarn("enable 出错，出错键值为\""+key+"\"，错误原因是"+ex.Message);
            return false;
        }
    }
    static int width=1920;
    static int height=1080;
    private bool _enable(string key){
        bool enable=false;
        if(key == "res") {
            string wxh="";
            if(!ModManager.GetSetting(this.ModIdStr, key,ref wxh)){
                return false;
            }
            var wh=wxh.Split('x');
            width=int.Parse(wh[0]);
            height=int.Parse(wh[1]);
            return true;
        }
        return ModManager.GetSetting(this.ModIdStr, key,ref enable) && enable;
    }
    public override void OnModSettingUpdate(){
        this.HarmonyInstance.UnpatchSelf();
        // if(enable("VC"))this.HarmonyInstance.PatchAll(typeof(SteamManagerUploadItemUpdate));
        if(enable("VC"))this.HarmonyInstance.PatchAll(typeof(ModManagerWriteModInfo));
        // DEBUG: 前端似乎不会卡顿
        // this.HarmonyInstance.PatchAll(typeof(UI_CombatUpdate));

#if DEBUG_TREESEED
        this.HarmonyInstance.PatchAll(typeof(UI_CollectResourceGetWudangHeavenlyTreeSeedAction));
        this.HarmonyInstance.PatchAll(typeof(UI_GetItemOnDisable));
        this.HarmonyInstance.PatchAll(typeof(UI_GetItemOnInit));
#endif
    }
#if DEBUG_TREESEED
    [HarmonyPatch(typeof(UI_GetItem),"OnInit")]
    public static class UI_GetItemOnInit {
        public static void Prefix(FrameWork.ArgumentBox argsBox) {
            if(argsBox.Get("CloseAction", out Action closeAction)) {
                logwarn($"(Prefix) CloseAction {closeAction} is not null and being set.");
            } else {
                logwarn($"(Prefix) OnInit Called without CloseAction.");
            }
        }
        public static void Postfix(FrameWork.ArgumentBox ____closeAction) {
            if(____closeAction is not null) {
                logwarn($"(Postfix) CloseAction {____closeAction} is set.");
            } else {
                logwarn($"(Postfix) CloseAction is not set.");
            }
        }
    }
    [HarmonyPatch(typeof(UI_GetItem),"OnDisable")]
    public static class UI_GetItemOnDisable {
        public static void Prefix(ref Action ____closeAction) {
            if(____closeAction is not null) {
                logwarn($"_closeAction {____closeAction} is not null and will be called.");
                try {
                    ____closeAction.Invoke();
                } catch(Exception ex) {
                    logwarn("Exception:", ex);
                }
            } else {
                logwarn($"OnDisable is called with _closeAction == null.");
            }
        }
    }
    [HarmonyPatch(typeof(UI_CollectResource),"GetWudangHeavenlyTreeSeedAction")]
    public static class UI_CollectResourceGetWudangHeavenlyTreeSeedAction {
        public static Action Postfix(Action res) {
            if(res != null) {
                logwarn($"Action {res} generated");
            }
            return res;
        }
    }
#endif
    [HarmonyPatch(typeof(ModManager),"WriteModInfo")]
    public static class ModManagerWriteModInfo {
        public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            instructions = new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(OpCodes.Ldsfld,typeof(Game).GetField("Instance")),
                    new CodeMatch(OpCodes.Ldfld,typeof(Game).GetField("GameVersion"))
                ).Repeat( (matcher) => {// Do the following for each match
                    logger("GameVersion detected(should appear only once?)");
                    matcher.SetAndAdvance(
                        OpCodes.Ldstr,"0.0.0.0-Signed-by-Neutron"
                    ).SetAndAdvance(
                        OpCodes.Nop,null
                    ).Advance(1); // advance(1)结尾是美德
                }).InstructionEnumeration();
            return instructions;
        }
    }
    // [HarmonyPatch(typeof(UI_Combat),"Update")]
    // public static class UI_CombatUpdate {
    //     static System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
    //     static IEnumerable<MethodInfo> TargetMethods(){
    //         foreach(var x in typeof(UI_Combat).GetMethods((BindingFlags)(-1))) {
    //             if (x.Name.ToLower().Contains("update")){
    //                 yield return x;
    //             }
    //         }
    //     }
    //     public static void Prefix(MethodBase __originalMethod, ref System.Diagnostics.Stopwatch __state){
    //         __state = System.Diagnostics.Stopwatch.StartNew();
    //         sw.Stop();
    //         if (sw.ElapsedMilliseconds<50) {
    //             logger($"{__originalMethod} called after {sw.ElapsedMilliseconds}ms");
    //         } else {
    //             logwarn($"{__originalMethod} called after {sw.ElapsedMilliseconds}ms");
    //         }
    //         sw.Restart();
    //     }
    //     public static void Postfix(MethodBase __originalMethod, System.Diagnostics.Stopwatch __state){
    //         __state.Stop();
    //         if (__state.ElapsedMilliseconds<50) {
    //             logger($"{__originalMethod} cost {__state.ElapsedMilliseconds}ms");
    //         } else {
    //             logwarn($"{__originalMethod} cost {__state.ElapsedMilliseconds}ms");
    //         }
    //     }
    // }

    // public static class SteamManagerUploadItemUpdate {
    //     static IEnumerable<MethodInfo> TargetMethods(){
    //         yield return typeof(ModManager).Assembly.GetType("SteamManager").GetMethod("UploadItemUpdate");
    //     }
    //     public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
    //         instructions = new CodeMatcher(instructions)
    //             .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
    //                 new CodeMatch(OpCodes.Ldsfld,typeof(Game).GetField("Instance")),
    //                 new CodeMatch(OpCodes.Ldfld,typeof(Game).GetField("GameVersion"))
    //             ).Repeat( (matcher) => {// Do the following for each match
    //                 logger("GameVersion detected(should appear only once)");
    //                 matcher.SetAndAdvance(
    //                     OpCodes.Ldstr,"99.99.99.99"
    //                 ).SetAndAdvance(
    //                     OpCodes.Nop,null
    //                 ).Advance(1); // advance(1)结尾是美德
    //             }).InstructionEnumeration();
    //         return instructions;
    //     }
    // }
}
