#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="$GAME_NAME"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

case $1 in
  V)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  v)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  VERBOSE) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  verbose) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  D)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  d)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  DEBUG)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  debug)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  *)       EXTRA_DEFINE=""                                               ;;
esac

_MODE__SELECT_=100

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.AIModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/Save.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/Domain.dll" \
  $(for i in "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize $EXTRA_DEFINE \
  - $UTILS && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
  git add ${FILE_NAME}
  case $2 in
    R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    *) git commit -am "$2" ;;
  esac
  git push
fi
exit





using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%;

[BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
public class Cheat : Neutron3529.ModEntry {
    public Cheat() : base("%%PLUGIN_ID%%") {}
    public override void Init() {
        base.Init();
        // 主要逻辑放在`utils.cs`中，这里的Awake2只是为了以防万一
        // 目前来说，这个函数的唯一用途是用来“叮”……
        // 就像这样：
        logger("叮~修改器启动，请安心游戏");
    }
    class very_dark : Entry {
        [Desc("黑暗仪式重置时，流逝时间修改为此数值（单位为秒）而非0")]
        static double val=864000;
        [HarmonyPatch(typeof(AscendCtrl),"DoShallowAscend")]
        static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            vlogger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
            instructions = new CodeMatcher(instructions)
            .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                          new CodeMatch(OpCodes.Ldc_R8),
                          new CodeMatch(OpCodes.Call, typeof(AscendCtrl).GetProperty("TimeFromLastAscned",(BindingFlags)(-1)).GetSetMethod(true)) // 32	0063	call	instance void AscendCtrl::set_TimeFromLastAscned(float64)
            // new CodeMatch(OpCodes.Call, typeof(Mafi.Base.Prototypes.Transport.PortShapesData).GetMethod("Kw",new Type[]{typeof(int)}))
            ).Repeat( matcher => matcher// Do the following for each match
                .SetAndAdvance(
                    OpCodes.Ldc_R8,val
                )
                .Advance(1) // avoid conflict.
                ).InstructionEnumeration();
            return instructions;
        }
    }
    [Desc("黑暗仪式重置时，talent用加法而非覆盖")]
    class very_very_dark : Entry {
        [HarmonyPatch(typeof(AscendCtrl),"GetTalent")]
        static double Postfix(double res) {
            return (BASE.HasMain()?BASE.main.S.talentFactor+res:res);
        }
    }
    [Desc("黑暗仪式重置时无视1小时间隔")]
    class very_fast_dark : Entry {
        [HarmonyPatch(typeof(AscendCtrl),"Available")]
        static bool Prefix(ref bool __result)=>!(__result=true);
    }
    class fast_quest : Entry {
        [Desc("完成任务速度乘数")]
        static float speed=0.005f;
        [HarmonyPatch(typeof(Squad),"MaxTime")]
        static float Postfix(float res)=>res*speed;
    }
    class fast_quest_refresh : Entry {
        [Desc("任务刷新计时器速率")]
        static float speed=5000f;
        [HarmonyPatch(typeof(RefreshQuestListController),"ActionIn1Second")]
        static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            vlogger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
            instructions = new CodeMatcher(instructions)
            .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                new CodeMatch(OpCodes.Ldc_R4,1f)
            ).Repeat( matcher => matcher// Do the following for each match
                .SetAndAdvance(
                    OpCodes.Ldc_R4,speed
                )
                .Advance(1) // avoid conflict.
                ).InstructionEnumeration();
            return instructions;
        }
    }
    class better_quest : Entry {
        [Desc("任务完成效果乘数")]
        static float speed=100000f;
        [HarmonyPatch(typeof(Squad),"CompleteEffectFactor")]
        static float Postfix(float res)=>res*speed;
    }
    class faster_equip_exp : Entry {
        [Desc("更快速获取装备经验")]
        static long mul=100000000L;
        [HarmonyPatch(typeof(Equipment),"AddExp")]
        static void Prefix(ref long exp)=>exp*=mul;
    }
    class faster_action_loop : Entry {
        [Desc("循环动作加速")]
        static double mul=100;
        [HarmonyPatch(typeof(Domain.LoopActions.LoopAction),"RowIntervalFactor")]
        static double Postfix(double res)=>res*mul;
    }
    [Desc("循环（在第一次消耗之后）减价")]
    class cheaper_action_loop : Entry {
        [HarmonyPatch(typeof(Domain.LoopActions.LoopAction),"CompletedAction")]
        static void Postfix(Domain.LoopActions.LoopAction __instance){
            if(__instance.progressCost!=null){
                __instance.progressCost.Clear();
            }
            if(__instance.initCost!=null){
                __instance.initCost.Clear();
            }
        }
    }
    class larger_storage : Entry {
        [Desc("允许物品溢出的最大比例")]
        static double mul=100;
        [HarmonyPatch(typeof(Resource),"Truncate", new Type[]{typeof(double)})]
        static void Prefix(ref double threshold)=>threshold*=mul;
    }
    [Desc("物品必爆")]
    class increasing_drop_prob : Entry {
        static IEnumerable<MethodBase> TargetMethods() {
            yield return typeof(ResearchDrop).GetMethod("Probability");
            yield return typeof(ResourceDrop).GetMethod("Probability");
        }
        static void Prefix(ref float ____probability)=>____probability=100;
    }

    [Desc("增加稀有敌人出现概率")]
    class increasing_rare_prob : Entry {
        [HarmonyPatch(typeof(RareEnemyData),"Possibility100", MethodType.Getter)]
        static float Postfix(float res)=>50;
    }
    [Desc("强制执行loop以跳过stuck判定",-1)]
    class bypass_stuck : Entry {
        [HarmonyPatch(typeof(Domain.LoopActions.LoopAction),"EffectIsCapped")]
        static bool Postfix(bool res)=>false;
    }
}
