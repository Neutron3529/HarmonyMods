#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="Library Of Ruina"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

__MODE_VERBOSE=99 # is the line number of "#define VERBOSE", may be modified
__MODE_DEBUG__=$((__MODE_VERBOSE+1))
__MODE_RELEASE=$((__MODE_DEBUG__+1))

case $1 in
    V)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    v)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    VERBOSE) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    verbose) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    D)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    d)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    DEBUG)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    debug)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    *)       _MODE__SELECT_=$__MODE_RELEASE     ;;
esac

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_ ) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.AIModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  $(for i in "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize `[ "$_MODE__SELECT_" == "$__MODE_DEBUG__" ] && echo -debug` \
  $UTILS - && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit

#define VERBOSE // the line of __MODE_VERBOSE
#define DEBUG



using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%
{
    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
    public class Cheat : BaseUnityPlugin {
        void Start() {
            var harmony=new Harmony("%%PLUGIN_ID%%");
            Logger.LogInfo("此Mod使用AGPL-v3许可发布，如果你使用了这里的代码，请按照相同许可发布你修改后的mod。");
#if DEBUG
            logger=Logger.LogInfo;
            logger=delegate(string s){Logger.LogInfo("%%NAMESPACE_ID%%:"+s);};
            logger("开始注入");
#endif
            if(Config.Bind("config", "LibraryFloorModelFeedBook", true, "烧书时可以烧出BOSS遗物").Value){
                harmony.PatchAll(typeof(LibraryFloorModelFeedBook));
                logger("烧书时可以烧出BOSS遗物-注入完成");
            }
            if(Config.Bind("config", "BattleUnitBufListDetailChangeDiceResult", true, "敌方永远掷骰子得最小值，我方永远掷骰子得最大值").Value){
                harmony.PatchAll(typeof(BattleUnitBufListDetailChangeDiceResult));
                logger("敌方永远掷骰子得最小值，我方永远掷骰子得最大值-注入完成");
            }
            logger("叮~修改器启动，请安心游戏");
        }
        [HarmonyPatch(typeof(LibraryFloorModel), "FeedBook")]
        class LibraryFloorModelFeedBook {
            static void Postfix() {
                foreach(int i in new int[]{505004,702005,702101,702102,702103,702104,702107,702108,703021,703310,902702,920301,920302,920401,920501,920602,9902405,9907517,9908111,9910506,9910201,9908112}){
                    if (Singleton<InventoryModel>.Instance.GetCardCount(i) == 0) {// 100*5的那个技能是清场，基本上会杀掉所有在场人员。平日不应使用
                        Singleton<InventoryModel>.Instance.AddCard(i, 150);
                    }
                }
            }
        }
        [HarmonyPatch(typeof(BattleUnitBufListDetail), "ChangeDiceResult")]
        class BattleUnitBufListDetailChangeDiceResult {
            static void Postfix(BattleDiceBehavior behavior, ref int diceResult) {
                if(behavior !=null && behavior.owner != null && behavior.owner.faction == Faction.Player){
                    diceResult=Mathf.Max(behavior.GetDiceMax(),diceResult);
                }else{
                    diceResult=Mathf.Min(behavior.GetDiceMin(),diceResult);
                }
            }
        }
        [HarmonyPatch(typeof(BattleDiceBehavior), "GetDiceMin")]
        class BattleDiceBehaviorGetDiceMin {
            static bool Prefix(BattleDiceBehavior __instance,ref int __result) {
                if(__instance !=null && __instance.owner != null && __instance.owner.faction == Faction.Player){
                    __result = __instance.GetDiceMax();
                    return false;
                }else{
                    return true;
                }
            }
        }
        [HarmonyPatch(typeof(BattleDiceBehavior), "GetDiceMax")]
        class BattleDiceBehaviorGetDiceMax {
            static bool Prefix(BattleDiceBehavior __instance,ref int __result) {
                if(__instance !=null && __instance.owner != null && __instance.owner.faction != Faction.Player){
                    __result = __instance.GetDiceMin();
                    return false;
                }else{
                    return true;
                }
            }
        }
        [HarmonyPatch(typeof(BattleUnitModel), "OnDie")]
        class BattleUnitModelOnDie {
            static void Postfix(BattleUnitModel __instance){
                if (__instance.faction == Faction.Enemy) for(int x=0;x<10;x++){
                    int emotionLevel = 5;
                    for (int i = emotionLevel; i >= 0; i--) {
                        DropTable dropTable;
                        if (__instance.UnitData.unitData.DropTable.TryGetValue(i, out dropTable))
                        {
                            using (List<DropBookDataForAddedReward>.Enumerator enumerator2 = dropTable.DropRemakeCompare(emotionLevel).GetEnumerator())
                            {
                                while (enumerator2.MoveNext())
                                {
                                    DropBookDataForAddedReward dropBookDataForAddedReward = enumerator2.Current;
                                    Singleton<StageController>.Instance.OnEnemyDropBookForAdded(dropBookDataForAddedReward);
                                    __instance.view.OnEnemyDropBook(dropBookDataForAddedReward.GetLorId());
                                }
                            }
                        }
                    }
                }
            }
        }
#if DEBUG
        public static Action<string> logger;
#else
        public static void logger(string s){}
#endif
    }
}

