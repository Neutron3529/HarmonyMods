#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin Compiler (python version, for windows users)
#   Copyright (C) 2022 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put the file you want to compile, and `utils.cs`
#   * in `steamapps` folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     python compile.py file-you-want-to compile.cs utils.cs # here python should be python3.
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * If you called this script for the first time, it would ask the location of
#   * dotnet and csc.dll, you should got them from https://dotnet.microsoft.com/en-us/download.
import os
import sys
dotnet=os.environ.get('DOTNET_MAIN','dotnet')
csc=os.environ.get('DOTNET_CSC_DLL','')
dotnet_csc=os.environ.get('DOTNET_CSC','')
dotnet_cfg='dotnet'
csc_cfg=''

def valid(d,c):
    global dotnet,csc
    if dotnet_csc != '' : return true
    elif os.system(d+" --version")==0 and os.path.exists(c):
        return true
    else:
        dotnet = "C:\\Program Files\\dotnet\\dotnet.exe" if os.path.exists("C:\\Program Files\\dotnet\\dotnet.exe") else '/usr/bin/dotnet'
        l = [os.path.join(i,"Roslyn","bincore","csc.dll") for i in [i for i in (os.path.join('/usr/share/dotnet/sdk',i) for i in (os.listdir('/usr/share/dotnet/sdk') if os.path.exists('/usr/share/dotnet/sdk') else [])) if os.path.isdir(i)] + [i for i in (os.path.join('C:\\Program Files\\dotnet\\sdk',i) for i in (os.listdir('C:\\Program Files\\dotnet\\sdk') if os.path.exists('C:\\Program Files\\dotnet\\sdk') else [])) if os.path.isdir(i)] if os.path.exists(os.path.join(i,"Roslyn","bincore","csc.dll"))]
        if len(l) > 0:
            csc=l[0]
        return os.path.exists(dotnet) and os.path.exists(csc) and os.system(dotnet+" --version")==0

def compile_file(argv,status):
    if len(argv)==1 :
        argv+=["utils.cs"]
    with open(argv[1]) as f:
        [conf,body]=f.read().split(r"#define VERBOSE")


if __name__=='__main__':
    status=0
    cfg_file = os.path.join(os.path.dirname(__file__) if os.path.dirname(__file__)!='' else os.getcwd(),'.compile.cfg')
    while True:
        if os.path.exists(cfg_file):
            with open(cfg_file) as f:
                [dotnet,csc]=[i.strip() for i in f.read().split('\n')]
                [dotnet_cfg,csc_cfg]=[dotnet,csc]
        if compile_file(sys.argv,status):
            if dotnet!=dotnet_cfg or csc!=csc_cfg:
                with open(cfg_file,'w') as f:
                    f.write(dotnet+'\n'+csc)
            break
        status+=1
        if not valid(dotnet,csc):
            print("    脚本未能正确识别dotnet csc的位置，请手动指定它们。\n通常情况下，在linux系统中，\n    dotnet  位于 /usr/bin/dotnet\n    csc.dll 位于 /usr/share/dotnet/sdk/${version}/Roslyn/bincore/csc.dll\n在windows系统中，\n    dotnet  位于 C:\\Program Files\\dotnet\\dotnet.exe\n    csc.dll 位于 C:\\Program Files\\dotnet\\sdk\\${version}\\Roslyn\\bincore\\csc.dll")
            if os.system(dotnet+" --version")!=0:dotnet=input("执行`"+dotnet+" --version`返回错误结果，这意味着dotnet的配置无效。请将dotnet的可执行文件拖动到命令窗口中，之后按下回车键")
            if not os.path.exists(csc):csc=input("找不到csc.dll，请将csc.dll拖动到命令窗口中，之后按下回车键")
    print("脚本执行完毕。如果脚本未能正确配置dotnet或者csc.dll导致编译出错，请设置环境变量$DOTNET_MAIN与$DOTNET_CSC_DLL，或者直接设置$DOTNET_CSC，将其作为编译命令")
