#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="Dream Engines"                            # might modify if the name mismatch.
export GAME_DIR="Dream Engines Nomad Cities"

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

__MODE_VERBOSE=98 # is the line number of "#define VERBOSE", may be modified
__MODE_DEBUG__=$((__MODE_VERBOSE+1))
__MODE_RELEASE=$((__MODE_DEBUG__+1))

case $1 in
    V)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    v)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    VERBOSE) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    verbose) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    D)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    d)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    DEBUG)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    debug)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    *)       _MODE__SELECT_=$__MODE_RELEASE     ;;
esac

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_ ) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/Infrastructure.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  $(for i in "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize `[ "$_MODE__SELECT_" == "$__MODE_DEBUG__" ] && echo -debug` \
  $UTILS - && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit

#define VERBOSE // the line of __MODE_VERBOSE
#define DEBUG



using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%
{
    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
    public class Cheat : BaseUnityPlugin {
#if DEBUG
        public static Action<string> logger;
#else
        public static void logger(string s){}
#endif
        public static float amplify_factor=1f;
        public static float inv_amplify_factor=1f;
        void Start() {
            var harmony=new Harmony("%%PLUGIN_ID%%");
            Logger.LogInfo("此Mod使用AGPL-v3许可发布，如果你使用了这里的代码，请按照相同许可发布你修改后的mod。");
#if DEBUG
            logger=Logger.LogInfo;
            logger=delegate(string s){Logger.LogInfo("%%NAMESPACE_ID%%:"+s);};
            logger("开始注入");
#endif
            if ((amplify_factor=Config.Bind<float>("config", "amplify_factor", 16f, "产量与传送带速度乘数").Value)>1f){
                inv_amplify_factor=1/amplify_factor;
                //harmony.PatchAll(typeof(CostOrProductionValuesGetUnitsPerCycleModifier));
                harmony.PatchAll(typeof(ProductionElementGetModifierOutputSpeed));
                harmony.PatchAll(typeof(MapObjectComponentProductionRemoveCosts));
                logger("Neutron3529-产量与传送带速度乘数-注入完成");
            }
            if ((inv_amplify_factor=Config.Bind<float>("config", "belt_speed_factor", 64f, "传送带速度乘数").Value)>1f){
                inv_amplify_factor=1/inv_amplify_factor;
                harmony.PatchAll(typeof(NewTransportSystemGetTickTime));
                logger("Neutron3529-传送带速度乘数-注入完成");
            }
            if (Config.Bind<bool>("config", "fix_random_tile", true, "刷怪位置固定").Value){
                harmony.PatchAll(typeof(MapUtilsGetRandomEdgeTile));
                logger("Neutron3529-刷怪位置固定-注入完成");
            }
            logger("Neutron3529-注入完成");
        }
        [HarmonyPatch(typeof(MobileBase.CostOrProductionValues), "GetUnitsPerCycleModifier")]
        class CostOrProductionValuesGetUnitsPerCycleModifier { //会同时修改输入和输出（特别是各种upkeep的消耗），因此不用
            public static void Postfix(ref float __result) {
                __result*=amplify_factor;
            }
        }
        [HarmonyPatch(typeof(MobileBase.ProductionElement), MethodType.Constructor,new Type[] { typeof(MobileBase.ProductionElement.State), typeof(MobileBase.IProductionOwner), typeof(MobileBase.AccurateInventory), typeof(MobileBase.AccurateInventory), typeof(MobileBase.ProductionElement.ProductionStatNames), typeof(MobileBase.ProductionModifierParams) })]
        class ProductionElementConstructor { // 如此修改，移动建筑时产出会指数爆炸，因此不用
            public static bool Prefix(ref MobileBase.AccurateInventory producePerCycle) {
                producePerCycle=producePerCycle.Mul(amplify_factor);
                return true;
            }
        }
        [HarmonyPatch(typeof(MobileBase.ProductionElement), "GetModifierOutputSpeed")]
        class ProductionElementGetModifierOutputSpeed {
            public static void Postfix(ref float __result) {
                __result*=amplify_factor;
            }
        }
        [HarmonyPatch(typeof(MobileBase.NewTransportSystem), "GetTickTime")]
        class NewTransportSystemGetTickTime {
            public static void Postfix(ref float __result) {
                __result*=inv_amplify_factor;
            }
        }
        [HarmonyPatch(typeof(MobileBase.MapObjectComponentProduction), "RemoveCosts")]
        class MapObjectComponentProductionRemoveCosts {
            public static bool Prefix(ref int amount, out int __state) {
                __state=amount;
                return true;
            }
            public static void Postfix(ref int __result, int __state) {
                __result=__state;
            }
        }
        [HarmonyPatch(typeof(Infrastructure.MapUtils), "GetRandomEdgeTile")]
        class MapUtilsGetRandomEdgeTile {
//             public static bool Prefix(MobileBase.RaidSpawner __instance, Infrastructure.GridPosition __result) {
//                 __result=new MapSearcher(__instance.map.Size).SetAllowDiagonal(false).SetValidFunc((int x, int y) => !__instance.BlockedTilesInstance.blockedTiles[x, y] && flowField.GetDirection(x, y) != null).GetClosestValidNeighbor(new Infrastructure.GridPosition(0,0));
//                 return false;
//             }
            public static Infrastructure.GridPosition Postfix() {
                return new Infrastructure.GridPosition(0,0);
            }
        }
    }
}

