using System;
using System.Reflection;
using BepInEx;

namespace Neutron3529.Utils{
    class Targets {
        public static Action<string> logger=(s)=>{};
        public static bool Prepare(MethodBase __originalMethod){
            if(__originalMethod==null){
                logger("first prepare, should return true.");
                return true;
            }
            bool non_generic=!__originalMethod.IsGenericMethod;
            bool not_getter_setter=!(__originalMethod.Name.Length>4 && ((__originalMethod.Name[0]=='g' || __originalMethod.Name[0]=='s') && __originalMethod.Name[1]=='e' && __originalMethod.Name[2]=='t' && __originalMethod.Name[3]=='_'));
            logger("prepare "+__originalMethod+", non_generic="+non_generic+", not_getter_setter="+not_getter_setter);
            return non_generic && not_getter_setter;
        }
    }
}
/* Usage:
        class __Targeting__ {
            static bool Prepare(MethodBase __originalMethod){
                return Neutron3529.Utils.Targets.Prepare(logger,__originalMethod);
            }
            static IEnumerable<MethodBase> TargetMethods() {
                Type[] t=typeof(Building).Assembly.GetTypes();
                foreach(Type ty in t){
                    MethodInfo[] m=ty.GetMethods();
                    foreach(MethodInfo i in m)if(i.Name=="EndTick"){
                        yield return i;
                    }
                }
            }
        }

        这么搞似乎真的不如复制粘贴代码方便

        class Targets {
            public static bool Prepare(MethodBase __originalMethod){
                if(__originalMethod==null){
                    logger("first prepare, should return true.");
                    return true;
                }
                bool non_generic=!__originalMethod.IsGenericMethod;
                bool not_getter_setter=!(__originalMethod.Name.Length>4 && ((__originalMethod.Name[0]=='g' || __originalMethod.Name[0]=='s') && __originalMethod.Name[1]=='e' && __originalMethod.Name[2]=='t' && __originalMethod.Name[3]=='_'));
                logger("prepare "+__originalMethod+", non_generic="+non_generic+", not_getter_setter="+not_getter_setter);
                return non_generic && not_getter_setter;
            }
            static IEnumerable<MethodBase> TargetMethods() {
                foreach(Type ty in nre type[]{ // type name here

                    }){
                    MethodInfo[] m=ty.GetMethods();
                    foreach(MethodInfo i in m)if(i.Name=="Update"){ // method name here
                        yield return i;
                    }
                }
            }
        }
 */
