#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="TWOKF"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

__MODE_VERBOSE=99 # is the line number of "#define VERBOSE", may be modified
__MODE_DEBUG__=$((__MODE_VERBOSE+1))
__MODE_RELEASE=$((__MODE_DEBUG__+1))

case $1 in
    V)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    v)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    VERBOSE) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    verbose) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    D)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    d)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    DEBUG)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    debug)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    *)       _MODE__SELECT_=$__MODE_RELEASE     ;;
esac

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_ ) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.AIModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  $(for i in "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize `[ "$_MODE__SELECT_" == "$__MODE_DEBUG__" ] && echo -debug` \
  - && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit

#define VERBOSE // the line of __MODE_VERBOSE
#define DEBUG



using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;
using UnityEngine.UI;

namespace %%NAMESPACE_ID%%
{
    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.1")]
    public class Cheat : BaseUnityPlugin {
        public static int skill_mul=100;
        public static int talent_add=100;
        public static string traits="10006,10007,10008,10009,10012,10025,10026,10027,10028,10029,10031,10032,10033,10034,10035,10041,10044,10045,10046,10047,10048,10049,10050,10051,10052,10053,10054,10055,10056,10057,10058,10059,10060,10061,10062,10063,10064,10070,10071,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143,10144,10145,10146,10147,10148,10149,10150,20001,20002,20003,20004,20005,20006,20007,40004,40005,40006,40007,40008,40009,40010,40011,1001,1002,1003,1004,1005,1006,1007,1008,1009,1012,1026,1027,1028,1029,1030,10013,10014,10015,10016,10017,10018,10019,10020,10021,10022,10023,10024";
        public static string KongFu="2011,4037,10027,10015,9047";
        void Start() {
            var harmony=new Harmony("%%PLUGIN_ID%%");
            Logger.LogInfo("此Mod使用AGPL-v3许可发布，如果你使用了这里的代码，请按照相同许可发布你修改后的mod。");
#if DEBUG
            logger=Logger.LogInfo;
            loggeri=(i,s) =>{logger(s);return i;};
            logger("开始注入");
#endif
            if(Config.Bind("config", "console_dalao", true, "随手抄了一下控制台dalao的功能，高亮隐藏物品").Value){
                harmony.PatchAll(typeof(gang_e01TableLoadPatch));
                logger("随手抄了一下控制台dalao的功能，高亮隐藏物品-patched");
            }
            if((talent_add=Config.Bind("config", "roll_to_the_best", talent_add, "升级时按最优可能来增加各属性，并将获得的天赋点数改为此数值").Value)>=0){
                harmony.PatchAll(typeof(LevelUpControllerRoll));
                logger("升级时按最优可能来增加各属性，并将获得的天赋点数改为此数值-patched");
            }
            if((skill_mul=Config.Bind("config", "skill_mul", skill_mul, "技能空挥经验倍数(请不要将这个数值设置为大于100的数字，否则可能会出问题)").Value)>1){
                harmony.PatchAll(typeof(BattleObjectState_Event));
                logger("技能空挥经验倍数(请不要将这个数值设置为大于100的数字，否则可能会出问题)");
            }
            if(Config.Bind("config", "no_stat_check", true, "取消学习功法和装备物品的属性值检测").Value){
                harmony.PatchAll(typeof(VariousCheck));
                logger("取消学习功法和装备物品的属性值检测-patched");
            }
//             if((traits=Config.Bind("config", "start_traits", traits, "roll人物时使用这些特性id而非系统生成的特性id。id从前往后覆盖系统选择，数量不应超过3个").Value).Length >0){
//                 harmony.PatchAll(typeof(Starter3ControllerRollTraits));
//                 logger("roll人物时使用这些特性id而非系统生成的特性id。id从前往后覆盖系统选择，数量不应超过3个-patched");
//             }
            if((traits=Config.Bind("config", "start_traits", traits, "roll人物时额外添加这些特性id").Value).Length+(KongFu=Config.Bind("config", "start_KongFu", KongFu, "roll人物时额外添加这些武学id").Value).Length >0){
                harmony.PatchAll(typeof(Starter3ControllerOnButtonUp));
                logger("roll人物时额外添加这些特性id-patched");
            }
            if(Config.Bind("config", "steal_the_map", true, "踩脚趾获得偷窃效果，且范围增加").Value){
                harmony.PatchAll(typeof(gang_b03TableLoad));
                harmony.PatchAll(typeof(AttackDataGetForecastRangeByAttackType));
                logger("踩脚趾获得偷窃效果，且范围增加，为了优化程序执行，这里省略了范围大于4的技能的范围预报（省略原理：让系统误以为招式等级至多为12，于是招式的射程与攻击范围不能大于4）-patched");
            }
            logger("叮~修改器启动，请安心游戏");
        }
        [HarmonyPatch(typeof(AttackData),"GetForecastRangeByAttackType")]
        class AttackDataGetForecastRangeByAttackType {
            static void Prefix(string[] _type){
                if (int.Parse(_type[4])>12){
                    _type[4]="12";
                }
            }
        }
        [HarmonyPatch(typeof(gang_b03Table),"Load")]
        class gang_b03TableLoad {
            static void Postfix(gang_b03Table __instance, List<gang_b03Table.Row> ___rowList)
            {
                foreach (gang_b03Table.Row item in ___rowList)
                {
                    /*if(item.ID=="12001"){
                        item.Name="偷天换日";
                        item.Attckstyle="C|01|1";
                        item.LV="27";
                        item.Area="9";
                        item.Range="9";
                        item.Expendadd="0";
                    }else */if (item.ID=="10001" ||item.ID=="10028" ){
                        item.Attckstyle="C|01|1";
                        item.LV="27";
                        item.Area="9";
                        item.Damage="0";
                        item.Damageadd="0";
                        item.Range="9";
                        item.Combo="100"; // 5 hits.
                        item.Skill1="StealATK&500|50&1|0&0|0&2";
                        item.Skill2="HPsteal&5|5&1|0&0|0&2";
                        item.Skill2="MPsteal&5|5&1|0&0|0&2";
                        item.Skill3="Burn&100|100&1|0&0|0&2";
                        item.Skill4="Mad&100|100&1|0&0|0&2";
                        item.Expendadd="0";
                    }
                }
            }
        }
        [HarmonyPatch(typeof(Starter3Controller),"OnButtonUp")]
        class Starter3ControllerOnButtonUp {
            static void Postfix()
            {
                CharaData charaData = SharedData.Instance(false).GetCharaData(SharedData.Instance(false).playerid);
                if(traits.Length>0){
                    var x=traits.Split(',');
                    foreach(var j in x){
                        charaData.m_TraitList.Remove(j);
                        charaData.m_TraitList.Add(j);
                    }
                }
                if(KongFu.Length>0){
                    var y=KongFu.Split(',');
                    foreach(var i in y){
                        var j = new KongFuData();
                        j.kf = SharedData.Instance(false).b03.Find_ID(i);
                        j.lv = 1;
                        j.exp = 0f;
                        j.proficiency = 0;
                        charaData.m_KongFuList.Add(j);
                    }
                }
            }
        }
        [HarmonyPatch(typeof(Starter3Controller),"RollTraits")]
        class Starter3ControllerRollTraits {
            static MethodInfo RunTrait=typeof(Starter3Controller).GetMethod("RunTrait",(BindingFlags)(-1));
            static void Postfix(Starter3Controller __instance,SpiderMan[] ___SpiderMen,Color ___stat_txt_common,Color ___stat_txt_good_2,Color ___stat_txt_good_3,Color ___stat_txt_darklight)
            {
                int num = 0;
                string[] traitl = Cheat.traits.Split(',');
                foreach (SpiderMan spiderMan in ___SpiderMen)
                {
                    if (num < traitl.Length)
                    {
                        gang_b06Table.Row row = SharedData.Instance(false).b06.Find_id(traitl[num]);
                        spiderMan.traitValue = traitl[num];
                        spiderMan.GetComponent<Text>().text = row.name;
                        spiderMan.transform.Find("desc").GetComponent<Text>().text = row.note;
                        spiderMan.transform.Find("Hover/Text").GetComponent<Text>().text = row.comment;
                        if (row.good == "1")
                        {
                            spiderMan.GetComponent<Text>().color = ___stat_txt_common;
                        }
                        else if (row.good == "2")
                        {
                            spiderMan.GetComponent<Text>().color = ___stat_txt_good_2;
                        }
                        else if (row.good == "3")
                        {
                            spiderMan.GetComponent<Text>().color = ___stat_txt_good_3;
                        }
                        else
                        {
                            spiderMan.GetComponent<Text>().color = ___stat_txt_darklight;
                        }
                        Cheat.Starter3ControllerRollTraits.RunTrait.Invoke(__instance, new object[] { row.add1, row.attribute1 });
                        Cheat.Starter3ControllerRollTraits.RunTrait.Invoke(__instance, new object[] { row.add2, row.attribute2 });
                        Cheat.Starter3ControllerRollTraits.RunTrait.Invoke(__instance, new object[] { row.add3, row.attribute3 });
                        Cheat.Starter3ControllerRollTraits.RunTrait.Invoke(__instance, new object[] { row.add4, row.attribute4 });
                        num++;
                    }
                }
//                 int num = 0;
//                 string[] traitl = Cheat.traits.Split(',');
//                 for (int xxx = 0; xxx < 2; xxx++)
//                 {
//                     for (int i = 0; i < ___SpiderMen.Length; i++)
//                     {
//                         SpiderMan spiderMan = ___SpiderMen[i];
//                         Cheat.logger("SpiderMen长度：" + ___SpiderMen.Length.ToString());
//                         if (num < traitl.Length && (i >= 3 || spiderMan.GetComponent<Text>().text == ""))
//                         {
//                             gang_b06Table.Row row = SharedData.Instance(false).b06.Find_id(traitl[num]);
//                             spiderMan.traitValue = traitl[num];
//                             if (xxx == 0)
//                             {
//                                 spiderMan.GetComponent<Text>().text = row.name;
//                                 spiderMan.transform.Find("desc").GetComponent<Text>().text = row.note;
//                                 spiderMan.transform.Find("Hover/Text").GetComponent<Text>().text = row.comment;
//                                 if (row.good == "1")
//                                 {
//                                     spiderMan.GetComponent<Text>().color = ___stat_txt_common;
//                                 }
//                                 else if (row.good == "2")
//                                 {
//                                     spiderMan.GetComponent<Text>().color = ___stat_txt_good_2;
//                                 }
//                                 else if (row.good == "3")
//                                 {
//                                     spiderMan.GetComponent<Text>().color = ___stat_txt_good_3;
//                                 }
//                                 else
//                                 {
//                                     spiderMan.GetComponent<Text>().color = ___stat_txt_darklight;
//                                 }
//                             }
//                             Cheat.Starter3ControllerRollTraits.RunTrait.Invoke(__instance, new object[] { row.add1, row.attribute1 });
//                             Cheat.Starter3ControllerRollTraits.RunTrait.Invoke(__instance, new object[] { row.add2, row.attribute2 });
//                             Cheat.Starter3ControllerRollTraits.RunTrait.Invoke(__instance, new object[] { row.add3, row.attribute3 });
//                             Cheat.Starter3ControllerRollTraits.RunTrait.Invoke(__instance, new object[] { row.add4, row.attribute4 });
//                             num++;
//                         }
//                         else
//                         {
//                             traitl = traitl.Where((string source, int index) => source != spiderMan.traitValue).ToArray<string>();
//                         }
//                     }
//                     for (int num2 = num; num2 < traitl.Length; num2++)
//                     {
//                         ___SpiderMen = ___SpiderMen.Concat(new SpiderMan[]
//                         {
//                             new SpiderMan()
//                         }).ToArray<SpiderMan>();
//                     }
//                 }
            }
//             static bool Prepare(MethodBase __originalMethod){
//                 if(__originalMethod==null){
//                     logger("first prepare of "+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.FullName+", should return true.");
//                     return true;
//                 }
//                 bool non_generic=!__originalMethod.IsGenericMethod;
//                 bool not_getter_setter=!(__originalMethod.Name.Length>4 && ((__originalMethod.Name[0]=='g' || __originalMethod.Name[0]=='s') && __originalMethod.Name[1]=='e' && __originalMethod.Name[2]=='t' && __originalMethod.Name[3]=='_'));
//                 logger("prepare "+__originalMethod.DeclaringType.ToString()+" -- "+__originalMethod+", non_generic="+non_generic+", not_getter_setter="+not_getter_setter);
//                 return non_generic && not_getter_setter;
//             }
        }
        class VariousCheck {
            static bool Prefix(ref bool __result) {
                __result=true;
                return false;
            }
            static IEnumerable<MethodBase> TargetMethods(){
                yield return typeof(PackageController).GetMethod("CheckEquipment",(BindingFlags)(-1));
                yield return typeof(PackageController).GetMethod("CheckKFBook",(BindingFlags)(-1));
            }
            static bool Prepare(MethodBase __originalMethod){
                if(__originalMethod==null){
                    logger("first prepare of "+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.FullName+", should return true.");
                    return true;
                }
                bool non_generic=!__originalMethod.IsGenericMethod;
                bool not_getter_setter=!(__originalMethod.Name.Length>4 && ((__originalMethod.Name[0]=='g' || __originalMethod.Name[0]=='s') && __originalMethod.Name[1]=='e' && __originalMethod.Name[2]=='t' && __originalMethod.Name[3]=='_'));
                logger("prepare "+__originalMethod.DeclaringType.ToString()+" -- "+__originalMethod+", non_generic="+non_generic+", not_getter_setter="+not_getter_setter);
                return non_generic && not_getter_setter;
            }
        }
        [HarmonyPatch(typeof(BattleObject), "State_Event")]
        class BattleObjectState_Event {
            static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
                logger("%%PLUGIN_ID%%-正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
                instructions = new CodeMatcher(instructions)
                    .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                        new CodeMatch(OpCodes.Ldfld,typeof(BattleObject).GetField("proficiency")),
                        new CodeMatch(OpCodes.Ldc_I4_1),
                        new CodeMatch(OpCodes.Add),
                        new CodeMatch(OpCodes.Stfld,typeof(BattleObject).GetField("proficiency"))
                    ).Repeat( matcher =>
                        matcher
                        .Advance(1) // Move cursor to after ldfld
                        .SetAndAdvance(
                            OpCodes.Ldc_I4_S,skill_mul
                        )
                        .Advance(loggeri(1,"%%PLUGIN_ID%%-已注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"指令 +1")) // Move cursor to after ldfld
                    ).InstructionEnumeration();
                return instructions;
            }
//             static bool Prepare(MethodBase __originalMethod){
//                 if(__originalMethod==null){
//                     logger("first prepare of "+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.FullName+", should return true.");
//                     return true;
//                 }
//                 bool non_generic=!__originalMethod.IsGenericMethod;
//                 bool not_getter_setter=!(__originalMethod.Name.Length>4 && ((__originalMethod.Name[0]=='g' || __originalMethod.Name[0]=='s') && __originalMethod.Name[1]=='e' && __originalMethod.Name[2]=='t' && __originalMethod.Name[3]=='_'));
//                 logger("prepare "+__originalMethod.DeclaringType.ToString()+" -- "+__originalMethod+", non_generic="+non_generic+", not_getter_setter="+not_getter_setter);
//                 return non_generic && not_getter_setter;
//             }
        }
        [HarmonyPatch(typeof(LevelUpController), "Roll")]
        class LevelUpControllerRoll {
            static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
                logger("%%PLUGIN_ID%%-正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
                instructions = new CodeMatcher(instructions)
                    .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                        new CodeMatch(i=> i.opcode==OpCodes.Call && ((MethodInfo)(i.operand)) == typeof(UnityEngine.Random).GetProperty("value").GetGetMethod()) // match method 2
                    ).Repeat( matcher =>
                        matcher
                        .Advance(1) // Move cursor to after ldfld
                        .InsertAndAdvance(
                            new CodeInstruction(OpCodes.Pop),
                            new CodeInstruction(OpCodes.Ldc_R4,0f)
                        )
                        .Advance(loggeri(1,"%%PLUGIN_ID%%-已注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"指令 +1")) // Move cursor to after ldfld
                    ).InstructionEnumeration();
                instructions = new CodeMatcher(instructions)
                    .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                        new CodeMatch(OpCodes.Ldfld,typeof(CharaData).GetField("m_Talent")),
                        new CodeMatch(OpCodes.Ldc_I4_1)
                    ).Repeat( matcher =>
                        matcher
                        .Advance(1) // Move cursor to after ldfld
                        .SetAndAdvance(
                            OpCodes.Ldc_I4_S,talent_add
                        )
                        .Advance(loggeri(1,"%%PLUGIN_ID%%-已注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"指令 +1")) // Move cursor to after ldfld
                    ).InstructionEnumeration();
                return instructions;
            }
//             static bool Prepare(MethodBase __originalMethod){
//                 if(__originalMethod==null){
//                     logger("first prepare of "+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.FullName+", should return true.");
//                     return true;
//                 }
//                 bool non_generic=!__originalMethod.IsGenericMethod;
//                 bool not_getter_setter=!(__originalMethod.Name.Length>4 && ((__originalMethod.Name[0]=='g' || __originalMethod.Name[0]=='s') && __originalMethod.Name[1]=='e' && __originalMethod.Name[2]=='t' && __originalMethod.Name[3]=='_'));
//                 logger("prepare "+__originalMethod.DeclaringType.ToString()+" -- "+__originalMethod+", non_generic="+non_generic+", not_getter_setter="+not_getter_setter);
//                 return non_generic && not_getter_setter;
//             }
        }

        [HarmonyPatch(typeof(gang_e01Table), "Load")] // 这段代码来自控制台dalao，因为实在太好用了，我直接抄过来了。
        internal class gang_e01TableLoadPatch
        {
            // Token: 0x06000007 RID: 7 RVA: 0x0000220C File Offset: 0x0000040C
            public static void Postfix(gang_e01Table __instance, List<gang_e01Table.Row> ___rowList)
            {
//                 logger("executed.");
                foreach (gang_e01Table.Row item in ___rowList)
                {
//                     logger(item.action+" : "+item.trigger+" : "+item.display);
                    bool isget = item.action.Split(new char[] { '|' })[0] == "GET";
                    if (isget && item.trigger == "CLICK" && item.display == "Prefabs/Field/Dummy")
                    {
                        item.display = "Prefabs/Effect/GroundLight";
//                         logger("found.");
                    }
                }
            }
        }
#if DEBUG
        public static Func<int,string,int> loggeri;
        public static Action<string> logger;
#else
        public static void logger(string s){}
#endif
    }
}

