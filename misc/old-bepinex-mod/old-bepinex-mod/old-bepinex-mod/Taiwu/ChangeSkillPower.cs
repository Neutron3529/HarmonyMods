#!/bin/bash -e
#   * compile instructions: put this file in
#   *         `steamapps/common/Dyson Sphere Program/CustomMod`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

if [ -z "$__DOTNET_CSC" ]; then
    export __DOTNET_CSC="`find /usr/share/dotnet -type f -name dotnet` `find /usr/share/dotnet -name csc.dll`"
    echo '$'"__DOTNET_CSC not set yet, you should execute"
    echo "export __DOTNET_CSC='$__DOTNET_CSC'"
    echo "manually, or this alert will occur each time you execute this script."
fi

__MODE_VERBOSE=81 # is the line number of "#define VERBOSE", may be modified
__MODE_DEBUG__=$((__MODE_VERBOSE+1))
__MODE_RELEASE=$((__MODE_DEBUG__+1))

case $1 in
    V)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    v)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    VERBOSE) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    verbose) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    D)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    d)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    DEBUG)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    debug)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    *)       _MODE__SELECT_=$__MODE_RELEASE     ;;
esac

export GAME_NAME="${0%\.cs}" # now, file name should be equals to ${GAME_NAME}.cs, the benefit is, we could only change the name, rather than change filename and $GAME_NAME.
export GAME_NAME="The Scroll Of Taiwu Alpha V1.0"
export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"
export GAME_BASE_DIR="../common/The Scroll Of Taiwu"
export PLUGIN_ID="Neutron3529.Taiwu.changeSkillPower"
export NAMESPACE_ID="Neutron3529.changeSkillPower"

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_ ) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $__DOTNET_CSC -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/Assembly-CSharp-886238F0C737D5551CA4940C9724A53C94395A280021C721ED03A5241482CB97.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/plugins/YanCore/YanLib.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/plugins/YanCore/UnityUIKit.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/plugins/YanCore/TaiwuUIKit.dll" \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize `[ "$_MODE__SELECT_" == "$__MODE_DEBUG__" ] && echo -debug` \
  - && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit


#define VERBOSE // the line of __MODE_VERBOSE
#define DEBUG
#define USE_UI
// MUST USE_UI

using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

#if USE_UI

using TaiwuUIKit.GameObjects;
using UnityUIKit.Core;
using UnityUIKit.GameObjects;
using YanLib.ModHelper;

#endif

namespace %%NAMESPACE_ID%%
{

    public class Settings {
        // Token: 0x06000008 RID: 8 RVA: 0x00003ED4 File Offset: 0x000020D4
        public void Init(ConfigFile config) {
            this.Config = config;
        }

        // Token: 0x06000009 RID: 9 RVA: 0x000040E3 File Offset: 0x000022E3
        public void Save() {
            this.Config.Save();
        }
        private ConfigFile Config;
    }
#if USE_UI
    public class UI {
        public int skill_id=0;
        public int actor_id=10001;
        public int actor_id_2=10001;
        public bool refresh=false;
        public class Sub {
            public TaiwuLabel lab;
            public TaiwuInputField taiwuInputField;
            public string what;
            public Action refresh=()=>{};
            public Sub(string what,Action<string,InputField> func){
                this.what=what;
                this.taiwuInputField = new TaiwuInputField();
                this.taiwuInputField.Name = what;
                this.taiwuInputField.Text = "0";
                this.lab = new TaiwuLabel { Name = "Label", Text = what };
                this.taiwuInputField.OnEndEdit = delegate(string value, InputField inputField) {
                    if(!Main.ui.refresh){
                        func(value,inputField);
                        this.refresh();
                    }
                };
            }
            public void Addto(Container container){
                container.Children.Add(this.lab);
                container.Children.Add(this.taiwuInputField);
                return;
            }
        }
        public class CT {
            public TaiwuToggle taiwuToggle;
            public int id;
            public int[] mod;
            public Action refresh=()=>{};
            public CT(int id, int[] mod,string name, string title="点击切换",string tip="点击即可切换。"){
                this.taiwuToggle = new TaiwuToggle();
                this.taiwuToggle.Text = name;
                this.mod=mod;
                this.id=id;
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = title;
                taiwuToggle.TipContant = tip;
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(!Main.ui.refresh){
                        Main.ui.skill_id-=(Main.ui.skill_id%this.mod[0])-(Main.ui.skill_id%this.mod[1]);
                        Main.ui.skill_id+=this.id*this.mod[1];
                        this.refresh();
                    }
                };
            }
            public CT Addto(Container container){
                container.Children.Add(this.taiwuToggle);
                return this;
            }
        }
        public static ModHelper Mod;
        public Action refresh2=()=>{};
        public Action<string> logger=(x)=>{};
        public void Init() {
            logger=(x)=>Main.logger(x);
            Mod = new ModHelper("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%");
            Container container = new Container();
            container.Group.Spacing = 5f;
            container.Group.Direction = (Direction)1;
            container.Element.PreferredSize.Add(0f);
            container.Element.PreferredSize.Add(360+60*8f);

            Container container2 = new Container();
            container2.Element.PreferredSize.Add(0f);
            container2.Element.PreferredSize.Add(360f);
            container2.Group.Direction = (Direction)1;
            container2.Group.Spacing = 10f;
            container.Children.Add(container2);
            var lab=new TaiwuLabel { Name = "Label", Text = "\n\n\n\n\n\n\n\n\n\n" };
            container2.Children.Add(lab);
            //var size=container2.Element.PreferredSize;
            { // 招式强度修改
                List<CT> toggles=new List<CT>();
#if DISCARD
                if (false){
                    var xtaiwuToggle = new TaiwuToggle();
                    var xtaiwuSlider=new TaiwuSlider();
                    container2 = new Container();
                    container2.Element.PreferredSize.Add(0f);
                    container2.Element.PreferredSize.Add(120f);
                    container2.Group.Direction = 0;
                    container2.Group.Spacing = 4f;
                    container.Children.Add(container2);
                    xtaiwuToggle.Text = "点击习得/遗忘当前选择的功法\n拖动右边滑条修改功法逆练数\n当前逆练数为：";
                    xtaiwuToggle.isOn = false;
                    xtaiwuToggle.TipTitle = "点击切换状态";
                    xtaiwuToggle.TipContant = "修炼进度为0的功法会习得该功法，否则会遗忘\n习得时会将书读满（视为10正练）遗忘时会同时清空读书情况";
                    xtaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                    {
                        if(value){
                            DateFile.instance.actorGongFas[actor_id][skill_id][1] = 0;
                            DateFile.instance.actorGongFas[actor_id][skill_id][2] = 0;
                            if(DateFile.instance.gongFaBookPages.ContainsKey(skill_id)){
                                DateFile.instance.gongFaBookPages.Remove(skill_id);
                            }
                            xtaiwuToggle.isOn = false;
                        }
                    };
                    container2.Children.Add(xtaiwuToggle);
                    container2.Children.Add(xtaiwuSlider);
                }
#endif
                container2 = new Container();
                container2.Element.PreferredSize.Add(0f);
                container2.Element.PreferredSize.Add(60f);
                container2.Group.Direction = 0;
                container2.Group.Spacing = 4f;
                container.Children.Add(container2);
                //var x=new Sub("太吾id",(string value, InputField inputField)=>{actor_id=(value.ParseInt()==10001||value.ParseInt()==0)?DateFile.instance.mianActorId:value.ParseInt();skill_id=value;})
                var y=new Sub("招式id",(string value, InputField inputField)=>{
                    actor_id=DateFile.instance.mianActorId;
                    skill_id=value.ParseInt();
                });
                var z=new Sub("招式威力",(string value, InputField inputField)=>{
                    actor_id=DateFile.instance.mianActorId;
                    if(value.ParseInt()==0)
                        DateFile.RemoveGongfaEternalPowerChange(actor_id, skill_id, actor_id, skill_id);
                    else
                        DateFile.AddGongfaEternalPowerChange(actor_id, skill_id, value.ParseInt()-DateFile.GetGongfaEternalPowerChange(actor_id, skill_id), actor_id, skill_id);
                });
                y.Addto(container2);
                z.Addto(container2);
                {
                    container2 = new Container();
                    container2.Element.PreferredSize.Add(0f);
                    container2.Element.PreferredSize.Add(60f);
                    container2.Group.Direction = 0;
                    container2.Group.Spacing = 4f;
                    container.Children.Add(container2);
                    string[] names=new string[]{"内功","身法","绝技","拳掌","指法","腿法","暗器","剑法","刀法","长兵","奇门","软兵","御射","乐器","未知","庄稼把式","相枢","剑冢","奇书"};
                    int[] mod=new int[]{1000000,10000};
                    for(int i=0;i<19;i++){
                        toggles.Add(new CT(i,mod,names[i]).Addto(container2));
                        if(i==10){
                            container2 = new Container();
                            container2.Element.PreferredSize.Add(0f);
                            container2.Element.PreferredSize.Add(60f);
                            container2.Group.Direction = 0;
                            container2.Group.Spacing = 4f;
                            container.Children.Add(container2);
                        }
                    }
                }
                {
                    container2 = new Container();
                    container2.Element.PreferredSize.Add(0f);
                    container2.Element.PreferredSize.Add(60f);
                    container2.Group.Direction = 0;
                    container2.Group.Spacing = 4f;
                    container.Children.Add(container2);
                    string[] names=new string[]{"无门无派","「<color=#EDA723FF>少林派</color>」","「<color=#EDA723FF>峨眉派</color>」","「<color=#EDA723FF>百花谷</color>」","「<color=#EDA723FF>武当派</color>」","「<color=#EDA723FF>元山派</color>」","「<color=#EDA723FF>狮相门</color>」","「<color=#EDA723FF>然山派</color>」","「<color=#EDA723FF>璇女派</color>」","「<color=#EDA723FF>铸剑山庄</color>」","「<color=#EDA723FF>空桑派</color>」","「<color=#EDA723FF>无量金刚宗</color>」","「<color=#EDA723FF>五仙教</color>」","「<color=#EDA723FF>界青门</color>」","「<color=#EDA723FF>伏龙坛</color>」","「<color=#EDA723FF>血犼教</color>」"};
                    int[] mod=new int[]{10000,100};
                    for(int i=0;i<16;i++){
                        toggles.Add(new CT(i,mod,names[i]).Addto(container2));
                        if(i==8){
                            container2 = new Container();
                            container2.Element.PreferredSize.Add(0f);
                            container2.Element.PreferredSize.Add(60f);
                            container2.Group.Direction = 0;
                            container2.Group.Spacing = 4f;
                            container.Children.Add(container2);
                        }
                    }
                }
                {
                    container2 = new Container();
                    container2.Element.PreferredSize.Add(0f);
                    container2.Element.PreferredSize.Add(60f);
                    container2.Group.Direction = 0;
                    container2.Group.Spacing = 4f;
                    container.Children.Add(container2);
                    string[] names=new string[]{"","<color=#8E8E8EFF>下·九品</color>","<color=#FBFBFBFF>中·八品</color>","<color=#6DB75FFF>上·七品</color>","<color=#8FBAE7FF>奇·六品</color>","<color=#63CED0FF>秘·五品</color>","<color=#AE5AC8FF>极·四品</color>","<color=#E3C66DFF>超·三品</color>","<color=#F28234FF>绝·二品</color>","<color=#E4504DFF>神·一品</color>"};
                    int[] mod=new int[]{100,1};
                    for(int i=1;i<10;i++){
                        toggles.Add(new CT(i,mod,names[i]).Addto(container2));
                    }
                }

                var refresh=()=>{
                    Main.ui.refresh=true;
                    if(!DateFile.instance.gongFaDate.ContainsKey(skill_id))
                        y.lab.Text="技能"+skill_id+"不存在，修改时需谨慎";
                    else
                        y.lab.Text="技能:"+DateFile.instance.SetColoer(20001 + int.Parse(DateFile.instance.gongFaDate[skill_id][2]), DateFile.instance.gongFaDate[skill_id][0]+"("+skill_id+")", false);
                    z.lab.Text="当前技能威力:"+DateFile.GetGongfaEternalPowerChange(actor_id, skill_id);
                    int counter=0,line=2;
                    if(DateFile.instance.gongfaEternalPowerChange.ContainsKey(actor_id))
                        lab.Text="当前太吾(id"+actor_id+")技能威力的永久修改，下面显示所有经过修改的武功以及其id\n"+string.Join(", ",(from gong_id in DateFile.instance.gongfaEternalPowerChange[actor_id].Keys where DateFile.instance.gongFaDate.ContainsKey(gong_id) select DateFile.instance.SetColoer(20001 + int.Parse(DateFile.instance.gongFaDate[gong_id][2]), DateFile.instance.gongFaDate[gong_id][0]+"("+gong_id+":"+DateFile.GetGongfaEternalPowerChange(actor_id, gong_id)+((counter++&3)==3?")\n":")"), false)).ToArray())+"\n无法解析名称的修改如果存在会显示在这里：";
                        line+=counter>>2;
                        counter=0;
                        if(DateFile.instance.gongfaEternalPowerChange.ContainsKey(actor_id)){
                            lab.Text+=string.Join(", ",(from gong_id in DateFile.instance.gongfaEternalPowerChange[actor_id].Keys where !DateFile.instance.gongFaDate.ContainsKey(gong_id) select "("+gong_id+":"+DateFile.GetGongfaEternalPowerChange(actor_id, gong_id)+((counter++&7)==2?")\n":")")).ToArray());
                        }else{
                            lab.Text="（无对应功法调整）";
                        }
                        //size[1]=60f*line;
                        //container.Element.PreferredSize[1]=60*8f+60f*line;
                    foreach(CT ct in toggles){
                        ct.taiwuToggle.isOn = (Main.ui.skill_id%ct.mod[0])-(Main.ui.skill_id%ct.mod[1])==ct.id*ct.mod[1];
                    }
                    y.taiwuInputField.Text=skill_id.ToString();
                    Main.ui.refresh=false;
                };
                y.refresh=refresh;
                z.refresh=refresh;
                foreach(CT ct in toggles){
                    ct.refresh=refresh;
                }
            }
            Container container_big=new Container();
            container_big.Name = "%%PLUGIN_ID%%";
            container_big.Group.Spacing = 5f;
            container_big.Group.Direction = (Direction)1;
            container_big.Element.PreferredSize.Add(0f);
            container_big.Element.PreferredSize.Add(360+60*8f+500f);
            container_big.Children.Add(container);
            container = new Container();
            container_big.Children.Add(container);
            container.Group.Spacing = 5f;
            container.Group.Direction = (Direction)1;
            container.Element.PreferredSize.Add(0f);
            container.Element.PreferredSize.Add(360+60*8f);

            container2 = new Container();
            container2.Element.PreferredSize.Add(0f);
            container2.Element.PreferredSize.Add(60f);
            container2.Group.Direction = (Direction)1;
            container2.Group.Spacing = 10f;
            container.Children.Add(container2);
            {
                var xx=new TaiwuLabel { Name = "Label", Text = "在下方填写人物id，对指定人物增加特性                         " };
                container2.Children.Add(xx);
                container2 = new Container();
                container2.Element.PreferredSize.Add(0f);
                container2.Element.PreferredSize.Add(60f);
                container2.Group.Direction = 0;
                container2.Group.Spacing = 4f;
                container.Children.Add(container2);
                var y=new Sub("id：",(string value, InputField inputField)=>{
                    if(!Main.ui.refresh){
                        actor_id_2=value.ParseInt();
                        refresh2();
                    }
                });
                y.refresh=()=>{xx.Text="在下方填写人物id，对指定人物增加特性，当前人物："+DateFile.instance.SetColoer(10004, DateFile.instance.GetActorName(actor_id_2, false, false), false)+"("+DateFile.instance.SetColoer(20011, actor_id_2.ToString(), false)+")，向往"+DateFile.instance.SetColoer(10004, DateFile.instance.GetGangDate(int.Parse(DateFile.instance.GetActorDate(actor_id_2, 9, false)), 0), false);};
                y.Addto(container2);
                TaiwuToggle taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "洗白特性";
                taiwuToggle.TipContant = "清空所有id小于200的特性，方便后续满蓝满红加点";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(GameData.Characters.HasChar(actor_id_2)&&value){
                        foreach(int i in from score in DateFile.instance.GetActorDate(actor_id_2, 101, false).Split('|') where score.ParseInt() > 0 && score.ParseInt()<200 select score.ParseInt())DateFile.instance.RemoveActorFeature(actor_id_2,i);
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);
                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "满蓝特性";
                taiwuToggle.TipContant = "全28蓝特性+4武林大会特性，不包括几个中性的特性";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(GameData.Characters.HasChar(actor_id_2)&&value){
                        foreach(int i in new int[]{3, 9, 15, 21, 27, 33, 39, 45, 51, 57, 63, 69, 75, 81, 87, 93, 99, 105, 111, 117, 123, 129, 135, 141, 147, 153, 159, 165, 9003, 9006, 9009, 9012})DateFile.instance.AddActorFeature(actor_id_2,i);
                        tg.isOn=false;
                    }
                };

                container2.Children.Add(taiwuToggle);
                TaiwuToggle xtaiwuToggle=new TaiwuToggle();
                xtaiwuToggle.isOn = false;
                xtaiwuToggle.TipTitle = "说明";
                xtaiwuToggle.Text = "变身赛亚人";
                xtaiwuToggle.TipContant = "全主要属性发挥+100%，全防御属性发挥+300%，内息每时节归零，毒抗增加，平A毒力增强，对赛亚人使用这个开关可以取消赛亚人状态";
                xtaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(!Main.ui.refresh){
                        if(!GameData.Characters.HasCharProperty(actor_id_2, 110)){
                            for(int i=0;i<12;i++){
                                GameData.Characters.SetCharProperty(actor_id_2, 1361+i, (GameData.Characters.GetCharProperty(actor_id_2, 1361+i).ParseInt()+(i<6?100:300)).ToString());
                            }
                            GameData.Characters.SetCharProperty(actor_id_2, 40, "-80000");
                            for(int i=0;i<6;i++){
                                GameData.Characters.SetCharProperty(actor_id_2, 41+i, "100000");
                            }
                            GameData.Characters.SetCharProperty(actor_id_2, 110, "9900");
                        }else{
                            for(int i=0;i<12;i++){
                                GameData.Characters.RemoveCharProperty(actor_id_2, 1361+i);
                            }
                            GameData.Characters.RemoveCharProperty(actor_id_2, 40);
                            for(int i=0;i<6;i++){
                                GameData.Characters.RemoveCharProperty(actor_id_2, 41+i);
                            }
                            GameData.Characters.RemoveCharProperty(actor_id_2, 110);
                        }
                    }
                };
                container2.Children.Add(xtaiwuToggle);

                var ztaiwuToggle=new TaiwuToggle();
                ztaiwuToggle.isOn = false;
                ztaiwuToggle.TipTitle = "说明";
                ztaiwuToggle.Text = "放气充气";
                ztaiwuToggle.TipContant = "当actor的无属性内力大于0时，将其置0，否则置28067.0（修得5505点大混元之后，再补充28067.0点真气即可将4属性真气点到100）";
                ztaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(!Main.ui.refresh){
                        if(value){
                            GameData.Characters.SetCharProperty(actor_id_2, 706, "280670");
                        } else {
                            GameData.Characters.RemoveCharProperty(actor_id_2, 706);
                        }
                        refresh2();
                    }
                };
                container2.Children.Add(ztaiwuToggle);
                /*taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "绝育经";
                taiwuToggle.TipContant = "经文是必定遗传的白骰子特性，于是这里加绝育，保证经文不遗传。";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(GameData.Characters.HasChar(actor_id_2)&&value){
                        foreach(int i in new int[]{11003,11006,11009,11012,11015,11018,11021,11024})DateFile.instance.AddActorFeature(actor_id_2,i);
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);*/
                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "满红特性";
                taiwuToggle.TipContant = "不包括蛊特性，因为被下n个成蛊的家伙多半活不过第二天";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(GameData.Characters.HasChar(actor_id_2)&&value){
                        foreach(int i in new int[]{6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90, 96, 102, 108, 114, 120, 126, 132, 138, 144, 150, 156, 162, 168, 7001, 7008, 7011, 7012, 8004})DateFile.instance.AddActorFeature(actor_id_2,i);
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);
                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "满蛊特性";
                taiwuToggle.TipContant = "想让人死你直说……别搞这么麻烦\n（金蚕不是成蛊，成蛊会吃掉其他蛊导致重伤）";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(GameData.Characters.HasChar(actor_id_2)&&value){
                        foreach(int i in new int[]{11003,11006,11009,11012,11015,11018,11020,11024})DateFile.instance.AddActorFeature(actor_id_2,i);
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);
                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "早入轮回";
                taiwuToggle.TipContant = "将人物寿命置1000，同时将人物所有主要属性调整到1000，造诣调整到1000。若人物寿命达到1000，将此人全属性置0，全属性发挥置-500，年龄置5";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    value=GameData.Characters.GetCharProperty(actor_id_2,11).ParseInt()==1000;
                    if(value){
                        if(GameData.Characters.HasChar(actor_id_2)){
                            for(int i=0;i<6;i++){
                                GameData.Characters.SetCharProperty(actor_id_2, 61+i, "1000");
                            }
                            for(int i=0;i<16;i++){
                                GameData.Characters.SetCharProperty(actor_id_2, 501+i, "1000");
                            }
                            for(int i=0;i<14;i++){
                                GameData.Characters.SetCharProperty(actor_id_2, 601+i, "1000");
                            }
                            GameData.Characters.SetCharProperty(actor_id_2, 11,"1000");
                        }
                    }else{
                        if(GameData.Characters.HasChar(actor_id_2)){
                            for(int i=0;i<6;i++){
                                GameData.Characters.SetCharProperty(actor_id_2, 61+i, "0");
                            }
                            for(int i=0;i<16;i++){
                                GameData.Characters.SetCharProperty(actor_id_2, 501+i, "0");
                            }
                            for(int i=0;i<14;i++){
                                GameData.Characters.SetCharProperty(actor_id_2, 601+i, "0");
                            }
                            for(int i=0;i<12;i++){
                                GameData.Characters.SetCharProperty(actor_id_2, 1361+i, "-500");
                            }
                            GameData.Characters.SetCharProperty(actor_id_2, 11,"5");
                        }
                    }
                };
                container2.Children.Add(taiwuToggle);
                var ytaiwuToggle=new TaiwuToggle();
                ytaiwuToggle.isOn = false;
                ytaiwuToggle.TipTitle = "说明";
                ytaiwuToggle.Text = "置年龄为5/15";
                ytaiwuToggle.TipContant = "将5岁小孩子年龄改成15岁，其他人年龄改成5岁";
                ytaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if((!Main.ui.refresh)&&GameData.Characters.HasCharProperty(actor_id_2, 11)){
                        if(GameData.Characters.GetCharProperty(actor_id_2,11).ParseInt()==5){
                            GameData.Characters.SetCharProperty(actor_id_2, 11,"15");
                        }else{
                            GameData.Characters.SetCharProperty(actor_id_2, 11,"5");
                        }
                        refresh2();
                    }
                };
                container2.Children.Add(ytaiwuToggle);

                var ctaiwuToggle=new TaiwuToggle();
                ctaiwuToggle.isOn = false;
                ctaiwuToggle.TipTitle = "说明";
                ctaiwuToggle.Text = "万般善恶";
                ctaiwuToggle.TipContant = "把此人立场改成仁善，名誉改为万善翁";
                ctaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(!Main.ui.refresh){
                        GameData.Characters.SetCharProperty(actor_id_2, 16, value?"126":"1000");//仁善，非常偏向刚正，方便随时切立场惩戒
                        //DateFile.instance.RemoveActorFameTime(actor_id_2,false,120,100);
                        if(!DateFile.instance.actorFame.ContainsKey(actor_id_2)){
                            DateFile.instance.SetActorFameList(actor_id_2, 1, 1, 0);//结交良善……以此创建actor_id_2的字典
                        }
                        foreach(int key in DateFile.instance.actorFameDate.Keys)
                            if(value^(DateFile.instance.actorFameDate[key][1].ParseInt()<0)){
                                var data=DateFile.instance.actorFameDate[key];
                                if(!DateFile.instance.actorFame[actor_id_2].ContainsKey(key)){
                                    DateFile.instance.actorFame[actor_id_2].Add(key,new int[]{0,0});
                                }
                                DateFile.instance.actorFame[actor_id_2][key][0]=1000;
                                DateFile.instance.actorFame[actor_id_2][key][1]=data[2].ParseInt()*(data[3].ParseInt()==0?1:data[4].ParseInt());
                            } else if(DateFile.instance.actorFame[actor_id_2].ContainsKey(key))DateFile.instance.actorFame[actor_id_2].Remove(key);
                        refresh2();
                    }
                };
                container2.Children.Add(ctaiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "更改向往";
                taiwuToggle.TipContant = "更改目标人物向往门派\n为防止出现BUG，仅在15个门派中循环，对15门派以外的人，此按钮无效。\n\n该选项将会影响商人出售物品种类，具体影响如下\n  服牛帮：2 5 15（峨眉派 元山派 血犼教）\n  文山书海阁：1 7（少林派 然山派）\n  五湖商会：4 12（武当派 五仙教）\n  大武魁商号：6 11（狮相门 金刚宗）\n  回春堂：3 10（百花谷 空桑派）\n  公输坊：9 14（铸剑山庄 伏龙坛）\n  奇货斋：8 13（璇女派 界青门）";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(!Main.ui.refresh){
                        int target=DateFile.instance.GetActorDate(actor_id_2, 9, false).ParseInt();
                        if(1<=target && target<=15){
                            GameData.Characters.SetCharProperty(actor_id_2, 9, ((target%15)+1).ToString());//设置向往门派
                            y.refresh();
                        }
                    }
                };
                container2.Children.Add(taiwuToggle);

                var xxtaiwuToggle=new TaiwuToggle();
                xxtaiwuToggle.isOn = false;
                xxtaiwuToggle.TipTitle = "说明";
                xxtaiwuToggle.Text = "入魔";
                xxtaiwuToggle.TipContant = "令此人入魔值变为500，瞬间入魔";
                xxtaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(!Main.ui.refresh){
                        if(value){
                            DateFile.instance.SetActorXXChange(actor_id_2, 500, true, true);
                        }else{
                            DateFile.instance.SetActorXXChange(actor_id_2, 0, true, true);
                        }
                        refresh2();
                    }
                };
                container2.Children.Add(xxtaiwuToggle);

                container2 = new Container();
                container2.Element.PreferredSize.Add(0f);
                container2.Element.PreferredSize.Add(60f);
                container2.Group.Direction = (Direction)0;
                container2.Group.Spacing = 10f;
                container.Children.Add(container2);

                int actor_id_3=0;
                string ztext="";
                var z=new Sub("令此人",(string value, InputField inputField)=>{
                    actor_id_3=value.ParseInt();
                    if(actor_id_3!=0 && GameData.Characters.HasChar(actor_id_3)){
                        ztext="令"+DateFile.instance.SetColoer(10004, DateFile.instance.GetActorName(actor_id_3, false, false), false)+"("+DateFile.instance.SetColoer(20011, actor_id_3.ToString(), false)+")";
                    }else if(actor_id_3==0){
                        inputField.Text="0";
                        actor_id_3=0;
                    }else if(!GameData.Characters.HasChar(actor_id_3)){
                        inputField.Text="0";
                        ztext="令（<color=#E4504DFF>Error：</color>人物id不存在）";
                    }else{
                        inputField.Text="0";
                        ztext="令（<color=#E4504DFF>Error：</color>未知错误）";
                    }
                    refresh2();
                });
                z.refresh=()=>{z.lab.Text=ztext;};
                z.Addto(container2);

                var btaiwuToggle=new TaiwuToggle();
                btaiwuToggle.isOn = false;
                btaiwuToggle.TipTitle = "说明";
                btaiwuToggle.Text = "立刻生子";
                btaiwuToggle.TipContant = "如果选定人物怀孕，则立刻让选定人物生下一个孩子\n请确保不要在读档之后立刻使用，否则会触发bug";
                btaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if((!Main.ui.refresh)){
                        if(DateFile.instance.actorLife[actor_id_3].ContainsKey(901)){
                            if(value && DateFile.instance.actorLife[actor_id_3][901][0]<0){// value在前文判定过，的确是DateFile.instance.actorLife[actor_id_3][901][0]<0
                                DateFile.instance.actorLife[actor_id_3].Remove(901);
                            }else if(DateFile.instance.actorLife[actor_id_3][901][0]>0){
                                if(UIDate.instance.changTrunEvents==null)UIDate.instance.changTrunEvents=new List<int[]>();
                                List<int> actorAtPlace;
                                if(actor_id_3==DateFile.instance.mianActorId) actorAtPlace=new List<int> {DateFile.instance.mianPartId, DateFile.instance.mianPlaceId};
                                else actorAtPlace=DateFile.instance.GetActorAtPlace(actor_id_3);
                                if(actorAtPlace != null){
                                    AI.CharacterAge.OnChildbirth(actor_id_3, actorAtPlace[0], actorAtPlace[1], actor_id_3, DateFile.instance.mianActorId);
                                }
                            }
                        }else if(GameData.Characters.HasChar(actor_id_2) && GameData.Characters.HasChar(actor_id_3)){
                            int num = int.Parse(DateFile.instance.GetActorDate(actor_id_2, 14, false));
                            if (num != int.Parse(DateFile.instance.GetActorDate(actor_id_3, 14, false)))
                            {
                                PeopleLifeAI.instance.AISetChildren((num == 1) ? actor_id_2 : actor_id_3, (num == 1) ? actor_id_3 : actor_id_2, 1, 1);
                            }
                            else
                            {
                                DateFile.instance.ChangeActorFeature(actor_id_2, 4001, 4002);
                                DateFile.instance.ChangeActorFeature(actor_id_3, 4001, 4002);
                                DateFile.instance.SetActorFameList(actor_id_2, 401, 5, 0);
                                DateFile.instance.SetActorFameList(actor_id_3, 401, 5, 0);
                                GEvent.OnEvent(eEvents.Copulate, new object[]
                                {
                                    actor_id_2,
                                    actor_id_3
                                });
                            }
                            /*
                            // 模拟怀孕逻辑
                            int fatherId=actor_id_2;
                            int motherId=actor_id_3;
                            GEvent.OnEvent(eEvents.Copulate, new object[] {fatherId, motherId});
                            DateFile.instance.actorLife[motherId].Add(901, new List<int>
                            {
                                UnityEngine.Random.Range(7, 10),
                                fatherId,
                                motherId,
                                setFather,
                                setMother
                            });
                            DateFile.instance.pregnantFeature.Add(motherId, new string[]
                            {
                                Characters.GetCharProperty(fatherId, 101),
                                Characters.GetCharProperty(motherId, 101)
                            });*/
                        }
                        refresh2();
                    }
                };
                container2.Children.Add(btaiwuToggle);

                var mtaiwuToggle=new TaiwuToggle();
                mtaiwuToggle.isOn = false;
                mtaiwuToggle.TipTitle = "说明";
                mtaiwuToggle.Text = "结婚";
                mtaiwuToggle.TipContant = "与上一行指定的人物结婚";
                mtaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        if(GameData.Characters.HasChar(actor_id_2) && GameData.Characters.HasChar(actor_id_3)){
                            if(int.Parse(DateFile.instance.GetActorDate(actor_id_2, 14, false))==int.Parse(DateFile.instance.GetActorDate(actor_id_3, 14, false)) && (!(mtaiwuToggle.Text == "同性结婚？确认吗？"))){
                                mtaiwuToggle.Text = "同性结婚？";
                            }else{
                                DateFile.instance.AddSocial(actor_id_2, actor_id_3, 306);
                                DateFile.instance.AddSocial(actor_id_3, actor_id_2, 306);
                                DateFile.instance.AddSocial(actor_id_2, actor_id_3, 309);
                                DateFile.instance.AddSocial(actor_id_3, actor_id_2, 309);
                                DateFile.instance.AddSocial(actor_id_2, actor_id_3, 312);
                                DateFile.instance.AddSocial(actor_id_3, actor_id_2, 312);
                            }
                            tg.isOn=false;
                        }else{
                            mtaiwuToggle.Text="Error?";
                            mtaiwuToggle.TipContant = "出现了一个错误，可能是两个actor_id中有一个不存在导致的\n这个错误不影响游戏的正常进行，但你需要单击开关来确认这个错误。";
                        }
                    }else{
                        mtaiwuToggle.Text="结婚";
                        mtaiwuToggle.TipContant = "与上一行指定的人物结婚";
                    }
                };
                container2.Children.Add(mtaiwuToggle);

                var gtaiwuToggle=new TaiwuToggle();
                gtaiwuToggle.isOn = false;
                gtaiwuToggle.TipTitle = "说明";
                gtaiwuToggle.Text = "群婚";
                gtaiwuToggle.TipContant = "当选定人物性别与上一行指定的人物相同时，可以开启群婚指令，令选定人物与每一个上一行指定人物的配偶结婚";
                gtaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        if(GameData.Characters.HasChar(actor_id_2) && GameData.Characters.HasChar(actor_id_3)){
                            if(int.Parse(DateFile.instance.GetActorDate(actor_id_2, 14, false))==int.Parse(DateFile.instance.GetActorDate(actor_id_3, 14, false))){
                                foreach(int num223 in from s in DateFile.instance.GetActorSocial(actor_id_2, 309, true, false) orderby s select s){
                                    DateFile.instance.AddSocial(num223, actor_id_3, 306);
                                    DateFile.instance.AddSocial(actor_id_3, num223, 306);
                                    DateFile.instance.AddSocial(num223, actor_id_3, 309);
                                    DateFile.instance.AddSocial(actor_id_3, num223, 309);
                                    DateFile.instance.AddSocial(num223, actor_id_3, 312);
                                    DateFile.instance.AddSocial(actor_id_3, num223, 312);
                                }
                                tg.isOn=false;
                            }else{
                                gtaiwuToggle.Text="Error?";
                                gtaiwuToggle.TipContant = "目前只支持二部图的群婚，如果想做完全图的群婚，你需要在完全图里选择一个同性，在群婚之后与其结婚";
                            }
                        }else{
                            gtaiwuToggle.Text="Error?";
                            gtaiwuToggle.TipContant = "出现了一个错误，可能是两个actor_id中有一个不存在导致的\n这个错误不影响游戏的正常进行，但你需要单击开关来确认这个错误。";
                        }
                    }else{
                        gtaiwuToggle.Text = "群婚";
                        gtaiwuToggle.TipContant = "当选定人物性别与上一行指定的人物相同时，可以开启群婚指令，令选定人物与每一个上一行指定人物的配偶结婚";
                    }
                };
                container2.Children.Add(gtaiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "人形春药";
                taiwuToggle.TipContant = "令自己的每一个活着的结发夫妻触发“情难自已”，每一个“情难自已”都会取消所有女性妻子取消怀孕冷却，如果正在怀孕，将怀孕时间减小到过月生子（应当放心，这样可以正常生蛐蛐），之后与所有活着的结发夫妻（不论性别）春宵（同性春宵会有名誉问题）\n\n人多的时候或许会卡……如果这里是配合群婚直接生子而非减小怀孕冷却，真的会卡\n可能存在过月BUG，太吾使用前记得先过月";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        if(DateFile.instance.HaveLifeDate(actor_id_3, 309)){
                            if(UIDate.instance.changTrunEvents==null)UIDate.instance.changTrunEvents=new List<int[]>();
                            foreach(int actorId1 in from s in DateFile.instance.GetActorSocial(actor_id_3, 309, false, false) orderby s select s)if(DateFile.instance.HaveLifeDate(actorId1, 309)){
                                foreach(int actorId2 in from s in DateFile.instance.GetActorSocial(actorId1, 309, false, false) orderby s select s){
                                    if(DateFile.instance.actorLife.ContainsKey(actorId2)&&DateFile.instance.actorLife[actorId2].ContainsKey(901)){
                                        if(DateFile.instance.actorLife[actorId2][901][0]<0)
                                            DateFile.instance.actorLife[actorId2].Remove(901);
                                        else{
                                            DateFile.instance.actorLife[actorId2][901][0]-=(DateFile.instance.actorLife[actorId2][901][0]%1000)-1;
                                        }
                                    }
                                    int num = int.Parse(DateFile.instance.GetActorDate(actorId1, 14, false));
                                    bool flag3 = num != int.Parse(DateFile.instance.GetActorDate(actorId2, 14, false));
                                    if (flag3)
                                    {
                                        PeopleLifeAI.instance.AISetChildren((num == 1) ? actorId1 : actorId2, (num == 1) ? actorId2 : actorId1, 1, 1);
                                    }
                                    else
                                    {
                                        DateFile.instance.ChangeActorFeature(actorId1, 4001, 4002);
                                        DateFile.instance.ChangeActorFeature(actorId2, 4001, 4002);
                                        DateFile.instance.SetActorFameList(actorId1, 401, 5, 0);
                                        DateFile.instance.SetActorFameList(actorId2, 401, 5, 0);
                                        GEvent.OnEvent(eEvents.Copulate, new object[]
                                        {
                                            actorId1,
                                            actorId2
                                        });
                                    }
                                }
                            }
                        }
                        refresh2();//刷新开关状态，因为这里很可能修改了怀孕状态，需要刷新。
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "强力推举";
                taiwuToggle.TipContant = "令全部结发夫妻为二品，结发夫妻的结发夫妻为一品（方便区别二部图）\n遍历顺序按结发夫妻id优先，对id a<b<c，若abc两两为结发夫妻，对c使用推举，会使得b为二品而a为一品\n会触发过月BUG，记得使用前先过月。";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        if(DateFile.instance.HaveLifeDate(actor_id_3, 309)){
                            var second=new SortedSet<int>();
                            var first=new SortedSet<int>();
                            foreach(int actorId1 in DateFile.instance.GetActorSocial(actor_id_3, 309, false, false))if(DateFile.instance.HaveLifeDate(actorId1, 309)){
                                second.Add(actorId1);
                                foreach(int actorId2 in from s in DateFile.instance.GetActorSocial(actorId1, 309, false, false) orderby s select s){
                                    first.Add(actorId2);
                                }
                            }
                            foreach(int actorId1 in second){ // 执行完整的二品身份晋升流程
                                int toGangId = int.Parse(DateFile.instance.GetActorDate(actorId1, 19, true));
                                if(((uint)(toGangId-1))<15){
                                    //DateFile.instance.GangActorLevelUp(actorId1, taiwuVillage, 9);
                                    for(int level=9;level>1;level--)
                                        DateFile.instance.GangActorLevelUp(actorId1, toGangId, level);
                                }
                            }
                            if(UIDate.instance.changTrunEvents==null)UIDate.instance.changTrunEvents=new List<int[]>();
                            foreach(int actorId1 in first){ // 执行完整的一品身份晋升流程
                                int toGangId = int.Parse(DateFile.instance.GetActorDate(actorId1, 19, true));
                                if(((uint)(toGangId-1))<15){
                                    //DateFile.instance.GangActorLevelUp(actorId1, taiwuVillage, 9);
                                    for(int level=9;level>0;level--)
                                        DateFile.instance.GangActorLevelUp(actorId1, toGangId, level);
                                }
                            }
                            // 多次晋升的好处是会给自己分配一群师父
                        }
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "离婚";
                taiwuToggle.TipContant = "与所有结发夫妻离婚（无论死活），方便之后再次结婚，方便太吾主动生猴子";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        if(DateFile.instance.HaveLifeDate(actor_id_3, 309)){
                            foreach(int num223 in DateFile.instance.GetActorSocial(actor_id_3, 309, true, false)){
                                DateFile.instance.RemoveActorSocial(actor_id_3, num223, 309);
                                DateFile.instance.RemoveActorSocial(num223, actor_id_3, 309);
                            }
                        }
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "绝义";
                taiwuToggle.TipContant = "与所有知心和金兰断交，离婚并删除所有倾心爱慕和两情相悦（无论死活）";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        if(DateFile.instance.HaveLifeDate(actor_id_3, 301)){
                            foreach(int num223 in DateFile.instance.GetActorSocial(actor_id_3, 301, true, false)){
                                DateFile.instance.RemoveActorSocial(actor_id_3, num223, 301);
                                DateFile.instance.RemoveActorSocial(num223, actor_id_3, 301);
                            }
                        }
                        if(DateFile.instance.HaveLifeDate(actor_id_3, 308)){
                            foreach(int num223 in DateFile.instance.GetActorSocial(actor_id_3, 308, true, false)){
                                DateFile.instance.RemoveActorSocial(actor_id_3, num223, 308);
                                DateFile.instance.RemoveActorSocial(num223, actor_id_3, 308);
                            }
                        }
                        foreach(int i in new int[]{306,309,312})if(DateFile.instance.HaveLifeDate(actor_id_3, i)){
                            foreach(int num223 in DateFile.instance.GetActorSocial(actor_id_3, i, true, false)){
                                DateFile.instance.RemoveActorSocial(actor_id_3, num223, 306);
                                DateFile.instance.RemoveActorSocial(num223, actor_id_3, 306);
                                DateFile.instance.RemoveActorSocial(actor_id_3, num223, 309);
                                DateFile.instance.RemoveActorSocial(num223, actor_id_3, 309);
                                DateFile.instance.RemoveActorSocial(actor_id_3, num223, 312);
                                DateFile.instance.RemoveActorSocial(num223, actor_id_3, 312);
                            }
                        }
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "斩尘缘";
                taiwuToggle.TipContant = "计划是断掉全部知心，金兰和结发夫妻，但现在只探明了312是倾心爱慕，306是两情相悦，309是结发夫妻，于是这里从300开始一直删到312，理论上应该可以删干净。";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    for(int i=300;i<313;i++)
                    if (DateFile.instance.HaveLifeDate(actor_id_3, i)){
                        foreach(int num223 in DateFile.instance.GetActorSocial(actor_id_3, i, true, false)){
                            DateFile.instance.RemoveActorSocial(actor_id_3, num223, i);
                            for(int j=300;j<313;j++)DateFile.instance.RemoveActorSocial(num223, actor_id_3, j);
                        }
                    }
                    tg.isOn=false;
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "苦海难渡";
                taiwuToggle.TipContant = "令此人的全部活着的友好关系（不包括太吾）与此人变为双向血海深仇";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    for(int i=300;i<313;i++)
                    if (DateFile.instance.HaveLifeDate(actor_id_3, i)){
                        foreach(int num223 in DateFile.instance.GetActorSocial(actor_id_3, i, false, false)){
                            if(num223!=DateFile.instance.mianActorId){
                                DateFile.instance.AddSocial(num223, actor_id_3, 402);
                                DateFile.instance.AddSocial(actor_id_3, num223, 402);
                            }
                        }
                    }
                    tg.isOn=false;
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "泯恩仇";
                taiwuToggle.TipContant = "清除所有人对此人的仇恨，之后将此人心情调整到乐极\n如果选中了太吾，将太吾添加到所有仇人的恩深义重列表，并令所有仇人对太吾倾心爱慕，好感度改为60000\n....否则清除此人对所有人的仇恨";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        foreach (int actor in DateFile.instance.actorLife.Keys)
                        {
                            bool flag=false;
                            if (DateFile.instance.GetActorSocial(actor, 402, false, false).Contains(actor_id_3))
                            {
                                DateFile.instance.RemoveActorSocial(actor, actor_id_3, 402);
                                flag=true;
                            }
                            if (DateFile.instance.GetActorSocial(actor, 401, false, false).Contains(actor_id_3))
                            {
                                DateFile.instance.RemoveActorSocial(actor, actor_id_3, 401);
                                flag=true;
                            }
                            if(flag && actor_id_3==DateFile.instance.mianActorId){
                                DateFile.instance.ChangeFavor(actor, 600000, true, true);
                                DateFile.instance.AddSocial(actor, actor_id_3, 307);
                            }
                        }
                        if(actor_id_3!=DateFile.instance.mianActorId){
                            if(DateFile.instance.actorLife.ContainsKey(actor_id_3))
                                for(int index=401;index<=402;index++)
                                    if(DateFile.instance.actorLife[actor_id_3].ContainsKey(index))
                                        DateFile.instance.actorLife[actor_id_3].Remove(index);
                        }
                        GameData.Characters.SetCharProperty(actor_id_3, 4, "100");//心情，乐极
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                container2 = new Container();
                container2.Element.PreferredSize.Add(0f);
                container2.Element.PreferredSize.Add(60f);
                container2.Group.Direction = (Direction)0;
                container2.Group.Spacing = 10f;
                container.Children.Add(container2);

                var wtaiwuToggle=new TaiwuToggle();
                wtaiwuToggle.isOn = false;
                wtaiwuToggle.TipTitle = "说明";
                wtaiwuToggle.Text = "切到太吾";
                wtaiwuToggle.TipContant = "以太吾的id填入第一行选框，在当前人物为太吾时，这个开关可以让太吾毕业。";
                wtaiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(!Main.ui.refresh){
                        if(value){
                            actor_id_2=DateFile.instance.mianActorId;
                            y.taiwuInputField.Text=actor_id_2.ToString();
                            y.refresh();
                        }else{
                            for(int i=1;i<145;i++){
                                if (DateFile.instance.skillBookPages.ContainsKey(i)){
                                    for(int j=0;j<10;j++)DateFile.instance.skillBookPages[i][j]=1;
                                }else{
                                    DateFile.instance.skillBookPages[i]=new int[]{1,1,1,1,1,1,1,1,1,1};
                                }
                                if(DateFile.instance.actorSkills.ContainsKey(i)){
                                    DateFile.instance.actorSkills[i][0]=100;
                                    DateFile.instance.actorSkills[i][1]=10;
                                }else{
                                    DateFile.instance.actorSkills[i]=new int[]{100,10};
                                }
                            }
                            if(!DateFile.instance.actorGongFas.ContainsKey(actor_id_2))DateFile.instance.actorGongFas.Add(actor_id_2,new SortedDictionary<int, int[]>());
                            foreach(int i in DateFile.instance.gongFaDate.Keys)if(i<160000){
                                if (DateFile.instance.gongFaBookPages.ContainsKey(i)){
                                    for(int l=0;l<10;l++)DateFile.instance.gongFaBookPages[i][l]=1;
                                }else{
                                    DateFile.instance.gongFaBookPages[i]=new int[]{1,1,1,1,1,1,1,1,1,1};
                                }
                                if(DateFile.instance.actorGongFas[actor_id_2].ContainsKey(i)){
                                    DateFile.instance.actorGongFas[actor_id_2][i][0]=100;
                                    DateFile.instance.actorGongFas[actor_id_2][i][1]=10;
                                }else{
                                    DateFile.instance.actorGongFas[actor_id_2][i]=new int[]{100,10,0};
                                }
                            }
                            for(int i=1;i<16;i++){
                                if (DateFile.instance.GangPartValueAdd.ContainsKey(i))
                                {
                                    DateFile.instance.GangPartValueAdd[i]=1000;
                                } else {
                                    DateFile.instance.GangPartValueAdd.Add(i, 1000);
                                }
                            }
                        }
                        refresh2();
                    }
                };
                container2.Children.Add(wtaiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "基础引子";
                taiwuToggle.TipContant = "向仓库增加最高级引子";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        //Warehouse.instance.ChangeItem
                        foreach(int itemId in new int[]{3007,3014,3107,3114,3207,3214,3307,3314,4004,4008,4012,4016,4020,4024,4028,4032,4036,4040,4044,4048,4052,4056,4060,4064,4068,4072,4076,4080,4084,4088,4092,4096,4207,4214,4221,4228,4235,4242})
                            DateFile.instance.GetItem(-999, itemId, 1, false);
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "精制材料";
                taiwuToggle.TipContant = "向仓库增加最高级精制材料";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        //Warehouse.instance.ChangeItem
                        foreach(int itemId in new int[]{3507,3514,3607,3614,3707,3714,3807,3814})
                            DateFile.instance.GetItem(-999, itemId, 10, false);
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "无限捆仙绳";
                taiwuToggle.TipContant = "你知道你要干什么";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        //Warehouse.instance.ChangeItem
                        DateFile.instance.GetItem(-999, 74109, 1, false);//false会让捆仙绳的耐久锁定在0/-1，于是捆仙绳耐久不会继续消耗。
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);
                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "无限毒药";
                taiwuToggle.TipContant = "你知道你要干什么";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        //Warehouse.instance.ChangeItem
                        foreach(int itemId in new int[]{100009,100018,100027,100036,100045,100054,100112,100124,100136,100147,100148,100184,100196,100208,100220,100232,100244})
                            DateFile.instance.GetItem(-999, itemId, 1, false);//false会让毒药的耐久锁定在0/-1，于是耐久不会继续消耗。这里多给了一个二品疗伤药，用途是淬毒。
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "七只蛐蛐";
                taiwuToggle.TipContant = "对应单项七元属性max的蛐蛐，附带超级加强";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        for(int colorId=15;colorId<=21;colorId++){
                            int actorId = DateFile.instance.MianActorID();
                            int itemId = DateFile.instance.MakeNewItem(10000, 0, 0, 50, 20);
//                             List<int> list = new List<int>(DateFile.instance.cricketDate.Keys);
//                             List<int> list2 = new List<int>();
//                             for (int i = 0; i < list.Count; i++)
//                             {
//                                 int num = list[i];
//                                 bool flag = int.Parse(DateFile.instance.cricketDate[num][5]) == 1;
//                                 if (flag)
//                                 {
//                                     for (int j = 0; j < int.Parse(DateFile.instance.cricketDate[num][6]); j++)
//                                     {
//                                         list2.Add(num);
//                                     }
//                                 }
//                             }
//                             int colorId = list2[UnityEngine.Random.Range(0, list2.Count)];
                            int partId = 0;
                            GetQuquWindow.instance.MakeQuqu(itemId, colorId, partId);
                            GameData.Items.SetItemProperty(itemId, 2006, "1000");// 蛐蛐胜场
                            GameData.Items.SetItemProperty(itemId, 2007, "0");// 蛐蛐蜇龄


                            DateFile.instance.AddCricketValue(itemId, 98, 10000);//寿命+10000
                            DateFile.instance.AddCricketValue(itemId, 21, 100);//三属性+100
                            DateFile.instance.AddCricketValue(itemId, 22, 100);//三属性+100
                            DateFile.instance.AddCricketValue(itemId, 23, 100);//三属性+100
                            DateFile.instance.AddCricketValue(itemId, 31, 100);//暴击概率+100
                            DateFile.instance.AddCricketValue(itemId, 36, 100);//击伤概率+100
                            DateFile.instance.AddCricketValue(itemId, 33, 100);//格挡概率+100
                            DateFile.instance.AddCricketValue(itemId, 34, 100);//格挡减伤+100
                            DateFile.instance.GetItem(actorId, itemId, 1, false, 0, 0);
                        }
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "这是非常明显的作弊功能";
                taiwuToggle.Text = "神九品装";
                taiwuToggle.TipContant = "九品软木持，铁刀，铁剑，软布拂尘，软布玄，软木针匣，软木笛，软布鞭，软木琴各1件（10x各种铸剑精炼，并将武器发挥上限设为500%）\n一个反向精制破甲的九品软布鞭防止烁光烁血\n写作九品但破刃坚韧为100且全部属性为该部位最高值的防具三件套";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        foreach(int itemId in new int[]{82910,52901,52501,72501,72301,80301,81101,72701,83101}){
                            int weapon_id=DateFile.instance.MakeNewItem(itemId, 0, 0, 999, 999);
                            DateFile.instance.ChangItemDate(weapon_id, 711, 10*(500-int.Parse(DateFile.instance.GetItemDate(weapon_id, 711, true, -1))), false);//发挥
                            DateFile.instance.ChangItemDate(weapon_id, 603, 10*(10000-int.Parse(DateFile.instance.GetItemDate(weapon_id, 603, true, -1))), false);//坚韧
                            for(int i=0;i<10;i++)specific_weapon_call(weapon_id,new int[]{
                                109,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.GongBuDuYiJian(0,0,false));
                                110,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ShengXieCanJian(0,0,false));
                                111,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.LongYuanQiXingJianFa(0,0,false));
                                105,106,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ChunJunJianQi(0,0,false));
                                103,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.JuQueQianJunJian(0,0,false));
                                110,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ShengXieCanJian(0,0,false));
                                108,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ShiErLuYuChangCiJian(0,0,false));
                                112,113,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.TaiEWuLiangJian(0,0,false));
                                101,102,107,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ZhanLuJianFa(0,0,false));
                            });
                            DateFile.instance.actorItemsDate[-999].Add(weapon_id,1);
                        }
                        {
                            int weapon_id=DateFile.instance.MakeNewItem(72701, 0, 0, 999, 999);
                            DateFile.instance.ChangItemDate(weapon_id, 711, 10*(500-int.Parse(DateFile.instance.GetItemDate(weapon_id, 711, true, -1))), false);//发挥
                            DateFile.instance.ChangItemDate(weapon_id, 603, 10*(50000-int.Parse(DateFile.instance.GetItemDate(weapon_id, 603, true, -1))), false);//坚韧
                            for(int i=0;i<10;i++)specific_weapon_call(weapon_id,new int[]{
                                109,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.GongBuDuYiJian(0,0,false));
                                110,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ShengXieCanJian(0,0,false));
                                111,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.LongYuanQiXingJianFa(0,0,false));
                                105,106,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ChunJunJianQi(0,0,false));
                                103,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.JuQueQianJunJian(0,0,false));
                                110,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ShengXieCanJian(0,0,false));
                                108,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ShiErLuYuChangCiJian(0,0,false));
                                112,113,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.TaiEWuLiangJian(0,0,false));
                                101,102,107,//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ZhanLuJianFa(0,0,false));
                            });
                            DateFile.instance.ChangItemDate(weapon_id, 601, 10*(0-int.Parse(DateFile.instance.GetItemDate(weapon_id, 601, true, -1))), false);//破甲，由于小于0的破甲无效，这里将破甲设置为0
                            DateFile.instance.actorItemsDate[-999].Add(weapon_id,1);
                        }
                        {
                            int weapon_id=DateFile.instance.MakeNewItem(71101, 0, 0, 999, 999);//纨素巾 施展速度13 内息18
                            GameData.Items.SetItemProperty(weapon_id, 603, "50000");//坚韧
                            GameData.Items.SetItemProperty(weapon_id, 601, "10000");//破刃
                            GameData.Items.SetItemProperty(weapon_id, 51361, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51362, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51363, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51364, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51365, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51366, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 50022, "110"); // 守御效率+110 昭辰束
                            GameData.Items.SetItemProperty(weapon_id, 50071, "100"); // 力道+100 九头蛟
                            GameData.Items.SetItemProperty(weapon_id, 50072, "100"); // 精妙+100 空明冠
                            GameData.Items.SetItemProperty(weapon_id, 50073, "100"); // 迅疾+100 紫鸢吟
                            GameData.Items.SetItemProperty(weapon_id, 50081, "50"); // 护体+50 玄铁笠
                            GameData.Items.SetItemProperty(weapon_id, 50082, "50"); // 御气+50 蚩尤角
                            GameData.Items.SetItemProperty(weapon_id, 50083, "100"); // 内息+100 太始苍束
                            GameData.Items.SetItemProperty(weapon_id, 50084, "100"); // 卸力+100 轩辕冕
                            GameData.Items.SetItemProperty(weapon_id, 50085, "100"); // 拆招+100 金霞冠
                            GameData.Items.SetItemProperty(weapon_id, 50086, "100"); // 闪避+100 天蚕束
                            GameData.Items.SetItemProperty(weapon_id, 51101, "25"); // 提气速度0.25 混沌玄巾
                            GameData.Items.SetItemProperty(weapon_id, 51102, "25"); // 架势速度0.25 蝉壳宝束
                            GameData.Items.SetItemProperty(weapon_id, 51106, "60"); // 施展速度60% 天蚕束
                            //GameData.Items.SetItemProperty(weapon_id, 50023, "1000"); // 疗伤效率
                            //GameData.Items.SetItemProperty(weapon_id, 51108, "1000"); // 武器切换
                            //GameData.Items.SetItemProperty(weapon_id, 51109, "1000"); // 移动速度
                            //GameData.Items.SetItemProperty(weapon_id, 51110, "1000"); // 驱毒效率
                            //GameData.Items.SetItemProperty(weapon_id, 51369, "1000"); // 内息%
                            DateFile.instance.actorItemsDate[-999].Add(weapon_id,1);
                        }
                        {
                            int weapon_id=DateFile.instance.MakeNewItem(71901, 0, 0, 999, 999);//布帛鞋 移动速度13 内息18
                            GameData.Items.SetItemProperty(weapon_id, 603, "50000");//坚韧
                            GameData.Items.SetItemProperty(weapon_id, 601, "10000");//破刃
                            GameData.Items.SetItemProperty(weapon_id, 51361, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51362, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51363, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51364, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51365, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51366, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 50022, "110"); // 守御效率+110 魁龙屐
                            GameData.Items.SetItemProperty(weapon_id, 50071, "100"); // 力道+100 金甲足
                            GameData.Items.SetItemProperty(weapon_id, 50072, "100"); // 精妙+100 昆仑足
                            GameData.Items.SetItemProperty(weapon_id, 50073, "100"); // 迅疾+100 枯荣由它
                            GameData.Items.SetItemProperty(weapon_id, 50081, "50"); // 护体+50 囚神锁
                            GameData.Items.SetItemProperty(weapon_id, 50082, "50"); // 御气+50 九星履
                            GameData.Items.SetItemProperty(weapon_id, 50083, "100"); // 内息+100 玄都宝靴
                            GameData.Items.SetItemProperty(weapon_id, 50084, "100"); // 卸力+100 五浊娑婆
                            GameData.Items.SetItemProperty(weapon_id, 50085, "100"); // 拆招+100 辟地神行
                            GameData.Items.SetItemProperty(weapon_id, 50086, "100"); // 闪避+100 凌波仙履
                            GameData.Items.SetItemProperty(weapon_id, 51101, "30"); // 提气速度0.30 蝉壳靴
                            GameData.Items.SetItemProperty(weapon_id, 51102, "30"); // 架势速度0.30 太冲神靴
                            GameData.Items.SetItemProperty(weapon_id, 51109, "60"); // 移动速度0.60 凌波仙履
                            //GameData.Items.SetItemProperty(weapon_id, 50023, "1000"); // 疗伤效率
                            //GameData.Items.SetItemProperty(weapon_id, 51106, "1000"); // 施展速度
                            //GameData.Items.SetItemProperty(weapon_id, 51108, "1000"); // 武器切换
                            //GameData.Items.SetItemProperty(weapon_id, 51110, "1000"); // 驱毒效率
                            //GameData.Items.SetItemProperty(weapon_id, 51369, "1000"); // 内息%
                            DateFile.instance.actorItemsDate[-999].Add(weapon_id,1);
                        }
                        {
                            int weapon_id=DateFile.instance.MakeNewItem(73501, 0, 0, 999, 999);//贯头衣 护体+18 御气+24
                            GameData.Items.SetItemProperty(weapon_id, 603, "50000");//坚韧
                            GameData.Items.SetItemProperty(weapon_id, 601, "10000");//破刃
                            GameData.Items.SetItemProperty(weapon_id, 51361, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51362, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51363, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51364, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51365, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 51366, "40");//六维发挥
                            GameData.Items.SetItemProperty(weapon_id, 50081, "260"); // 护体+260 青玄宝铠
                            GameData.Items.SetItemProperty(weapon_id, 50082, "220"); // 御气+220 衲元衫
                            DateFile.instance.actorItemsDate[-999].Add(weapon_id,1);
                        }
                    }
                    tg.isOn=false;
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "理论上游戏中可以得到这些装备";
                taiwuToggle.Text = "高级装备";
                taiwuToggle.TipContant = "软布防具+马+工布剑x1+工布大砍刀x3+龙渊软木持x1，所有装备仿照遗惠逻辑生成";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        //Warehouse.instance.ChangeItem
                        foreach(int itemId in new int[]{52509,52909,52909,52909}){
                            int weapon_id=DateFile.instance.MakeNewItem(itemId, 0, 0, 999, 999);
                            for(int i=0;i<10;i++)specific_weapon_call(weapon_id,new int[]{109});//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.GongBuDuYiJian(0,0,false));
                            DateFile.instance.actorItemsDate[-999].Add(weapon_id,1);
                        }
                        foreach(int itemId in new int[]{70909,71709,72809,83609}){
                            int weapon_id=DateFile.instance.MakeNewItem(itemId, 0, 0, 999, 999);
                            DateFile.instance.actorItemsDate[-999].Add(weapon_id,1);
                        }
                        foreach(int itemId in new int[]{82918}){
                            int weapon_id=DateFile.instance.MakeNewItem(itemId, 0, 0, 999, 999);
                            //for(int i=0;i<5;i++)specific_weapon_call(weapon_id,new int[]{110});//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.ShengXieCanJian(0,0,false));
                            for(int i=0;i<10;i++)specific_weapon_call(weapon_id,new int[]{111});//new v2.SpecialEffects.AttackSkills.ZhuJianShanZhuang.Sword.LongYuanQiXingJianFa(0,0,false));
                            DateFile.instance.actorItemsDate[-999].Add(weapon_id,1);
                        }
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "武林礼包";
                taiwuToggle.TipContant = "万化灵符x1 师尊遗骨x1 赤血王珠x10 白鹿角x1";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        //Warehouse.instance.ChangeItem
                        foreach(int itemId in new int[]{31,32,34,35})
                            DateFile.instance.GetItem(-999, itemId, itemId==34?10:1, false);
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                taiwuToggle=new TaiwuToggle();
                taiwuToggle.isOn = false;
                taiwuToggle.TipTitle = "说明";
                taiwuToggle.Text = "蛊惑人心";
                taiwuToggle.TipContant = "令与太吾同一格的所有NPC化魔";
                taiwuToggle.onValueChanged = delegate(bool value, Toggle tg)
                {
                    if(value){
                        foreach(int actor in DateFile.instance.HaveActor(DateFile.instance.mianPartId, DateFile.instance.mianPlaceId,true,false,false)){
                            DateFile.instance.SetActorXXChange(actor, 500, true, true);
                        }
                        tg.isOn=false;
                    }
                };
                container2.Children.Add(taiwuToggle);

                refresh2=()=>{
                    Main.ui.refresh=true;
                    if(actor_id_2==0){
                        actor_id_2=DateFile.instance.mianActorId;
                        y.taiwuInputField.Text=actor_id_2.ToString();
                        y.refresh();
                    }
                    if(actor_id_3==0){
                        actor_id_3=DateFile.instance.mianActorId;
                        z.taiwuInputField.Text=actor_id_3.ToString();
                        ztext="令太吾("+actor_id_3+")";
                        z.refresh();
                    }
                    if(int.Parse(DateFile.instance.GetActorDate(actor_id_2, 997, false))!=0){
                        if(GameData.Characters.HasCharProperty(actor_id_2, 110)){
                            xtaiwuToggle.isOn = true;
                            xtaiwuToggle.Text="取消赛亚人";
                        }else{
                            xtaiwuToggle.isOn = false;
                            xtaiwuToggle.Text="变身赛亚人";
                        }
                        if(GameData.Characters.GetCharProperty(actor_id_2,11).ParseInt()==5){
                            ytaiwuToggle.isOn=true;
                            ytaiwuToggle.Text="置年龄为15";
                        }else{
                            ytaiwuToggle.isOn=false;
                            ytaiwuToggle.Text="置年龄为5";
                        }
                        if(GameData.Characters.GetCharProperty(actor_id_2,706).ParseInt()>0){
                            ztaiwuToggle.isOn=true;
                            ztaiwuToggle.Text="放气";
                            ztaiwuToggle.TipContant="人物存在无属性内气，点击开关给此人放气";
                        }else{
                            ztaiwuToggle.isOn=false;
                            ztaiwuToggle.Text="充气";
                            ztaiwuToggle.TipContant="人物不存在无属性内气，点击开关给此人充气";
                        }
                        if(actor_id_2==DateFile.instance.mianActorId){
                            wtaiwuToggle.isOn=true;
                            wtaiwuToggle.Text="太吾毕业";
                            wtaiwuToggle.TipContant="太吾满功法满技艺，全门派支持度撑满，只对太吾生效，仅在当前人物id为太吾id时生效，其他时候会将id指向太吾id";
                        }else{
                            wtaiwuToggle.isOn=false;
                            wtaiwuToggle.Text="切到太吾";
                            wtaiwuToggle.TipContant="以太吾的id填入第一行选框，在当前人物为太吾时，这个开关可以让太吾毕业。";
                        }
                        if(DateFile.instance.GetActorFame(actor_id_2)>0){
                            ctaiwuToggle.isOn=true;
                            ctaiwuToggle.Text = "万恶";
                            ctaiwuToggle.TipContant = "把此人立场改成唯我，名誉改为万恶翁";
                        }else{
                            ctaiwuToggle.isOn=false;
                            ctaiwuToggle.Text = "万善";
                            ctaiwuToggle.TipContant = "把此人立场改成仁善，名誉改为万善翁";
                        }
                        if(DateFile.instance.GetLifeDate(actor_id_2, 501, 0)>=200){
                            xxtaiwuToggle.isOn = true;
                            xxtaiwuToggle.Text = "伏魔";
                            xxtaiwuToggle.TipContant = "解除此人入魔状态";
                        }else{
                            xxtaiwuToggle.isOn = false;
                            xxtaiwuToggle.Text = "入魔";
                            xxtaiwuToggle.TipContant = "令此人入魔值变为500，瞬间入魔";
                        }
                        if(int.Parse(DateFile.instance.GetActorDate(actor_id_3, 997, false))!=0){
                            if(DateFile.instance.HaveLifeDate(actor_id_3, 901)){
                                btaiwuToggle.Interactable=true;
                                if(DateFile.instance.actorLife[actor_id_3][901][0]>0){
                                    btaiwuToggle.isOn=true;
                                    btaiwuToggle.Text = "立刻生子";
                                    btaiwuToggle.TipContant = "立刻让选定人物生下一个孩子\n已知BUG与hotfix：UIDate.instance.changTrunEvents在读档后为null，而mod的解法是将null改为new List<int[]>以防止报错\n（过月也可以给UIDate.instance.changTrunEvents赋值）";
                                    // 测试过 新开游戏之后过月，然后读档不过月使用此功能，仍然会因为某个变量未初始化而报错。使用此功能需保证读取存档后曾经过月
                                }else{
                                    btaiwuToggle.isOn=false;
                                    btaiwuToggle.Text = "怀孕冷却";
                                    btaiwuToggle.TipContant = "取消选定人物的怀孕CD";
                                }
                            }else{
                                btaiwuToggle.Interactable=false;
                                btaiwuToggle.isOn=false;
                                btaiwuToggle.Text = "春宵";
                                btaiwuToggle.TipContant = "当前人物不在怀孕/怀孕冷却阶段，此按钮因此改为春宵\n点击按钮时将触发此人与上一行选中人物的春宵（如果两个人的id可查）";
                            }
                        }
                    }else{xx.Text="在下方填写人物id，对指定人物增加特性...当前人物id无效";}
                    Main.ui.refresh=false;
                };
            }
            Mod.SettingUI = container_big;
        }
        public static void specific_weapon_call(int weapon_id, int[] normalAddEquipChangeId){
            foreach (int key in normalAddEquipChangeId)
            {
                DateFile.instance.ChangItemDate(weapon_id, int.Parse(DateFile.instance.changeEquipDate[key][2]), int.Parse(DateFile.instance.changeEquipDate[key][3]) * 10, false);
            }
            if (DateFile.instance.itemExtraChangeTimesData.ContainsKey(weapon_id))
            {
                DateFile.instance.itemExtraChangeTimesData[weapon_id]++;
            }
            else
            {
                DateFile.instance.itemExtraChangeTimesData.Add(weapon_id, 1);
            }
        }
    }
#endif
    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.5")]
    public class Main : BaseUnityPlugin {
        private static Action<string> _logger=s=>{};//用于内联代码
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
        public static void logger(string s,string extra_id=""){
#if DEBUG
            _logger(extra_id+":"+s);
#endif
        }
//        public static Settings settings = new Settings();
#if USE_UI
        public static UI ui=new UI();
#endif

        private void Awake() {
#if DEBUG
            _logger=Logger.LogInfo;
            logger("开始注入");
#endif
#if USE_UI
            if(true){
                ui.Init();
            }
#endif
        }
    }
}

