# 太吾Mod教程 手抄本

刚刚看到首页被顶起来的陈贴……竟然还是UMM的……果然大家需要一个Mod教程吗？

事先说明，这个教程的好处是，需要的依赖极其少……你甚至可以用Linux完成这一切操作

坏处是，你将不能使用最新IDE自带的各种自动补全功能

如果正练C#，或许对有编程工作经验的人来说，会发挥得更好。

但这里是逆练手抄本，完全是用来降低发挥需求的。

# 在开始正文之前，对debug的说明

有时候BUG来自Mod本身，此时查看`BepInEx`目录下的`LogOutput.log`多半可以获得出错信息。

但有时候`BepInEx`本身存在BUG，当你发现打开游戏之后找不到`BepInEx/LogOutput.log`，你应该做的是检查BepInEx的安装是否成功。

`BepInEx`的载入流程是，通过`UnityDoorstop`载入`BepInEx`，之后由`BepInEx`载入各种Mod插件，而这里，`UnityDoorstop`的载入方法其实利用了一个病毒特性：

> windows会优先搜索当前文件夹的dll，之后搜索系统dll

于是一个你这辈子都不会点开的`winhttp.dll`可以“感染”游戏，让游戏加载其本不应该加载的，`BepInEx`的dll。

理论上，这样的操作多见于病毒软件（修改exe的功能）

因此，可能会有正义感过剩的杀毒软件把BepInEx干掉

> 由于linux下只需要设置steam的启动选项为`WINEDLLOVERRIDES="winhttp=n,b" %command%`，即可正常玩耍

> 这里推荐无法安装BIE插件的同学，使用Linux[手动滑稽]

## 逆练页·其一 安装太吾所需 （完整）

我们需要的东西并不多，一个`BepInEx`，一个`csc.exe`，一个太吾游戏本体，和一个解密过的dll

`csc.exe`（或者`csc.dll`）是C Sharp(C#)的编译器，用法会在稍后讲解

BepInEx最好用最新版(目前来说是5，虽然的确有6，但6用的人不多，6或许是给IL2CPP用的……)，最新版可以在Github下载，至于Github怎么连，怕隐私泄漏的可以试试自行搭建`nginx`本地反代服务器（对linux系统很简单），或者`steam++`（对windows系统很友好）

解密dll可以用dalao写的 https://github.com/vizv/TaiwuDecompiler

csc可以自行下载几个G的visual studio，但这里是逆练，逆练的我只推荐下载 https://dotnet.microsoft.com/en-us/download/visual-studio-sdks 里面某个最新的SDK，安装之后找到里面的`csc.exe`或者找到`dotnet.exe`和`csc.dll`

以下我会用`$__DOTNET_CSC`指代把`csc.exe`拖到命令提示符，或者依次把`dotnet.exe`和`csc.dll`拖到命令提示符之后得到的字符串


## 逆练页·其二 编译C#所用命令 （完整）

由于我们没有使用任何dotnet的配置文件，我们需要手工进行编译

编译命令一般可以用

```bash
$__DOTNET_CSC -nologo -t:library \
```

开头，之后使用若干行

```bash
  -r:把需要引用的dll拖过来 \
```

把需要的dll引用过来，最后以

```bash
  -out:（输出文件，带扩展名） -optimize （你的源代码）
```

结尾，比如对太吾，你可以用

```bash
$__DOTNET_CSC -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  -r:"${GAME_BASE_DIR}/The Scroll Of Taiwu Alpha V1.0_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/The Scroll Of Taiwu Alpha V1.0_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/The Scroll Of Taiwu Alpha V1.0_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/The Scroll Of Taiwu Alpha V1.0_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/The Scroll Of Taiwu Alpha V1.0_Data/Managed/mscorlib.dll" \
  -r:"${GAME_BASE_DIR}/The Scroll Of Taiwu Alpha V1.0_Data/Managed/Assembly-CSharp-886238F0C737D5551CA4940C9724A53C94395A280021C721ED03A5241482CB97.dll" \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/first_mod.dll \
  -optimize \
  你的输入.cs
```

编译一个最简单的Mod

> 记得把`${GAME_BASE_DIR}`换成你的太吾所在的文件夹，把`Assembly-CSharp-886238F0C737D5551CA4940C9724A53C94395A280021C721ED03A5241482CB97.dll`换成你解包的dll

## 逆练页·其三 使用UI （残缺）

UI是程序员的两大天敌之一（另一个是文档）

在BIE框架下，目前来说，YanCore是一个比较成熟的UI，很多Mod都依赖于YanCore。如果你希望使用YanCore，你需要在安装YanCore之后，引用其dll，引用代码如下：

```bash
  -r:"${GAME_BASE_DIR}/BepInEx/plugins/YanCore/YanLib.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/plugins/YanCore/UnityUIKit.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/plugins/YanCore/TaiwuUIKit.dll" \
```

关于YanCore，我没找到说明，一切用法都是自己`dnSpy`逆练出来的，因而没办法提供太多有用的东西。

值得注意的是，`YanCore`可以作为另一种触发patch的方法，你可以使用YanCore定义并触发代码你写的片段（写修改器的时候，借助YanCore做UI比手搓一个简单很多）

## 逆练页·其四 一个简单Mod框架 （完整）

配合Harmony 2.0，我们可以非常愉快地写出一个Mod

> 放心，这不是手机里那个Harmony

一个合格的Mod分如下几部分：

### using部分

一般把这段using无脑复制过去就行

```CSharp
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;
using UnityEngine.UI;
```
最后那两个或许不必要，如果你不跟UI打交道，你根本不需要这个。

事实上这里什么都不写大概也不会有问题，因为你总可以用类似`new HarmonyLib.Harmony()`的手段来达到`using HarmonyLib`之后写`new Harmony()`相同的效果。

### 主class部分

与其说这是一个主class，倒不如说这是整个Mod的核心。

一般情况下，Modder会把所有Patch放在主class之下。

我推荐这么写主class，这么写的好处是，一旦你准备发布release版本，你可以直接注释掉你的#define DEBUG，此时理论上，因为`System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining`的存在，你的logger会被`inline`优化掉（但实际上并无卵用……这么写主要是自己开心）

```CSharp
#define DEBUG
namespace %%NAMESPACE_ID%%
{


    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "%%VERSION%%")]
    public class Balance : BaseUnityPlugin {
        private static Action<string> _logger=s=>{};//用于内联代码
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
        public static void logger(string s,string extra_id=""){
#if DEBUG
            _logger(extra_id+":"+s);
#endif
        }

        public static KeyCode RepeatJumpKey=KeyCode.V;
        public static KeyCode TanTuiKey=KeyCode.X;

        void Awake() {
            var harmony=new Harmony("%%PLUGIN_ID%%");
#if DEBUG
            _logger=Logger.LogInfo;
            logger("开始注入");
#endif
// Mod加载逻辑
            logger("加载完成");
        }
// 完成各种patch
    }
}
```
把主逻辑写完，剩下的就是在改`%%PLUGIN_ID%%`，`%%NAMESPACE_ID%%`和`%%VERSION%%`之后，填充`Mod加载逻辑`和`完成各种patch`了

## 逆练页·其五 Mod加载逻辑 （完整）

加载Mod的时候，你的确可以直接写`harmony.PatchAll()`一把梭，而你大概一定会被人家问“dalao你这个mod的xx功能能不能单独拿出来呢”这样的问题。

为了解决类似问题，你需要按需加载，也就是写一写加载逻辑。

一个简单的加载逻辑可以写成如下的形式


```CSharp
            if((int)(RepeatJumpKey=(KeyCode)Config.Bind("config", "RepeatJumpKey", (int)RepeatJumpKey, "反复横跳快捷键，默认为V，设为0关闭反复横跳patch").Value)!=0){
                TanTuiKey=(KeyCode)Config.Bind("config", "TanTuiKey", (int)TanTuiKey, "反复横跳切换弹腿缩地的快捷键，默认为X").Value;
                logger("patch RepeatJump");
                harmony.PatchAll(typeof(BattleSystemUpdate));
                logger("RepeatJump patched");
            }
```

这里使用`Config.Bind`生成`BepInEx`的配置文件，理论上除了config那个变量不需要改以外，其他三个变量都可以按需修改。其中第三个变量是当前变量的默认值，第二个变量是出现在config里面的key的名称（注意，这里产生重名不会触发BIE的任何警告，但重名会产生很多问题！），第四个是key的描述。

基本上这几个东西按需填写即可。

这里出现的`harmony.PatchAll(typeof(BattleSystemUpdate))`只是通知Harmony，该干活了。真正干活的是接下来的三个模块：

## 逆练页·其六 patch （完整）

这里介绍一个简单的完成patch的方法：借助标记。

比如如果你想写一个反复横跳的Mod，你发现你可以通过在游戏执行`BattleSystem.Update`之前通过修改几个简单变量来完成你的Mod，你可以使用如下代码：

```CSharp
        [HarmonyPatch(typeof(BattleSystem), "Update")] // 这个是整个类的一个标记，告诉harmony这个类是针对游戏哪一个部分的patch，对简单的mod，这样做patch已经足够了。
        public static class BattleSystemUpdate {
            public static int distance_1st=20;
            public static int distance_2nd=90;
            public static bool distancenable=false;
            public static bool tantui=false;
            public static bool first_run=true;
            public static int from=0;
            public static int target=0;
            static bool Prefix(BattleSystem __instance){
                if(Input.GetKeyDown(RepeatJumpKey)){
                    distance_1st=__instance.actorNeedRange;
                    distance_2nd=__instance.battleRange;
                    distancenable=!distancenable;
                    from=0;
                }
                if(Input.GetKeyDown(TanTuiKey)){
                    tantui=!tantui;
                    from=0;
                }
                if(distancenable){
                    __instance.autoBattle.transform.Rotate(new Vector3(0f, 0f, 100f) * (tantui?-2*Time.deltaTime:Time.deltaTime));
                    if(!__instance.autoBattle.isOn && __instance.actorNeedRangeSlider.interactable){
                        if(from!=__instance.battleRange){
                            from=__instance.battleRange;
                            int to=from==distance_1st?distance_2nd:distance_1st;
                            target=(tantui&&(BattleVaule.instance.GetMoveSpeed(true, DateFile.instance.mianActorId, true, 0)<15000))?Mathf.Clamp(to,from-5,from+5):to;
                        }
                        __instance.SetNeedRange(true, target, false);
                    }
                }
                return true;
            }
        }
```

这里有不少好玩的东西，比如

```CSharp
static bool Prefix()是前缀，返回值的含义是，是否继续原方法（true是继续执行，false是停止执行）
static void Postfix()是后缀，一般不会有返回值
一个特殊的情况是，static IEnumerator Postfix(IEnumerator __result)，此时Postfix接受一个迭代器，而返回一个迭代器。这是Postfix的pass-through特性，使用这个特性，我们可以对迭代器进行修改。
前缀和后缀都可以接受额外的参数：
__instance，相当于在原方法下使用this
__result，相当于原方法的返回值，应该注意，Prefix修改返回值之后必须使用return false，否则修改不会生效。
___field，相当于原方法里面的this.field，这个参数的用处是，当field是私有field的时候，我们无法直接调用__instance.field，但我们可以把它当作一个参数___field直接传给我们的前缀后缀
另外还有个Transpiler，这里非常不推荐初学者逆练这玩意，关于这个会放在最后一页，而且只会放一个逃课方法。毕竟这是减造诣需求的手抄本
```

## 逆练页·其七 同时patch多个方法

有时候，你需要让一个patch同时对几个不同的函数生效，比如，你想让“时间加速”这个功能对所有生成WaitForSeconds的迭代器生效，理论上你可以列出许多迭代器，但你不能使用多个标记。当多个标记同时出现在一个class上面的时候，只有一个标记会生效（别问我是哪一个……）

此时，你需要的是一个`static IEnumerable<MethodInfo> TargetMethods()`函数，比如：
```CSharp
        static class Faster{
            static IEnumerable<MethodInfo> TargetMethods(){
                foreach(MethodInfo i in new MethodInfo[]{
                    typeof(QuquBattleSystem).GetMethod("Damage",(BindingFlags)(-1)),
                    typeof(QuquBattleSystem).GetMethod("SetBattleStateText",(BindingFlags)(-1)),
                    typeof(QuquBattleSystem).GetMethod("QuquBattleLoopStart",(BindingFlags)(-1)),
                    typeof(QuquBattleSystem).GetMethod("QuquAttack",(BindingFlags)(-1)),
                    typeof(QuquBattleSystem).GetMethod("QuquBaseAttack",(BindingFlags)(-1)),
                    typeof(QuquBattleSystem).GetMethod("SetActorAnimation",(BindingFlags)(-1))
                }){
                    if(i==null){logger("FATAL:NULL i in QuquBattleSystem");}else{
                    logger("修改QuquBattleSystem的"+i.ToString()+"方法");
                    yield return i;
                    }
                }
            }
            //在这里放你的fix
        }
```

## 逆练页·其八 patch迭代器，或者一些奇奇怪怪的东西

C#的迭代器其实是一个MoveNext函数，这个函数是编译器直接生成的，你很难直接在源代码里面看到。

直接用类似`typeof(BattleEndWindow).GetMethod("BattleEndDone",(BindingFlags)(-1))`的手段是获取不到我们需要patch的`MoveNext`方法的。

此时，一个补救方法是之前说的，`pass-through Postfix`

但我们有更简单的方法：使用`AccessTools.EnumeratorMoveNext(typeof(BattleEndWindow).GetMethod("BattleEndDone",(BindingFlags)(-1)))`即可获取`MoveNext`方法

当然，“更简单”的方法其实是改标记，我们可以用类似`[HarmonyPatch(typeof(BattleSystem), "HideBattleState",MethodType.Enumerator)]`的方法直接把patch对象从`HideBattleState`改成真正要patch的，它的`MoveNext`方法。

类似的，`typeof(List<int[]>).GetProperty("Count").GetGetMethod()`和`MethodType.Getter`可以把修改目标指向某个property的get函数，`.GetSetMethod`和`MethodType.Setter`是set函数，`typeof(..).GetConstructor()`和`MethodType.Constructor`可以取到某个class的Constructor。

---

如果你的标记写得不好，或许会有0个/多个函数符合你的要求，这时候你可以试着用刚刚提到的`TargetMethods`完成函数的筛选（或者直接一把梭）

或者，在标记里面多放一点东西

比如我当年给戴森球用过这个：

```CSharp
[HarmonyPatch(typeof(StorageComponent), "TakeTailItems",new Type[]{typeof(int) ,typeof(int),typeof(bool)},new ArgumentType[]{ArgumentType.Ref,ArgumentType.Ref,ArgumentType.Normal})]
```

应该不会有比这个再难用的标记了

## 逆练页·其九 Debug（完整）

这一篇是完整的，但它比残页还短……因为这里只会告诉你一个万能的标记：

```CSharp
[HarmonyDebug]
```

东西给你了，剩下的大概只有八仙过海了。

这玩意其实是给下一篇用的，上面那些东西，借助logger本已够用，本不必再麻烦这位大神。

## 逆练页·其十 Transpiler（残缺）

讲道理这里的篇幅并不短，但这一页仍然是残页。

我没办法提供太多帮助，毕竟到这里还没有内息紊乱的大神多半不需要这个。

这里只会介绍几个逃课工具

### 最标准的写法，单行之类的替换

```CSharp
            static IEnumerable<CodeInstruction> Transpiler(MethodBase original,IEnumerable<CodeInstruction> instructions) {
                logger("加速Transpiler正在注入"+original.Name);
                ConstructorInfo waitForSeconds=typeof(UnityEngine.WaitForSeconds).GetConstructor(new Type[]{typeof(float)});
                foreach(var i in instructions){
                    if(
                        (i.opcode==OpCodes.Call && ((MethodInfo)i.operand).Name=="SetDelay")
                    ||  (i.opcode==OpCodes.Newobj && ((ConstructorInfo)i.operand)==waitForSeconds)
                        ){
                        if(i.opcode==OpCodes.Call)logger("找到SetDelay");else logger("找到waitForSeconds");
                        yield return new CodeInstruction(OpCodes.Ldc_R4,0.1f);
                        yield return new CodeInstruction(OpCodes.Mul,null);
                    }
                    yield return i;
                }
            }
```

### 进阶，如果希望patch的指令包含label

```CSharp
                            var label = new CodeInstruction(OpCodes.Nop);
                            label.labels=instruction.labels;
                            instruction.labels=new List<System.Reflection.Emit.Label>();
                            yield return label;
                            // 在这里写你的逻辑
                            // 一定保证你的逻辑在label之后，否则你的逻辑会被br跳过。
                            yield return instruction;
```

### 逃课，CodeMatcher

适当运用CodeMatcher可以节省大量逻辑。

```CSharp
        public static class BuildingWindowUpdateInformation {
            public static IEnumerable<MethodInfo> TargetMethods() {
                foreach(MethodInfo i in new MethodInfo[]{typeof(BuildingWindow).GetMethod("UpdateSkillInformation",(BindingFlags)(-1)),typeof(BuildingWindow).GetMethod("UpdateGongFaInformation",(BindingFlags)(-1))}){
                    logger("修改类别"+i.ToString()+"的chooseStudyValue赋值");
                    yield return i;
                }
            }
            public static IEnumerable<CodeInstruction> Transpiler(MethodBase original,IEnumerable<CodeInstruction> instructions) {
                logger((citang_shl?"祠堂Transpiler-左移计算-正在注入":"祠堂Transpiler-乘法计算-正在注入")+original.Name);
                return new CodeMatcher(instructions)
                    .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                        new CodeMatch(OpCodes.Ldc_I4_3),
                        new CodeMatch(OpCodes.Add),
                        new CodeMatch(i=> i.opcode==OpCodes.Stfld && ((FieldInfo)i.operand).Name == "chooseStudyValue") // match method 2
                    ).Repeat( matcher => // Do the following for each match
                        matcher
                        .Advance(1) // Move cursor to after ldfld
                        .SetAndAdvance(
                            citang_shl?OpCodes.Shl:OpCodes.Mul,null
                        )
                    ).InstructionEnumeration();
            }
        }
```

### 逃课，重要逻辑使用函数而非IL

如果你的逻辑非常复杂，使用IL大概一定会出问题

此时，使用函数或许会更好一些

一般情况下，你可以用`CodeInstruction(OpCodes.Call,typeof(你的class名称).GetMethod("你需要的函数"))`声明你需要调用的函数。

把这玩意写进IL，之后你就可以在函数里完成复杂逻辑，而不必把逻辑一行一行转化成IL

```CSharp
        [HarmonyPatch(typeof(BattleEndWindow), "BattleEnd")]
        class BattleEndWindowBattleEnd {
            public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions){ // 通过ThirdItemId的读写修改进行注入
                logger("   ->搜索Ldfld battleBooty后面的.Count");
                uint flag=0xDEADBEFF;
                int count=0;
                // MethodInfo find=typeof(List<int[]>).GetMethod("get_Count",(BindingFlags)(-1)); // -1是BindingFlags.All的意思。
                MethodInfo find=typeof(List<int[]>).GetProperty("Count").GetGetMethod(); // -1是BindingFlags.All的意思。
                var iter=instructions.GetEnumerator();
                while (iter.MoveNext()) {
                    count++;
                    CodeInstruction instruction=iter.Current;
                    // instruction.opcode==OpCodes.Callvirt && (MethodInfo)instruction.operand == find
                    // instruction.opcode==OpCodes.Ldfld && ((FieldInfo)instruction.operand).Name == "name"
                    if(flag!=0 && instruction.opcode==OpCodes.Ldfld && ((FieldInfo)instruction.operand).Name == "battleBooty"){
                        if(iter.MoveNext()){
                            count++;
                            var instruction2=iter.Current;
                            if(instruction2.opcode==OpCodes.Callvirt && (MethodInfo)instruction2.operand == find){
                                logger("[II]:found");
                                yield return new CodeInstruction(OpCodes.Ldfld,typeof(BattleEndWindow).GetField("joinEnemy",(BindingFlags)(-1)));
                                yield return new CodeInstruction(OpCodes.Ldarg_0);
                                yield return new CodeInstruction(OpCodes.Ldfld,typeof(BattleEndWindow).GetField("battleBooty",(BindingFlags)(-1)));
                                yield return new CodeInstruction(OpCodes.Ldarg_1);
                                yield return new CodeInstruction(OpCodes.Ldarg_2);
                                yield return new CodeInstruction(OpCodes.Call,typeof(BattleEndWindowBattleEnd).GetMethod("ExtraLoot"));
                                yield return new CodeInstruction(OpCodes.Ldarg_0);
                                yield return instruction;
                                yield return instruction2;
                                logger("[II]:patched");
                                flag=0;
                            } else { yield return instruction;yield return instruction2; }
                        } else { yield return instruction; }
                    } else {yield return instruction;}
                }
                if(flag!=0){logger("[EE]:注入失败，请检查源码是否发生了改变");}else{logger("[II]:Done.");}
            }
            public static void ExtraLoot(List<int> ___joinEnemy, List<int[]> ___battleBooty, bool actorWin, int actorRun){
            //以下省略
```

# 结语

读完10篇手抄的，大概可以在满足基本dnSpy造诣需求之后，空手撸Mod

但我很怀疑这种睡前写就的短文能有多少人看得懂

就这样吧

反正会出真传本的
