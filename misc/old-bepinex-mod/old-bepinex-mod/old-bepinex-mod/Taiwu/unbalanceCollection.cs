#!/bin/bash -e
#   * compile instructions: put this file in
#   *         `steamapps/common/Dyson Sphere Program/CustomMod`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :
#region shell_script
if [ -z "$__DOTNET_CSC" ]; then
    export __DOTNET_CSC="`find /usr/share/dotnet -type f -name dotnet` `find /usr/share/dotnet -name csc.dll`"
    echo '$'"__DOTNET_CSC not set yet, you should execute"
    echo "export __DOTNET_CSC='$__DOTNET_CSC'"
    echo "manually, or this alert will occur each time you execute this script."
fi

__MODE_VERBOSE=81 # is the line number of "#define VERBOSE", may be modified
__MODE_DEBUG__=$((__MODE_VERBOSE+1))
__MODE_RELEASE=$((__MODE_DEBUG__+1))

case $1 in
    V)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    v)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    VERBOSE) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    verbose) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    D)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    d)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    DEBUG)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    debug)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    *)       _MODE__SELECT_=$__MODE_RELEASE     ;;
esac

export GAME_NAME="${0%\.cs}" # now, file name should be equals to ${GAME_NAME}.cs, the benefit is, we could only change the name, rather than change filename and $GAME_NAME.
export GAME_NAME="The Scroll Of Taiwu Alpha V1.0"
export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"
export GAME_BASE_DIR="../common/The Scroll Of Taiwu"
export PLUGIN_ID="Neutron3529.Taiwu.Unbalance.Collection"
export NAMESPACE_ID="Neutron3529.Unbalance.Collection"

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_ ) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $__DOTNET_CSC -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/Assembly-CSharp-886238F0C737D5551CA4940C9724A53C94395A280021C721ED03A5241482CB97.dll" \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize \
  - && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit

#endregion
#define VERBOSE // the line of __MODE_VERBOSE
#define DEBUG



using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%
{
    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.5")]
    public class Cheat : BaseUnityPlugin {
        private static Action<string> _logger=s=>{};//用于内联代码
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
        public static void logger(string s,string extra_id=""){
#if DEBUG
            _logger(extra_id+":"+s);
#endif
        }

#region static_variales
        public static int[] nadd=new int[]{51101,51102,51106,51107,50097,50098};
        public static int[] nadd2=new int[]{1000,1000,1000,1000,10000,10000};
        public static int[] dadd=new int[]{//50501,50502,50503,50504,50505,50506,50507,50508,50509,50510,50511,50512,50513,50514,50515,50516,50601,50602,50603,50604,50605,50606,50607,50608,50609,50610,50611,50612,50613,50614,
            51109,51111,50015,908,
            50071,50072,50073,50092,50093,50094,
            50081,50082,50083,50084,50085,50086,
            51367,51368,51369,51370,51371,51372
        };
        public static int[] dadd2=new int[]{//100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,
            3000,100,50000,1000,
            100,100,100,100,100,100,
            100,100,100,100,100,100,
            100,100,100,100,100,100
        };
        public static int[] aadd=new int[]{92,93,601,603,902,904,905,711,501};
        public static int[] aadd2=new int[]{10000,100,600,600,100,10000,40000,400,-300};
        public static int alen=0;
        public static int dlen=0;
        public static int nlen=0;
        public static List<int> normal_add;
        public static List<int> def_add=new List<int>();
        public static bool complete_book=false;
        public static bool skill_always_win=true;
        public static bool better_daibu=true;
        public static bool better_weapon=false;

        public static bool drop_even_if_failed=true;
        public static bool drop_with_qiecuo=true;
        public static int extra_wuqi_percentage=0;
        public static int extra_maozi_percentage=0;
        public static int extra_hujia_percentage=0;
        public static int extra_xiezi_percentage=0;
        public static int extra_baowu_percentage=0;
        public static int extra_wear_percentage=100;
        public static int extra_ququ_percentage=100;
        public static int extra_daibu_percentage=100;

        public static int extra_xiaohaopin_percentage=100; // 100000 - 200000
        public static int extra_shiwu_percentage=100; // 200000 - 300000
        public static int extra_3xxxxx_percentage=0; // 300000 - 400000
        public static int extra_jiyishu_percentage=100; // 400000 - 500000
        public static int extra_zhenchuanshu_percentage=100; // 500000 - 700000
        public static int extra_shouchaoshu_percentage=100; // 700000 - 900000
        public static int extra_default_percentage=100;
        public static int DayTimeMul=16;
        public static bool citang_shl=true;

        public static Harmony harmony=new Harmony("%%PLUGIN_ID%%");
#endregion

        void Start() {
#if DEBUG
            _logger=Logger.LogInfo;
#endif
#region old
            /*
            if (Config.Bind<bool>("config", "N101", true, "进行额外武器迅疾+精制").Value){
                flag=true;
                Add(101);
            }
            if (Config.Bind<bool>("config", "N102", true, "进行额外武器精妙+精制").Value){
                flag=true;
                Add(102);
            }
            if (Config.Bind<bool>("config", "N103", true, "进行额外武器破甲+精制").Value){
                flag=true;
                Add(103);
            }
            if (Config.Bind<bool>("config", "N104", true, "进行额外武器坚韧+精制").Value){
                flag=true;
                Add(104);
//             }
            if (Config.Bind<bool>("config", "N105", true, "进行额外武器破体+精制").Value){
                flag=true;
                Add(105);
            }
            if (Config.Bind<bool>("config", "N106", true, "进行额外武器破气+精制").Value){
                flag=true;
                Add(106);
            }
            if (Config.Bind<bool>("config", "N107", true, "进行额外武器力道+精制").Value){
                flag=true;
                Add(107);
            }
            if (Config.Bind<bool>("config", "N108", true, "进行额外武器重量-精制").Value){
                flag=true;
                Add(108);
            }
            if (Config.Bind<bool>("config", "N109", true, "进行额外武器伤害+精制").Value){
                flag=true;
                Add(109);
            }
            if (Config.Bind<bool>("config", "N110", true, "进行额外武器追击+精制").Value){
                flag=true;
                Add(110);
            }
            if (Config.Bind<bool>("config", "N111", true, "进行额外武器变招+精制").Value){
                flag=true;
                Add(111);
            }
            if (Config.Bind<bool>("config", "N112", true, "进行额外武器距离+精制").Value){
                flag=true;
                Add(112);
            }
            if (Config.Bind<bool>("config", "N113", true, "进行额外武器距离+精制").Value){
                flag=true;
                Add(113);
            }
            normal_add=def_add;
            def_add=new List<int>();
            if (Config.Bind<bool>("config", "N201", true, "进行额外防具闪避+精制").Value){
                flag=true;
                Add(201);
            }
            if (Config.Bind<bool>("config", "N202", true, "进行额外防具拆招+精制").Value){
                flag=true;
                Add(202);
            }
            if (Config.Bind<bool>("config", "N203", true, "进行额外防具破刃+精制").Value){
                flag=true;
                Add(203);
            }
            if (Config.Bind<bool>("config", "N204", true, "进行额外防具坚韧+精制").Value){
                flag=true;
                Add(204);
            }
            if (Config.Bind<bool>("config", "N205", true, "进行额外防具护体+精制").Value){
                flag=true;
                Add(205);
            }
            if (Config.Bind<bool>("config", "N206", true, "进行额外防具御气+精制").Value){
                flag=true;
                Add(206);
            }
            if (Config.Bind<bool>("config", "N207", true, "进行额外防具卸力+精制").Value){
                flag=true;
                Add(207);
            }
            if (Config.Bind<bool>("config", "N208", true, "进行额外防具重量-精制").Value){
                flag=true;
                Add(208);
            }
            if (Config.Bind<bool>("config", "N301", true, "进行额外人物灵敏+精制").Value){
                flag=true;
                Add(301);
            }
            if (Config.Bind<bool>("config", "N302", true, "进行额外人物根骨+精制").Value){
                flag=true;
                Add(302);
            }
            if (Config.Bind<bool>("config", "N303", true, "进行额外人物体质+精制").Value){
                flag=true;
                Add(303);
            }
            if (Config.Bind<bool>("config", "N304", true, "进行额外人物膂力+精制").Value){
                flag=true;
                Add(304);
            }
            if (Config.Bind<bool>("config", "N305", true, "进行额外人物定力+精制").Value){
                flag=true;
                Add(305);
            }
            if (Config.Bind<bool>("config", "N306", true, "进行额外人物悟性+精制").Value){
                flag=true;
                Add(306);
            }
            if (Config.Bind<bool>("config", "N307", true, "进行额外外伤上限+精制").Value){
                flag=true;
                Add(307);
            }
            if (Config.Bind<bool>("config", "N308", true, "进行额外内伤上限+精制").Value){
                flag=true;
                Add(308);
            }
            aadd=Config.Bind<int[]>("config", "extra_aadd_id", aadd, "额外精制id(对全部物品生效)，请确保这个数组与下面的数组长度相同").Value;
            aadd2=Config.Bind<int[]>("config", "extra_aadd_count", aadd2, "额外精制id对应的精制数值(10次精炼之后物品加值，而非单次)").Value;
            if (aadd.Length==aadd2.Length){
                flag=true;
                alen=aadd.Length;
                logger("全部物品add生效，共 "+alen.ToString()+" 项");
            }else{
                alen=0;
            }
            nadd=Config.Bind<int[]>("config", "extra_nadd_id", nadd, "额外武器/缺省精制id，请确保这个数组与下面的数组长度相同").Value;
            nadd2=Config.Bind<int[]>("config", "extra_nadd_count", nadd2, "额外精制id对应的精制数值(10次精炼之后物品加值，而非单次)").Value;
            if (nadd.Length==nadd2.Length){
                flag=true;
                nlen=nadd.Length;
                logger("缺省add生效，共 "+nlen.ToString()+" 项");
            }else{
                nlen=0;
            }
            dadd=Config.Bind<int[]>("config", "extra_dadd_id", dadd, "额外防具/饰品增加人物属性的精制转换为下列id，请确保这个数组与下面的数组长度相同").Value;
            dadd2=Config.Bind<int[]>("config", "extra_dadd_count", dadd2, "额外精制id对应的精制数值(10次精炼之后物品加值，而非单次)").Value;
            if (dadd.Length==dadd2.Length){
                flag=true;
                dlen=dadd.Length;
                logger("防具/饰品add生效，共 "+dlen.ToString()+" 项");
            }else{
                dlen=0;
            }*/
#endregion
#region small_change
            logger("开始注入Start");
            if(Config.Bind<bool>("config", "skill_always_win", true, "较艺时只有对方NPC会扣血").Value){
                logger("patch skill_always_win");
                harmony.PatchAll(typeof(SkillBattleSystemSetBattlerHp));
                logger("skill_always_win patched");
            }
            if(Config.Bind<bool>("config", "fast_practice_gong", true, "实战必定读完完整页，或者5%残页").Value){
                logger("patch fast_practice");
                harmony.PatchAll((typeof(DateFileGetGongFaBookAddValue)));
                logger("fast_practice patched");
            }
            if(Config.Bind<bool>("config", "fast_practice_art", true, "较艺时候每次总能读一页书").Value){
                logger("patch fast_practice");
                harmony.PatchAll((typeof(DateFileGetSkillBookAddValue)));
                logger("fast_practice patched");
            }
            if(Config.Bind<bool>("config", "citang", true, "祠堂传功无年龄限制").Value){
                logger("patch citang");
                harmony.PatchAll((typeof(CiTangMianAge)));
                if(Config.Bind<bool>("config", "citang_presitage", true, "祠堂传功不消耗声望").Value){
                    logger("patch citang_prestige");
                    harmony.PatchAll((typeof(BuildingWindowStudyNeedPrestige)));
                    if(Config.Bind<bool>("config", "citang_superman", true, "学满技能之后传功效果修改").Value){
                        logger("patch citang_superman");
                        citang_shl=Config.Bind<bool>("config", "citang_shl", true, "选true则修满之后左移3位（相当于*8），选false则只是使用普通的乘法（在这里是*3）").Value;
                        harmony.PatchAll((typeof(BuildingWindowUpdateInformation)));
                        logger("citang_superman patched");
                    }
                    logger("citang_prestige patched");
                }
                logger("citang patched");
            }
            if ((DayTimeMul=Config.Bind("config", "GetMaxDayTime", DayTimeMul, "每时节时间乘数，大于1开启patch。如果这里填2430……你很有可能在一个时节花费72900秒").Value)>1){
                logger("GetBookAddValue");
                harmony.PatchAll(typeof(DateFileGetMaxDayTime));
                logger("GetBookAddValue done");
            }
            if(Config.Bind("config", "StudySkillUp", true, "修习增长经验为100而非1~2").Value){
                logger("patch StudySkillUp");
                harmony.PatchAll((typeof(BuildingWindowStudySkillUp)));
                logger("StudySkillUp patched");
            }
            if(Config.Bind("config", "QuquBattleSystemUpdateQuquHp", false, "电脑蛐蛐HPSP在出场恒定为1--由于我们的蛐蛐非常强大，这里默认关闭").Value){
                logger("patch QuquBattleSystemUpdateQuquHp");
                harmony.PatchAll((typeof(QuquBattleSystemUpdateQuquHp)));
                logger("QuquBattleSystemUpdateQuquHp patched");
            }
            if(Config.Bind("config", "MakeSystemSetMakeItem", true, "制作物品使用遗惠逻辑").Value){
                logger("patch MakeSystemSetMakeItem");
                harmony.PatchAll((typeof(MakeSystemSetMakeItem)));
                //harmony.PatchAll((typeof(MakeSystemMakeNewItem)));
                logger("MakeSystemSetMakeItem patched");
            }
            if(Config.Bind("config", "DateFileGetMianActorEquipGongFa", true, "功法页第七页正练，第八页冲解，第九页逆练，在切换至第九页时刷新三者。").Value){
                logger("patch DateFileGetMianActorEquipGongFa");
                harmony.PatchAll((typeof(DateFileGetMianActorEquipGongFa)));
                logger("DateFileGetMianActorEquipGongFa patched");
            }
            if(Config.Bind("config", "THBandModelOnMonthPass", true, "茶马帮不需要管理补给").Value){
                logger("patch THBandModelOnMonthPass");
                harmony.PatchAll((typeof(THBandModelOnMonthPass)));
                logger("THBandModelOnMonthPass patched");
            }
//             if(Config.Bind("config", "Taiyin_positive_Pregnancy", true, "太阴一明珏正练时，只要运用者不为怀孕状态，运用者将在每时节开始时恢复童女身").Value){
//                 logger("patch Taiyin_positive_Pregnancy");
// //                harmony.PatchAll((typeof(CharacterAgeOnChildbirth)));
// //                harmony.PatchAll((typeof(CharacterAgeOnAbortion)));
//                 logger("Taiyin_positive_Pregnancy patched");
//             }
#endregion
#region breaking_change
            if(Config.Bind<bool>("config", "better_daibu", false, "修理物品时对正在装备的代步进行修改，增强物品掉率，略增加失心人救治").Value){
                logger("patch better_daibu");
                harmony.PatchAll((typeof(MakeSystemStartFixItem)));
                better_weapon=Config.Bind<bool>("config", "better_weapon", false, "精制时候出圣品武器").Value;
                if(better_weapon){logger("better_weapon set to true.");}else{logger("默认不进行better_weapon的修改，如果需要，请自行修改配置文件");}
                logger("better_daibu patched");
            }else{
                logger("默认不进行better_daibu的修改，如果需要，请自行修改配置文件");
            }
            if(Config.Bind<bool>("config", "whole_book", false, "书籍没有残页").Value){
                logger("patch whole_book");
                harmony.PatchAll((typeof(DateFileGetBookPage)));
                logger("whole_book patched");
            }else{
                logger("默认不进行whole_book的修改（因为没必要），如果需要，请自行修改配置文件");
            }
            if(Config.Bind<bool>("config", "VERY_IMBA", false, "精制出来的武器变成真·神器").Value){
                logger("patch VERY_IMBA");
                harmony.PatchAll(Assembly.GetAssembly(typeof(MakeSystemStartChangeItem)));
                logger("VERY_IMBA patched");
            }else{
                logger("默认不进行VERY_IMBA的修改，如果需要，请自行修改配置文件");
            }
            if (Config.Bind("config", "DROP_ALL", false, "开启可能会坏档的，额外物品掉落（物品掉落是安全的，不安全的是太吾储存物品的方式……几千重量的物品很容易把你卡得不想玩游戏）").Value){
                logger("检测到drop_all开关被打开，即将进行非常破坏游戏体验的patch");

                drop_even_if_failed=Config.Bind("config", "always_drop", drop_even_if_failed, "永远开启额外掉落（哪怕打输了，对面也会掉落物品）").Value;
                drop_with_qiecuo=Config.Bind("config", "drop_with_qiecuo", drop_with_qiecuo, "切磋胜利时开启额外掉落（只有战斗胜利才会掉落物品）").Value;
                extra_wuqi_percentage=Config.Bind("config", "extra_wuqi_percentage", extra_wuqi_percentage, "额外增加 人物装备武器 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_maozi_percentage=Config.Bind("config", "extra_maozi_percentage", extra_maozi_percentage, "额外增加 人物装备帽子 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_hujia_percentage=Config.Bind("config", "extra_hujia_percentage", extra_hujia_percentage, "额外增加 人物装备护甲 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_xiezi_percentage=Config.Bind("config", "extra_xiezi_percentage", extra_xiezi_percentage, "额外增加 人物装备鞋子 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_baowu_percentage=Config.Bind("config", "extra_baowu_percentage", extra_baowu_percentage, "额外增加 人物装备宝物 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_wear_percentage=Config.Bind("config", "extra_wear_percentage", extra_wear_percentage, "额外增加 人物装备衣装 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_ququ_percentage=Config.Bind("config", "extra_ququ_percentage", extra_ququ_percentage, "额外增加 人物装备蛐蛐 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_daibu_percentage=Config.Bind("config", "extra_daibu_percentage", extra_daibu_percentage, "额外增加 人物装备代步 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;

                extra_xiaohaopin_percentage=Config.Bind("config", "extra_xiaohaopin_percentage", extra_xiaohaopin_percentage, "额外增加 消耗品（id100000 - 200000） 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_shiwu_percentage=Config.Bind("config", "extra_shiwu_percentage", extra_shiwu_percentage, "额外增加 食物（id200000 - 300000） 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_3xxxxx_percentage=Config.Bind("config", "extra_3xxxxx_percentage", extra_3xxxxx_percentage, "额外增加 3系列物品（id300000 - 400000，似乎不存在） 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_jiyishu_percentage=Config.Bind("config", "extra_jiyishu_percentage", extra_jiyishu_percentage, "额外增加 技艺书（id400000 - 500000） 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_zhenchuanshu_percentage=Config.Bind("config", "extra_zhenchuanshu_percentage", extra_zhenchuanshu_percentage, "额外增加 真传功法书（id500000 - 700000） 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_shouchaoshu_percentage=Config.Bind("config", "extra_shouchaoshu_percentage", extra_shouchaoshu_percentage, "额外增加 手抄功法书（id700000 - 900000） 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                extra_default_percentage=Config.Bind("config", "extra_default_percentage", extra_default_percentage, "额外增加 默认物品 的掉落率（0=关闭此功能，100=必定掉落，负数可能会影响此Mod是否加载）").Value;
                harmony.PatchAll(typeof(BattleEndWindowBattleEnd));
                logger("游戏体验破坏完成，由于游戏会在patch位置之前把临时生成的外道的装备清空，所以这个补丁并不能对外道生效");
            }else{
                logger("drop_all保持关闭");
            }
#endregion
            logger("加载完成");
        }
        public static void Add(int what) {
            logger("  解析待加载的项目:"+what.ToString());
            def_add.Add(what);
        }
#region small_change_patch
        [HarmonyPatch(typeof(SkillBattleSystem), "SetBattlerHp")]
        public static class SkillBattleSystemSetBattlerHp { // 较艺时候只有对方会扣血
            public static bool Prefix(ref bool isActor,ref int vlaue) {
                if (vlaue>0){
                    isActor=false;
                    vlaue=10;
                }
                return true;
            }
        }
        [HarmonyPatch(typeof(DateFile), "GetSkillBookAddValue")]//较艺阅读进度恒定为2000（刚好读完残页）
        public static class DateFileGetSkillBookAddValue {
            public static bool Prefix(ref int __result) {
                __result=2000;
                return false;
            }
        }
        [HarmonyPatch(typeof(DateFile), "GetGongFaBookAddValue")]//实战阅读进度恒定为100（刚好读完残页，逆练本会产生内息紊乱）
        public static class DateFileGetGongFaBookAddValue {
            public static bool Prefix(ref int __result) {
                __result=100;
                return false;
            }
        }
        [HarmonyPatch(typeof(DateFile), "GetMaxDayTime")]
        public static class DateFileGetMaxDayTime {
            public static int Postfix(int __result) {
                return __result * DayTimeMul;
            }
        }
        [HarmonyPatch(typeof(BuildingWindow), "StudySkillUp")]
        public static class BuildingWindowStudySkillUp {
            public static bool Prefix(BuildingWindow __instance,int ___studySkillId) {
                int num = DateFile.instance.MianActorID();
                int num3 = ___studySkillId;
                if (__instance.studySkillTyp == 17){
                    int curr = DateFile.instance.GetGongFaLevel(num, num3, 0);
                    if(curr>=100)return true;
                    DateFile.instance.ChangeActorGongFa(num, num3, 99-curr, 0, 0, false);// 把目标设置在99的好处是不会漏遗惠，而原始程序一定能补上最后的1%.
                }else{
                    int curr = DateFile.instance.GetSkillLevel(num3);
                    if(curr>=100)return true;
                    if(!DateFile.instance.skillBookPages.ContainsKey(num3)){
                        DateFile.instance.skillBookPages.Add(num3, new int[]{1,1,1,1,1,1,1,1,1,1});
                    }
                    for(int i=0;i<DateFile.instance.skillBookPages[num3].Length;i++){
                        DateFile.instance.skillBookPages[num3][i]=1;
                        if (!DateFile.instance.actorSkills.ContainsKey(num3))DateFile.instance.ChangeMianSkill(num3, 0, 0, true);
                        DateFile.instance.AddActorScore(304, 10000);
                        DateFile.instance.AddActorScore(305, 10000);
                    }
                    DateFile.instance.ChangeMianSkill(num3, 99-curr, 0, false);
                }
                return true;
            }
        }
        [HarmonyPatch(typeof(DateFile), "GetMianActorEquipGongFa")]
        public static class DateFileGetMianActorEquipGongFa{
            public static void Postfix(DateFile __instance,int index) {
                if(index==8){//第九页=8
                    int actorId = __instance.MianActorID();
                    for(int i=6;i<9;i++)if (__instance.mianActorEquipGongFas.ContainsKey(i)) {
                        for(int j=0;j<5;j++){
                            if(__instance.mianActorEquipGongFas[i].ContainsKey(j)){
                                foreach(int gongFaId in __instance.mianActorEquipGongFas[i][j]) if(gongFaId>0){
                                    __instance.actorGongFas[actorId][gongFaId][0] = 100;
                                    __instance.actorGongFas[actorId][gongFaId][1] = 10;
                                    __instance.actorGongFas[actorId][gongFaId][2] = i==6?0:(i==7?5:6);
                                    if(!DateFile.instance.gongFaBookPages.ContainsKey(gongFaId)){
                                        DateFile.instance.gongFaBookPages.Add(gongFaId,new int[]{1,1,1,1,1,1,1,1,1,1});
                                    } else {
                                        for(int xi=0;xi<10;xi++)DateFile.instance.gongFaBookPages[gongFaId][xi]=1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public static class CiTangMianAge
        {
            public static IEnumerable<MethodInfo> TargetMethods() {
                foreach(MethodInfo i in new MethodInfo[]{typeof(BuildingWindow).GetMethod("UpdateActorStudy",(BindingFlags)(-1)),typeof(BuildingWindow).GetMethod("SetStudyWindow",(BindingFlags)(-1))}){
                    logger("修改类别"+i.ToString()+"的MianAge方法");
                    yield return i;
                }
            }
            public static IEnumerable<CodeInstruction> Transpiler(MethodBase original,IEnumerable<CodeInstruction> instructions) {
                logger("祠堂Transpiler正在注入"+original.Name);
                foreach(var i in instructions){
                    yield return i;
                    if(i.opcode==OpCodes.Callvirt && ((MethodInfo)i.operand).Name=="MianAge"){
                        logger("找到MianAge");
                        yield return new CodeInstruction(OpCodes.Pop,null);
                        yield return new CodeInstruction(OpCodes.Ldc_I4_S,100);
                    }
                }
            }
        }
        [HarmonyPatch(typeof(BuildingWindow), "StudyNeedPrestige")]//祠堂不需要声望
        public static class BuildingWindowStudyNeedPrestige {
            public static bool Prefix(ref int __result){
                __result=0;
                return false;
            }
        }
        public static class BuildingWindowUpdateInformation {
            public static IEnumerable<MethodInfo> TargetMethods() {
                foreach(MethodInfo i in new MethodInfo[]{typeof(BuildingWindow).GetMethod("UpdateSkillInformation",(BindingFlags)(-1)),typeof(BuildingWindow).GetMethod("UpdateGongFaInformation",(BindingFlags)(-1))}){
                    logger("修改类别"+i.ToString()+"的chooseStudyValue赋值");
                    yield return i;
                }
            }
            public static IEnumerable<CodeInstruction> Transpiler(MethodBase original,IEnumerable<CodeInstruction> instructions) {
                logger((citang_shl?"祠堂Transpiler-左移计算-正在注入":"祠堂Transpiler-乘法计算-正在注入")+original.Name);
                return new CodeMatcher(instructions)
                    .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                        new CodeMatch(OpCodes.Ldc_I4_3),
                        new CodeMatch(OpCodes.Add),
                        new CodeMatch(i=> i.opcode==OpCodes.Stfld && ((FieldInfo)i.operand).Name == "chooseStudyValue") // match method 2
                    ).Repeat( matcher => // Do the following for each match
                        matcher
                        .Advance(1) // Move cursor to after ldfld
                        .SetAndAdvance(
                            citang_shl?OpCodes.Shl:OpCodes.Mul,null
                        )
                    ).InstructionEnumeration();
            }
        }
        [HarmonyPatch(typeof(DateFile), "GetBookPage")]//书籍没有残页
        public static class DateFileGetBookPage {
            public static bool Prefix(ref int[] __result){
                __result=new int[]{1,1,1,1,1,1,1,1,1,1};
                return false;
            }
        }
        [HarmonyPatch(typeof(QuquBattleSystem), "UpdateQuquHp")]//电脑蛐蛐暴毙
        public static class QuquBattleSystemUpdateQuquHp {
            public static bool Prefix(int index, int[] ___enemyQuquHp, int[] ___enemyQuquSp){
                ___enemyQuquHp[index] = Mathf.Min(___enemyQuquHp[index], 1);
                ___enemyQuquSp[index] = Mathf.Min(___enemyQuquSp[index], 1);
                return true;
            }
        }
        [HarmonyPatch(typeof(MakeSystem), "SetMakeItem")]
        public static class MakeSystemSetMakeItem { // 设置makePower
            public static bool Prefix() {
                DateFile.instance.makePower=true;
                return true;
            }
        }/*
        [HarmonyPatch(typeof(DateFile), "MakeNewItem")]
        public static class MakeSystemMakeNewItem { // 调用MakeNewItem时再次确保我们正确地使用了makePower。
            public static bool Prefix(ref int buffObbs,ref int bookObbs,ref int sBuffObbs) {
                buffObbs=999;
                bookObbs=1000;
                sBuffObbs=100;
                return true;
            }
        }*/
        [HarmonyPatch(typeof(THBandModel), "OnMonthPass")]
        public static class THBandModelOnMonthPass {
            public static bool Prefix(THBandModel __instance) {
                typeof(THBandModel).GetField("_supply",(BindingFlags)(-1)).SetValue(__instance,105); // 补给恒定为105，大概会在过月时候减到100
                if(__instance.bandFame<100000)typeof(THBandModel).GetField("_bandFame",(BindingFlags)(-1)).SetValue(__instance,100000); //保证商队名誉不小于100000
                if(__instance.direction == eDiretion.West)typeof(THBandModel).GetField("_distanceToTWC",(BindingFlags)(-1)).SetValue(__instance,__instance.distanceToTWC+99); // 让商队迅速远离太吾村
                else if(__instance.direction == eDiretion.East)typeof(THBandModel).GetField("_distanceToTWC",(BindingFlags)(-1)).SetValue(__instance,Mathf.Min(__instance.distanceToTWC,1)); // 迅速召回商队
                return true;
            }
        }
#endregion
        /*
        [HarmonyPatch(typeof(AI.CharacterAge), "ChangeAge")]
        public static class CharacterAgeChangeAge { // 多线程
            public static void AddInjury(int actorId,int id,int power){
                var a=DateFile.instance.actorInjuryDate;
                if (a[actorId].ContainsKey(id)) {
                    a[actorId][id] += power;
                } else {
                    a[actorId].Add(id, power);
                }
            }
            public static void Postfix(int actorId, int mainActorId, System.Random random, AI.AgeChangeModifiedData modifiedData){
                if(DateFile.instance.GetActorDate(actorId, 14, false).ParseInt()==2 && DateFile.instance.GetActorDate(actorId, 11, true).ParseInt()>14 && DateFile.GetGongfaEternalPowerChange(actorId, WANBI_ID)>=0){//如果运功者真实性别为女性且年龄大于14
                    int flag=DateFile.instance.GetGongFaFTyp(actorId,WANBI_ID);
                    if(flag!=-1){
                        List<int> actorAtPlace = DateFile.instance.GetActorAtPlace(actorId);
                        int poison_count=0;
                        float c=0f;
                        for(int i=0;i<6;i++){
                            int han=DateFile.instance.Poison(actorId, i);
                            if(han<=0)continue;
                            int hanMax=DateFile.instance.MaxPoison(actorId, i);
                            int deltaHan=Mathf.Min(han,hanMax/5);
                            if((flag!=0&&i==4) || (flag!=1&&i==5)){//罩门毒素的计算，冲解时两种毒素都是罩门
                                deltaHan=Mathf.Max(han,hanMax/10);//罩门毒素只泻出更少的一部分，
                                c+=(float)(9*deltaHan)/(float)hanMax;//且会给运用者造成更高的伤害
                                if(han>=hanMax){
                                    poison_count+=2;
                                    if(DateFile.GetGongfaEternalPowerChange(actorId, WANBI_ID)>=0){
                                        if(i==4){
                                            modifiedData.AddChangTrunEvents.Add(new int[] {
                                                WANBI_BREAK_FU_EVENT_ID,
                                                actorAtPlace[0],
                                                actorAtPlace[1],
                                                actorId, 540303 });
                                        }else{
                                            modifiedData.AddChangTrunEvents.Add(new int[] {
                                                WANBI_BREAK_HUAN_EVENT_ID,
                                                actorAtPlace[0],
                                                actorAtPlace[1],
                                                actorId, 100518 });
                                        }
                                        DateFile.AddGongfaEternalPowerChange(actorId, WANBI_ID, -500, actorId, WANBI_ID);
                                        GameData.Characters.SetCharProperty(actorId, 1361+0, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+1, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+2, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+3, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+4, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+5, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+6, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+7, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+8, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+9, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+10, "-1000");
                                        GameData.Characters.SetCharProperty(actorId, 1361+11, "-1000");
                                    }
                                }
                            }
                            GameData.Characters.SetCharProperty(actorId, 51+i, (han-deltaHan).ToString()); // 完璧状态下，每时节都会尝试泻毒
                            c+=(float)(deltaHan)/(float)hanMax;//但会因此造成身体损伤
                            poison_count+=(han>=hanMax)?1:0;
                        }
                        AddInjury(actorId, 1, (int)(c*1000f));//胸背·擦伤
                        AddInjury(actorId, 4, (int)(c*1000f));//胸背·淤阻
                        AddInjury(actorId, 7, (int)(c*1000f));//腰腹·擦伤
                        AddInjury(actorId, 10, (int)(c*1000f));//腰腹·淤阻
                        AddInjury(actorId, 13, (int)(c*1000f));//头颈·擦伤
                        AddInjury(actorId, 16, (int)(c*1000f));//头颈·淤阻
                        AddInjury(actorId, 19, (int)(c*1000f));//左臂·擦伤
                        AddInjury(actorId, 22, (int)(c*1000f));//左臂·淤阻
                        AddInjury(actorId, 25, (int)(c*1000f));//右臂·擦伤
                        AddInjury(actorId, 28, (int)(c*1000f));//右臂·淤阻
                        AddInjury(actorId, 31, (int)(c*1000f));//左腿·擦伤
                        AddInjury(actorId, 34, (int)(c*1000f));//左腿·淤阻
                        AddInjury(actorId, 37, (int)(c*1000f));//右腿·擦伤
                        AddInjury(actorId, 40, (int)(c*1000f));//右腿·淤阻
                        if(poison_count>=2) {
                            GameData.Characters.SetCharProperty(actorId, 11, "15");
                            DateFile.instance.actorGongFas[actorId][WANBI_ID][1] = 0;
                            DateFile.instance.actorGongFas[actorId][WANBI_ID][2] = 0;
                            if(actorId==mainActorId){
                                DateFile.instance.gongFaBookPages.Remove(WANBI_ID);
                            }
                            return;
                        }
                        List<int> actorFeature = DateFile.instance.GetActorFeature(actorId, true);
                        if(actorFeature.Contains(4002)){
                            modifiedData.ChangeActorFeature.Add(new AI.ChangeActorFeatureData(actorId, 4002, 4001));
                            GameData.Characters.SetCharProperty(actorId, 54, (GameData.Characters.GetCharProperty(actorId, 54).ParseInt()+300).ToString()); // 如果只是普通破身，恢复完璧并产生寒毒

                            GameData.Characters.SetCharProperty(actorId, 81, (GameData.Characters.GetCharProperty(actorId, 81).ParseInt()+25).ToString());
                            GameData.Characters.SetCharProperty(actorId, 82, (GameData.Characters.GetCharProperty(actorId, 82).ParseInt()+25).ToString());
                            GameData.Characters.SetCharProperty(actorId, 83, (GameData.Characters.GetCharProperty(actorId, 83).ParseInt()+250).ToString());
                            GameData.Characters.SetCharProperty(actorId, 84, (GameData.Characters.GetCharProperty(actorId, 84).ParseInt()+25).ToString());
                            GameData.Characters.SetCharProperty(actorId, 85, (GameData.Characters.GetCharProperty(actorId, 85).ParseInt()+25).ToString());
                            GameData.Characters.SetCharProperty(actorId, 86, (GameData.Characters.GetCharProperty(actorId, 86).ParseInt()+25).ToString());

                            AddInjury(actorId, 57, 600);//全身·瘫痪
                            modifiedData.AddChangTrunEvents.Add(new int[] {
                                WANBI_RECOVER_EVENT_ID,
                                actorAtPlace[0],
                                actorAtPlace[1],
                                actorId });
                        } else if(actorFeature.Contains(4003)){
                            int lifeDate = DateFile.instance.GetLifeDate(actorId, 901, 1);
                            if ((actorId == mainActorId || lifeDate == mainActorId && DateFile.instance.GetLifeDate(actorId, 901, 3) == 1) || PeopleLifeAI.instance.allFamily.Contains(actorId)) {
                                modifiedData.AddChangTrunEvents.Add(new int[] { 254, actorAtPlace[0], actorAtPlace[1], actorId });
                            }
                            if (DateFile.instance.GetLifeDate(actorId, 901, 3) == 1) {
                                modifiedData.AiSetMassage.Add(new AI.AiSetMassageData(106, lifeDate, actorAtPlace[0], actorAtPlace[1], new int[] { actorId }));
                                modifiedData.SetActorMood.Add(new AI.SetActorMoodData(lifeDate, -25, 100));
                            }
                            modifiedData.AiSetMassage.Add(new AI.AiSetMassageData(105, actorId, actorAtPlace[0], actorAtPlace[1], null));
                            modifiedData.SetActorMood.Add(new AI.SetActorMoodData(actorId, -500, 0));//完璧不破，百邪不侵

                            modifiedData.ChangeActorFeature.Add(new AI.ChangeActorFeatureData(actorId, 4003, 4001));
                            if(DateFile.instance.HaveLifeDate(actorId,501)){
                                modifiedData.ActorLife.Add(new AI.ActorLifeData(AI.ModificationType.Assign, actorId, 501, 0, 0));//橡树化值归零
                            }
                            if (DateFile.instance.pregnantFeature.ContainsKey(actorId)) {
                                modifiedData.RemovePregnantFeatureData.Add(actorId);
                            }
                            if (DateFile.instance.samsaraPlatformChildrenData.ContainsKey(actorId)) {
                                modifiedData.RemoveSamsaraPlatformChildrenData.Add(actorId);
                            }

                            GameData.Characters.SetCharProperty(actorId, 53, (GameData.Characters.GetCharProperty(actorId, 53).ParseInt()+300).ToString()); // 如果春宵是带生孩子的，流产并产生赤毒

                            GameData.Characters.SetCharProperty(actorId, 81, (GameData.Characters.GetCharProperty(actorId, 81).ParseInt()+25).ToString());
                            GameData.Characters.SetCharProperty(actorId, 82, (GameData.Characters.GetCharProperty(actorId, 82).ParseInt()+25).ToString());
                            GameData.Characters.SetCharProperty(actorId, 83, (GameData.Characters.GetCharProperty(actorId, 83).ParseInt()+250).ToString());
                            GameData.Characters.SetCharProperty(actorId, 84, (GameData.Characters.GetCharProperty(actorId, 84).ParseInt()+25).ToString());
                            GameData.Characters.SetCharProperty(actorId, 85, (GameData.Characters.GetCharProperty(actorId, 85).ParseInt()+25).ToString());
                            GameData.Characters.SetCharProperty(actorId, 86, (GameData.Characters.GetCharProperty(actorId, 86).ParseInt()+25).ToString());

                            AddInjury(actorId, 48, 600);//心神·失魂
                            modifiedData.AddChangTrunEvents.Add(new int[] {
                                WANBI_KILLCHILD_EVENT_ID,
                                actorAtPlace[0],
                                actorAtPlace[1],
                                actorId });
                        } else if(actorFeature.Contains(4001)){
                            if(poison_count!=0){
                                modifiedData.ChangeActorFeature.Add(new AI.ChangeActorFeatureData(actorId, 4001, 4002));
                            }
                            if(actorFeature.Contains(1002)){
                                GameData.Characters.SetCharProperty(actorId, 53, (GameData.Characters.GetCharProperty(actorId, 53).ParseInt()+300).ToString()); // 石芯玉女每时节积累寒毒
                            }
                        }
                    }
                }
            }
        }

        [HarmonyPatch(typeof(ArchiveSystem.GameData.ReadonlyData), "Load")]
        public static class ReadonlyDataLoad {
            public static void Postfix(){
                logger("event_and_effect_patched.");
                NEED_PATCHED=false;
                DateFile.instance.trunEventDate[WANBI_KILLCHILD_EVENT_ID]=new Dictionary<int,string>(DateFile.instance.trunEventDate[254]);
                DateFile.instance.trunEventDate[WANBI_KILLCHILD_EVENT_ID][0]="化去胎儿";
                DateFile.instance.trunEventDate[WANBI_KILLCHILD_EVENT_ID][99]="梦到<color=#E3C66DFF>「相枢」</color>入怀，大惊失色，强忍心中悲痛，念起<color=#E4504DFF>「完璧不破法」</color>口诀，日夜行功，终于在|将腹中胎儿化去，虽然因为战胜了<color=#E3C66DFF>「相枢」</color>没有感到太过痛苦，但心神的损伤终究不可避免。";
                DateFile.instance.trunEventDate[WANBI_RECOVER_EVENT_ID]=new Dictionary<int,string>(DateFile.instance.trunEventDate[229]);
                DateFile.instance.trunEventDate[WANBI_RECOVER_EVENT_ID][0]="瘫痪在床";
                DateFile.instance.trunEventDate[WANBI_RECOVER_EVENT_ID][99]="因在|春宵时不慎被伤到<color=#E4504DFF>「完璧不破法」</color>的罩门，行功出岔，瘫痪在床。";
                DateFile.instance.trunEventDate[WANBI_BREAK_HUAN_EVENT_ID]=new Dictionary<int,string>(DateFile.instance.trunEventDate[219]);
                DateFile.instance.trunEventDate[WANBI_BREAK_HUAN_EVENT_ID][0]="幻毒惑心";
                DateFile.instance.trunEventDate[WANBI_BREAK_HUAN_EVENT_ID][99]="自恃有<color=#E4504DFF>「完璧不破法」</color>护体，对自己身重幻毒毫无察觉，终于在|毒发。\n毒发时，她一边求欢一边饮酒，直到将店中镇店的|都喝尽了方才惊醒\n可一身功力早就化为流水随着美酒与凑热闹的男人们远去了";
                DateFile.instance.trunEventDate[WANBI_BREAK_FU_EVENT_ID]=new Dictionary<int,string>(DateFile.instance.trunEventDate[219]);
                DateFile.instance.trunEventDate[WANBI_BREAK_FU_EVENT_ID][0]="腐毒断肠";
                DateFile.instance.trunEventDate[WANBI_BREAK_FU_EVENT_ID][99]="自恃有<color=#E4504DFF>「完璧不破法」</color>护体，对自己身重腐毒毫无察觉，终于在|毒发。\n毒发时，她痛得满地打滚不断呻吟，疼痛难忍之时偶会运功猛击自己的腹部试图减轻痛苦\n终究不慎在一式|之下将自己破功。";

                int wanbi_positive_id=DateFile.instance.gongFaDate[WANBI_ID][103].ParseInt();
                DateFile.instance.gongFaFPowerDate[wanbi_positive_id][99]+="\n\n完璧不破法会在每时节恢复成年非童女身女性运功者的童女身，但每次从破身状态恢复时会产生赤毒与全身性外伤。\n运用者会误将腹中胎儿当作相枢化身，并会因此运功化去腹中胎儿。此时时会产生大量赤毒并导致心神内伤。\n产生寒毒和赤毒时会微量提高人物的防御属性。\n\n运功者每时节会运功排毒，排毒量至为当前毒素与毒素耐受上限的1/5中更小的数值，但在任意有一种毒超过耐受上限时，运功者会因为排毒而破身。如果两种或以上的毒同时突破上限，运功者会因为功法反噬而暂时破功。破功状态会清除功法研读进度（但不会清除心法补全数据），且将大于15岁运用者的年龄重置为15岁。\n\n破功时完璧不破法不会恢复童女身。\n\n<color=#AE5AC8FF>幻毒</color>是功法正练或冲解的罩门，过月时如果罩门毒素超过上限，则永久破功，功法威力永久-500%，且与真气无关的主要属性和防御属性归零。";
                int wanbi_negative_id=DateFile.instance.gongFaDate[WANBI_ID][104].ParseInt();
                DateFile.instance.gongFaFPowerDate[wanbi_negative_id][99]+="\n\n完璧不破法会在每时节恢复成年非童女身女性运功者的童女身，但每次从破身状态恢复时会产生赤毒与全身性外伤。\n运用者会误将腹中胎儿当作相枢化身，并会因此运功化去腹中胎儿。此时时会产生大量赤毒并导致心神内伤。\n产生寒毒和赤毒时会微量提高人物的防御属性。\n\n运功者每时节会运功排毒，排毒量至为当前毒素与毒素耐受上限的1/5中更小的数值，但在任意有一种毒超过耐受上限时，运功者会因为排毒而破身。如果两种或以上的毒同时突破上限，运功者会因为功法反噬而暂时破功。破功状态会清除功法研读进度（但不会清除心法补全数据），且将大于15岁运用者的年龄重置为15岁。\n\n破功时完璧不破法不会恢复童女身。\n\n<color=#6DB75FFF>腐毒</color>是功法逆练或冲解的罩门，过月时如果罩门毒素超过上限，则永久破功，功法威力永久-500%，且与真气无关的主要属性和防御属性归零。";
            }
        }*/
        // DateFile.instance.Poison(actorId, i, true) >= DateFile.instance.MaxPoison(actorId, i, true);
        /*
        [HarmonyPatch(typeof(AI.CharacterAge), "ChangeAge")]
        public static class CharacterAgeChangeAge { // 多线程
            public static void Postfix(System.Random random, int actorId, AI.AgeChangeModifiedData modifiedData){
                int flag=DateFile.instance.GetGongFaFTyp(actorId,TAIYIN_ID);
                if(flag==0){
//                     List<int> actorFeature = DateFile.instance.GetActorFeature(actorId, true);
//                     if(actorFeature.Contains(4002))
//                         DateFile.instance.ChangeActorFeature(actorId, 4002, 4001);
                }else if(flag==1){ // 逆练可以在破身之后孤雌生子
                    List<int> actorFeature = DateFile.instance.GetActorFeature(actorId, true);
                    if(actorFeature.Contains(4001)){
                    }else if(actorFeature.Contains(4002)){
                        GameData.Characters.SetCharProperty(actorId, 39, "8000"); //内息恒定800.0
                        int age=int.Parse(DateFile.instance.GetActorDate(actorId, 11, false));
                        int health=DateFile.instance.Health(actorId);
                        if (age>14){
                            DateFile.instance.actorLife[actorId].Add(901, new List<int>
                            {
                                1,
                                actorId,
                                actorId,
                                true,
                                true
                            });
                            DateFile.instance.pregnantFeature.Add(actorId, new string[]
                            {
                                Characters.GetCharProperty(actorId, 101),
                                Characters.GetCharProperty(actorId, 101)
                            });
                            AddInjury(actorId, 1, 10*age);//胸背·擦伤
                            AddInjury(actorId, 4, 10*age);//胸背·淤阻
                            AddInjury(actorId, 7, 10*age);//腰腹·擦伤
                            AddInjury(actorId, 10, 10*age);//腰腹·淤阻
                            AddInjury(actorId, 13, 10*age);//头颈·擦伤
                            AddInjury(actorId, 16, 10*age);//头颈·淤阻
                            AddInjury(actorId, 19, 10*age);//左臂·擦伤
                            AddInjury(actorId, 22, 10*age);//左臂·淤阻
                            AddInjury(actorId, 25, 10*age);//右臂·擦伤
                            AddInjury(actorId, 28, 10*age);//右臂·淤阻
                            AddInjury(actorId, 31, 10*age);//左腿·擦伤
                            AddInjury(actorId, 34, 10*age);//左腿·淤阻
                            AddInjury(actorId, 37, 10*age);//右腿·擦伤
                            AddInjury(actorId, 40, 10*age);//右腿·淤阻
                            DateFile.AddGongfaEternalPowerChange(actorId, TAIYUAN_ID, 10, actorId, TAIYUAN_ID);
                            DateFile.AddGongfaEternalPowerChange(actorId, YAOCHI_ID, 10, actorId, YAOCHI_ID);
                            modifiedData.ChangeActorFeature.Add(new AI.ChangeActorFeatureData(actorId, 4002, 4003));
                        }
                    }else if(actorFeature.Contains(4003)){
                    }
                }
            }
        }
        public const int TAIYIN_ID=803;
        public const int TAIYUAN_ID=TAIYIN_ID-1;
        public const int YAOCHI_ID=TAIYIN_ID-2;
        [HarmonyPatch(typeof(AI.CharacterAge), "OnChildbirth")]//正练怀孕生子之后恢复15岁童女之身，逆练生子之后恢复童女身，但会受到伤害
        public static class CharacterAgeOnChildbirth {  // 单线程
            public static void Postfix(int actorId,int mainActorId){
                int flag=DateFile.instance.GetGongFaFTyp(actorId,TAIYIN_ID);
                if(flag==0 || flag==1){
                    DateFile.instance.actorLife[actorId][901][0] = -99;
                    DateFile.instance.ChangeActorFeature(actorId, 4002, 4001);
                    GameData.Characters.SetCharProperty(actorId, 11, "15");
                    DateFile.instance.actorGongFas[actorId][TAIYIN_ID][1] = 0;
                    DateFile.instance.actorGongFas[actorId][TAIYIN_ID][2] = 0;
                    if(actorId==mainActorId){
                        DateFile.instance.gongFaBookPages.Remove(TAIYIN_ID);
                        //DateFile.instance.RemoveMainActorEquipGongFa(TAIYIN_ID);
                    }
                    if(flag==1){
                        DateFile.AddGongfaEternalPowerChange(actorId, TAIYUAN_ID, 10, actorId, TAIYUAN_ID);
                        DateFile.AddGongfaEternalPowerChange(actorId, YAOCHI_ID, 10, actorId, YAOCHI_ID);
                    }
                }
            }
        }*/
#region breaking_change_patch
        /*[HarmonyDebug] // 还是用祠堂养赛亚人好了
        [HarmonyPatch(typeof(AI.CharacterAge), "OnPregnancy")] // 茄子表示物品属于BUG，需要Fix而不是Repair，一看就是没玩过红警的……
        public static class CharacterAgeOnPregnancy {
            public static IEnumerable<CodeInstruction> DO_NOT_WORK_Transpiler(MethodBase original,IEnumerable<CodeInstruction> instructions) {
                logger("正在注入AI.CharacterAge OnPregnancy");
                return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                        // 226	0226	ldloc.0
                        // 227	0227	ldc.i4.5
                        // 228	0228	beq.s	236 (0234) ldc.i4.1
                        // 229	022A	ldloc.0
                        // 230	022B	ldc.i4.3
                        // 231	022C	beq.s	236 (0234) ldc.i4.1
                        // 232	022E	ldloc.0
                        // 233	022F	ldc.i4.1
                        // 234	0230	ceq
                        // 235	0232	br.s	237 (0235) br.s 239 (0238)
                        // 236	0234	ldc.i4.1
                        // 237	0235	br.s	239 (0238) stloc.s V_9 (9)
                        // 238	0237	ldc.i4.0
                        new CodeMatch(i=>i.opcode==OpCodes.Ldloc_0),
                        new CodeMatch(i=>i.opcode==OpCodes.Ldc_I4_5),
                        new CodeMatch(i=>i.opcode==OpCodes.Beq),
                        new CodeMatch(i=>i.opcode==OpCodes.Ldloc_0),
                        new CodeMatch(i=>i.opcode==OpCodes.Ldc_I4_3),
                        new CodeMatch(i=>i.opcode==OpCodes.Beq),
                        new CodeMatch(i=>i.opcode==OpCodes.Ldloc_0),
                        new CodeMatch(i=>i.opcode==OpCodes.Ldc_I4_1),
                        new CodeMatch(i=>i.opcode==OpCodes.Ceq),
                        new CodeMatch(i=>i.opcode==OpCodes.Br),
                        new CodeMatch(i=>i.opcode==OpCodes.Ldc_I4_1),
                        new CodeMatch(i=>i.opcode==OpCodes.Br),
                        new CodeMatch(i=>{logger("xxfound.");return true;})
                    ).Repeat( matcher => // Do the following for each match
                        matcher
                        .Advance(12) // Move cursor to after ldfld
                        .SetAndAdvance(
                            OpCodes.Ldc_I4_1,null
                        )
                    ).InstructionEnumeration();
            }
            public static void Postfix(int actorId,int mainActorId,AI.AgeChangeModifiedData modifiedData) {
                if (mainActorId == DateFile.instance.GetLifeDate(actorId, 901, 2)&&DateFile.instance.actorLife[actorId][901][0]<=10 && DateFile.instance.actorLife[actorId][901][0]>0 )
                    modifiedData.SetEvent.Add(new AI.SetEventData(new int[] { 0, mainActorId, 248 }, false, true));
            }
        }*/
        [HarmonyPatch(typeof(MakeSystem), "StartFixItem")] // 茄子表示物品属于BUG，需要Fix而不是Repair，一看就是没玩过红警的……
        public static class MakeSystemStartFixItem {
            public static void Postfix() {
                int daibu=int.Parse(DateFile.instance.GetActorDate(DateFile.instance.MianActorID(), 311, false)); // 装备代步修改
                if(daibu>0&&!DateFile.instance.itemsChangeDate.ContainsKey(daibu)){
                    DateFile.instance.ChangItemDate(daibu, 92, 1000000, true);
                    DateFile.instance.ChangItemDate(daibu, 93, 1000, true);
                }
                if(better_weapon) {
                    foreach(int i in new int[]{0,1,2,3,5,6}) {
                        int weapon=int.Parse(DateFile.instance.GetActorDate(DateFile.instance.MianActorID(), 301+i, false)); // 装备代步修改
                        if(weapon>0){
                            if(i<=3){
                                DateFile.instance.ChangItemDate(weapon, 502, 10*(200-int.Parse(DateFile.instance.GetItemDate(weapon, 502, true, -1))), false);//最近攻击距离
                                DateFile.instance.ChangItemDate(weapon, 503, 10*(900-int.Parse(DateFile.instance.GetItemDate(weapon, 503, true, -1))), false);//最远攻击距离
                                DateFile.instance.ChangItemDate(weapon, 711, 10*(1000-int.Parse(DateFile.instance.GetItemDate(weapon, 711, true, -1))), false); // 发挥，只有武器可以有发挥
                            }
                            DateFile.instance.ChangItemDate(weapon, 902, 10*(100-int.Parse(DateFile.instance.GetItemDate(weapon, 902, true, -1))), false);//耐久上限
                            DateFile.instance.ChangItemDate(weapon, 501, 10*(0-int.Parse(DateFile.instance.GetItemDate(weapon, 501, true, -1))), false); // 重量
                            DateFile.instance.ChangItemDate(weapon, 601, 10*(10000-int.Parse(DateFile.instance.GetItemDate(weapon, 601, true, -1))), false);//破刃/甲
                            DateFile.instance.ChangItemDate(weapon, 603, 10*(10000-int.Parse(DateFile.instance.GetItemDate(weapon, 603, true, -1))), false);//坚韧
                        }
                    }
                }
            }
        }
        [HarmonyPatch(typeof(MakeSystem), "StartChangeItem")]
        public static class MakeSystemStartChangeItem {
            public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions){ // 通过ThirdItemId的读写修改进行注入
                logger("   ->搜索ldc.i4.0后面的GetChangeVaule");
                uint flag=0xDEADBEFF;
                int count=0;
                MethodInfo find=typeof(MakeSystem).GetMethod("GetChangeVaule",(BindingFlags)(-1)); // -1是BindingFlags.All的意思。
                foreach (var instruction in instructions) { // 理论上只有before和hit节需要处理
                    if(flag==0xDEADBEFF && instruction.opcode==OpCodes.Ldc_I4_0){ // before
                        yield return instruction;
                        flag=0xDEADCAFE;
                    } else if(flag==0xDEADCAFE){
                        //logger("[II]:DEADCAFE at "+count+", current instruction: "+instruction.opcode.ToString());
                        // instruction.opcode==OpCodes.Callvirt && (MethodInfo)instruction.operand == typeof(what_you_want).GetMethod("to_call")
                        // instruction.opcode==OpCodes.Ldfld && ((FieldInfo)instruction.operand).Name == "name"
                        if(instruction.opcode==OpCodes.Call && (MethodInfo)instruction.operand == find){ // hit
                            logger("[II]:找到int changeVaule = this.GetChangeVaule(i, 0);. count="+count+" current instruction: "+instruction.opcode.ToString());
                            //yield return instruction; //int
                            // stack: [this i 0]
                            yield return new CodeInstruction(OpCodes.Call,typeof(MakeSystemStartChangeItem).GetMethod("GetChangeVaule_0"));
                            // instruction should return int(=0) for skipping further instructions
                            flag=0;
                        }else{ // miss
                            yield return instruction;
                            flag=0xDEADBEFF;
                        }
                    } else { // after
                        yield return instruction;
                    }
                    count++;
                }
                if(flag!=0)logger("[EE]:注入失败，请检查源码是否发生了改变");
            }
            public static int GetChangeVaule_0(MakeSystem makeSystem, int index, int zero){
                int num=int.Parse(DateFile.instance.GetItemDate(makeSystem.thirdItemId[index], 51, true, -1));
                if (int.Parse(DateFile.instance.changeEquipDate[num][4])==0) {
                    num += (int.Parse(DateFile.instance.GetItemDate(makeSystem.secondItemId, 1, true, -1)) - 1) * 100;
                } // 原始绿皮代码
                int ret=int.Parse(DateFile.instance.changeEquipDate[num][2]);
                if (num<200) {
                    // 武器
                    const int x=70;
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 502, 10*(200-int.Parse(DateFile.instance.GetItemDate(makeSystem.secondItemId, 502, true, -1))), false);//最近攻击距离
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 503, 10*(900-int.Parse(DateFile.instance.GetItemDate(makeSystem.secondItemId, 503, true, -1))), false);//最远攻击距离
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 711, 10*(1000-int.Parse(DateFile.instance.GetItemDate(makeSystem.secondItemId, 711, true, -1))), false); // 发挥，只有武器可以有发挥
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 14, 10000, false);//武器变招
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 16, 10000, false);//武器追击
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, x+1, 100000, true);//六种毒
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, x+2, 100000, true);
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, x+3, 100000, true);
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, x+4, 100000, true);
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, x+5, 100000, true);
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, x+6, 100000, true);
                } else if (num<300) {
                    // 防具
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 50022, 10000, false); // 守御效率
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 50023, 10000, false); // 疗伤效率
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 51110, 10000, false); // 驱毒效率
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 51101, 10000, false); // 提气速度
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 51102, 10000, false); // 架势速度
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 51106, 10000, false); // 施展速度
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 51108, 10000, false); // 武器切换
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 51109, 10000, false); // 移动速度
                    DateFile.instance.ChangItemDate(makeSystem.secondItemId, 51369, 100, false); // 内息%
                } else if (num<400) {
                    // 饰品
                }
                DateFile.instance.ChangItemDate(makeSystem.secondItemId, 902, 10*(1000-int.Parse(DateFile.instance.GetItemDate(makeSystem.secondItemId, 902, true, -1))), false); // 重量
                DateFile.instance.ChangItemDate(makeSystem.secondItemId, 501, 10*(0-int.Parse(DateFile.instance.GetItemDate(makeSystem.secondItemId, 501, true, -1))), false); // 重量
                DateFile.instance.ChangItemDate(makeSystem.secondItemId, 601, 100000, false);//破刃/甲
                DateFile.instance.ChangItemDate(makeSystem.secondItemId, 603, 100000, false);//坚韧
                return ret;
            }
            /*
            public static int Craft(int changeTyp,MakeSystem makeSystem){
                if (changeTyp>0){
                    for(int num=0;num<alen;num++){
                        DateFile.instance.ChangItemDate(makeSystem.secondItemId, aadd[num], aadd2[num], false);
                    }
                    if( (changeTyp>=50061&&changeTyp<=50066) || (changeTyp>=50081&&changeTyp<=50086) ){ // 对防具增加护体 御气 内息 卸力 拆招 闪避 时，或对饰品修改人物五维时，修改成下列增强
                        foreach(int num in def_add){
                            Dictionary<int, string> dictionary = DateFile.instance.changeEquipDate[num];
                            DateFile.instance.ChangItemDate(makeSystem.secondItemId, int.Parse(dictionary[2]), 10*int.Parse(dictionary[3]), false);
                        }
                        for(int num=0;num<dlen;num++){
                            DateFile.instance.ChangItemDate(makeSystem.secondItemId, dadd[num], dadd2[num], false);
                        }
                    }else{
                        foreach(int num in normal_add){
                            Dictionary<int, string> dictionary = DateFile.instance.changeEquipDate[num];
                            DateFile.instance.ChangItemDate(makeSystem.secondItemId, int.Parse(dictionary[2]), 10*int.Parse(dictionary[3]), false);
                        }
                        for(int num=0;num<nlen;num++){
                            DateFile.instance.ChangItemDate(makeSystem.secondItemId, nadd[num], nadd2[num], false);
                        }
                    }
                }
                return 0;
            }*/
        }
        [HarmonyPatch(typeof(BattleEndWindow), "BattleEnd")]
        class BattleEndWindowBattleEnd {
            public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions){ // 通过ThirdItemId的读写修改进行注入
                logger("   ->搜索Ldfld battleBooty后面的.Count");
                uint flag=0xDEADBEFF;
                int count=0;
                // MethodInfo find=typeof(List<int[]>).GetMethod("get_Count",(BindingFlags)(-1)); // -1是BindingFlags.All的意思。
                MethodInfo find=typeof(List<int[]>).GetProperty("Count").GetGetMethod(); // -1是BindingFlags.All的意思。
                var iter=instructions.GetEnumerator();
                while (iter.MoveNext()) {
                    count++;
                    CodeInstruction instruction=iter.Current;
                    // instruction.opcode==OpCodes.Callvirt && (MethodInfo)instruction.operand == find
                    // instruction.opcode==OpCodes.Ldfld && ((FieldInfo)instruction.operand).Name == "name"
                    if(flag!=0 && instruction.opcode==OpCodes.Ldfld && ((FieldInfo)instruction.operand).Name == "battleBooty"){
                        if(iter.MoveNext()){
                            count++;
                            var instruction2=iter.Current;
                            if(instruction2.opcode==OpCodes.Callvirt && (MethodInfo)instruction2.operand == find){
                                logger("[II]:found");
                                yield return new CodeInstruction(OpCodes.Ldfld,typeof(BattleEndWindow).GetField("joinEnemy",(BindingFlags)(-1)));
                                yield return new CodeInstruction(OpCodes.Ldarg_0);
                                yield return new CodeInstruction(OpCodes.Ldfld,typeof(BattleEndWindow).GetField("battleBooty",(BindingFlags)(-1)));
                                yield return new CodeInstruction(OpCodes.Ldarg_1);
                                yield return new CodeInstruction(OpCodes.Ldarg_2);
                                yield return new CodeInstruction(OpCodes.Call,typeof(BattleEndWindowBattleEnd).GetMethod("ExtraLoot"));
                                yield return new CodeInstruction(OpCodes.Ldarg_0);
                                yield return instruction;
                                yield return instruction2;
                                logger("[II]:patched");
                                flag=0;
                            } else { yield return instruction;yield return instruction2; }
                        } else { yield return instruction; }
                    } else {yield return instruction;}
                }
                if(flag!=0){logger("[EE]:注入失败，请检查源码是否发生了改变");}else{logger("[II]:Done.");}
            }
            public static void ExtraLoot(List<int> ___joinEnemy, List<int[]> ___battleBooty, bool actorWin, int actorRun){
                if(drop_even_if_failed||(actorWin&&(drop_with_qiecuo||BattleSystem.instance.battleTyp == 0))){
                    int num = BattleSystem.instance.ActorId(true, false);
                    for (int num36 = 0; num36 < DateFile.instance.enemyBattlerIdDate.Count; num36++) {
                        int num37 = DateFile.instance.enemyBattlerIdDate[num36];
                        bool flag33 = ___joinEnemy.Contains(num37);
                        if (!flag33) {
                            int num38 = (num37 == BattleSystem.instance.mianEnemyId) ? 100 : 50;
                            List<int> list2 = new List<int>();

                            List<int> list = new List<int>();
                            // index :301-303:武器，304:帽子 305:衣装 306:防具 307:鞋子 308-310:宝物 311:代步 312:蛐蛐
                            if (UnityEngine.Random.Range(0, 100)<extra_wuqi_percentage){list.Add(301);list.Add(302);list.Add(303);}
                            if (UnityEngine.Random.Range(0, 100)<extra_maozi_percentage){list.Add(304);}
                            if (UnityEngine.Random.Range(0, 100)<extra_wear_percentage){list.Add(305);}
                            if (UnityEngine.Random.Range(0, 100)<extra_hujia_percentage){list.Add(306);}
                            if (UnityEngine.Random.Range(0, 100)<extra_xiezi_percentage){list.Add(307);}
                            if (UnityEngine.Random.Range(0, 100)<extra_baowu_percentage){list.Add(308);list.Add(309);list.Add(310);}
                            if (UnityEngine.Random.Range(0, 100)<extra_ququ_percentage){list.Add(311);}
                            if (UnityEngine.Random.Range(0, 100)<extra_daibu_percentage){list.Add(312);}

                            foreach (int index in list)
                            {
                                int num40 = int.Parse(DateFile.instance.GetActorDate(num37, index, false));
                                bool flag34 = num40 > 0 && int.Parse(DateFile.instance.GetItemDate(num40, 3, true, -1)) == 1;
                                if (flag34)
                                {
                                    //int num41 = int.Parse(DateFile.instance.GetItemDate(num40, 997, true, -1)) * num34 / 100 * num32 / 100;
                                    bool flag35 = true;//UnityEngine.Random.Range(0, 100) < num41 * num38 / 100;
                                    if (flag35)
                                    {
                                        list2.Add(num40);
                                    }
                                }
                            }
                            bool flag36 = list2.Count > 0;
                            if (flag36)
                            {
                                foreach(int num42 in list2){
                                    DateFile.instance.ChangeTwoActorItem(num37, num, num42, 1, -1, 0, 0);
                                    ___battleBooty.Add(new int[]
                                    {
                                        num42,
                                        1
                                    });
                                }
                            }
                            Dictionary<int, int> dictionary = new Dictionary<int, int>();
                            List<int> list3 = new List<int>(DateFile.instance.actorItemsDate[num37].Keys);
                            for (int num43 = 0; num43 < list3.Count; num43++)
                            {
                                int num44 = list3[num43];
                                bool flag37 = num44 > 0 && int.Parse(DateFile.instance.GetItemDate(num44, 3, true, -1)) == 1;
                                if (flag37)
                                {
                                    //int num45 = int.Parse(DateFile.instance.GetItemDate(num44, 997, true, -1)) * num34 / 100 * num32 / 100;
                                    bool flag38 = true;//UnityEngine.Random.Range(0, 100) < num45 * num38 / 100;
                                    if (flag38)
                                    {
                                        int itemNumber = DateFile.instance.GetItemNumber(num37, num44);
                                        dictionary.Add(num44, itemNumber);
                                    }
                                }
                            }
                            bool flag39 = dictionary.Count > 0;
                            if (flag39)
                            {
                                List<int> list4 = new List<int>(dictionary.Keys);
                                int num46 = list4.Count;//Mathf.Min(3, list4.Count);
                                for (int index = list4.Count-1; index >= 0; index--)
                                {
                                    int num48 = list4[index];
                                    //bool flag40 = UnityEngine.Random.Range(0, 100) < 100 - num47 * 20;
                                    int percent=extra_default_percentage;
                                    if(100000<=num48&&num48<200000){percent=extra_xiaohaopin_percentage;}
                                    if(200000<=num48&&num48<300000){percent=extra_shiwu_percentage;}
                                    if(300000<=num48&&num48<400000){percent=extra_3xxxxx_percentage;}
                                    if(400000<=num48&&num48<500000){percent=extra_jiyishu_percentage;}
                                    if(500000<=num48&&num48<700000){percent=extra_zhenchuanshu_percentage;}
                                    if(700000<=num48&&num48<900000){percent=extra_shouchaoshu_percentage;}

                                    if (UnityEngine.Random.Range(0, 100)<percent) {
                                        int num49 = dictionary[num48];
                                        list4.Remove(list4[index]);
                                        DateFile.instance.ChangeTwoActorItem(num37, num, num48, num49, -1, 0, 0);
                                        ___battleBooty.Add(new int[] { num48, num49 });
                                    }
                                }
                            }
                            bool flag41 = num37 < 0;
                            if (flag41)
                            {
                                for (int num50 = 0; num50 < 12; num50++)
                                {
                                    DateFile.instance.SetActorEquip(num37, 301 + num50, 0);
                                }
                                List<int> list5 = new List<int>(DateFile.instance.actorItemsDate[num37].Keys);
                                for (int num51 = 0; num51 < list5.Count; num51++)
                                {
                                    int itemId2 = list5[num51];
                                    DateFile.instance.LoseItem(num37, itemId2, DateFile.instance.GetItemNumber(num37, itemId2), true, true, -1);
                                }
                            }
                        }
                    }
                }
            }
        }
#endregion
    }
}

#region Taiyin
#if USE_DISCARD_CODE
        //public const int TAIYIN_ID=809;
        public const int TAIYIN_ID=803;
        public const int TAIYUAN_ID=TAIYIN_ID-1;
        public const int YAOCHI_ID=TAIYIN_ID-2;
        [HarmonyPatch(typeof(AI.CharacterAge), "OnChildbirth")]//正练怀孕生子之后恢复童女之身，逆练生子之后遗忘此武功的修习进度
        public static class CharacterAgeOnChildbirth {  // 单线程
            public static void Postfix(int actorId,int mainActorId){
                int flag=DateFile.instance.GetGongFaFTyp(actorId,TAIYIN_ID);
                logger("BIRTH: actor"+actorId+"mian"+mainActorId+",flag="+flag);
                if(flag==0){
                    // DateFile.instance.ChangeActorFeature(actorId, 4002, 4001); // 保留这一时节的数据，在下一时节恢复处子身
                    GameData.Characters.SetCharProperty(actorId, 11, "5");
                    DateFile.instance.actorGongFas[actorId][TAIYIN_ID][1] = 0;
                    DateFile.instance.actorGongFas[actorId][TAIYIN_ID][2] = 0;
                }else if(flag==1){
                    logger("actor"+actorId+"mian"+mainActorId);
                    DateFile.instance.actorLife[actorId][901][0] = -99;
                    GameData.Characters.SetCharProperty(actorId, 11, "15");
                    DateFile.instance.ChangeActorFeature(actorId, 4002, 4001);
                    DateFile.instance.actorGongFas[actorId][TAIYIN_ID][0] = 100;
                    DateFile.instance.actorGongFas[actorId][TAIYIN_ID][1] = 0;
                    DateFile.instance.actorGongFas[actorId][TAIYIN_ID][2] = 0;
                    if(actorId==mainActorId){
                        DateFile.instance.gongFaBookPages.Remove(TAIYIN_ID);
                        //DateFile.instance.RemoveMainActorEquipGongFa(TAIYIN_ID);
                    }
                }
            }
        }
    #region magic_notes
    /*
    this.CatchActorAtt = new int[]
    {
        4,
        18,
        25,
        101,
        12,
        39,//当前内息
        34,//34-37像是4个免伤开关（左臂/右臂/左腿/右腿，=1免伤（大概是断臂的意思））
        35,
        36,
        37,
        31,
        51,//六种毒数量
    ..    56,
        61,//主要属性
    ..    66,
        -61,//铭刻？
    ..    -66,
        706,//无属性内力数量
        801,
        802,
        901,
        501,//技艺资质
    ..    516,
        -501,//铭刻？
    ..    -516,
        601,//功法资质
    ..    614,
        -601,//铭刻？
    ..    -614,
        551,//早熟3-均衡2-晚成4
        651,//早熟3-均衡2-晚成4
        401,//人物拥有资源
    ..    407,//这里的大概是钱
        995,
        996
    };
    2 出家？SetLoveSocial  bool flag3 = int.Parse(DateFile.instance.GetActorDate(actorId2, 2, false)) != 0 || int.Parse(DateFile.instance.GetActorDate(actorId1, 2, false)) != 0; 会增加401。
    4 心情？
    8 (getNpc || int.Parse(this.GetActorDate(num, 8, false)) == 1)
    9 商队相关 shopTyp = int.Parse(DateFile.instance.GetGangDate(int.Parse(DateFile.instance.GetActorDate(this.actorShopId, 9, false)), 16));
    11 年龄
    12 健康
    13 健康上限
    14 性别 男1女2
    15 魅力
    16 处世立场
    17 男生女相/女生男相
    18 入魔值？？入魔是501？ Datafile.instance.GetLifeDate(actorId, 501, 0); Datafile.instance.actorLife[actorId][501][0]
    24 生育能力
    25 出生时节
    26: (getDieActor || int.Parse(this.GetActorDate(num, 26, false)) == 0)
    41..46  毒抗（== -1对preset actor来说是百毒不侵，但对Actor来说并不是这样）
    51..56  膂力 体质 灵敏 根骨 悟性 定力
    81..86  护体 御气 内息 卸力 拆招 闪避
    1361..1372 膂力 体质 灵敏 根骨 悟性 定力 护体 御气 内息 卸力 拆招 闪避 这12项属性的百分比发挥

    202 喜好物品
    203 厌恶物品
    997 gang？出现在poison判定：bool flag = int.Parse(this.presetActorDate[int.Parse(this.GetActorDate(key, 997, false))][41 + poisonId]) == -1;
    */

    #endregion
    #region aux_pregnancy
        static void change(int actorId, int key, int val){
            string a=GameData.Characters.GetCharProperty(actorId, key);
            if(a!=null)
                val+=a.ParseInt();
            if(val<0)
                val=0;
            GameData.Characters.SetCharProperty(actorId, key, val.ToString());
        }
        static void recover(System.Random random, int actorId, int key, int val, int to=0){
            int curr=0;
            if(to==0){
                string a=GameData.Characters.GetCharProperty(actorId,-key);
                if(a!=null)
                    to=a.ParseInt();
            }
            string b=GameData.Characters.GetCharProperty(actorId, key);
            if(b!=null)
                curr=b.ParseInt();
            if(curr<to){
                curr+=Mathf.Max(1,(int)((float)(to-curr)*((float)random.Next(0,100))/(float)val));
                GameData.Characters.SetCharProperty(actorId, key, curr.ToString());
            }
        }
        #region aux_pregnancy_GetFaceCharm
        static int GetFaceCharm(int newActorGender, int actorID) {
            string[] Xarray3 = GameData.Characters.GetCharProperty(actorID, 995).Split(new char[]
            {
                '|'
            });
            int[] faceDate = new int[Xarray3.Length];
            for (int j = 0; j < Xarray3.Length; j++)
            {
                faceDate[j] = int.Parse(Xarray3[j]);
            }
            int num = 0;
            string[] array = new string[faceDate.Length];
            int num2 = newActorGender - 1;
            int num3 = Mathf.Min(faceDate[0], int.Parse(GetSprites.instance.actorFaceDate[num2][99]) - 1);
            for (int i = 0; i < faceDate.Length; i++)
            {
                array[i] = faceDate[i].ToString();
                switch (i)
                {
                case 1:
                {
                    int num4 = SingletonObject.getInstance<DynamicSetSprite>().GetGroupLength("actorFace", new int[]
                    {
                        num2,
                        num3,
                        2
                    }) / 3;
                    bool flag = faceDate[i] >= num4 * 2;
                    if (flag)
                    {
                        num += 4;
                    }
                    else
                    {
                        bool flag2 = faceDate[i] >= num4;
                        if (flag2)
                        {
                            num += 2;
                        }
                    }
                    break;
                }
                case 2:
                {
                    int num5 = SingletonObject.getInstance<DynamicSetSprite>().GetGroupLength("actorFace", new int[]
                    {
                        num2,
                        num3,
                        3
                    }) / 3;
                    bool flag3 = faceDate[i] >= num5 * 2;
                    if (flag3)
                    {
                        num += 2;
                    }
                    else
                    {
                        bool flag4 = faceDate[i] == 0 || faceDate[i] >= num5;
                        if (flag4)
                        {
                            num++;
                        }
                    }
                    break;
                }
                case 3:
                {
                    int num6 = SingletonObject.getInstance<DynamicSetSprite>().GetGroupLength("actorFace", new int[]
                    {
                        num2,
                        num3,
                        4
                    }) / 3;
                    bool flag5 = faceDate[i] >= num6 * 2;
                    if (flag5)
                    {
                        num += 8;
                    }
                    else
                    {
                        bool flag6 = faceDate[i] >= num6;
                        if (flag6)
                        {
                            num += 4;
                        }
                    }
                    break;
                }
                case 4:
                {
                    int num7 = SingletonObject.getInstance<DynamicSetSprite>().GetGroupLength("actorFace", new int[]
                    {
                        num2,
                        num3,
                        6
                    }) / 3;
                    bool flag7 = faceDate[i] >= num7 * 2;
                    if (flag7)
                    {
                        num += 2;
                    }
                    else
                    {
                        bool flag8 = faceDate[i] >= num7;
                        if (flag8)
                        {
                            num++;
                        }
                    }
                    break;
                }
                case 5:
                {
                    int num8 = SingletonObject.getInstance<DynamicSetSprite>().GetGroupLength("actorFace", new int[]
                    {
                        num2,
                        num3,
                        7
                    }) / 3;
                    bool flag9 = faceDate[i] >= num8 * 2;
                    if (flag9)
                    {
                        num += 4;
                    }
                    else
                    {
                        bool flag10 = faceDate[i] >= num8;
                        if (flag10)
                        {
                            num += 2;
                        }
                    }
                    break;
                }
                }
            }
            return num*45;
        }
        static void AddInjury(int actorId,int id,int power){
            var a=DateFile.instance.actorInjuryDate;
            if (a[actorId].ContainsKey(id)) {
                a[actorId][id] += power;
            } else {
                a[actorId].Add(id, power);
            }
        }
        #endregion
        static void tryGrow(int actorId,int grow){
            int age=int.Parse(DateFile.instance.GetActorDate(actorId, 11, true))+grow;//current age
            int maxAge=int.Parse(DateFile.instance.GetActorDate(actorId, 13, true));
            int health=DateFile.instance.Health(actorId)-grow;
            if(maxAge<=age){
                int realMaxAge=int.Parse(DateFile.instance.GetActorDate(actorId, 13, false)) + age - maxAge + 1;
                GameData.Characters.SetCharProperty(actorId, 13, realMaxAge.ToString() );//保证最大年龄至少有1岁
                for(int i=0;i<6;i++){
                    DateFile.instance.ChangePoison(actorId, i, 200*(age - maxAge +1));
                    AddInjury(actorId, 60, 600*(age - maxAge +1));//全身·断脉
                    // change(actorId, 51 + i,UnityEngine.Random.Range(0,200*(age - maxAge +1)));
                }
                health=0;//由于结算在死亡结算之后，所以健康归0仍然不会立刻死亡
            }
            GameData.Characters.SetCharProperty(actorId, 12, health.ToString() );
            GameData.Characters.SetCharProperty(actorId, 11, age.ToString() );
        }
    #endregion
        [HarmonyPatch(typeof(AI.CharacterAge), "OnAbortion")]//逆练怀孕流产之后会受到两个程度分别为300%的失魂和瘫痪伤势，之后全主要属性-10，魅力-100，年龄*2
        public static class CharacterAgeOnAbortion { // 多线程
            public static void Postix(int actorId,int mainActorId, ref int __result){
                int flag=DateFile.instance.GetGongFaFTyp(actorId,TAIYIN_ID);
                if(flag==1){
                    AddInjury(actorId, 48, 7200);//心神·失魂
                    AddInjury(actorId, 57, 7200);//全身·瘫痪
                    for(int i=0;i<6;i++){
                        change(actorId, 61 + i,-200);//减少主要属性
                    }
                    for(int i=0;i<16;i++){
                        change(actorId,501 + i,-200);//减少技艺资质
                    }
                    for(int i=0;i<14;i++){
                        change(actorId,601 + i,-200);//减少功法资质
                    }
                    GameData.Characters.SetCharProperty(actorId, 15, "-900");//减少魅力到下限
                    //change(actorId, 551,3);//将功法和技艺锁定到早熟
                    //change(actorId, 651,3);//因为这家伙很可能已经长不大了……
                    int age=int.Parse(DateFile.instance.GetActorDate(actorId, 11, true))<<1;
                    tryGrow(actorId,age);
                    __result=-99;
                }
            }
        }
        [HarmonyPatch(typeof(AI.CharacterAge), "ChangeAge")]
        public static class CharacterAgeChangeAge { // 多线程
            public static void Postfix(System.Random random, int actorId, AI.AgeChangeModifiedData modifiedData){
                int flag=DateFile.instance.GetGongFaFTyp(actorId,TAIYIN_ID);
                if(flag==0){
                    List<int> actorFeature = DateFile.instance.GetActorFeature(actorId, true);
                    if(actorFeature.Contains(4002))
                        DateFile.instance.ChangeActorFeature(actorId, 4002, 4001);
                }
                if(flag==1){
                    List<int> actorFeature = DateFile.instance.GetActorFeature(actorId, true);
                    if(actorFeature.Contains(4002)){
                        GameData.Characters.SetCharProperty(actorId, 39, "8000"); //内息恒定800.0
                        int age=int.Parse(DateFile.instance.GetActorDate(actorId, 11, false))-1;
                        int health=DateFile.instance.Health(actorId);
                        if (age>14){
                            GameData.Characters.SetCharProperty(actorId, 11, age.ToString() );//年龄-1，不触发其他年龄改变会发生的操作（损失的健康不会恢复）
                            for(int i=0;i<6;i++){
                                change(actorId, 61 + i,-12);//继续减少主要属性
                                //change(actorId,-(61 + i),1);//缓慢增加属性潜质
                            }
                            for(int i=0;i<16;i++){
                                change(actorId,501 + i,-12);//继续减少技艺资质
                                //change(actorId,-(501 + i),1);//缓慢增加技艺潜质
                            }
                            for(int i=0;i<14;i++){
                                change(actorId,601 + i,-12);//继续减少功法资质
                                //change(actorId,-(601 + i),1);//缓慢增加功法潜质
                            }
                            //GameData.Characters.SetCharProperty(actorId, 15, (900-(age-15)/15).ToString());//开始恢复魅力
                            AddInjury(actorId, 1, 10*age);//胸背·擦伤
                            AddInjury(actorId, 4, 10*age);//胸背·淤阻
                            AddInjury(actorId, 7, 10*age);//腰腹·擦伤
                            AddInjury(actorId, 10, 10*age);//腰腹·淤阻
                            AddInjury(actorId, 13, 10*age);//头颈·擦伤
                            AddInjury(actorId, 16, 10*age);//头颈·淤阻
                            AddInjury(actorId, 19, 10*age);//左臂·擦伤
                            AddInjury(actorId, 22, 10*age);//左臂·淤阻
                            AddInjury(actorId, 25, 10*age);//右臂·擦伤
                            AddInjury(actorId, 28, 10*age);//右臂·淤阻
                            AddInjury(actorId, 31, 10*age);//左腿·擦伤
                            AddInjury(actorId, 34, 10*age);//左腿·淤阻
                            AddInjury(actorId, 37, 10*age);//右腿·擦伤
                            AddInjury(actorId, 40, 10*age);//右腿·淤阻
                        }else{
                            DateFile.AddGongfaEternalPowerChange(actorId, TAIYUAN_ID, 10, actorId, TAIYUAN_ID);
                            DateFile.AddGongfaEternalPowerChange(actorId, YAOCHI_ID, 10, actorId, YAOCHI_ID);
                            modifiedData.ChangeActorFeature.Add(new AI.ChangeActorFeatureData(actorId, 4002, 4001));
                        }
                    }else if(actorFeature.Contains(4001)){
                        int age=int.Parse(DateFile.instance.GetActorDate(actorId, 11, false));
                        if(54 && (random.Next(0,1000000)<(age-7)*(age-7)-64)){
                            modifiedData.ChangeActorFeature.Add(new AI.ChangeActorFeatureData(actorId, 4001, 4002));
                            GameData.Characters.SetCharProperty(actorId, 11, (int.Parse(DateFile.instance.GetActorDate(actorId, 11, false))+10).ToString() );//年龄+10
                            GameData.Characters.SetCharProperty(actorId, 12, (int.Parse(DateFile.instance.GetActorDate(actorId, 12, false))-10).ToString() );//健康-10
                        }else{
                            for(int i=0;i<6;i++){
                                recover(random, actorId, 61 + i,1000);//恢复主要属性
                            }
                            for(int i=0;i<16;i++){
                                recover(random, actorId,501 + i,1000);//恢复技艺资质
                            }
                            for(int i=0;i<14;i++){
                                recover(random, actorId,601 + i,1000);//恢复功法资质
                            }
                            recover(random, actorId,15,1000,GetFaceCharm(2,actorId));
                        }
                    }else if(actorFeature.Contains(4003)){
                        for(int i=0;i<6;i++){
                            change(actorId, 61 + i,-25);//继续减少主要属性
                            change(actorId,-(61 + i),1);//缓慢增加属性潜质
                        }
                        for(int i=0;i<16;i++){
                            change(actorId,501 + i,-25);//继续减少技艺资质
                            change(actorId,-(501 + i),1);//缓慢增加技艺潜质
                        }
                        for(int i=0;i<14;i++){
                            change(actorId,601 + i,-25);//继续减少功法资质
                            change(actorId,-(601 + i),1);//缓慢增加功法潜质
                        }
                        GameData.Characters.SetCharProperty(actorId, 11, (int.Parse(DateFile.instance.GetActorDate(actorId, 11, false))+1).ToString() );//年龄+1
                        if(53)DateFile.instance.ChangeActorHealth(actorId, 0, actorId==mainActorId);
                    }
                }
            }
        }
#endif
#endregion
