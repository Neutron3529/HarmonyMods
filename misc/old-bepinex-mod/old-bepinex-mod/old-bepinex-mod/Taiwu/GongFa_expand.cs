#!/bin/bash -e
#   * compile instructions: put this file in
#   *         `steamapps/common/Dyson Sphere Program/CustomMod`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :
#region shell_script
if [ -z "$__DOTNET_CSC" ]; then
    export __DOTNET_CSC="`find /usr/share/dotnet -type f -name dotnet` `find /usr/share/dotnet -name csc.dll`"
    echo '$'"__DOTNET_CSC not set yet, you should execute"
    echo "export __DOTNET_CSC='$__DOTNET_CSC'"
    echo "manually, or this alert will occur each time you execute this script."
fi

__MODE_VERBOSE=81 # is the line number of "#define VERBOSE", may be modified
__MODE_DEBUG__=$((__MODE_VERBOSE+1))
__MODE_RELEASE=$((__MODE_DEBUG__+1))

case $1 in
    V)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    v)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    VERBOSE) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    verbose) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    D)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    d)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    DEBUG)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    debug)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    *)       _MODE__SELECT_=$__MODE_RELEASE     ;;
esac

export GAME_NAME="${0%\.cs}" # now, file name should be equals to ${GAME_NAME}.cs, the benefit is, we could only change the name, rather than change filename and $GAME_NAME.
export GAME_NAME="The Scroll Of Taiwu Alpha V1.0"
export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"
export GAME_BASE_DIR="../common/The Scroll Of Taiwu"
export PLUGIN_ID="Neutron3529.Taiwu.GongFa_Effect"
export NAMESPACE_ID="Neutron3529.GongFa_Effect"

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_ ) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $__DOTNET_CSC -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/Assembly-CSharp-886238F0C737D5551CA4940C9724A53C94395A280021C721ED03A5241482CB97.dll" \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize -deterministic\
  - && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit

#endregion
#define VERBOSE // the line of __MODE_VERBOSE
#define DEBUG



using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%
{
    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
    public class Cheat : BaseUnityPlugin {
        private static Action<string> _logger=s=>{};//用于内联代码
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
        public static void logger(string s,string extra_id=""){
#if DEBUG
            _logger(extra_id+":"+s);
#endif
        }

#region static_variales
        public static int WANBI_ID=20309;
        public static int WANBI_KILLCHILD_EVENT_ID=WANBI_ID+1000000;
        public static int WANBI_RECOVER_EVENT_ID=WANBI_ID+2000000;
        public static int WANBI_NORMAL_EVENT_ID=WANBI_ID+3000000;
        public static int WANBI_BREAK_HUAN_EVENT_ID=WANBI_ID+4000000;
        public static int WANBI_BREAK_FU_EVENT_ID=WANBI_ID+5000000;
        public static int WANBI_KILLCHILD_XX_ID=WANBI_ID+6000000;
        public static int WANBI_KILLCHILD_XXX_ID=WANBI_ID+7000000;
        public static bool WANBI_LESS=true;
        public static bool NEED_PATCHED=true;
        public static Harmony harmony=new Harmony("%%PLUGIN_ID%%");
        public static bool with_place_id=true;//不想要
#endregion
        void Awake() {
#if DEBUG
            _logger=Logger.LogInfo;
#endif
            logger("开始注入Awake");
            int flag=0;
            with_place_id=Config.Bind("config", "with_place_id", with_place_id, "自定义脚本的V-V-Place显示地格id").Value;

            //小作文谁爱写谁写……我是不准备维护小作文了……太花时间
            if((WANBI_KILLCHILD_EVENT_ID=Config.Bind("config", "WANBI_KILLCHILD_EVENT_ID", WANBI_KILLCHILD_EVENT_ID, "此ID设为0时取消patch。开启patch时，NPC堕胎时会出现提示（完璧在恢复完璧的时候会略微增强人物属性，此时游戏会用这个ID作为过月事件，原堕胎图片作为原始图片进行汇报）").Value)>0){
                flag++;
            }
            if((WANBI_RECOVER_EVENT_ID=Config.Bind("config", "WANBI_RECOVER_EVENT_ID", WANBI_RECOVER_EVENT_ID, "此ID设为0时取消patch。开启patch时，NPC会使用雨恨云愁的图片对事件进行汇报（跟刚才一样，你不会想遇到一个蹲在深山老林里的大BOSS的）").Value)>0){
                flag++;
            }
            if((WANBI_NORMAL_EVENT_ID=Config.Bind("config", "WANBI_NORMAL_EVENT_ID", WANBI_NORMAL_EVENT_ID, "此ID设为0时取消patch。开启patch时，NPC会使用堕入魔道的图片对事件进行汇报（跟刚才一样，你不会想遇到一个蹲在深山老林里的大BOSS的）").Value)>0){
                flag++;
            }
            if((WANBI_BREAK_HUAN_EVENT_ID=Config.Bind("config", "WANBI_BREAK_HUAN_EVENT_ID", WANBI_BREAK_HUAN_EVENT_ID, "此ID设为0时取消patch。开启patch时，NPC被幻毒破功时会出现提示（完璧破功的前提是过月时中相应的毒）。").Value)>0){
                flag++;
            }
            if((WANBI_BREAK_FU_EVENT_ID=Config.Bind("config", "WANBI_BREAK_FU_EVENT_ID", WANBI_BREAK_FU_EVENT_ID, "此ID设为0时取消patch。开启patch时，NPC被腐毒破功时会出现提示。由于这几个event跟Mod的完璧不破高度绑定，必须开启这几个Mod选项才能保证完璧不破不出红字，同样地，想关掉这几个选项，必须先关闭完璧不破然后过月，保证存档中不出现完璧不破新加入的事件之后再关闭这四个选项。").Value)>0){
                flag++;
            }
            if((WANBI_KILLCHILD_XX_ID=Config.Bind("config", "WANBI_KILLCHILD_XX_ID", WANBI_KILLCHILD_XX_ID, "此ID设为0时取消patch。开启patch时，被完璧不破法采补的NPC会以此ID提示。").Value)>0){
                flag++;
            }
            if((WANBI_KILLCHILD_XXX_ID=Config.Bind("config", "WANBI_KILLCHILD_XXX_ID", WANBI_KILLCHILD_XXX_ID, "此ID设为0时取消patch。开启patch时，因连续采补被功法反噬的NPC会以此ID提示。").Value)>0){
                flag++;
            }
            WANBI_LESS=Config.Bind("config", "WANBI_LESS", WANBI_LESS, "不显示完整的总纲说明").Value;


            if(flag>=6)if((WANBI_ID=Config.Bind("config", "WANBI_BUPO_ENABLE_ID", WANBI_ID, "此ID设为0时取消patch。开启patch时，正练，逆练或者冲解的完璧不破法会在每时节恢复成年非童女身女性运功者的童女身，每次破身恢复会产生大量寒毒与全身性外伤，若破身导致怀孕，则流产，产生大量赤毒并导致心神内伤。产生寒毒和赤毒时会微量提高人物的防御属性。运功者每时节会运功泄毒，泄毒量至为当前毒素与毒素耐受上限的1/5中更小的数值。但如果在完璧状态下有一种毒超过耐受上限，则运功者会因为泄毒而破身。如果两种或以上的毒同时突破上限，运功者会因为功法反噬而暂时破功。破功状态会清除功法研读进度（但不会清除心法补全数据），且将大于15岁运用者的年龄重置为15岁。破功时完璧不破法的恢复/避孕效果会暂时失效（方便生孩子）。此外腐毒是功法逆练的罩门，幻毒是功法正练的罩门，两者都是冲解的罩门，过月时如果罩门毒素超过上限，则功法威力永久减少-500%，且主要属性和防御属性归零").Value)>0){
                WANBI_ID=301;//test-only.
                logger("patch WANBI_BUPO");
                harmony.PatchAll(typeof(ReadonlyDataLoad));
                harmony.PatchAll(typeof(CharacterAgeChangeAge));
                harmony.PatchAll(typeof(DateFileGetTurnEventContent));
                harmony.PatchAll(typeof(PeopleLifeAIAISetChildren));
                logger("WANBI_BUPO patched");
            }
            logger("Awake完成");
        }
#region aux
        public static void AddInjury(int actorId,int id,int power,bool remove=false){
            var a=DateFile.instance.actorInjuryDate;
            if (a[actorId].ContainsKey(id)) {
                a[actorId][id] += power;
                if(remove && a[actorId][id]<=0)a[actorId].Remove(id);
            } else {
                a[actorId].Add(id, power);
            }
        }
        public static void heal(int actorId,ref int wai,ref int nei){ //每100%毒对应7000点轻伤
            var a=DateFile.instance.actorInjuryDate;
            for(int i=0;i<60;i++){
                int id=i+1;
                if (a[actorId].ContainsKey(id)) {
                    int dec=Mathf.Min(a[actorId][id],(id==48||id==57)?2000:6000);
                    AddInjury(actorId,id,-dec,true);
                    if(id==48||id==57)dec*=6;
                    if(i%6<3){
                        wai+=((i%3)+1)*dec;
                    }else{
                        nei+=((i%3)+1)*dec;
                    }
                }
            }
        }
    #region wanbi
        public static void wanbi_effect(int actorId, int mainActorId, System.Random random, AI.AgeChangeModifiedData modifiedData){
            if(DateFile.instance.GetActorDate(actorId, 14, false).ParseInt()==2 && DateFile.instance.GetActorDate(actorId, 11, true).ParseInt()>14 && DateFile.GetGongfaEternalPowerChange(actorId, WANBI_ID)>=0){//如果运功者真实性别为女性且年龄大于14 // GetActorEquipGongFa(id)
                //logger(DateFile.instance.GetActorName(actorId, false, false)+"("+actorId+"+) GetMaxGongFaSp1-3="+DateFile.instance.GetMaxGongFaSp(actorId, 1, true)+","+DateFile.instance.GetMaxGongFaSp(actorId, 2, true)+","+DateFile.instance.GetMaxGongFaSp(actorId, 3, true)+"903-905="+DateFile.instance.GetActorDate(actorId, 903, false)+","+DateFile.instance.GetActorDate(actorId, 904, false)+","+DateFile.instance.GetActorDate(actorId, 905, false));
                //DateFile.instance.GetActorDate(actorId, 902~905, false)对应4个真气（催破，轻灵，护体，内息），可以借此检查人物是否凝聚了对应真气
                int flag=DateFile.instance.GetGongFaFTyp(actorId,WANBI_ID);//GetGongFaFTyp(actorId,WANBI_ID);
                if(flag!=-1&&(DateFile.instance.GetActorDate(actorId, 904, false).ParseInt()!=0)){//DateFile.instance.GetActorEquipGongFa(actorId)[0].Contains(WANBI_ID)这个判别标准太容易被AI绕过了，我不想谷主因为不装备相应功法而失去罩门。相比之下，不装备护体真气的弱鸡更容易对付一些。
                    List<int> actorAtPlace = DateFile.instance.GetActorAtPlace(actorId);
                    List<int> actorFeature = DateFile.instance.GetActorFeature(actorId, false);
                    int poison_count=0;
                    int c=0;

                    if(DateFile.instance.HaveLifeDate(actorId,901)&&!actorFeature.Contains(4003)){ // 将不能怀孕的期限删除
                        if(DateFile.instance.actorLife[actorId][901][0]!=1)
                            modifiedData.ActorLife.Add(new AI.ActorLifeData(AI.ModificationType.Remove, actorId, 901, 0, 0));//取消怀孕冷静期
                    }
                    for(int i=0;i<6;i++){
                        int han=DateFile.instance.Poison(actorId, i);
                        if(han<=0)continue;
                        int hanMax=DateFile.instance.MaxPoison(actorId, i);
                        int deltaHan=Mathf.Min(han,200);
                        if((flag!=0&&i==4) || (flag!=1&&i==5)){//罩门毒素的计算，冲解时两种毒素都是罩门
                            deltaHan=Mathf.Min(han,100);//罩门毒素只泻出更少的一部分，
                            c+=9*deltaHan;//且会给运用者造成更高的伤害
                            if(han>=hanMax){
                                poison_count=2;
                                c=-100000;//破功时清除罩门对应的毒素不会造成额外损伤，且必定清除全部罩门毒素（不然会产生一段非常高的伤害）
                                GameData.Characters.SetCharProperty(actorId, 51+4, "0");
                                GameData.Characters.SetCharProperty(actorId, 51+5, "0");
                                deltaHan=han=0;
                                if(DateFile.GetGongfaEternalPowerChange(actorId, WANBI_ID)>=0){
                                    if(i==4){
                                        modifiedData.AddChangTrunEvents.Add(new int[] {
                                            WANBI_BREAK_FU_EVENT_ID,
                                            actorAtPlace[0],
                                            actorAtPlace[1],
                                            actorId, 540303 });
                                    }else{
                                        modifiedData.AddChangTrunEvents.Add(new int[] {
                                            WANBI_BREAK_HUAN_EVENT_ID,
                                            actorAtPlace[0],
                                            actorAtPlace[1],
                                            actorId, 100518 });
                                    }
                                    DateFile.AddGongfaEternalPowerChange(actorId, WANBI_ID, -500, actorId, WANBI_ID);
                                    for(int j=0;j<12;j++)GameData.Characters.SetCharProperty(actorId, 1361+j, "-1000");
                                }
                            }
                        }
                        GameData.Characters.SetCharProperty(actorId, 51+i, (han-deltaHan).ToString()); // 运功时，每时节都会尝试泻毒
                        c+=deltaHan;//但会因此造成身体损伤
                        poison_count+=(han>=hanMax)?1:0;
                    }

                    //运功后遗症结算
                    if(c>0){
                        for(int i=1;i<=40;i+=3)AddInjury(actorId, i, c);//1点毒对应7点伤害（0.7%，HP损失为0.001*(60+60+100+40*4)=0.38）
                        //4非罩门毒发共计800点毒伤，700点伤害，HP-304，在正常理解范围之内
                        //罩门伤口发作时，2000伤对应120点毒，720点伤害，HP-273，在正常理解范围之内
                    }
                    int nei=0,wai=0;
                    heal(actorId,ref wai,ref nei); //每100%毒对应7000点轻伤，因为罩门伤害的存在可以简化成100毒对应7000伤，于是这里累计10000点伤化100毒
                    if(c>0){
                        for(int i=1;i<=60;i++)AddInjury(actorId, i, 0);
                    }
                    //全伤口（1-2-3级伤口）治疗共计3780点毒
                    //logger(DateFile.instance.GetActorName(actorId, false, false)+actorId+"化伤时化去"+nei+"点内伤和"+wai+"点外伤，有51和52毒分别"+DateFile.instance.Poison(actorId, 0)+"和"+DateFile.instance.Poison(actorId, 1)+"点");
                    //GameData.Characters.SetCharProperty(actorId, 51+0, (DateFile.instance.Poison(actorId, 0)+wai/100).ToString()); // 运功时，每时节都会尝试泻毒
                    modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(actorId, 51+0, (DateFile.instance.Poison(actorId, 0)+wai/100).ToString()));
                    //罩门伤口最多提供12点毒素，对应840点普通伤害，HP减少4
                    //GameData.Characters.SetCharProperty(actorId, 51+1, (DateFile.instance.Poison(actorId, 1)+nei/100).ToString()); // 运功时，每时节都会尝试泻毒
                    modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(actorId, 51+1, (DateFile.instance.Poison(actorId, 1)+nei/100).ToString()));

                    if(poison_count>=2) {
                        DateFile.instance.actorGongFas[actorId][WANBI_ID][1] = 0;
                        DateFile.instance.actorGongFas[actorId][WANBI_ID][2] = 0;
                        if(actorId==mainActorId){
                            DateFile.instance.gongFaBookPages.Remove(WANBI_ID);
                        }
                        return;
                    }
                    if(actorFeature.Contains(4002)){
                        modifiedData.ChangeActorFeature.Add(new AI.ChangeActorFeatureData(actorId, 4002, 4001));
                        GameData.Characters.SetCharProperty(actorId, 54, (GameData.Characters.GetCharProperty(actorId, 54).ParseInt()+300).ToString());
                        AddInjury(actorId, 57, 600);//全身·瘫痪
                        modifiedData.AddChangTrunEvents.Add(new int[] {
                            WANBI_RECOVER_EVENT_ID,
                            actorAtPlace[0],
                            actorAtPlace[1],
                            actorId });
                    } else if(actorFeature.Contains(4003)){
                        int oldage=GameData.Characters.GetCharProperty(actorId,11).ParseInt();
                        int normal_leak=0,leak=0,k=0,neixi_wenluan=GameData.Characters.GetCharProperty(actorId, 39).ParseInt()-2000;
                        var father_lambda=(int fatherId)=>{
    //                         if ((actorId == mainActorId || fatherId == mainActorId && DateFile.instance.GetLifeDate(actorId, 901, 3) == 1) || PeopleLifeAI.instance.allFamily.Contains(actorId)) {
    //                             modifiedData.AddChangTrunEvents.Add(new int[] { 254, actorAtPlace[0], actorAtPlace[1], actorId });
    //                         }
                            modifiedData.AiSetMassage.Add(new AI.AiSetMassageData(105, actorId, actorAtPlace[0], actorAtPlace[1], null));
    //                        modifiedData.SetActorMood.Add(new AI.SetActorMoodData(actorId, -500, 0));//完璧不破，百邪不侵

                            modifiedData.ChangeActorFeature.Add(new AI.ChangeActorFeatureData(actorId, 4003, 4001));
                            if(DateFile.instance.HaveLifeDate(actorId,501))
                                modifiedData.ActorLife.Add(new AI.ActorLifeData(AI.ModificationType.Remove, actorId, 501, 0, 0));//删除相枢值？
                            if(DateFile.instance.actorLife[actorId][901][0]!=1){
                                modifiedData.ActorLife.Add(new AI.ActorLifeData(AI.ModificationType.Remove, actorId, 901, 0, 0));//取消怀孕冷静期
                            }else{
                                return;//马上生孩子的时候不能删除生孩子的数据
                            }
                            if (DateFile.instance.pregnantFeature.ContainsKey(actorId)) {
                                modifiedData.RemovePregnantFeatureData.Add(actorId);
                            }
                            if (DateFile.instance.samsaraPlatformChildrenData.ContainsKey(actorId)) {
                                modifiedData.RemoveSamsaraPlatformChildrenData.Add(actorId);
                            }
                            for(int i=0;i<6;i++){
                                if(DateFile.instance.GetActorDate(fatherId, 81 + i, true).ParseInt()<=0){
                                    k++;
                                    //logger(DateFile.instance.GetActorName(actorId, false, false)+" "+DateFile.instance.GetActorName(fatherId, false, false)+"i"+81 + i+"DateFile.instance.GetActorDate(fatherId, 81 + i, true).ParseInt()(<=0)="+DateFile.instance.GetActorDate(fatherId, 81 + i, true).ParseInt());
                                    GameData.Characters.SetCharProperty(actorId, 51+i, (GameData.Characters.GetCharProperty(actorId, 51+i).ParseInt()+100).ToString()); // 将对应属性吸干之后，继续采补会中毒
                                } else {
                                    //logger(" "+DateFile.instance.GetActorName(actorId, false, false)+" "+DateFile.instance.GetActorName(fatherId, false, false)+"i"+81 + i+"DateFile.instance.GetActorDate(fatherId, 81 + i, true).ParseInt()(>0)="+DateFile.instance.GetActorDate(fatherId, 81 + i, true).ParseInt());
                                    GameData.Characters.SetCharProperty(actorId, 81+i, (GameData.Characters.GetCharProperty(actorId, 81+i).ParseInt()+(i==2?10:1)).ToString());
                                    GameData.Characters.SetCharProperty(fatherId, 81+i, (GameData.Characters.GetCharProperty(fatherId, 81+i).ParseInt()-(i==2?100:10)).ToString());
                                }
                            }
                            for(int i=0;i<12;i++){
                                if(DateFile.instance.GetAttrAgeVaule(fatherId, i)<25){
                                    k++;
                                    //logger(DateFile.instance.GetActorName(actorId, false, false)+" "+DateFile.instance.GetActorName(fatherId, false, false)+"i"+1361+ i+"DateFile.instance.GetAttrAgeVaule(fatherId, i)(<25)="+DateFile.instance.GetAttrAgeVaule(fatherId, i));
                                    GameData.Characters.SetCharProperty(actorId, 51+(i%6), (GameData.Characters.GetCharProperty(actorId, 51+(i%6)).ParseInt()+100).ToString()); // 将对应属性吸干之后，继续采补会中毒
                                } else {
                                    //logger(" "+DateFile.instance.GetActorName(actorId, false, false)+" "+DateFile.instance.GetActorName(fatherId, false, false)+"i"+1361+ i+"DateFile.instance.GetAttrAgeVaule(fatherId, i)(>=25)="+DateFile.instance.GetAttrAgeVaule(fatherId, i));
                                    GameData.Characters.SetCharProperty(actorId, 1361+i, (GameData.Characters.GetCharProperty(actorId, 1361+i).ParseInt()+1).ToString());
                                }
                                GameData.Characters.SetCharProperty(fatherId, 1361+i, (GameData.Characters.GetCharProperty(fatherId, 1361+i).ParseInt()-10).ToString());
                            }
                            for(int i=0;i<16;i++){
                                int l=GameData.Characters.GetCharProperty(fatherId, 501 + i).ParseInt()-10;
                                if(l<=0){
                                    //logger(DateFile.instance.GetActorName(actorId, false, false)+" "+DateFile.instance.GetActorName(fatherId, false, false)+"i"+501+ i+"DateFile.instance.GetAttrAgeVaule(fatherId, i)(<25)="+l);
                                    k++;l=0;neixi_wenluan+=100;
                                } else {
                                    GameData.Characters.SetCharProperty(actorId, 501+i, (GameData.Characters.GetCharProperty(actorId, 501+i).ParseInt()+5).ToString());
                                }
                                GameData.Characters.SetCharProperty(fatherId, 501+i, l.ToString());
                            }
                            for(int i=0;i<14;i++){
                                int l=GameData.Characters.GetCharProperty(fatherId, 601 + i).ParseInt()-10;
                                if(l<=0){
                                    //logger(DateFile.instance.GetActorName(actorId, false, false)+" "+DateFile.instance.GetActorName(fatherId, false, false)+"i"+601+ i+"DateFile.instance.GetAttrAgeVaule(fatherId, i)(<25)="+l);
                                    k++;l=0;neixi_wenluan+=100;
                                } else {
                                    GameData.Characters.SetCharProperty(actorId, 601+i, (GameData.Characters.GetCharProperty(actorId, 601+i).ParseInt()+5).ToString());
                                    GameData.Characters.SetCharProperty(fatherId, 601+i, l.ToString());
                                }
                            }
                            modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(fatherId, 11, (GameData.Characters.GetCharProperty(fatherId,11).ParseInt()+k/4).ToString()));
                            //GameData.Characters.SetCharProperty(fatherId, 11, (GameData.Characters.GetCharProperty(fatherId,11).ParseInt()+k/4).ToString());
                            if (DateFile.instance.GetLifeDate(actorId, 901, 3) == 1 && k+Mathf.Max(neixi_wenluan/100,-k)>0) {
                                modifiedData.AiSetMassage.Add(new AI.AiSetMassageData(106, fatherId, actorAtPlace[0], actorAtPlace[1], new int[] { actorId }));
                                modifiedData.SetActorMood.Add(new AI.SetActorMoodData(fatherId, -5, 1000));
                                modifiedData.SetActorMood.Add(new AI.SetActorMoodData(fatherId, 10, 0));

                                modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(fatherId, 24, (Mathf.Clamp(DateFile.instance.GetActorDate(fatherId,24,false).ParseInt()+10,0,15000)).ToString()));
                                if(DateFile.instance.GetActorDate(actorId,24,false).ParseInt()>-100){
                                    normal_leak+=1;
                                    modifiedData.AddChangTrunEvents.Add(new int[] {
                                        WANBI_KILLCHILD_XX_ID,
                                        (actorAtPlace[0]<<16)|actorAtPlace[1],actorId,fatherId
                                    });
                                }else{
                                    leak+=1;
                                    modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(fatherId,11, Mathf.Max(Mathf.Min(GameData.Characters.GetCharProperty(fatherId,11).ParseInt(),15),GameData.Characters.GetCharProperty(fatherId,11).ParseInt()-1).ToString()));
                                    DateFile.instance.RemoveActorSocial(fatherId, actorId, 309);
                                    DateFile.instance.RemoveActorSocial(actorId, fatherId, 309);
                                    DateFile.instance.RemoveActorSocial(fatherId, actorId, 312);
                                    DateFile.instance.RemoveActorSocial(actorId, fatherId, 312);
                                    DateFile.instance.RemoveActorSocial(fatherId, actorId, 306);
                                    DateFile.instance.RemoveActorSocial(actorId, fatherId, 306);
                                    DateFile.instance.AddSocial(fatherId, actorId, 402);
                                    modifiedData.AddChangTrunEvents.Add(new int[] {
                                        WANBI_KILLCHILD_XXX_ID,
                                        (actorAtPlace[0]<<16)|actorAtPlace[1],actorId,fatherId
                                    });
                                }
                            } else {
                                if(oldage>15){ // 精元质量不差的时候，返老还童
                                    oldage-=1;
                                }
                                modifiedData.AddChangTrunEvents.Add(new int[] {
                                    WANBI_KILLCHILD_EVENT_ID,
                                    (actorAtPlace[0]<<16)|actorAtPlace[1],actorId,fatherId
                                });
                            }
                        };
                        father_lambda(DateFile.instance.GetLifeDate(actorId, 901, 1));
                        for(int i=5;i<DateFile.instance.actorLife[actorId][901].Count;i++)
                            father_lambda(DateFile.instance.GetLifeDate(actorId, 901, i));
                        modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(actorId , 39,Mathf.Max(0,neixi_wenluan).ToString()));


                        k+=Mathf.Max(neixi_wenluan/100,-k-2);
                        modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(actorId,11,oldage.ToString()));
                        //modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(actorId , 39,Mathf.Max(0,neixi_wenluan).ToString()));
                        GameData.Characters.SetCharProperty(actorId, 39, Mathf.Max(0,neixi_wenluan).ToString());
                        //logger(DateFile.instance.GetActorName(actorId, false, false)+"k"+k+"nei"+neixi_wenluan);
                        modifiedData.ChangeActorHealth.Add(new AI.ChangeActorHealthData(actorId, GameData.Characters.GetCharProperty(actorId,12).ParseInt()-k, actorId==mainActorId));
                        modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(actorId, 11, (GameData.Characters.GetCharProperty(actorId,11).ParseInt()+10*leak).ToString()));
                        modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(actorId, 24, (Mathf.Clamp(DateFile.instance.GetActorDate(actorId,24,false).ParseInt()-100*leak-k,-15000,15000)).ToString()));

                    } else if(actorFeature.Contains(4001)){
                        modifiedData.AddChangTrunEvents.Add(new int[] { WANBI_NORMAL_EVENT_ID, actorAtPlace[0], actorAtPlace[1], actorId });
                        int cc=DateFile.instance.GetActorDate(actorId,24,false).ParseInt()+10;
                        if(cc>150)modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(actorId, 53, (GameData.Characters.GetCharProperty(actorId, 53).ParseInt()+3*cc).ToString()));// 久旷-寒毒
                        modifiedData.SetCharProperty.Add(new AI.SetCharPropertyData(actorId, 24, (Mathf.Clamp(cc,-15000,15000)).ToString()));
                        //GameData.Characters.SetCharProperty(actorId, 53, (GameData.Characters.GetCharProperty(actorId, 53).ParseInt()+300).ToString());
                        AddInjury(actorId, 48, 600);//心神·失魂
                    }
                }
            }
        }
        // 颜色速查：sed -E 's/(.*),.*<color=#([0-9A-F]+)FF>.*/\1,<color=#FF\2>      <color=#\2FF><\/color>/g' /me/converted/fixex/TextColor_Date.txt
        static void wanbi_adddesc(){
            /*
             -1 patch usage:
             V-MActor|在|V-V-Place|运起|Gong|偷窃了|V-HActor|的|V-Item|，在日记中写下了|Date|今天是个好日子。
             V-MActor是主要角色（淡黄色），V-FActor是友善角色（粉红）,V-HActor是敌对角色（暗红），V-Actor是不带颜色的Actor，V-Item是带颜色的物品，Date是不带变量的日期，V-V-Place是需要填写两个变量的人物所在地 V-Count是常量指针，可以指向contents中len+count位置（也就是，参数必须为负数，-1代表最右边那个字符串，-2代表从右往左数第二个……）
             前面的V-代表这个字符串对应几个变量
             并非强制，但应当作为一种要求
             这样可以很方便地使用搜索功能保证变量不重不漏。
             V-p-Count 是一个特例，它其实是一个（多级）指针，接受一个负数变量-count，含义是用split之后，从右往左数第count段content代替当前的V-p-Count。如果这一段指令不是纯文本，你需要继续为其消耗程序提供的变量。如果指向的仍然是指针，还需要继续进行替换。
             使用指针可以在一段文字中实现branch功能：两段一样长的文字可以使用指针进行区分，不一样长的，短文字可以用指向空字符串的Count进行填充。
             使用了Count的段落可以使用End提前结束文字处理，以避开“显示指针内容其实在正文中”的限制
             理论上这样可行，但我强烈不建议如此。
             ---
             上面那个是废稿，因为需要patch一个switch
             新方法是利用有限参数进行无限修改
             2号方法可以保存5个参数，5号方法可以保存3个参数
             于是，如果将地点压缩成一个参数（<<16 | ），我们可以存地点+2人物id
             5参数的，我们甚至还可以存功法。
             */
//按|V-Gong|总纲，行功时需时时调理阴阳水火，否则，或是<color=#FF44A7FF>烈火烹油</color>，或是<color=#8FBAE7FF>水沸火熄</color>。
            var template=new Dictionary<int,string>(DateFile.instance.trunEventDate[254]);//痛失骨肉
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_EVENT_ID]=new Dictionary<int,string>(template);//痛失骨肉
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_EVENT_ID][0]="采阳补阴";//夫妻分居 or
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_EVENT_ID][-1]="-1";//采用custom脚本
            //DateFile.instance.trunEventDate[WANBI_KILLCHILD_EVENT_ID][1]="2";//这样设置时，在设置事件时只能且必须传递5个参数。
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_EVENT_ID][98]=DateFile.instance.trunEventDate[224][98];//图片：照镜子
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_EVENT_ID][99]="FActor-1|以"+ProcessGong(DateFile.instance,WANBI_ID)+"中不录文字的<color=#E4504DFF>不传之秘</color>运功，以<color=#FF44A7FF>春宵</color>时<color=#FF44A7FF>采补</color>|MActor-2|吸取的大量优质<color=#FBFBFBFF>精元</color>，将之与行功产生的<color=#8FBAE7FF>肾精</color>相合，先运功二十四个小周天，取精炼水，化做<color=#9B8773FF>胎气</color>；后运功四十八个大周天，炼胎取气，强精壮神。\n\n因<color=#FBFBFBFF>精元</color>炼化得法，|FActor-1|变得更有活力了。而|MActor-2|因为精元耗散，身体虚弱了很多。\n";

            //"MActor-2|因|FActor-1|那一身生人勿近的<color=#8FBAE7FF>冰寒</color>气息迷得<color=#FBFBFBFF>辗转反侧</color>，几经<color=#6DB75FFF>接触</color>之后发现|FActor-1|的内心并不像她外表那样<color=#8FBAE7FF>冰冷</color>。在与|FActor-1|<color=#FF44A7FF>几度春风</color>之后，更是被迷她得<color=#AE5AC8FF>神魂颠倒</color>。脑海中整日都是<color=#FF44A7FF>春宵</color>时|FActor-1|那具哪怕自己<color=#8E8E8EFF>精疲力竭</color>也仍是<color=#FF44A7FF>意尤未满</color>的<color=#63CED0FF>玉体娇躯</color>，三日不见|FActor-1|，便意以为受其嫌弃，<color=#8E8E8EFF>余生</color>再无与其<color=#FF44A7FF>相见</color>之日，寻去<color=#63CED0FF>回春堂</color>找<color=#8FBAE7FF>大夫</color>求来一坛据说是混了|V-Item|的|V-Item|一饮而尽。隐约中|MActor-2|似是看见了|MActor-2|的身影。\n\n|FActor-1|运起"+ProcessGong(DateFile.instance,WANBI_ID)+"已有一段时日，在<color=#8FBAE7FF>寒气</color>润养下玉体逾加晶莹，却也因这完璧之身，绝难在与人<color=#FF44A7FF>春宵</color>时<color=#FF44A7FF>动情</color>，催不动行功时滋长的肾水心火完成调和。若是月事一到，气血翻涌，强水弱火相交，便会成一个<color=#8FBAE7FF>水沸火熄</color>之局，伤身害命。\n|||还没写完|||\n|MActor-2|高估了自己的体力，也低估了<color=#FF44A7FF>媚功</color>刺激之下，|FActor-1|的龙精虎猛.两人只<color=#FF44A7FF>春宵</color>一刻，便已房倒屋塌，而未及一个时辰，吸饱精元的|MActor-2|便因精元炼化速度赶不上动情时的体力消耗，生生在<color=#FF44A7FF>春宵</color>中昏死过去。\n\n待第二日|MActor-2|起身，刚一运功，便发觉身子有了反应，内息错乱，肠胃痉孪，干呕不止。|MActor-2|知道，这是"+ProcessGong(DateFile.instance,WANBI_ID)+"所说的，行功时不可动情，否则纯阴与真阳相触，生了胎气。\n\n|MActor-2|";
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XX_ID]=new Dictionary<int,string>(template);
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XX_ID][0]="春宵告饶";
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XX_ID][-1]="-1";//采用custom脚本
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XX_ID][98]=DateFile.instance.trunEventDate[254][98];//图片：痛失骨肉
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XX_ID][99]="FActor-1|因吸取的<color=#8E8E8EFF>精元</color>浊气盛过<color=#FBFBFBFF>精气</color>，不得不在<color=#FF44A7FF>春宵</color>时运转"+ProcessGong(DateFile.instance,WANBI_ID)+"调理内息，一个不慎，浊气上涌，<color=#FF44A7FF>破功</color>动情，大量<color=#8FBAE7FF>肾水</color>顺势与<color=#8E8E8EFF>精元</color>相合，而<color=#FF44A7FF>心火</color>因无<color=#8FBAE7FF>寒气</color>压制，悄然自生。|FActor-1|见势不对一边告饶一边奋力运功收束水火，调理阴阳，却未曾料得一丝<color=#9B8773FF>胎气</color>竟随水火相交自生后，随着肾水收入丹田。\n\n|MActor-2|虽对|FActor-1|倾心日久，却总是害怕<color=#FF44A7FF>春宵</color>时|FActor-1|那如狼似虎的表现。这一日|MActor-2|苦苦支撑，本以为要春宵到昏死过去，终却见得|FActor-1|求饶的神色不似作伪，心满意足之下，合上疲惫的双眼，却把头歪向|FActor-1|一边。半梦半醒的|MActor-2|并不知道，此时他的一呼一吸都会成为引导|FActor-1|行功出岔的导火索。\n\n|MActor-2|先是觉得枕边人呼吸急促有些不适，本想用手揉揉|FActor-1|让她稍稍舒服一些，却只觉手中玉人猛一抽搐，待|MActor-2|睁眼看是，竟发现|FActor-1|自闺床之上滚到了地上，捂着肚子，干呕不止。|MActor-2|大惊，下意识问起了|FActor-1|的身体状况，而|FActor-1|阴阳相冲，浑浑噩噩之际，早就失了判别能力，一边捂着肚子，一边喊着「胎……胎……」，却是连一句完整的「胎气太盛，阴阳失衡」都说不出口。|MActor-2|见|FActor-1|嘴唇泛白，捂着肚子，不住干呕，意识到了自己这日渐消瘦的身子骨竟也是有了亲生骨肉，略有些心满意足，强撑疲惫的身子起身要去给|FActor-1|请<color=#63CED0FF>「回春堂」</color>的<color=#8FBAE7FF>「大夫」</color>。|FActor-1|将这一切都看在心里，想说话，却因不住干呕说不出口。\n\n一个时辰后，当|MActor-2|终于确认自己请不到肯出诊的大夫，拿着几幅安胎药回家时，看见|FActor-1|神色如常，有些不知所措。\n\n「我是说，你太大惊小怪了。」「当然是胃不舒服肚子疼了，还能是什么，你那么用力……」\n「大概的确是我不懂吧……」\n\n|MActor-2|被|FActor-1|这几句话迷得神魂颠倒，虽然身子有些力不从心，却越发向往起与|FActor-1|春宵的\n\n与|MActor-2|告别之后，|FActor-1|的脸色瞬间差了好多，身子里的浊气，因在与|MActor-2|交谈时停下了行功压榨，在体内四处流窜，冲脉入脑。\n|FActor-1|虽得<color=#8E8E8EFF>精元</color>调和水火增进了完璧之体，那股浊气在身体内肆虐时带来的亏空，须得良药，方可弥补；而破功时流出的真阴，需禁绝男色一段时日，方能弥补。\n";
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XXX_ID]=new Dictionary<int,string>(template);
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XXX_ID][0]="动情破功";
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XXX_ID][-1]="-1";//采用custom脚本
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XXX_ID][98]=DateFile.instance.trunEventDate[229][98];//图片：雨恨云愁
            DateFile.instance.trunEventDate[WANBI_KILLCHILD_XXX_ID][99]="FActor-1|因吸取了<color=#8E8E8EFF>精元</color>过于浑浊，无法弥补激发肾水后产生的水火失衡；被阳气侵蚀的真阴在周身流转，更加剧了欲火焚心。|FActor-1|只知道四处找男人发泄欲火，却不想她的身子已经约束不住水火，在<color=#FF44A7FF>春宵</color>时，被|MActor-2|的阳气一激之下，神情恍惚，当场破功，真阴不受控制地泄了|MActor-2|一身，之后虚弱的|FActor-1|更是满口污言秽语直喊着要汉子泄火。意识到|FActor-1|一直在运转采补功法的|MActor-2|看到|FActor-1|的丑态一下子清醒了过来，下定决心与|MActor-2|割席断交。因大量真阴泄漏，|FActor-1|此苍老了十岁，尽管完璧法会逐渐恢复|FActor-1|泄掉的真阴，但失去真阴带来的亏空，只能用更多优质精元来弥补。\n少量真阴随着皮膜渗入|MActor-2|的体内，令|MActor-2|年轻了少许。但被采补的亏空，并非这少许真阴所能弥补。";
            //actorId,actorId,fatherId,actorId,actorId,fatherId,actorId,actorId,fatherId,actorId,fatherId,actorId,fatherId,actorId,fatherId,actorId,actorId,fatherId,actorId,actorId,actorId,fatherId,actorId,fatherId,actorId
            DateFile.instance.trunEventDate[WANBI_RECOVER_EVENT_ID]=new Dictionary<int,string>(template);
            DateFile.instance.trunEventDate[WANBI_RECOVER_EVENT_ID][0]="瘫痪在床";
            DateFile.instance.trunEventDate[WANBI_RECOVER_EVENT_ID][98]=DateFile.instance.trunEventDate[242][98];//图片：祛病疗伤
            DateFile.instance.trunEventDate[WANBI_RECOVER_EVENT_ID][99]="因欲火焚身，在|<color=#FF44A7FF>春宵</color>时忘记运起"+ProcessGong(DateFile.instance,WANBI_ID)+"，被<color=#FF44A7FF>情人</color><color=#9B8773FF>破身</color>之后泄了肾水，却因<color=#FF44A7FF>情人</color>精力不济，没能泄掉心中欲火，终在月事来临之日，因心血失了肾水压制，反冲完璧之身，一度落得<color=#E4504DFF>全身·瘫痪</color>。卧床修养数日后，方才用新生的肾水约束住了心火。正应了总纲中那句<color=#8FBAE7FF>「水沸火熄」</color>。\n";
            DateFile.instance.trunEventDate[WANBI_NORMAL_EVENT_ID]=new Dictionary<int,string>(template);
            DateFile.instance.trunEventDate[WANBI_NORMAL_EVENT_ID][0]="久旷之身";
            DateFile.instance.trunEventDate[WANBI_NORMAL_EVENT_ID][98]=DateFile.instance.trunEventDate[231][98];//图片：与人结怨
            DateFile.instance.trunEventDate[WANBI_NORMAL_EVENT_ID][99]="在|因许久不经人事未能泄出因行功"+ProcessGong(DateFile.instance,WANBI_ID)+"而满溢的肾水，行功之时不觉月事提前，而肾水中满溢的<color=#8FBAE7FF>寒气</color>随月事引发的气血流转，攻入心脉，化作<color=#8FBAE7FF>寒毒</color>。正应了总纲中那句<color=#FF44A7FF>「烈火烹油」</color>。";
            DateFile.instance.trunEventDate[WANBI_BREAK_HUAN_EVENT_ID]=new Dictionary<int,string>(template);
            DateFile.instance.trunEventDate[WANBI_BREAK_HUAN_EVENT_ID][0]="幻毒惑心";
            DateFile.instance.trunEventDate[WANBI_BREAK_HUAN_EVENT_ID][98]=DateFile.instance.trunEventDate[219][98];//图片：下毒成功
            DateFile.instance.trunEventDate[WANBI_BREAK_HUAN_EVENT_ID][99]="自恃有"+ProcessGong(DateFile.instance,WANBI_ID)+"护体，对自己身中幻毒毫无察觉，终于毒发。\n毒发时，她只觉得自己如入梦境，在|疯狂求欢，一连数日\n终究因为欲求不满，疯狂运转的"+ProcessGong(DateFile.instance,WANBI_ID)+"在炼化完吸取的<color=#FBFBFBFF>精元</color>之后，如疾风扫落叶一般化掉了她的一身精血，和一条对"+ProcessGong(DateFile.instance,WANBI_ID)+"至关重要的经脉。\n";
            DateFile.instance.trunEventDate[WANBI_BREAK_FU_EVENT_ID]=new Dictionary<int,string>(template);
            DateFile.instance.trunEventDate[WANBI_BREAK_FU_EVENT_ID][0]="腐毒断肠";
            DateFile.instance.trunEventDate[WANBI_BREAK_FU_EVENT_ID][98]=DateFile.instance.trunEventDate[219][98];//图片：下毒成功
            DateFile.instance.trunEventDate[WANBI_BREAK_FU_EVENT_ID][99]="自恃有"+ProcessGong(DateFile.instance,WANBI_ID)+"护体，对自己身中腐毒毫无察觉，终于在|毒发。\n毒发时，她痛得满地打滚不断呻吟，疼痛难忍之时偶会运功猛击自己的腹部试图减轻痛苦\n辗转七日之后，她凭借回光返照的清醒，试图以<color=#E4504DFF>「十二弦奇针功」</color>疗伤，终究因为行针出错，坏了功体，精血漏尽。\n";

            DateFile.instance.gongFaDate[WANBI_ID][99]="完璧之身对处子来说，既是<color=#F28234FF>恩赐</color>，也是<color=#E4504DFF>诅咒</color>。妄图维持完璧的女子，虽得一时风光无限，终难逃<color=#E4504DFF>玉殒香消</color>。\n"+ (WANBI_LESS?"":"按"+ProcessGong(DateFile.instance,WANBI_ID)+"总纲，女子一旦习得此功，则身如完璧，百邪不侵，再不需要真气护体。若强行护体真气，真气将隔绝水火。行功逾久，心火逾旺，而肾水逾寒。妄图凝聚护体真气之女子，或是<color=#FF44A7FF>烈火烹油</color>，或是<color=#8FBAE7FF>水沸火熄</color>。\n\n在心法等级高过五级时，成年女性修习者的"+ProcessGong(DateFile.instance,WANBI_ID)+"会产生特殊效果和罩门：\n\n修习者每个时节都会运功<color=#6DB75FFF>泄毒疗伤</color>。\n<color=#6DB75FFF>疗伤</color>时"+ProcessGong(DateFile.instance,WANBI_ID)+"会根据<color=#6DB75FFF>疗伤</color>的伤害种类和伤口程度产生<color=#E3C66DFF>烈毒</color>或者<color=#63CED0FF>郁毒</color>，在需要<color=#6DB75FFF>泄毒</color>时，修习者会催动功法制造伤口以加速<color=#6DB75FFF>泄毒</color>。\n\n时节更替时，"+ProcessGong(DateFile.instance,WANBI_ID)+"会根据本时节修习者的<color=#FF44A7FF>春宵</color>行为产生特殊效果：\n如果不进行<color=#FF44A7FF>春宵</color>，会因久旷之身产生300<color=#8FBAE7FF>寒毒</color>与一个伤势程度60%的<color=#AE5AC8FF>心神·失魂</color>伤口；若<color=#FF44A7FF>春宵</color>时未能成功<color=#FF44A7FF>采补</color>，会行功出岔，产生300<color=#E4504DFF>赤毒</color>与一个伤势程度60%的<color=#E4504DFF>全身·瘫痪</color>伤口。\n在成功<color=#FF44A7FF>采补</color>之后，修习者会将<color=#FF44A7FF>采补</color>得到的<color=#FBFBFBFF>精元</color>炼化，微量增加防御属性，并使修习者变年轻。但被<color=#FF44A7FF>采补</color>的一方会大量减少所有属性，有时会因采补变得苍老。\n修习者在采补时会因为<color=#FF44A7FF>采补</color>的<color=#FBFBFBFF>精元</color>不纯，导致六种毒素增加，并视修习者的内息状态影响健康\n\n"+ProcessGong(DateFile.instance,WANBI_ID)+"的<color=#6DB75FFF>疗伤</color>效果对<color=#E4504DFF>全身·瘫痪</color>与心神·失魂<color=#AE5AC8FF>心神·失魂</color>两种伤势不明显，<color=#6DB75FFF>疗伤</color>时产生的毒素更高；对<color=#AE5AC8FF>幻毒</color>和<color=#6DB75FFF>腐毒</color>两种毒素的<color=#6DB75FFF>泄毒</color>效果不明显，<color=#6DB75FFF>泄毒</color>量更少而造成的伤势程度更高\n修习者可能会因为<color=#6DB75FFF>泄毒</color>过量而<color=#E4504DFF>破功</color>。<color=#E4504DFF>破功</color>状态会清除功法研读进度。特别地，<color=#AE5AC8FF>幻毒</color>和<color=#6DB75FFF>腐毒</color>是"+ProcessGong(DateFile.instance,WANBI_ID)+"的罩门，若<color=#6DB75FFF>泄毒</color>时这两种毒素超过修习者的毒素抵抗，则修习者<color=#E4504DFF>永久破功</color>，功法威力<color=#E4504DFF>永久-500%</color>，且与真气无关的主要属性和防御属性<color=#E4504DFF>永久归零</color>。");
            for(int i=0;i<2;i++){
                int wanbi_effect_id=DateFile.instance.gongFaDate[WANBI_ID][103+i].ParseInt();
                DateFile.instance.gongFaFPowerDate[wanbi_effect_id][99]+="\n<color=#"+(i!=0?"AE5AC8FF>幻":"6DB75FFF>腐")+"毒</color>不再是成年女性运功者的罩门";
            }
        }
    #endregion
#endregion

    #if FORCE_ENABLE
    #region active_caibu
        [HarmonyPatch(typeof(PeopleLifeAI), "DoTrunAIChange")]
        public static class PeopleLifeAIDoTrunAIChange {
            public static bool Postfix(int actorId, int mapId, int tileId, int mainActorId, bool isTaiwuAtThisTile, int worldId, Dictionary<int, List<int>> mainActorItems, int[] aliveChars, int[] deadChars){
                if(__result || !DateFile.instance.GetActorFeature(actorId, false).Contains(1002))__result;
                int flag=DateFile.instance.GetGongFaFTyp(motherId,WANBI_ID);
                if (flag!=-1)
                {
                    List<int> list66 = new List<int>();// 用于存储异性
                    List<int> list67 = new List<int>();
                    List<int> list68 = new List<int>();
                    List<int> list69 = new List<int>();

                    //List<int> list76 = new List<int>();// 用于存储同性，用于功法反噬
                    //List<int> list77 = new List<int>();
                    //List<int> list78 = new List<int>();
                    //List<int> list79 = new List<int>();
                    for (int num233 = 0; num233 < aliveChars.Count; num233++)
                    {
                        int num234 = aliveChars[num233];
                        bool male=int.Parse(DateFile.instance.GetActorDate(num234, 14, false)) == 1;
                        bool flag424 = DateFile.instance.GetActorFeature(num234, false).Contains(male ? 1001 : 1002);
                        if (!flag424)
                        {
                            bool flag425 = int.Parse(DateFile.instance.GetActorDate(num234, 11, false)) <= 14;
                            if (!flag425)
                            {
                                bool flag426 = int.Parse(DateFile.instance.GetActorDate(num234, 2, false)) == 0;
                                if (flag426)
                                {
                                    if (DateFile.instance.GetActorSocial(actorId, 309, false, false).Contains(num234))//夫妻
                                    {
                                        if(male)list66.Add(num234);//else list76.Add(num234);//窝边草
                                    } else if (DateFile.instance.GetActorSocial(actorId, 306, false, false).Contains(num234))
                                    {
                                        if(male)list67.Add(num234);//else list77.Add(num234);//两情相悦，固定炮友
                                    } else if (DateFile.instance.GetActorSocial(actorId, 312, false, false).Contains(num234))
                                    {
                                        if(male)list68.Add(num234);//else list78.Add(num234);//倾心爱慕，潜在目标
                                    } else
                                        if(male)list69.Add(num234);//else list79.Add(num234);//陌生人
                                }
                            }
                        }
                    }
                    bool flag430 = int.Parse(DateFile.instance.GetActorDate(actorId, 2, false)) == 0;
                    int[] happy_mean=new int[]{5,10,};
                    int[] ohappy_mean=new int[]{};
                    List<List<int>> lists=new List<List<int>>{list76,list77,list67,list68,list78,list69,list79,list66};
                    for(int i=0;i<8;i++)foreach(var num235 in lists[i])
                    {
                        int num235 = list66[UnityEngine.Random.Range(0, list66.Count)];
                        if (num235 == mainActorId || TAIYIN_CAIBU_ALERT)
                        {
                            this.aiTrunEvents.Add(new int[]
                            {
                                TAIYIN_CAIBU_ID,
                                mapId<<16 | tileId,
                                actorId,
                                mainActorId
                            });
                        }
                        this.AiMoodChange(actorId, UnityEngine.Random.Range(-10, 11));
                        this.AiMoodChange(num235, UnityEngine.Random.Range(-10, 11));
                        bool flag433 = 2/*num2*/ != int.Parse(DateFile.instance.GetActorDate(num235, 14, false));
                        if (flag433)
                        {//采补逻辑
//                            bool flag434 = this.AISetChildren((num2 == 1) ? actorId : num235, (num2 == 1) ? num235 : actorId, 1, 1);
//                            if (flag434)
//                            {
//                                num6 -= 50;
//                            }
                        }
                        else
                        {//同性
                            bool flag443 = UnityEngine.Random.Range(0, 100) < 50;
                            if (flag443)
                            {
                                DateFile.instance.AddSocial(num237, actorId, 402);
                            }
                            this.AISetMassage(97, num237, mapId, tileId, new int[1], actorId, true);
                        }
                        GEvent.OnEvent(eEvents.Copulate, new object[]
                        {
                            actorId,
                            num235
                        });
                    }
                }
            }
        }
    #endregion
    #endif
        [HarmonyPatch(typeof(AI.CharacterAge), "ChangeAge")]
        public static class CharacterAgeChangeAge { // 多线程
            public static void Postfix(int actorId, int mainActorId, System.Random random, AI.AgeChangeModifiedData modifiedData){
                wanbi_effect(actorId,mainActorId,random,modifiedData);
            }
        }
        [HarmonyPatch(typeof(ArchiveSystem.GameData.ReadonlyData), "Load")]
        public static class ReadonlyDataLoad {
            public static void Postfix(){
                NEED_PATCHED=false;
                wanbi_adddesc();
            }
        }
        #region aux_process_functions
        public static string ProcessItem(DateFile __instance, int itemid, string[] itemsp=null){
            if(itemsp==null){
                itemsp=__instance.massageDate[10][0].Split(new char[] { '|' });
            }
            if(__instance.gongFaDate.ContainsKey(itemid)&&__instance.gongFaDate[itemid].ContainsKey(2) &&__instance.gongFaDate[itemid].ContainsKey(0) )
                return __instance.SetColoer(20001 + int.Parse(__instance.GetItemDate(itemid, 8, true, -1)), itemsp[0] + __instance.GetItemDate(itemid, 0, false, -1) + itemsp[1], false);
            else
                return "<color=#323232FF>某不知名物品</color>";
        }
        public static string ProcessGong(DateFile __instance, int itemid, string[] itemsp=null){
            if(itemsp==null){
                itemsp=__instance.massageDate[10][0].Split(new char[] { '|' });
            }
            if(__instance.gongFaDate.ContainsKey(itemid)&&__instance.gongFaDate[itemid].ContainsKey(2) &&__instance.gongFaDate[itemid].ContainsKey(0) )
                return __instance.SetColoer(20001 + int.Parse(__instance.gongFaDate[itemid][2]), itemsp[0] + __instance.gongFaDate[itemid][0] + itemsp[1], false);
            else
                return "<color=#323232FF>某不知名功法</color>";
        }
        public static string ProcessDate(DateFile __instance, int itemid){
            int dayTrun = __instance.GetDayTrun(itemid);
            return string.Format("{0}{1}{2}{3}{4} {5}", new object[] {
                __instance.massageDate[16][1],
                __instance.SetColoer(10003, __instance.year.ToString(), false),
                __instance.massageDate[16][3],
                __instance.SetColoer(10003, __instance.solarTermsDate[dayTrun][99], false),
                __instance.massageDate[16][4],
                __instance.SetColoer(10003, __instance.solarTermsDate[dayTrun][1], false)
            });
        }
        public static string ProcessPlace(DateFile __instance, int place0,int place1){
            if(__instance.partWorldMapDate.ContainsKey(place0) && __instance.partWorldMapDate[place0].ContainsKey(0)){
                return with_place_id?__instance.SetColoer(10002, string.Format("{0}-{1}{2}(id.{3})", __instance.partWorldMapDate[place0][0], __instance.GetNewMapDate(place0, place1, 98), __instance.GetNewMapDate(place0, place1, 0),place1), false):__instance.SetColoer(10002, string.Format("{0}-{1}{2}", __instance.partWorldMapDate[place0][0], __instance.GetNewMapDate(place0, place1, 98), __instance.GetNewMapDate(place0, place1, 0)), false);
            } else return "<color=#323232FF>某不知名地点</color>";
        }
        #endregion
        [HarmonyPatch(typeof(PeopleLifeAI), "AISetChildren")]
        public static class PeopleLifeAIAISetChildren{
            public static void Postfix(int fatherId, int motherId){
                int actorId=motherId;
                if(DateFile.instance.GetActorDate(actorId, 14, false).ParseInt()==2 && DateFile.instance.GetActorDate(actorId, 11, true).ParseInt()>14 && DateFile.GetGongfaEternalPowerChange(actorId, WANBI_ID)>=0 && DateFile.instance.GetGongFaFTyp(actorId,WANBI_ID)!=-1&&(DateFile.instance.GetActorDate(actorId, 904, false).ParseInt()!=0)){
                    if(!DateFile.instance.HaveLifeDate(motherId, 901)){
                        DateFile.instance.actorLife[motherId].Add(901, new List<int>
                        {
                            3,//这里填1会导致过月生子，填2会导致过月事件出红字（因为这个用于记录春宵事件的假孩子不应该存在），填0会导致长达100月的怀孕冷却。
                            fatherId,
                            motherId,
                            1,
                            1
                        });
                        DateFile.instance.pregnantFeature.Add(motherId, new string[]
                        {
                            GameData.Characters.GetCharProperty(fatherId, 101),
                            GameData.Characters.GetCharProperty(motherId, 101)
                        });
                        DateFile.instance.ChangeActorFeature(motherId, 4002, 4003);
                    } else {
                        if(fatherId!=DateFile.instance.actorLife[motherId][901][1]){
                            DateFile.instance.actorLife[motherId][901].Add(fatherId);
                        }
                    }
                }
            }
        }
        [HarmonyPatch(typeof(DateFile), "GetTurnEventContent")] // 这里还需要在window那里patch一下switch命令，不然会报错，一个解法是把case改成2，这样可以传5个参数，
        public static class DateFileGetTurnEventContent {
            public static bool Prefix(DateFile __instance, int eventId, int[] eventArgs, ref List<string> __result){
                if(__instance.trunEventDate.ContainsKey(eventId) && __instance.trunEventDate[eventId].ContainsKey(-1)){
                    __result=new List<string>{ __instance.trunEventDate[eventId][0] ,"..."};
                    string[] contents=__instance.trunEventDate[eventId][99].Split(new char[] { '|' });
                    string content="";
                    int currarg=0;
                    int len=0;
                    if(eventArgs!=null)
                        len=eventArgs.Length;
                    else{return true;}
                    var itemsp=__instance.massageDate[10][0].Split(new char[] { '|' });
                    for(int i=0;i<contents.Length;i++){
                        string ss=contents[i];
                        string[] verb=ss.Split(new char[] { '-' });
                        string s=verb[0];
                        if(s=="Actor"){
                            content+=__instance.GetActorName(eventArgs[verb[1].ParseInt()], false, false);
                        } else if(s=="MActor"){
                            content+=__instance.SetColoer(10002, __instance.GetActorName(eventArgs[verb[1].ParseInt()], false, false), false);
                        } else if(s=="FActor"){
                            content+=__instance.SetColoer(10006, __instance.GetActorName(eventArgs[verb[1].ParseInt()], false, false), false);
                        } else if(s=="HActor"){
                            content+=__instance.SetColoer(10004, __instance.GetActorName(eventArgs[verb[1].ParseInt()], false, false), false);
                        } else if(s=="Date"){
                            content+=ProcessDate(__instance,eventArgs[verb[1].ParseInt()]);
                        } else if(s=="Item"){
                            content+=ProcessItem(__instance,eventArgs[verb[1].ParseInt()],itemsp);
                        } else if(s=="Gong"){
                            content+=ProcessGong(__instance,eventArgs[verb[1].ParseInt()],itemsp);
                        } else if(s=="Place"){
                            content+=ProcessPlace(__instance,eventArgs[verb[1].ParseInt()>>16],eventArgs[verb[1].ParseInt()&65535]);
                            currarg+=1;
                        } else {
                            content+=ss;
                        }
                        currarg+=1;
                    }
                    __result[1]=content;
                    return false;
                }
                return true;
            }
        }
    }
}
