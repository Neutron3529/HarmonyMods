/*
 * Neutron3529's Unity Game Plugin
 * Copyright (C) 2022-2023 Neutron3529
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * This program is a part of other mod, which could not be used directly
 * all of the bash script would append this file after the main .cs file
 * if you want to compile program manually, you should append it by yourself.
 */

using System;
using System.Reflection;
using System.Collections.Generic;
using HarmonyLib;

#if DOORSTOP
using utils;
using BasePlugin = UnityEngine.MonoBehaviour;
#elif IL2CPP
using BepInEx.Unity.IL2CPP;
#else
using BasePlugin = BepInEx.BaseUnityPlugin;
#endif

#if !DOORSTOP
using BepInEx;
using BepInEx.Configuration;
#endif

namespace Neutron3529 {
    #if DOORSTOP
        public class ConfigEntry<T> {
            public T Value {get;set;}
        }
    #endif
    public enum Enable {
        lt,
        le,
        ne,
        ge,
        gt,
        always,
        never
    }
    #if finished
    // TODO: using Patch to replace HarmonyPatch
    [System.AttributeUsage(System.AttributeTargets.Method, AllowMultiple = true)]
    public class Patch : System.Attribute {
        public Patch(Type ty, string name, Type[] types=null, MethodType mty=null, )
        public MethodBase met;
    }
    #endif
    [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct | System.AttributeTargets.Field, AllowMultiple = false)]
    public class Desc : System.Attribute {
        public string desc;
        public string str;
        public double val;
        public Neutron3529.Enable enable=Neutron3529.Enable.gt;
        public Type ty=typeof(void);
        public Desc(string desc,string str) {
            this.desc = desc;
            this.str = str;
            this.val = 0;
        }
        public Desc(string desc,double val) {
            this.desc = desc;
            this.str = "";
            this.val = val;
        }
        public Desc() {
            this.desc = null;
            this.str = "";
            this.val = 0;
        }
        public Desc(string desc) {
            this.desc = desc;
            this.str = "";
            this.val = 0;
        }
        public Desc(string desc,Enable enable,double val) {
            this.desc = desc;
            this.str = "";
            this.val = val;
            this.enable = enable;
        }
        public Desc(string desc,string enable,double val) {
            this.desc = desc;
            this.str = "";
            this.val = val;
            this.enable=enable.ToLower() switch {
                "lt"=>Neutron3529.Enable.lt,
                "le"=>Neutron3529.Enable.le,
                "ne" =>Neutron3529.Enable.ne,
                "ge"=>Neutron3529.Enable.ge,
                "gt"=>Neutron3529.Enable.gt,
                "always"=>Neutron3529.Enable.always,
                "never"=>Neutron3529.Enable.never,
                _=>throw new Exception($"Desc项目禁用选项{enable}无效")
            };
        }
        public string desc_with_enable(string now)=>Type.GetTypeCode(this.ty) switch{
            TypeCode.Object=>$"{this.desc}{now}",
            TypeCode.Boolean=>$"{this.desc}{now}（{this.enable switch {Neutron3529.Enable.gt=>"为true时启用此修改",Neutron3529.Enable.always=>"总是启用",Neutron3529.Enable.never=>"不影响此项启用状态",_=>throw new Exception("bool的Enable情况只能为默认（其实是gt），always与never")}}）",
            TypeCode.String=>$"{this.desc}{now}（字符串长度在除去多余空白字符后{this.enable_op}）",
            _=>$"{this.desc}{now}（{this.enable_op}）"
        };
        public string desc_with_enable()=>desc_with_enable(string.Empty);
        public string enable_op=>this.enable switch{
            Neutron3529.Enable.lt=>$"小于{this.val}时启用此修改",
            Neutron3529.Enable.le=>$"小于等于{this.val}时启用此修改",
            Neutron3529.Enable.ne=>$"不等于{this.val}时启用此修改",
            Neutron3529.Enable.ge=>$"大于等于{this.val}时启用此修改",
            Neutron3529.Enable.gt=>$"大于{this.val}时启用此修改",
            Neutron3529.Enable.always=>"总是启用此修改",
            Neutron3529.Enable.never=>"不影响当前项目的启用状态",
            _=>throw new Exception($"枚举错误，this.disable的值{this.enable}不正确")
        };
        public string desc_with_disable(string now)=>Type.GetTypeCode(this.ty) switch{
            TypeCode.Object=>$"{this.desc}{now}",
            TypeCode.Boolean=>$"{this.desc}{now}（{this.enable switch {Neutron3529.Enable.gt=>"为false时禁用此修改",Neutron3529.Enable.always=>"总是启用",Neutron3529.Enable.never=>"不影响此项启用状态",_=>throw new Exception("bool的Enable情况只能为默认（其实是gt），always与never")}}）",
            TypeCode.String=>$"{this.desc}{now}（字符串长度在除去多余空白字符后{this.disable_op}）",
            _=>$"{this.desc}{now}（{this.disable_op}）"
        };
        public string desc_with_disable()=>desc_with_disable(string.Empty);
        public string disable_op=>this.enable switch{
            Neutron3529.Enable.lt=>$"大于等于{this.val}时不主动启用此修改",
            Neutron3529.Enable.le=>$"大于{this.val}时不主动启用此修改",
            Neutron3529.Enable.ne=>$"等于{this.val}时不主动启用此修改",
            Neutron3529.Enable.ge=>$"小于{this.val}时不主动启用此修改",
            Neutron3529.Enable.gt=>$"小于等于{this.val}时不主动启用此修改",
            Neutron3529.Enable.always=>"总是启用此修改",
            Neutron3529.Enable.never=>"不影响当前项目的启用状态",
            _=>throw new Exception($"枚举错误，this.enable的值{this.enable}不正确")
        };
        public bool Enable()=>this.val>=0; // for class desc only.
        public bool never<T>(T a, double b)=>false; //
        public bool always<T>(T a, double b)=>true;
        public bool match<T>(T v)=>this.enable switch {
            Neutron3529.Enable.lt=>System.Convert.ToDouble(v) < this.val,
            Neutron3529.Enable.le=>System.Convert.ToDouble(v) <=this.val,
            Neutron3529.Enable.ne=>System.Convert.ToDouble(v) !=this.val,
            Neutron3529.Enable.ge=>System.Convert.ToDouble(v) >=this.val,
            Neutron3529.Enable.gt=>System.Convert.ToDouble(v) > this.val,
            Neutron3529.Enable.always=>true,
            Neutron3529.Enable.never=>false,
            _=>false
        };
        public bool match(string v)=>this.match(v.Length);
        public bool match(bool v)=>this.enable switch {
            Neutron3529.Enable.gt=>v,
            Neutron3529.Enable.always=>true,
            Neutron3529.Enable.never=>false,
            _=>throw new Exception("bool的Enable情况只能为默认（其实是gt），always与never")
        };
        public const MethodType get=MethodType.Getter;
        public const MethodType set=MethodType.Setter;
        public const MethodType ctor=MethodType.Constructor;
        public const MethodType Get=MethodType.Getter;
        public const MethodType Set=MethodType.Setter;
        public const MethodType Ctor=MethodType.Constructor;
        public const MethodType GET=MethodType.Getter;
        public const MethodType SET=MethodType.Setter;
        public const MethodType CTOR=MethodType.Constructor;
        public const BindingFlags all=(BindingFlags)(-1);
        public const BindingFlags All=(BindingFlags)(-1);
        public const BindingFlags ALL=(BindingFlags)(-1);
    }
    public abstract class ModEntry : BasePlugin {
        public static HarmonyLib.Harmony harmony;
        public static DescConfig config;
        #if DEBUG
            public static Action<string> logger;
        #else
            public static void logger(string s){}
        #endif
        #if VERBOSE
            public static Action<string> vlogger;
        #else
            public static void vlogger(string s){}
        #endif
        static Action<string> _logwarn;
        public static void logwarn(string s, Exception ex=null){
            _logwarn(ex is null?s:(s+str_exception(ex)));
        }
        public static void logexception(Exception ex){
            _logwarn(str_exception(ex));
        }
        public static string str_exception(Exception ex){
            var sb=new System.Text.StringBuilder();
            _strexception(sb, ex, 0);
            return sb.ToString();
        }
        static void _strexception(System.Text.StringBuilder sb, Exception ex, int depth){
            var padding=depth==0?"\n":"\n"+new string(' ',depth*4);
            sb.Append("\n").Append(ex.GetType().Name).Append(": ").Append(ex.Message).Append("\n").Append(ex.StackTrace).Replace("\n",padding);
            if(ex.InnerException !=null){
                _strexception(sb,ex.InnerException,depth+1);
            }
        }
    #if finished
    // TODO: static 还需斟酌
        public class Entry<T> : Base<T> {}
        public static class BaseT {
            public static Dictionary<Type, BaseTy> entries=new();
        }
        public abstract class BaseTy {
            public bool enable=false;
            public string full_desc="";
            protected Dictionary<FieldInfo, object> dict=new Dictionary<FieldInfo, object>();
            public static readonly MethodInfo CONFIG_BIND=typeof(DescConfig).GetMethod("Bind",new Type[]{typeof(string),typeof(string),Type.MakeGenericMethodParameter(0),typeof(string)});
            public static readonly MethodInfo DESC_MATCH=typeof(Desc).GetMethod("match", new Type[]{Type.MakeGenericMethodParameter(0)});
            public abstract bool assign(FieldInfo f, Desc desc);
            public virtual void Init()=>Init(true);
            public abstract void Init(bool check_enable);
        }
        public abstract class Base<T> : BaseTy {
            public override bool assign(FieldInfo f, Desc desc){ // return whether the type is enabled.
                var met=typeof(T).GetMethod("assign", (BindingFlags)(-1), new Type[]{typeof(Base<T>),typeof(FieldInfo),typeof(Desc)});
                // static bool assign(this Base<T>, FieldInfo f, Desc desc)
                if(met is not null){
                    if(met.ReturnType==typeof(bool)){
                        return (bool)(met.Invoke(null, new object[]{this, f, desc}));
                    }
                }
                met=typeof(T).GetMethod("assign", (BindingFlags)(-1), new Type[]{typeof(FieldInfo),typeof(Desc)});
                // static bool assign(FieldInfo f, Desc desc)
                if(met is not null){
                    if(met.ReturnType==typeof(bool)){
                        return (bool)(met.Invoke(null, new object[]{f, desc}));
                    }
                }
                // TODO: 增加[assign]属性
                desc.ty=f.FieldType;
                string full_entry = typeof(T).Name+"."+f.Name;
                try {
                    var method=CONFIG_BIND.MakeGenericMethod(f.FieldType);
                    var prop=method.ReturnType.GetProperty("Value").GetGetMethod();
                    if(prop.ReturnType!=f.FieldType){
                        throw new Exception($"获取属性方法出错， prop:{prop.ReturnType} != field:{f.FieldType}");
                    }
                    dict[f]=method.Invoke(config,new object[]{"config",full_entry,f.GetValue(null),desc.desc_with_enable()});
                    var val=prop.Invoke(dict[f],null);
                    f.SetValue(null,val);
                    if(prop.ReturnType==typeof(string)){
                        return desc.match((string)val);
                    } else if (prop.ReturnType==typeof(bool)){
                        return desc.match((bool)val);
                    }else {
                        return (bool)(DESC_MATCH.MakeGenericMethod(prop.ReturnType).Invoke(desc, new object[]{val}));
                    }
                } catch (Exception ex) {
                    logwarn($"使用了不支持的ConfigEntry: {f.Name} : {f.FieldType} ，错误如下",ex);
                    return true;
                }
            }
            public override void Init(bool check_enable){
    #if VERBOSE
                vlogger("正在修改："+typeof(T).Name);
    #endif
                var type = typeof(T);
                this.full_desc="";
                Desc dsc = (Desc) Attribute.GetCustomAttribute(type, typeof(Desc));  // 判断类是否有Desc
                if(dsc!=null){
                    if(dsc.desc is null){
                        dsc.desc = typeof(T).Name;
                    }
                    this.enable=dsc.Enable();
                    this.assign(type.GetField("enable"),dsc);
                    full_desc=dsc.desc;
                }
                foreach(var f in type.GetFields((BindingFlags)(-1))){
                    Desc desc = (Desc) Attribute.GetCustomAttribute(f, typeof(Desc));
    #if VERBOSE
                    vlogger("正在检查："+type.Name+"的"+f.Name+"字段");
    #endif
                    if(desc!=null){
    #if VERBOSE
                        vlogger("正在检查："+type.Name+"的"+f.Name+"字段--包含desc信息");
    #endif
                        var flag=this.assign(f,desc);
                        enable|=flag;
                        var now=$"={f.GetValue(null)}";
                        if(full_desc.Length>0){
                            full_desc=full_desc+(dsc==null?" & ":" ： ")+(flag?desc.desc_with_disable(now):desc.desc_with_enable(now));
                            dsc=null;
                        }else{
                            full_desc=flag?desc.desc_with_disable(now):desc.desc_with_enable(now);
                        }
                    }
                }
                var met=type.GetMethod("Init", new Type[]{typeof(bool)});
                if(met is not null){
                    if(met.ReturnType==typeof(bool)){
                        enable=(bool)(met.Invoke(null, new object[]{check_enable}));
                    } else {
                        met.Invoke(null, new object[]{check_enable});
                    }
                }
            }
        }

        public abstract class Custom<T> : Base<T> {
            public abstract void Enable();
            public override void Init(bool check_enable){
                base.Init();
                if(check_enable){
                    if(enable){
                        try {
                            this.Enable();
                            logger($"已启用 {full_desc}");
                        } catch (Exception e) {
                            logwarn($"({this.GetType()})在执行Enable时出错，这导致Mod的「{this.full_desc}」功能失效。具体错误如下：");
                            logexception(e);
                        }
                    } else {
                        logger($"未启用 {full_desc}");
                    }
                } else {
                    logger($"开始手工处理 {full_desc} 的数据");
                }
            }
        }
        public abstract class Harmony<T> : Custom<T> {
            public override void Enable(){
                if(this.enable){
                    harmony.PatchAll(this.GetType());
                }
            }
        }
        public abstract class CustomHarmony<T> : Custom<T> {
            static MethodInfo met=typeof(T).GetMethod("Enable", new Type[0]);
            static bool flag=met is null || met.ReturnType!=typeof(bool);
            public override void Enable(){
                if(flag){
                    logwarn($"由于{(met is null?"enable is null":"met.ReturnType!=typeof(bool)")}，Harmony Enable执行失败");
                } else if((bool)(met.Invoke(null, null))){
                    harmony.PatchAll(this.GetType());
                }
            }
        }
        public abstract class CustomNoHarmony<T> : Custom<T> {
            static MethodInfo met=typeof(T).GetMethod("Enable", new Type[0]);
            static bool flag=met is null || met.ReturnType!=typeof(void);
            public override void Enable(){
                met.Invoke(null, null);
            }
        }
    #endif
        public abstract class Entry : Base {
            public override void Enable(){
                harmony.PatchAll(this.GetType());
            }
        }
        public class DescConfig {
            #if !DOORSTOP
                public BepInEx.Configuration.ConfigFile config;
            #endif
            public DescConfig(ModEntry entry){
            #if !DOORSTOP
                config = entry.Config;
            #else
                // TODO: adding config file.
            #endif
            }
            public ConfigEntry<T> ConfigEntry<T>(string arg1, string arg2, T arg3, string arg4){
            #if !DOORSTOP
                return this.config.Bind(arg1,arg2,arg3,arg4);
            #else
                return default(ConfigEntry<T>);
                // TODO: adding config file.
            #endif
            }
        }

        // Base类不会被Awake自动初始化，而Entry会。
        // Base应该配合new Base().Init()使用。
        public abstract class Base {
            public bool enable=false;
            public string full_desc="";
            static Dictionary<FieldInfo, object> dict=new Dictionary<FieldInfo, object>();
            public static void SetConfigValue<T>(FieldInfo f, T val) {
                if (dict.TryGetValue(f, out var v) && v is ConfigEntry<T> entry) {
                    entry.Value = val;
                } else {
                    logwarn($"either dict contains no key of {f}, or {f} does not has the type {typeof(T)}");
                }
            }

            #if MakeGenericMethodParameter
                public static readonly MethodInfo CONFIG_BIND=typeof(DescConfig).GetMethod("ConfigEntry",new Type[]{typeof(string),typeof(string),Type.MakeGenericMethodParameter(0),typeof(string)});
                public static readonly MethodInfo DESC_MATCH=typeof(Desc).GetMethod("match", new Type[]{Type.MakeGenericMethodParameter(0)});
            #endif
            public virtual bool assign(FieldInfo f, Desc desc){ // return whether the type is enabled.
                desc.ty=f.FieldType;
                string full_entry = this.GetType().Name+"."+f.Name;
                #if !MakeGenericMethodParameter
                    switch(Type.GetTypeCode(f.FieldType)) {
                        case TypeCode.SByte:
                            dict[f]=config.ConfigEntry("config",full_entry,(sbyte)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<sbyte>)dict[f]).Value));
                            return desc.match(((ConfigEntry<sbyte>)dict[f]).Value);
                        case TypeCode.Byte:
                            dict[f]=config.ConfigEntry("config",full_entry,(byte)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<byte>)dict[f]).Value));
                            return desc.match(((ConfigEntry<byte>)dict[f]).Value);
                        case TypeCode.Int16:
                            dict[f]=config.ConfigEntry("config",full_entry,(short)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<short>)dict[f]).Value));
                            return desc.match(((ConfigEntry<short>)dict[f]).Value);
                        case TypeCode.UInt16:
                            dict[f]=config.ConfigEntry("config",full_entry,(ushort)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<ushort>)dict[f]).Value));
                            return desc.match(((ConfigEntry<ushort>)dict[f]).Value);
                        case TypeCode.Int32:
                            dict[f]=config.ConfigEntry("config",full_entry,(int)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<int>)dict[f]).Value));
                            return desc.match(((ConfigEntry<int>)dict[f]).Value);
                        case TypeCode.UInt32:
                            dict[f]=config.ConfigEntry("config",full_entry,(uint)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<uint>)dict[f]).Value));
                            return desc.match(((ConfigEntry<uint>)dict[f]).Value);
                        case TypeCode.Int64:
                            dict[f]=config.ConfigEntry("config",full_entry,(long)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<long>)dict[f]).Value));
                            return desc.match(((ConfigEntry<long>)dict[f]).Value);
                        case TypeCode.UInt64:
                            dict[f]=config.ConfigEntry("config",full_entry,(ulong)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<ulong>)dict[f]).Value));
                            return desc.match(((ConfigEntry<ulong>)dict[f]).Value);
                        case TypeCode.Single:
                            dict[f]=config.ConfigEntry("config",full_entry,(float)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<float>)dict[f]).Value));
                            return desc.match(((ConfigEntry<float>)dict[f]).Value);
                        case TypeCode.Double:
                            dict[f]=config.ConfigEntry("config",full_entry,(double)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<double>)dict[f]).Value));
                            return desc.match(((ConfigEntry<double>)dict[f]).Value);
                        case TypeCode.Boolean:
                            dict[f]=config.ConfigEntry("config",full_entry,(bool)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<bool>)dict[f]).Value));
                            return desc.match(((ConfigEntry<bool>)dict[f]).Value);
                        case TypeCode.String:
                            dict[f]=config.ConfigEntry("config",full_entry,(string)(f.GetValue(this)),desc.desc_with_enable());
                            f.SetValue(this,(((ConfigEntry<string>)dict[f]).Value));
                            return desc.match(((ConfigEntry<string>)dict[f]).Value);
                        default:logwarn($"Unsupport type: {f.Name} : {f.FieldType}");return true;
                    }
                #else
                    try {
                        var method=CONFIG_BIND.MakeGenericMethod(f.FieldType);
                        var prop=method.ReturnType.GetProperty("Value").GetGetMethod();
                        if(prop.ReturnType!=f.FieldType){
                            throw new Exception($"获取属性方法出错， prop:{prop.ReturnType} != field:{f.FieldType}");
                        }
                        dict[f]=method.Invoke(config,new object[]{"config",full_entry,f.GetValue(this),desc.desc_with_enable()});
                        var val=prop.Invoke(dict[f],null);
                        f.SetValue(this,val);
                        if(prop.ReturnType==typeof(string)){
                            return desc.match((string)val);
                        } else if (prop.ReturnType==typeof(bool)){
                            return desc.match((bool)val);
                        }else {
                            return (bool)(DESC_MATCH.MakeGenericMethod(prop.ReturnType).Invoke(desc, new object[]{val}));
                        }
                    } catch (Exception ex) {
                        logwarn($"使用了不支持的ConfigEntry: {f.Name} : {f.FieldType} ，错误如下",ex);
                        return true;
                    }
                #endif
            }
            public virtual void Init()=>Init(true);
            public virtual void Init(bool check_enable){
    #if VERBOSE
                vlogger("正在修改："+this.GetType().Name);
    #endif
                var type = this.GetType();
                this.full_desc="";
                Desc dsc = (Desc) Attribute.GetCustomAttribute(type, typeof(Desc));  // 判断类是否有Desc
                if(dsc!=null){
                    if(dsc.desc is null){
                        dsc.desc = type.Name;
                    }
                    this.enable=dsc.Enable();
                    this.assign(type.GetField("enable"),dsc);
                    full_desc=dsc.desc;
                }
                foreach(var f in type.GetFields((BindingFlags)(-1))){
                    Desc desc = (Desc) Attribute.GetCustomAttribute(f, typeof(Desc));
    #if VERBOSE
                    vlogger("正在检查："+this.GetType().Name+"的"+f.Name+"字段");
    #endif
                    if(desc!=null){
    #if VERBOSE
                        vlogger("正在检查："+this.GetType().Name+"的"+f.Name+"字段--包含desc信息");
    #endif
                        if(desc.desc is null){
                            desc.desc = full_desc.Length>0?f.Name:(type.Name+"."+f.Name);
                        }
                        var flag=this.assign(f,desc);
                        enable|=flag;
                        var now=$"={f.GetValue(null)}";
                        if(full_desc.Length>0){
                            full_desc=full_desc+(dsc==null?" & ":" ： ")+(flag?desc.desc_with_disable(now):desc.desc_with_enable(now));
                            dsc=null;
                        }else{
                            full_desc=flag?desc.desc_with_disable(now):desc.desc_with_enable(now);
                        }
                    }
                }
                if(check_enable){
                    if(enable){
                        try {
                            this.Enable();
                            logger($"已启用 {full_desc}");
                        } catch (Exception e) {
                            logwarn($"({this.GetType()})在执行Enable时出错，这导致Mod的「{this.full_desc}」功能失效。具体错误如下：");
                            logexception(e);
                        }
                    } else {
                        logger($"未启用 {full_desc}");
                    }
                } else {
                    logger($"开始手工处理 {full_desc} 的数据");
                }
            }
            public abstract void Enable();
        }

        public abstract class Const : Base {
            public override bool assign(FieldInfo f, Desc desc){ // return whether the type is enabled.
                base.assign(f,desc);
                return true;
            }
            public override void Init(){
    #if VERBOSE
                vlogger("正在修改："+this.GetType().Name);
    #endif
                var type = this.GetType();
                this.full_desc="";
                Desc dsc = (Desc) Attribute.GetCustomAttribute(type, typeof(Desc));
                if(dsc!=null){
                    full_desc=dsc.desc;
                }
                foreach(var f in type.GetFields((BindingFlags)(-1))){
                    Desc desc = (Desc) Attribute.GetCustomAttribute(f, typeof(Desc));
    #if VERBOSE
                    vlogger("正在检查："+this.GetType().Name+"的"+f.Name+"字段");
    #endif
                    if(desc!=null){
    #if VERBOSE
                        vlogger("正在检查："+this.GetType().Name+"的"+f.Name+"字段--包含desc信息");
    #endif
                        this.assign(f,desc);
                        if(full_desc.Length>0){
                            full_desc=full_desc+(dsc==null?" & ":" ： ")+desc.desc;
                            dsc=null;
                        }else{
                            full_desc=desc.desc;
                        }
                    }
                }
                logger($"读取常数 {full_desc}");
            }
            public override void Enable(){}
        }
    #if IL2CPP
        public override void Load()=>Init();
    #else
        public virtual void Awake()=>Init();
    #endif
        static bool need_init=true;
        public virtual void Init() {
            if(need_init){
                need_init=false;
                logger("开始注入");
                Type[] t=new Type[0];
                foreach(var type in this.GetType().Module.GetTypes()){
                    if(!type.IsAbstract){
                        try{
    #if VERBOSE
                            vlogger("搜索到："+type.ToString());
    #endif
                            if(type.IsSubclassOf(typeof(Base))){
                                Base entry=(Base)(type.GetConstructor(t).Invoke(t));
                                if (entry!=null){
                                    entry.Init();
                                    // type.GetMethod("Init",t).Invoke(,t);
                                }else{logwarn($"{(type.IsSubclassOf(typeof(Entry))?"Entry":"Base")}：{type}的type.GetConstructor().Invoke()是null，这多半是mod出了问题，如果你看到这个，请联系mod作者。");}
                            }
    #if finished
                            else if(type.IsAbstract && type.IsSealed){
                                BaseTy entry=(BaseTy)(type.GetConstructor(t).Invoke(t));
                                if (entry!=null){
                                    entry.Init();
                                }else{logwarn($"{(type.IsSubclassOf(typeof(Entry))?"Entry":"Base")}：{type}的type.GetConstructor().Invoke()是null，这多半是mod出了问题，如果你看到这个，请联系mod作者。");}
                            }
    #endif
                        } catch (Exception e) {
                            logwarn($"{this.plugin_id}({type})出现了意料之外的错误，或许你可以联系Mod作者修复。");
                            logexception(e);
                        }
                    }
                }
            }
        }
        string plugin_id;
        public ModEntry(string plugin_id){
            this.plugin_id=plugin_id;
    #if IL2CPP
        #if DEBUG
            logger=Log.LogInfo;
            #if VERBOSE
            vlogger=Log.LogWarning;              // `vlogger` is defined in utils.cs
            #endif
        #endif
    #else
        #if DEBUG
            logger=Logger.LogInfo;
            #if VERBOSE
            vlogger=Logger.LogWarning;              // `vlogger` is defined in utils.cs
            #endif
        #endif
    #endif
    #if IL2CPP
            _logwarn=Log.LogWarning;
            Log.LogInfo("此Mod使用AGPL-v3许可发布，如果你使用了这里的代码，请按照相同许可发布你修改后的mod。");
    #else
            _logwarn=Logger.LogWarning;
            Logger.LogInfo("此Mod使用AGPL-v3许可发布，如果你使用了这里的代码，请按照相同许可发布你修改后的mod。");
    #endif
            harmony=new Harmony(plugin_id);   // `harmony` is defined in utils.cs
            config=new DescConfig(this);                          // `utils`   is defined in utils.cs
        }
    }
// BLACK MAGIC: save one more #if ...#endif by breaking the `{}` matching rule.
#if DOORSTOP
// TODO: must write a sepearted Unity loader, otherwise the dll could not be loaded?
    public static class Logger {
        static Logger() {
            System.IO.File.WriteAllText("./Neutron/Log.log", "");
        }
        public static void LogInfo(string s){
            System.IO.File.AppendAllText("./Neutron/Log.log", s+"\n");
        }
        public static void LogWarning(string s){
            System.IO.File.AppendAllText("./Neutron/Log.log", "Warning:"+s+"\n");
        }
    }
}
// harmony v2.3.3.0
namespace utils {
    public static class ImplHarmonyXPatchAll {
        public static void PatchAll(this Harmony harmony, Type type) {
             (new HarmonyLib.PatchClassProcessor(harmony, type)).Patch();
        }
    }
}
// doorstop v0.4.3
namespace Doorstop {
    class Entrypoint {
        public static void Start() {
            System.IO.File.WriteAllText("doorstop_hello.log", "Hello from Unity!");
            (new Neutron3529.Cheat.Cheat()).Awake();
        }
    }
#endif
}
