#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="$GAME_NAME"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

case $1 in
  V)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  v)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  VERBOSE) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  verbose) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  D)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  d)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  DEBUG)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  debug)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  *)       EXTRA_DEFINE=""                                               ;;
esac

_MODE__SELECT_=100

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.AIModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  $(for i in "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize $EXTRA_DEFINE \
  - $UTILS && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
  git add ${FILE_NAME}
  case $2 in
    R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    *) git commit -am "$2" ;;
  esac
  git push
fi
exit





using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%;
[BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
public class Cheat : Neutron3529.ModEntry {
    public Cheat() : base("%%PLUGIN_ID%%") {}
    void Start2() {
        // 主要逻辑放在`utils.cs`中，这里的start2只是为了以防万一
        // 目前来说，这个函数的唯一用途是用来“叮”……
        // 就像这样：
        logger("叮~修改器启动，请安心游戏");
    }
    [Desc("无限矿物"),HarmonyPatch(typeof(CItem_ContentExtractor), "Update01s")]
    public class CItem_ContentExtractorUpdate01s_FreeMiner:Entry{
        // public static int val;public void Init() {base.Init(ref val);}
        static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name+"正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
            instructions = new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(OpCodes.Dup),
                    new CodeMatch(OpCodes.Ldind_U2),
                    new CodeMatch(OpCodes.Ldc_I4_1),
                    new CodeMatch(OpCodes.Sub),
                    new CodeMatch(OpCodes.Conv_U2),
                    new CodeMatch(OpCodes.Stind_I2)
// 20	003D	dup
// 21	003E	ldind.u2
// 22	003F	ldc.i4.1
// 23	0040	sub
// 24	0041	conv.u2
// 25	0042	stind.i2
                ).Repeat( matcher => // Do the following for each match
                    matcher
                    .Advance(2) // Move cursor to after ldfld
                    .SetAndAdvance(
                        OpCodes.Ldc_I4_0,null
                    )
                ).InstructionEnumeration();
            return instructions;
        }
    }
    [Desc("加速挖矿-读条结束后满矿"),HarmonyPatch(typeof(CItem_ContentExtractor), "Update01s")]
    public class CItem_ContentExtractor_SpeedUp:Entry{
        // public static int val;public void Init() {base.Init(ref val);}
        static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name+"正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
            var from=typeof(CStack).GetMethod("Add",(BindingFlags)(-1));
            var to=typeof(CItem_ContentExtractor_SpeedUp).GetMethod("fine",(BindingFlags)(-1));
            // var from = AccessTools.Method(typeof(OriginalCode), nameof(OriginalCode.DoNothing));
            // var to = AccessTools.Method(typeof(ReplaceCode), nameof(ReplaceCode.ReplaceDoNothing));
            logger("%%PLUGIN_ID%%---正在替换："+from.ToString()+"->"+to.ToString());
            return Transpilers.MethodReplacer(instructions, from, to);
        }
        public static void fine(ref CStack instance, int count) {
            instance.nb=instance.nbMax;
        }
    }
    [HarmonyPatch(typeof(CItem_ContentExtractor), "Update01s")]
    public class CItem_ContentExtractor_Faster:Entry{
        [Desc("加速挖矿-读条速度乘数")]
        public static int val=10;
        static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name+"正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
            var from=typeof(CItemData).GetMethod("IncrementIFP",(BindingFlags)(-1),null,new Type[]{typeof(int2)},null);
            var to=typeof(CItem_ContentExtractor_Faster).GetMethod("fine",(BindingFlags)(-1));
            // var from = AccessTools.Method(typeof(OriginalCode), nameof(OriginalCode.DoNothing));
            // var to = AccessTools.Method(typeof(ReplaceCode), nameof(ReplaceCode.ReplaceDoNothing));
            logger("%%PLUGIN_ID%%---正在替换："+from.ToString()+"->"+to.ToString());
            return Transpilers.MethodReplacer(instructions, from, to);
        }
        public static bool fine(CItemData instance, int2 coords) {
            return instance.IncrementIFP(coords,val);
        }
    }
    [Desc("加速温室气体排放"),HarmonyPatch(typeof(CItem_ContentGreenHouse), "Update01s")]
    public class CItem_ContentGreenHouse_Update01s:Entry{
        static void Prefix(CItem_ContentGreenHouse __instance, int2 coords){
            __instance.dataProgress.SetValue(coords, __instance.dataProgress.valueMax);
        }
    }
    public class GDrones_ctor:Entry{
        [Desc("无人机速度乘数")]
        public static float val;
        public override void Enable(){
            GDrones.speed*=val;
            GDrones.durationTakeOff/=val;
            base.Enable();
        }
    }
}

