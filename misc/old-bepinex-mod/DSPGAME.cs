#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022-2024 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="Dyson Sphere Program"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

case $1 in
V)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
v)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
VERBOSE) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
verbose) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
D)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
d)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
DEBUG)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
debug)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
*)       EXTRA_DEFINE=""                                               ;;
esac

_MODE__SELECT_=100

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
-r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
`[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
-r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
-r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
-r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
-r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.AIModule.dll" \
-r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
-r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.UI.dll" \
-r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
$(for i in "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
-out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
-optimize $EXTRA_DEFINE \
- $UTILS && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
    R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
*) git commit -am "$2" ;;
esac
git push
fi
exit





using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%;
[BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
public class Cheat : Neutron3529.ModEntry {
    public Cheat() : base("%%PLUGIN_ID%%") {}
    public override void Init() {
        base.Init();
        // 主要逻辑放在`utils.cs`中，这里的Awake2只是为了以防万一
        // 目前来说，这个函数的唯一用途是用来“叮”……
        // 就像这样：
        logger("叮~修改器启动，请安心游戏");
    }
    class LabComponentInternalUpdateResearch_Fix : Entry {
        [Desc("电力乘数")]
        public static float val=-1;
        [HarmonyPatch(typeof(LabComponent), "InternalUpdateResearch")]
        public static bool Prefix(ref float power,float research_speed) {
            power = (float)((int)(research_speed + 2f))/research_speed;
            return true;
        }
        [HarmonyPatch(typeof(PowerSystem), "GameTick")]
        public static void Postfix(PowerSystem __instance) {
            for(int i=__instance.networkServes.Length-1;i>=0;i--) {
                __instance.networkServes[i]=val;
            }
        }
    }
    [Desc("打包机持续工作",-1)]
    [HarmonyPatch(typeof(PilerComponent), "InternalUpdate")]
    class PilerComponentInternalUpdate : Entry {
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions) {
            return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(i => i.opcode == OpCodes.Ldfld && ((FieldInfo)i.operand).Name == "consumerRatio") // match method 1
                ).Repeat( matcher => // Do the following for each match
                    matcher
                    .Advance(1) // Move cursor to after ldfld
                    .InsertAndAdvance(
                        new CodeInstruction(OpCodes.Pop),
                        new CodeInstruction(OpCodes.Ldc_R4,1f)
                    )
                ).InstructionEnumeration();
        }
    }

    [Desc("挖矿加速（CodeMatcher）")]
    public class PlanetTransportAndMiner_GameTick : Entry {
        static IEnumerable<MethodBase> TargetMethods()
        {
            MethodInfo[] m=typeof(FactorySystem).GetMethods();
            foreach(MethodInfo i in m)if(i.Name=="GameTick"){
                yield return i;
            }
            yield return typeof(PlanetTransport).GetMethod("GameTick");
        }
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions) {
            return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(OpCodes.Ldfld, typeof(GameHistoryData).GetField("miningSpeedScale",(BindingFlags)(-1))) // match method 2
                ).Repeat( matcher => matcher// Do the following for each match
                    .Advance(1) // Move cursor to after ldfld
                    .InsertAndAdvance(
                        new CodeInstruction(OpCodes.Pop),
                        new CodeInstruction(OpCodes.Ldc_R4,100f)
                    )
                ).InstructionEnumeration();
        }
    }

    [Desc("挖矿不消耗资源（CodeMatcher）")]
    [HarmonyPatch]
//         [HarmonyPatch(typeof(FactorySystem), "GameTick", new Type[]{ typeof(long), typeof(bool) })]
//         [HarmonyPatch(typeof(FactorySystem), "GameTick", new Type[]{ typeof(long), typeof(bool), typeof(int), typeof(int), typeof(int) })]
    public class FactorySystemGameTick : Entry {// used for 2  FactorySystem functions.
        static IEnumerable<MethodBase> TargetMethods()
        {
            MethodInfo[] m=typeof(FactorySystem).GetMethods();
            foreach(MethodInfo i in m)if(i.Name=="GameTick"){
                yield return i;
            }
            yield return typeof(PlanetTransport).GetMethod("GameTick");
        }
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions) {
            return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(OpCodes.Ldfld, AccessTools.Field(typeof(GameHistoryData),"miningCostRate")) // match method 2
                ).Repeat( matcher => matcher// Do the following for each match
                    .Advance(1) // Move cursor to after ldfld
                    .InsertAndAdvance(
                        new CodeInstruction(OpCodes.Pop),
                        new CodeInstruction(OpCodes.Ldc_R4,0f)
                    )
                ).InstructionEnumeration();
        }
    }
    [Desc("机甲瞬间合成")]
    [HarmonyPatch(typeof(MechaForge), "GameTick")]
    public class MechaForgeGameTick : Entry {
        public static void Prefix(ref MechaForge __instance) {
            if (__instance.tasks.Count > 0) {
                __instance.tasks[0].tick=__instance.tasks[0].tickSpend;
            }
        }
    }

    [Desc("机甲自由曲速（CodeMatcher）")]
    [HarmonyPatch(typeof(PlayerMove_Sail), "GameTick")]
    public class PlayerMove_SailGameTick : Entry {
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions) {
            return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(OpCodes.Ldfld, AccessTools.Field(typeof(Mecha),"thrusterLevel")),
                    new CodeMatch(OpCodes.Ldc_I4_3),
                    new CodeMatch(i => i.opcode == OpCodes.Blt)
                ).Advance(
                    1
                ).SetOpcodeAndAdvance(
                    OpCodes.Ldc_I4_0
                ).InstructionEnumeration();
        }
    }
    [Desc("机甲曲速不消耗翘曲器")]
    [HarmonyPatch(typeof(Mecha), "UseWarper")]
    class MechaUseWarper : Entry {
        public static bool Prefix(ref bool __result) {
            __result=true;
            return false;
        }
    }
    [Desc("机甲曲速不消耗能量")]
    [HarmonyPatch(typeof(PlayerMove_Sail), "UseWarpEnergy")]
    class PlayerMove_SailUseWarpEnergy : Entry {
        public static bool Prefix(ref bool __result) {
            __result=true;
            return false;
        }
    }
    [HarmonyPatch(typeof(MechaLab), "GameTick")]
    class MechaLabGameTick : Entry {
        [Desc("机甲研究室研究速度（CodeMatcher）")]
        public static int val=900000000;
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions) {
            return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(OpCodes.Ldarg_0),
                    new CodeMatch(OpCodes.Ldfld, AccessTools.Field(typeof(MechaLab),"gameHistory")),
                    new CodeMatch(OpCodes.Ldfld, AccessTools.Field(typeof(GameHistoryData),"techSpeed"))
                ).Advance(1).SetAndAdvance(
                    OpCodes.Pop,null
                ).SetAndAdvance(
                    OpCodes.Ldc_I4,val
                ).InstructionEnumeration();
        }
    }
    [Desc("开启二周目矩阵支援，在研究电磁学技术之后，获得成吨的矩阵，适量的解锁物品，几个矩阵实验室，和物流站",-1)]
    [HarmonyPatch(typeof(GameHistoryData), "GainTechAwards")]
    class GameHistoryDataGainTechAwards : Entry {
        static PlanetFactory nearestFactory;
        static Mecha mecha;
        static int randSeed=0;
        public static void AddItems(int id,int count){
            mecha.AddProductionStat(id,count,nearestFactory);
            GameMain.mainPlayer.TryAddItemToPackage(id,count, count*4, true, 0);
            UIItemup.Up(id, count);
        }
        public static void AddStats(int id,int count){
            mecha.AddProductionStat(id,count,nearestFactory);
            mecha.AddConsumptionStat(id,count,nearestFactory);
        }
        public static void AddTrashes(int id,int quantity,int count){
            mecha.AddProductionStat(id,count*quantity,nearestFactory);
            VectorLF3 vectorLF = Maths.QRotateLF(GameMain.mainPlayer.uRotation, new VectorLF3(0f, 1f, 0f));
            int nearStarId = (GameMain.data.localStar != null) ? (GameMain.data.localStar.id * 100) : 0;
            double nearStarGravity = (GameMain.data.localStar != null) ? GameMain.data.trashSystem.GetStarGravity(GameMain.data.localStar.id) : 0.0;
            for(int i=0;i<count;i++){
                TrashObject trashObj = new TrashObject(id, quantity, quantity*4, Vector3.zero, Quaternion.identity);
                TrashData trashData = default(TrashData);
                trashData.landPlanetId = 0;
                trashData.nearPlanetId = 0;
                trashData.nearStarId = nearStarId;
                trashData.nearStarGravity = nearStarGravity;
                trashData.lPos = Vector3.zero;
                trashData.lRot = Quaternion.identity;
                trashData.uPos = GameMain.mainPlayer.uPosition + RandomTable.SphericNormal(ref randSeed, 0.8);
                trashData.uRot = Quaternion.LookRotation(RandomTable.SphericNormal(ref randSeed, 1.0).normalized, vectorLF);
                trashData.uVel = GameMain.mainPlayer.uVelocity + RandomTable.SphericNormal(ref randSeed, 8.0) + vectorLF * 15.0;
                trashData.uAgl = RandomTable.SphericNormal(ref randSeed, 0.03);
                GameMain.data.trashSystem.container.NewTrash(trashObj, trashData);
            }
        }
        public static void Postfix(int itemId) {
            if(itemId==2301){
                randSeed=0;
                nearestFactory = GameMain.mainPlayer.nearestFactory;
                mecha = GameMain.mainPlayer.mecha;
//                     GameHistoryDataGainTechAwards.AddStats(1120,9000000);//氢
//                     GameHistoryDataGainTechAwards.AddStats(1121,2000000);//重氢
//                     GameHistoryDataGainTechAwards.AddStats(1126, 200000);//卡晶
//                     GameHistoryDataGainTechAwards.AddStats(1304, 200000);//阴间过滤器
                GameHistoryDataGainTechAwards.AddItems(1202, 30+150-10);//磁线圈，升级用，有10个多余（因为我们已经支付了这一项科技的成本）
                GameHistoryDataGainTechAwards.AddItems(1301, 40+220);//电路板，升级用
                GameHistoryDataGainTechAwards.AddItems(1201, 20+  0);//  齿轮，升级用
                GameHistoryDataGainTechAwards.AddItems(1101,  0+ 20);//  铁块，升级用
                GameHistoryDataGainTechAwards.AddItems(1104,  0+ 20);//  铜块，升级用
                GameHistoryDataGainTechAwards.AddItems(1203,  0+ 60);//电动机，升级用
                GameHistoryDataGainTechAwards.AddItems(1006,  0+210);//  煤矿，升级用
                GameHistoryDataGainTechAwards.AddItems(1103,  0+120);//  钢材，升级用
                GameHistoryDataGainTechAwards.AddItems(1030,  0+ 60);//  木材，升级用
                GameHistoryDataGainTechAwards.AddItems(1109,  0+ 60);//高能石墨升级用
                GameHistoryDataGainTechAwards.AddStats(1403,     60);//湮灭约束球
                GameHistoryDataGainTechAwards.AddItems(1803, 120);//燃料棒
                GameHistoryDataGainTechAwards.AddItems(2001,   1);//黄带，1个，用于治疗强迫症
//
//                     GameHistoryDataGainTechAwards.AddItems(6001,400+1400);//初始升级用矩阵
//                     GameHistoryDataGainTechAwards.AddItems(6002,400+1400);
//                     GameHistoryDataGainTechAwards.AddItems(6003,300+1200);

                GameHistoryDataGainTechAwards.AddItems(2103,  40);//物流站，40个
                GameHistoryDataGainTechAwards.AddItems(2104,  10);//物流站，10个
                GameHistoryDataGainTechAwards.AddItems(2105,  40);//物流站，40个
                GameHistoryDataGainTechAwards.AddItems(2316,  20);//大型采矿机，20个
                GameHistoryDataGainTechAwards.AddItems(5001,3000);//小飞机，100个
                GameHistoryDataGainTechAwards.AddItems(5002, 100);//大飞机，100个
                GameHistoryDataGainTechAwards.AddItems(2003,3000);//蓝带，3000个



//                     GameHistoryDataGainTechAwards.AddTrashes(6001,1000,156);
//                     GameHistoryDataGainTechAwards.AddTrashes(6002,1000,157);
//                     GameHistoryDataGainTechAwards.AddTrashes(6003,1000,124);
//                     GameHistoryDataGainTechAwards.AddTrashes(6004,1000,90+1);
//                     GameHistoryDataGainTechAwards.AddTrashes(6005,1000,((int)67.5)+1);
            }
        }
    }

    [Desc("允许抽水站抽地下水，允许在水上建造建筑")]
    [HarmonyPatch]
    class BuildTool_ClickBuildTool_BlueprintPasteCheckBuildConditions : Entry {
        static IEnumerable<MethodBase> TargetMethods()
        {
            yield return typeof(BuildTool_Click).GetMethod("CheckBuildConditions", AccessTools.all);
            yield return typeof(BuildTool_BlueprintPaste).GetMethod("CheckBuildConditions", AccessTools.all);
        }
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions) {
            return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(i => CodeInstructionExtensions.LoadsConstant(i,EBuildCondition.NeedWater) || CodeInstructionExtensions.LoadsConstant(i,EBuildCondition.NeedGround)),
                    new CodeMatch(OpCodes.Stfld, AccessTools.Field(typeof(BuildPreview),"condition"))
                ).Repeat( matcher => matcher// Do the following for each match
                    .SetOperandAndAdvance(
                        (int)(EBuildCondition.Ok)
                    )
                ).InstructionEnumeration();
        }
    }

    [Desc("特定物品的消耗改为虚空创造",-1)] // -1默认为禁用
    [HarmonyPatch(typeof(StorageComponent), "TakeTailItems",new Type[]{typeof(int),typeof(int),typeof(int),typeof(bool)},new ArgumentType[]{ArgumentType.Ref,ArgumentType.Ref,ArgumentType.Out,ArgumentType.Normal})]
    class StorageComponentTakeTailItems : Entry {
        static PlanetFactory nearestFactory;
        static Mecha mecha;
        static Dictionary<int,int[,]> dict = new Dictionary<int,int[,]> {
            {1101,new int [,]{{1001,1}}},//铁
            {1102,new int [,]{{1001,1}}},//磁铁
            {1104,new int [,]{{1002,1}}},//铜
            {1202,new int [,]{{1102,1},{1104,1}}},//磁线圈，多耗费0.5铜
            {1301,new int [,]{{1101,1},{1104,1}}},//电路板，多耗费0.5铜
            {6001,new int [,]{{1202,1},{1301,1}}},//=磁线圈 电路板
            {1109,new int [,]{{1006,2}}},//高能石墨
            {6002,new int [,]{{1109,2},{1120,2}}},
            {1112,new int [,]{{1109,1}}},//金刚石
            {1106,new int [,]{{1004,2}}},//钛
            {1118,new int [,]{{1106,3},{1117,1}}},//钛晶石
            {6003,new int [,]{{1112,1},{1118,1}}},
            {1105,new int [,]{{1003,2}}},//硅
            {1302,new int [,]{{1105,2},{1104,1}}},//微晶元件
            {1303,new int [,]{{1302,2},{1301,2}}},//处理器
            {1124,new int [,]{{1015,1}}},//碳纳米管
            {1113,new int [,]{{1105,1}}},//晶格硅
            {1114,new int [,]{{1007,1}}},//精炼油，未计算副产物氢
            {1115,new int [,]{{1114,2},{1109,1}}},//塑料
            {1402,new int [,]{{1124,2},{1113,2},{1115,1}}},//粒子宽带
            {6004,new int [,]{{1303,2},{1402,1}}},
            {6005,new int [,]{{1126,1},{1304,1},{1305,1},{1305,1},{1209,1},{1127,1}}},//未完
            {6006,new int [,]{{6001,1},{6002,1},{6003,1},{6004,1},{6005,1},{1122,1},{1208,1}}},
        };
        public static void AddItems(int id,int count){
            if(dict.ContainsKey(id)){
                int[,] x=dict[id];
                int len=x.GetLength(0);
                for (int i = 0; i < len; i++){
                    StorageComponentTakeTailItems.AddItems(x[i,0],count*x[i,1]);
                    mecha.AddConsumptionStat(x[i,0],count*x[i,1],nearestFactory);
                }
            }else{
                StorageComponentTakeTailItems.AddStats(id,count);
            }
            mecha.AddProductionStat(id,count,nearestFactory);
        }
        public static void AddStats(int id,int count){
            mecha.AddProductionStat(id,count,nearestFactory);
            mecha.AddConsumptionStat(id,count,nearestFactory);
        }
        public static bool Prefix(ref int itemId, ref int count) {
            switch(itemId){
                case 6001 :
                case 6002 :
                case 6003 :
                case 6004 :
                case 6005 :
                case 6006 :
                    nearestFactory = GameMain.mainPlayer.nearestFactory;
                    mecha = GameMain.mainPlayer.mecha;
                    StorageComponentTakeTailItems.AddItems(itemId,count);
                    return false;
                default : /* 可选的 */
                return true;
            }
        }
    }

    [Desc("购买科技不消耗元数据",-1)]
    class BuyOut : Entry {
        [HarmonyPrefix, HarmonyPatch(typeof(GameHistoryData), "CheckPropertyAdequateForBuyout")]
        public static bool check_return(ref bool __result, GameHistoryData __instance, int techId){
            if (!__instance.techStates.ContainsKey(techId)) {
                return true;
            } else {
                __result=true;
                return false;
            }
        }
        [HarmonyPrefix, HarmonyPatch(typeof(PropertySystem), "AddItemConsumption")]
        public static bool no_return(){
            return false;
        }
    }
    class TurretComponent_mul : Entry {
        [Desc("炮塔攻击力乘数")]
        static float mul=-1f;
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var i in typeof(TurretComponent).GetMethods()){
                if(i.Name.StartsWith("Shoot_")){
                    logger($"patching {i}");
                    yield return i;
                }
            }
        }
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions, MethodBase __originalMethod) {
            return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(i => i.opcode==OpCodes.Ldfld && ((FieldInfo)(i.operand)).DeclaringType==typeof(CombatUpgradeData) && ((FieldInfo)(i.operand)).Name.EndsWith("DamageScale") && ((FieldInfo)(i.operand)).FieldType==typeof(float))
                ).Repeat( matcher => {
                    logger($"{System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name} matches at {__originalMethod}");
                    matcher// Do the following for each match
                        .Advance(1)
                        .InsertAndAdvance(
                            new CodeInstruction(OpCodes.Ldc_R4,mul),
                            new CodeInstruction(OpCodes.Mul)
                        );
                }).InstructionEnumeration();
        }
    }
    class TurretComponent_range : Entry {
        [Desc("炮塔射程乘数，防空或许有用，但对地时候炮塔射程太大并不好，因为地球是圆的，射程过大会打到地板")]
        public static float mul=-1f;
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var i in typeof(TurretComponent).GetMethods()){
                if(i.Name.StartsWith("Search_")||i.Name=="ActiveEnemyUnits_Ground"||i.Name=="CheckEnemyIsInAttackRange"){
                    logger($"patching {i}");
                    yield return i;
                }
            }
            yield return typeof(DefenseSystem).GetMethod("MatchTurretPair",(BindingFlags)(-1));
            yield return typeof(BattleBaseComponent).GetMethod("AutoPickTrash",(BindingFlags)(-1));
        }
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions, MethodBase __originalMethod) {
            return new CodeMatcher(instructions)
            .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                          new CodeMatch(OpCodes.Ldfld,typeof(PrefabDesc).GetField("turretMaxAttackRange"))
            ).Repeat( matcher => {
                logger($"{System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name} matches {__originalMethod}");
                matcher// Do the following for each match
                .Advance(1)
                .InsertAndAdvance(
                    new CodeInstruction(OpCodes.Ldc_R4,__originalMethod.DeclaringType==typeof(BattleBaseComponent)?mul+0.1f:mul),
                    new CodeInstruction(OpCodes.Mul)
                );
            }).InstructionEnumeration();
        }
    }
    class TurretComponent_speed : Entry {
        [Desc("炮塔子弹速度乘数")]
        static float mul=TurretComponent_range.mul;
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var i in typeof(TurretComponent).GetMethods()){
                if(i.Name.StartsWith("Shoot_")){
                    logger($"patching {i}");
                    yield return i;
                }
            }
        }
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions, MethodBase __originalMethod) {
            return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(i=>i.opcode==OpCodes.Stfld && ((FieldInfo)(i.operand)).Name=="speed" && ((FieldInfo)(i.operand)).FieldType==typeof(float))
                ).Repeat( matcher => {
                    logger($"{System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name} matches {__originalMethod}");
                    matcher// Do the following for each match
                        .InsertAndAdvance(
                            new CodeInstruction(OpCodes.Ldc_R4,mul),
                            new CodeInstruction(OpCodes.Mul)
                        )
                        .Advance(1);
                }).InstructionEnumeration();
        }
    }
    class CombatStat_TickSkillLogic : Entry {
        [Desc("建筑无敌")]
        static bool ot0=true;
        [Desc("植物无敌")]
        static bool ot1=true;
        [Desc("矿脉无敌")]
        static bool ot2=true;
        [Desc("type 3无敌")]
        static bool ot3=false;
        [Desc("黑雾无敌")]
        static bool ot4=false;
        [Desc("type 5无敌")]
        static bool ot5=false;
        [Desc("无人机无敌")]
        static bool ot6=true;
        [Desc("type default无敌")]
        static bool ot_default=false;
        // read from HandleZeroHp
        // this.objectType == 4 => ref EnemyData ptr = ref skillSystem.sector.enemyPool[this.objectId];
        // this.objectType == 6 => ref CraftData ptr2 = ref skillSystem.sector.craftPool[this.objectId];
        // this.objectType == 0 => ref EntityData ptr4 = ref planetFactory.entityPool[this.objectId];
        // this.objectType == 1 => ref VegeData ptr6 = ref planetFactory.vegePool[this.objectId];
        // this.objectType == 2 => ref VeinData ptr7 = ref planetFactory.veinPool[this.objectId];
        [HarmonyPatch(typeof(CombatStat),"TickSkillLogic")]
        static void Prefix(ref CombatStat __instance){
            switch(__instance.objectType){
                case 0:if(ot0){break;}return;
                case 1:if(ot1){break;}return;
                case 2:if(ot2){break;}return;
                case 3:if(ot3){break;}return;
                case 4:if(ot4){break;}return;
                case 5:if(ot5){break;}return;
                case 6:if(ot6){break;}return;
                default:if(ot_default){break;}return;
            }
            __instance.hp=__instance.hpMax;
        }
    }
    [Desc("伊卡洛斯无敌"),HarmonyPatch(typeof(Mecha),"TakeDamage")]
    class MechaTakeDamage : Entry{
        static void Prefix(ref int damage){
            if(damage>0){damage=0;}
        }
    }
    [HarmonyPatch(typeof(TrashSystem),"AddTrashFromGroundEnemy")]
    class DropSystem : Entry {
        [Desc("黑雾每次掉落物品个数")]
        static int cnt=1000;
        static void Prefix(ref int count)=>count=cnt;
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions, MethodBase __originalMethod) {
            var cntr=0;
            logger($"%%PLUGIN_ID%%-{System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name}正在transpiler注入类{__originalMethod.DeclaringType}的{__originalMethod.Name}方法");
            return new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(OpCodes.Ldfld , typeof(ItemProto).GetField("StackSize")) // match method 1
                ).Repeat( matcher => {
                    logger($"%%PLUGIN_ID%%-{System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name}成功匹配{__originalMethod.DeclaringType}中{__originalMethod.Name}方法的IL指令");
                    matcher// Do the following for each match
                        .SetAndAdvance(
                            OpCodes.Pop,null
                        ).InsertAndAdvance(
                            new CodeInstruction(OpCodes.Ldc_I4,cnt)
                        ).Advance(1);
                        logger($"%%PLUGIN_ID%%-{System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name}完成第{++cntr}处修改");
                }).InstructionEnumeration();
        }
    }

    [Desc("带筛选的爪子可虚空创造黑雾物品")] // -1默认为禁用
    class StorageComponentTakeTailItemsFiltered_Old : Entry {
        [HarmonyPrefix, HarmonyPatch(typeof(StorageComponent), "TakeTailItems",new Type[]{typeof(int),typeof(int),typeof(int),typeof(bool)},new ArgumentType[]{ArgumentType.Ref,ArgumentType.Ref,ArgumentType.Out,ArgumentType.Normal})]
        public static bool Prefix1(StorageComponent __instance, ref int itemId, ref int count, ref int inc) {
            if (itemId<12000 && ItemProto.enemyDropMaskTable[itemId]!=0){
                //GameMain.mainPlayer.mecha.AddProductionStat(itemId,count,GameMain.mainPlayer.nearestFactory);
                if (GameMain.data.factories.FirstOrDefault((x)=>x is not null) is var f and not null) {
                    // GameMain.mainPlayer.mecha.AddProductionStat(itemId,count,f);
                    int[] productRegister = GameMain.statistics.production.factoryStatPool[f.index].productRegister;
                    lock (productRegister){
                        productRegister[itemId] += count;
                    }
                    inc=4*count;
                    return false;
                }
            }
            return true;
        }
        [HarmonyPrefix, HarmonyPatch(typeof(PlanetFactory), "PickFrom")]
        public static bool Prefix2(PlanetFactory __instance, int itemId, int entityId, ref int count, ref byte inc, ref byte stack) {
            if (itemId<12000 && ItemProto.enemyDropMaskTable[itemId]!=0 && __instance.entityPool[entityId].storageId > 0){
                int[] productRegister = GameMain.statistics.production.factoryStatPool[__instance.index].productRegister;
                lock (productRegister){
                    productRegister[itemId] += count;
                }
                stack = (byte)Math.Min(count,4);
                inc=(byte)(4*count);
                return false;
            }
            return true;
        }
    }
    [Desc("带筛选的爪子可虚空创造黑雾物品-尝试失败",-1)] // 这两个函数从来没有被调用过，原因未知
    class StorageComponentTakeTailItemsFiltered : Entry {
        [HarmonyPrefix, HarmonyPatch(typeof(PlanetFactory), "PickFromStorageFiltered",new Type[]{typeof(int),typeof(int),typeof(int),typeof(int)},new ArgumentType[]{ArgumentType.Normal,ArgumentType.Ref, ArgumentType.Normal,ArgumentType.Out})]
        public static bool DoNotWork(PlanetFactory __instance, ref int __result, ref int filter, ref int count, ref int inc) {
            if (filter<12000 && ItemProto.enemyDropMaskTable[filter]!=0){
                var obj = GameMain.statistics.production.factoryStatPool[__instance.index];
                int[] productRegister = obj.productRegister;
                lock (productRegister){
                    productRegister[filter] += count;
                }
                inc=4*count;
                __result = 0;
                return false;
            }
            return true;
        }
        [HarmonyPrefix, HarmonyPatch(typeof(PlanetFactory), "PickFromStorage",new Type[]{typeof(int),typeof(int),typeof(int),typeof(int)},new ArgumentType[]{ArgumentType.Normal,ArgumentType.Normal, ArgumentType.Normal,ArgumentType.Out})]
        public static bool Work(PlanetFactory __instance, ref int __result, int itemId, int count, ref int inc) {
            if (itemId<12000 && ItemProto.enemyDropMaskTable[itemId]!=0){
                var obj = GameMain.statistics.production.factoryStatPool[__instance.index];
                int[] productRegister = obj.productRegister;
                lock (productRegister){
                    productRegister[itemId] += count;
                }
                inc=4*count;
                __result = 0;
                return false;
            }
            return true;
        }
    }
    [Desc("修复负数round之后自动减1的问题",-1),HarmonyPatch(typeof(BuildingParameters),"CopyFromFactoryObject")]
    class BuildingParameters_rounding : Entry{
        static void Postfix(BuildingParameters __instance, int objectId, PlanetFactory factory){
            if(__instance.type==BuildingType.Belt && __instance.parameters!=null && __instance.parameters.Length==2 && __instance.parameters[1]<0){
                __instance.parameters[1]-=1;
            }
        }
    }
    [HarmonyPatch(typeof(Mecha),"ammoDamage",Desc.get)]
    class MechaKillall :Entry {
        [Desc("机甲子弹伤害倍率")]
        static int mul=100;
        static int Postfix(int value)=>value*mul;
    }
    class Fine : Entry {
        [Desc("输出dsbpb可用的i18n文本")]
        static bool output=true;
        [HarmonyPatch(typeof(VFPreload),"PreloadThread")]
        public static System.Collections.IEnumerator Postfix(System.Collections.IEnumerator res) {
            while (res.MoveNext()){
                yield return res.Current;
            }
            if(output) {
                if(!System.IO.Directory.Exists("proto")){System.IO.Directory.CreateDirectory("proto");}
                foreach (var field in typeof(LDB).GetProperties()) {
                    if (field.GetValue(null) is ProtoTable dataArray and not null){
                            var text = new System.Text.StringBuilder();
                            for (int i = 0; i < dataArray.Length; i++) {
                                text.Append($"{dataArray[i].ID}  {dataArray[i].name.Translate()}\n");
                            }
                            System.IO.File.WriteAllText("proto/"+field.Name+".txt", text.ToString());
                            logger($"{field.Name+".txt"} exported.");
                    } else {
                        logger($"skip field {field}");
                    }
                }
                SetConfigValue(typeof(Fine).GetField("output",(BindingFlags)(-1)), false);
                GC.Collect();
            }
        }
    }
}
