# 这里是RiftBreaker的脚本

这个脚本的主要用途是批量修改RiftBreaker的参数

包含4个脚本

`compile-rift-replacer.sh`使用Rust自动编译一个可执行文件用作脚本替换

`extract.sh`将RifBreaker的压缩包文件解压，以生成最新的文件

`modifying.sh`进行批量修改

`packing.sh`对修改结果打包

打包时候会不加警告地删除并重新生成`99_win_data.zip`，多数情况下这不会造成问题，如果真的出了问题，可以自行修改脚本
