CHECKER=sha512sum
CHECKFILE=unchanged.check
rm -rf source/entities && mkdir -p source/entities/units && cp -r source/original/entities/buildings source/entities/ && cp -r source/original/entities/units/drones source/entities/units/ # choose
find source/entities -type f -exec $CHECKER {} \; > $CHECKFILE

./rift-replacer -d source/entities/buildings/main/ -t out -m { -w 1 -f headquarters replace '\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"biomass_animal"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"biomass_plant"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"carbonium"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"cobalt"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"ferdonite"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"hazenite"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"palladium"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"rhodonite"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"steel"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"tanzanite"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"titanium"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"uranium"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"uranium_ore"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n\t\t\tGameplayResource\n\t\t\t{\n\t\t\t\tresource \t\t\t"uranium_ore"\n\t\t\t\tvalue\t\t\t \t"1"\n\t\t\t}\n'
./rift-replacer -rd source/entities/buildings -t Storages -m max -w 2 scale 100
./rift-replacer -rd source/entities/buildings -t out -m value -w 2 scale 100
./rift-replacer -rd source/entities/buildings -t RegenerationComponent -m regeneration -w 1 scale 100
./rift-replacer -d source/entities/buildings/defense -t HealthDesc -m max_health -w 1 scale 100
./rift-replacer -d source/entities/buildings/defense -t HealthDesc -m health -w 1 scale 100
./rift-replacer -d source/entities/buildings/defense -t WeaponMuzzles -m recoil_time -w 2 scale 0.25
./rift-replacer -d source/entities/buildings/defense -t TurretDesc -m speed -w 1 scale 2
# ./rift-replacer -d source/entities/buildings/defense -t TurretDesc -m aiming_cooldown -w 1 scale 0.25
# ./rift-replacer -d source/entities/buildings/defense -t TurretDesc -m aiming_target_time -w 1 scale 0.25
./rift-replacer -d source/entities/buildings/defense -t TurretDesc -m range_max -w 2 scale 1.5
./rift-replacer -d source/entities/buildings/defense -t TurretDesc -m aiming_range -w 2 scale 1.5

./rift-replacer -d source/entities/units/drones -t LuaDesc -m heal_amount -w 2 scale 100
./rift-replacer -d source/entities/units/drones -t MovementDataComponent -m base -w 2 overwrite 120

rm `LC_ALL=C $CHECKER -c $CHECKFILE | grep ": OK" | cut -d: -f1` && find source/entities/ -type d -empty -exec rmdir {} \; 2>/dev/null && rm $CHECKFILE
rm source/entities/buildings/resources/liquid_pump* # liquid_pump gain no benefits.
