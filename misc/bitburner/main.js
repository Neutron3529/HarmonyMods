import { prepare_environment, repeats, update_karma, scanExes, refreshServer, maintance_server, hackTarget, log, switch_hacknet, maintance_hacknet, ram_operate_once_ratio_max, FILE_DIE } from 'Utils/prepare.js'
import { sleep_interval, log_interval, scan_server_interval, hacknet_interval } from 'Utils/prepare.js'

/** @param {NS} ns**/
export async function main(ns) {
  // const HashesPerLevel=ns.formulas.hacknetServers.constants().HashesPerLevel
  const IsHacknetServer = eval("ns.hacknet.hashCapacity()") > 0;
  // java.lang.Class.forName('java.lang.Object').getMethod('getClass').invoke(ns)
  // ns.tprint(ns.formulas.hacknetServers.hashGainRate)
  ns.disableLog('ALL');
  prepare_environment(ns)
  const hacknet2 = eval('ns.hacknet')
  ns.tail()

  var threads = 0
  var scan_cumulator = repeats(scan_server_interval, sleep_interval)
  var log_cumulator = repeats(log_interval, sleep_interval)
  var hacknet_cumulator = repeats(hacknet_interval, sleep_interval)
  var limit = [4, 2, 1]
  var cnt = [4, 2, 1]
  try {
    if (ns.args.length >= 6) {
      cnt = ns.args.slice(1, 4).map((x) => parseInt(x))
      limit = ns.args.slice(4, 7).map((x) => parseInt(x))
      // ns.tprint(`parsing "${limit}" "${cnt}"`)
    }
    ns.tprint(`GWH ratio is ${limit}`)
  } catch (err) {
    ns.tprint("ERROR: Skipped calculating GWH ratio due to " + String(err));
  }
  // ns.tprint("init loop begin.")
  // for (var i=0;i<200;i++) {//Keeps everything running once per second 
  // 	scan_cumulator() &&
  // 		scanExes(ns),
  // 		effect = Math.min(5, refreshServer(ns)),
  // 		maintance_server(ns)
  // 	hackTarget(ns, effect, ratio_init)
  // 	if(log_cumulator()){
  // 		log(ns)
  // 		update_karma(ns)
  // 	} 
  // 	(hacknet_cumulator() || hacknet_cont_buying) && switch_hacknet(ns) && maintance_hacknet(ns, hacknet2)
  // 	await ns.asleep(sleep_interval)
  // }
  // ns.tprint("real loop begin.")
  var hacknet_cont_buying = false
  var ratio = 1
  while (checkStop(ns, FILE_DIE)) {//Keeps everything running once per second 
    scan_cumulator() && (
      scanExes(ns)
      , threads = refreshServer(ns)
      , maintance_server(ns)
    )
    // ns.tprint(`hackTarget "${effect}" "${limit}" "${cnt}"`)
    if (threads * ram_operate_once_ratio_max > limit.reduce((x, y) => x + y)) {
      ratio = Math.ceil(threads * ram_operate_once_ratio_max)
    }
    hackTarget(ns, threads, limit, cnt.map((x) => x * ratio)),
      log_cumulator() && (
        log(ns)
        , update_karma(ns)
      );
    hacknet_cumulator(hacknet_cont_buying)
      && switch_hacknet(ns)
      && (hacknet_cont_buying = maintance_hacknet(ns, hacknet2))
    await ns.asleep(sleep_interval)
  }
}
export function autocomplete(data, args) {
  return [...data.servers];
}