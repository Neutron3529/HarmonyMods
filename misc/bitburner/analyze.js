import { GWH_mem_usage, sleep_interval, ram_operate_once_ratio_max} from 'Utils/prepare.js'
export const Extra_G_Mult=1.1
export const Extra_W_Mult=1.1
export const RelaxedTimeCoef=1
export const HackRelaxedTimeCoef=1
export const HACK_MAX_PERCENTAGE = 0.1 // restrict hack programs to obtain up to HACK_MAX_PERCENTAGE money from a server at a single hack.js executed.
export const Default_hack_effect = 0.95
/** @param {NS} ns */
export async function main(ns) {
	ns.scriptKill("main.js","home")
	var grow_time,weak_time,hack_time
	var grow_sec=0.1
	var weak_sec=-0.05
	var hack_sec=0.002
	var hack_effect=ns.hackAnalyze(ns.args[0]);
	try{
		let server=ns.formulas.mockServer()
		server.hackDifficulty=server.minDifficulty=ns.getServerMinSecurityLevel(ns.args[0])
		hack_effect=ns.formulas.hacking.hackPercent(server,ns.getPlayer())
		grow_time=ns.formulas.hacking.growTime(server,ns.getPlayer())
		weak_time=ns.formulas.hacking.weakenTime(server,ns.getPlayer())
		hack_time=ns.formulas.hacking.hackTime(server,ns.getPlayer())
		server.hackDifficulty=ns.getServerSecurityLevel(ns.args[0])
		grow_time*=ns.getGrowTime(ns.args[0])/ns.formulas.hacking.growTime(server,ns.getPlayer())
		weak_time*=ns.getWeakenTime(ns.args[0])/ns.formulas.hacking.weakenTime(server,ns.getPlayer())
		hack_time*=ns.getHackTime(ns.args[0])/ns.formulas.hacking.hackTime(server,ns.getPlayer())
	} catch {
		grow_time=ns.getGrowTime(ns.args[0])
		weak_time=ns.getWeakenTime(ns.args[0])
		hack_time=ns.getHackTime(ns.args[0])
	}
	if(hack_effect==0){hack_effect=Default_hack_effect}
	var gcnt=Math.ceil(sleep_interval/ram_operate_once_ratio_max/grow_time*RelaxedTimeCoef)
	var wcnt=Math.ceil(sleep_interval/ram_operate_once_ratio_max/weak_time*RelaxedTimeCoef)
	var hcnt=Math.ceil(sleep_interval/ram_operate_once_ratio_max/hack_time*HackRelaxedTimeCoef)
	var grow_threads=1
	grow_threads=Math.max(1e-3,Extra_G_Mult*ns.growthAnalyze(ns.args[0],1/(1-hack_effect)))
	ns.tprint(`hack_effect:${hack_effect}\ngrow_threads:${grow_threads}\nhack_time:${ns.tFormat(hack_time)}(${ns.tFormat(hack_time*hcnt)})\ngrow_time:${ns.tFormat(grow_time)}(${ns.tFormat(grow_time*gcnt)})\nweak_time:${ns.tFormat(weak_time)}(${ns.tFormat(weak_time*wcnt)})\nhack_sec:${hack_sec}\ngrow_sec:${grow_sec}\nweak_sec${weak_sec}`)
	hack_time*=hcnt
	grow_time*=gcnt
	weak_time*=wcnt
	var weak_threads=Extra_W_Mult/wcnt*(hcnt/hack_time*hack_sec+grow_threads/grow_time*grow_sec)/-weak_sec*weak_time
	ns.tprint(`need ${grow_threads}x${gcnt} grow with 1x${hcnt} hack, along with ${weak_threads}x${wcnt} weaken`)
	ns.tprint(`${Math.log(1-HACK_MAX_PERCENTAGE)} ${Math.log(1-hack_effect)}`)
	var h=Math.ceil(Math.log(1-HACK_MAX_PERCENTAGE)/Math.log(1-hack_effect))
	var g=Math.ceil(h*hcnt*grow_threads/gcnt)
	var w=Math.ceil(-(g*gcnt*grow_sec+h*hcnt*hack_sec)/(weak_sec*wcnt))
	
	ns.tprint(`limited to ${g}x${gcnt} grow with ${h}x${hcnt} hack, along with ${w}x${wcnt} weaken`)

	ns.tprint(`./main.js ${ns.args[0]} ${gcnt} ${wcnt} ${hcnt} ${g} ${w} ${h}`)
	ns.spawn("main.js",{spawnDelay:100},ns.args[0],gcnt,wcnt,hcnt,g,w,h)
}
export function autocomplete(data, args) {
	return [...data.servers];
}