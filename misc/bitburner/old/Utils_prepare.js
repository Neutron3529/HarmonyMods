export const sleep_interval = 25
export const log_interval = 1000
export const scan_server_interval = 25000
export const hacknet_interval = 1000
export const ram_operate_once_ratio_max = 1 / 2000
const min_server_ram = 0;
const home_ram_reserve = 4;
const ram_operate_once_max = 64;
const hacknet_ratio_second = 3600
const calc_mathod = 'HIGH PROFIT'// should be one of ['HIGH PROFIT',' HIGH  EXP '] 
const cnt = "4" // extra ratio for G and W
const ServerPrefix = 'small' // do not use "server" since hacknet-server is also a server.
//const calc_mathod = ' HIGH  EXP '// remember to disable the line above when active it.
//const calc_mathod = 'N00DLE ONLY'// not test yet
// configs above could be modified before scripts run.
// Note: changing configs above have no effect for running scripts.
// things below is constants, which should NOT be changed unless the constant are incorrect.
export const GWH_mem_usage = [1.75, 1.75, 1.7]
const G_RATIO = [0.33, 0, 0.33, 0.3]
const W_RATIO = [0.67, 1, 0.66, 0.7]
const script_memory_usage = 5.45
const weaken_reduce = 0.05
const PORT = 1
export const files = ['/scripts/grow.js', '/scripts/weak.js', '/scripts/hack.js']
const exec = ['grow', 'weaken', 'hack']
// update_karma
const doc = eval("document");
const hook0 = doc.getElementById('overview-extra-hook-0');
const hook1 = doc.getElementById('overview-extra-hook-1');
// log
const cycle = Array.from({ length: 256 }, (v, k) => k.toString(16).padStart(2, "0").toUpperCase())

// variables

// global
var GWH_balance = [-1, -1, 1]
var exe = []
export var hosts = []
export var targets = []
var act = {}
var pre = "" // weaken and grow effects
var post = ""// hacknet related

// log
var x = 0
// hack target related
var hack_max_ram_once = 10

// hacknet
var hacknet_cost = 1;
var hacknet_purchased = true;
var hacknet_gain_back_seconds = 0;
var hacknet_itemType = 0
var hacknet_nodeNum = 0
var hacknet_reserve_ratio = 1
var hacknet_cache_reserve_ratio = 1
var IsHacknetServer = false
var hacknet_num_nodes = 0
// server
var purchasedServerMaxRam = 1048576
var server_upgrade_cost = 0
var server_need_claim = true
/** @param {NS} ns @param {number} money @param {number} ratio**/
export function checkM(ns, money, ratio = 1) {
	return money * ratio < ns.getServerMoneyAvailable("home")
}
export function repeats(interval, tick) {
	let x = Math.ceil(interval / tick) - 1
	var cntr = 0
	return (b = false) => {
		if (cntr > 0) {
			cntr--
			return b
		} else {
			cntr = x
			return true
		}
	}
}
/** @param {NS} ns**/
export async function prepare_environment(ns, files, exec) {
	var GWH_balance = [-1, -1, 1]
	exe = []
	hosts = []
	targets = []
	act = {}
	pre = "" // weaken and grow effects
	post = ""// hacknet related

	// log
	x = 0

	// hacknet
	hacknet_cost = 1;
	hacknet_purchased = true;
	hacknet_gain_back_seconds = 0;
	hacknet_itemType = 0
	hacknet_nodeNum = 0
	hacknet_reserve_ratio = 1
	hacknet_cache_reserve_ratio = 1
	IsHacknetServer = eval("ns.hacknet.hashCapacity()") > 0;
	hacknet_num_nodes = -1
	// server
	// purchasedServerMaxRam = 1048576
	server_upgrade_cost = 0
	server_need_claim = true
	for (var ram = 1048576; ram > 0; ram >>= 1) {
		if (ns.getPurchasedServerCost(ram) > 1e300 || ns.getPurchasedServerCost(ram) <= 0) {
			continue
		}
		purchasedServerMaxRam = ram
	}
	for (let j in files) {
		if (!ns.fileExists(files[j])) ns.write(files[j], `/** @param {NS} ns**/\nexport async function main(ns){for(var i=ns.args.length<2?1:parseInt(ns.args[1]);i>0;i--)await sub(ns)}\n/** @param {NS} ns**/\nasync function sub(ns) {await ns.${exec[j]}(ns.args[0])}\nexport function autocomplete(data, args) {return [...data.servers];}`, 'w')
	}
}
/** @param {any[]} x **/
export function arraySort(x) { x.sort((a, b) => b[0] - a[0]) }

/** @param {NS} ns @param {string} server**/
export function targets_priority(ns, server) {
	if (calc_mathod == 'HIGH PROFIT') { return server == ns.args[0] ? 1e300 : ns.getServerMaxMoney(server) / ns.getServerSecurityLevel(server) }
	else if (calc_mathod == ' HIGH  EXP ') { return server == ns.args[0] ? 1e300 : Math.pow(ns.getServerRequiredHackingLevel(server)) / ns.getServerSecurityLevel(server) }
	else return server == 'n00dles'
}

/** @param {NS} ns **/
export function scanExes(ns) {
	exe = []
	let all_exe = [ns.brutessh, ns.ftpcrack, ns.relaysmtp, ns.httpworm, ns.sqlinject]
	let all_name = ['brutessh.exe', 'ftpcrack.exe', 'relaysmtp.exe', 'httpworm.exe', 'sqlinject.exe']
	for (let hack in all_name) {
		if (ns.fileExists(all_name[hack])) { exe.push(all_exe[hack]) }
	}
}

function str(s) { if (s.length > 17) { return s.substring(0, 14) + '...' } else { return s } }
/** @param {NS} ns **/
export function log(ns, host_len = 11) {
	if (x < 255) {
		x++
	} else {
		x = 0
	}
	ns.clearLog();
	ns.print(pre)
	ns.print('═══╦══════════════════════════════════════════════')
	ns.print(` ${cycle[x]}║ ` + calc_mathod + '                    BALANCE     ')
	for (let t of targets.slice(0, host_len)) {
		if (!act[t[1]]) act[t[1]] = '-'
		let money = ns.getServerMoneyAvailable(t[1])
		let money_max = ns.getServerMaxMoney(t[1])
		ns.print(` ${act[t[1]]} ║ ${str(t[1])}` + (`${ns.formatNumber(money)} / ${ns.formatNumber(money_max)} : ${ns.formatPercent(money / money_max)}`).padStart(44 - str(t[1]).length))
	}
	ns.print('═══╩══════════════════════════════════════════════')
	ns.print(` EXE ${exe.length}/5 ║ HOSTS ${hosts.length} ║ TARGETS ${targets.length}`)
	ns.print('══════════════════════════════════════════════════')
	ns.print(post)
}

/** @param {NS} ns @param {string} host @param {string} current **/
export function scanServers(ns, host, current) {//Combined scan and check
	for (let server of ns.scan(current)) {
		if ((ns.getServerNumPortsRequired(server) <= exe.length || ns.getServerRequiredHackingLevel(server) == 1) && host != server && !server.startsWith("hacknet")) {
			if (ns.getServerUsedRam(server) == 0 && ns.getServerNumPortsRequired(server) <= exe.length) { for (let hack of exe) { hack(server) }; ns.nuke(server) }
			if (ns.getServerMaxMoney(server) != 0 && ns.getServerRequiredHackingLevel(server) <= ns.getHackingLevel() && ns.getServerMinSecurityLevel(server) < 100) {
				targets.push([targets_priority(ns, server), server])
			}
			if (ns.getServerMaxRam(server) > min_server_ram) {
				hosts.push([ns.getServerMaxRam(server), server, Math.floor(ns.getServerMaxRam(server) / GWH_mem_usage[1])])
			}
			ns.scp(files, server, 'home')
			scanServers(ns, current, server)
		}
	}
}
/** @param {NS} ns **/
export function refreshServer(ns) {
	act = {}
	targets = []
	hosts = [[ns.getServerMaxRam('home'), 'home', Math.floor((ns.getServerMaxRam('home') - script_memory_usage - home_ram_reserve) / GWH_mem_usage[1])]]

	scanServers(ns, "home", "home")

	arraySort(targets)
	arraySort(hosts)
	var sum = 0
	for (let host of hosts) { sum += host[2] }
	var effect = sum * weaken_reduce;
	pre = `Threads for weaken or grow: ${sum}\nweaken effect             : ${effect.toFixed(4)}`
	return effect;
}


/** @param {NS} ns **/
export function update_karma(ns) {
	try {
		const headers = []
		const values = [];
		headers.push("Karma");
		values.push(ns.heart.break());
		headers.push("ScrExp");
		values.push(ns.formatNumber(ns.getScriptExpGain()) + '/sec');
		hook0.innerText = headers.join(" \n");
		hook1.innerText = values.join("\n");
	} catch (err) {
		ns.tprint("ERROR: Update Skipped: " + String(err));
	}
}

/** @param {NS} ns **/
export function myGetPurchasedServers(ns) {
	return ns.scan("home").filter((x) => x.startsWith(ServerPrefix))
}
// Newest analyze & hack version - Utils/prepare.js, part 2 编辑

/** @param {NS} ns **/
export function maintance_server(ns) {
	let old_server = myGetPurchasedServers(ns)
	var server_count = old_server.length
	var old = "Server status:\n"
	while (server_count < ns.getPurchasedServerLimit() && checkM(ns, ns.getPurchasedServerCost(4096))) {
		let target = Math.min(purchasedServerMaxRam, Math.pow(2, Math.floor(Math.log2(ns.getServerMoneyAvailable("home") / ns.getPurchasedServerCost(4096))) + 12))
		let x = ns.purchaseServer(ServerPrefix, target)
		if ("" == x) {
			old += `  failed to buy server with ${target} GiB memory.\n`
			break
		} else {
			old += `  buying server ${x} with ${target} GiB memory.\n`
			server_count++
		}
		server_need_claim = true
	}
	server_upgrade_cost = Infinity
	for (let x of old_server) {
		let ram = ns.getServerMaxRam(x) * 2;
		let cost = ns.getPurchasedServerUpgradeCost(x, ram);
		server_upgrade_cost = Math.min(server_upgrade_cost, cost)
		if (cost > 0 && checkM(ns, cost)) {
			if (ns.upgradePurchasedServer(x, ram)) {
				old += `  upgrading server ${x} to ${ram} GiB memory.\n`
				server_need_claim = true
			}
		}
	}
	if (server_need_claim) {
		ns.tprint(old
			+ (server_count < ns.getPurchasedServerLimit() ? `  new server needs ${ns.formatNumber(ns.getPurchasedServerCost(4096))} to buy` : "  maximum server count reaches")
			+ `\n  upgrade old server needs ${ns.formatNumber(server_upgrade_cost)}`)
		server_need_claim = false
	}
}

export function fRam(ns, host) {
	return Math.min(Math.max(ns.getServerMaxRam(host) - ns.getServerUsedRam(host) - (host == 'home' ? home_ram_reserve : 0), 0), Math.max(ram_operate_once_max, ram_operate_once_ratio_max * ns.getServerMaxRam(host)))
}

/**@param{NS}ns@param{number}weaken_effect@param{number[]}ratio@param{number[]}limit**/
export function hackTarget(ns, weaken_effect, ratio = [], limit = []) {
	// post+=`\n${GWH_balance}`
	if (limit.length >= 3) {
		// ns.print(`${limit} ${GWH_mem_usage}`)
		hack_max_ram_once = limit[0] * GWH_mem_usage[0] + limit[1] * GWH_mem_usage[1] + limit[2] * GWH_mem_usage[2]
	} else {
		hack_max_ram_once = 1e300
	}
	hackAll(ns, targets[0][1], weaken_effect, ratio, limit)
}

function negfloor(x) { return Math.min(Math.floor(-x)) }
/**@param{NS}ns@param{string}target@param{number}weaken_effect@param{number[]}ratio@param{number[]}limit**/
export function hackAll(ns, target, weaken_effect, ratio = [], limit = []) {
	var hType = 0
	if (ns.getServerSecurityLevel(target) > ns.getServerMinSecurityLevel(target) + weaken_effect) {
		if (ns.getServerMoneyAvailable(target) > ns.getServerMaxMoney(target) * .50) {
			hType = 3
		} else {
			hType = 1
		}
	} else if (ns.getServerMoneyAvailable(target) < ns.getServerMaxMoney(target) * .80) {
		hType = 0
	} else {
		hType = 2
	}
	for (let host of hosts) {
		var [g, w, h] = GWH_balance
		var free = fRam(ns, host[1]);
		if (free >= hack_max_ram_once) {
			// ns.print(`hType=${hType} free=${ns.formatRam(free)}>${ns.formatRam(hack_max_ram_once)}`)
			hType != 1 && ns.exec(files[0], host[1], limit[0], target, ns.args[1], "auto-batching")
			ns.exec(files[1], host[1], limit[1], target, ns.args[2], "auto-batching")
			hType == 2 && ns.exec(files[2], host[1], limit[2], target, ns.args[3], "auto-batching")
			continue
		}
		if (ratio.length == 3) {
			let gwh = [Math.floor(Math.min(free / GWH_mem_usage[0], -g)), Math.floor(Math.min(free / GWH_mem_usage[1], -w)), hType == 2 ? Math.floor(Math.min(free / GWH_mem_usage[2], -h)) : 0]
			let idx = gwh.indexOf(Math.max(...gwh));
			// ns.tprint(`deal ${GWH_balance} gwh=${gwh} idx=${idx}`)
			if (gwh[idx] > 0 && ns.exec(files[idx], host[1], gwh[idx], target, ns.args[idx + 1] || "1", `${hType}:${gwh[idx]}`) > 0) { GWH_balance[idx] += gwh[idx]; free -= GWH_mem_usage[idx] * gwh[idx] }
		}

		if (ratio.length == 3) {
			g = Math.floor(free / GWH_mem_usage[0] * (hType == 3 ? ratio[0] / 4 * 3 : ratio[0]));
			w = Math.floor(free / GWH_mem_usage[1] * (hType == 3 ? ratio[0] / 4 + ratio[1] + ratio[2] / 3 : ratio[1]));
			h = Math.floor(free / GWH_mem_usage[2] * (hType == 3 ? ratio[2] / 3 * 2 : ratio[2]));
		} else {
			g = Math.floor(free / GWH_mem_usage[0] * G_RATIO[hType]);
			w = Math.floor(free / GWH_mem_usage[1] * W_RATIO[hType]);
			h = (free - g * GWH_mem_usage[0] - w * GWH_mem_usage[1]) / GWH_mem_usage[2]
		}
		if (hType == 0) {
			GWH_balance[2] += h
			h = 0
		} else if (hType == 1) {
			h = g = 0
			w = Math.floor(free / GWH_mem_usage[1]);
		}
		if (g > 0 && GWH_balance[0] < 0 && ns.exec(files[0], host[1], g, target, hType == 0 ? "1" : (ns.args[1] || cnt), `${hType}:${g}`) > 0) { GWH_balance[0] += g }
		if (w > 0 && GWH_balance[1] < 0 && ns.exec(files[1], host[1], w, target, hType == 1 ? "1" : (ns.args[2] || cnt), `${hType}:${w}`) > 0 && hType != 1) { GWH_balance[1] += w }
		if (h > 0 && GWH_balance[2] < 0 && ns.exec(files[2], host[1], h, target, ns.args[3] || "1", `${hType}:${h}`) > 0) { GWH_balance[2] += h }
		if (ratio.length == 3 && g + w + h > 0) {
			let maxs = [GWH_balance[0] * ratio[0], GWH_balance[1] * ratio[1], GWH_balance[2] * ratio[2]]
			// ns.tprint(`balancing ${GWH_balance} maxs=${maxs}`)
			let mval = Math.max(...maxs)
			let idx = maxs.indexOf(mval)
			let mres = GWH_balance[idx] / ratio[idx]
			GWH_balance[0] -= mres * ratio[0]
			GWH_balance[1] -= mres * ratio[1]
			GWH_balance[2] -= mres * ratio[2]
			if (Math.min(...GWH_balance) >= 0) {
				GWH_balance = [-1, -1, 1]
			}
			// ns.tprint(`balancing ${GWH_balance} mval=${mval} mres=${mres} idx=${idx}`)
		}
		// tarIndex++// */
	}
	if (hType == 0) {
		act[target] = 'G'
	} else if (hType == 1) {
		act[target] = 'W'
	} else if (hType == 2) {
		act[target] = 'H'
	} else act[target] = 'U' // 
}

/** @param {NS} ns **/
export async function switch_hacknet(ns) {
	if (!(ns.fileExists('.hacknet_off.js') || ns.fileExists('.hacknet_on.js'))) {
		if (await ns.prompt("enable hacknet buyer?")) {
			ns.write('.hacknet_on.js', '')
		} else {
			ns.write('.hacknet_off.js', '')
		}
	}
	return ns.fileExists('.hacknet_on.js');
}

export function getProd(level, ram, cores) {
	return IsHacknetServer
		? level * Math.pow(1.07, Math.log2(ram)) * (1 + (cores - 1) / 5)
		: (level * 1.5) * Math.pow(1.035, ram - 1) * ((cores + 5) / 6)
}
/** @param {Hacknet} hacknet2 @param {number} idx @param {number} calibrate**/
export function idxProd(hacknet2, idx, hacknet_costs) {
	// for(var i=0;i<5;i++){
	//   var stat = hacknet2.getNodeStats(i)
	//   var calibrate = stat.production / getProd(stat.level, stat.ram, stat.cores)
	//   ns.tprint(`calibrate constants: ${i} : ${calibrate} ${stat.production} ${getProd(stat.level, stat.ram, stat.cores)}`)
	// }
	let stat = hacknet2.getNodeStats(idx)
	let calibrate = stat.production / getProd(stat.level, stat.ram, stat.cores) * (IsHacknetServer ? 250000 : 1)
	var base = getProd(stat.level, stat.ram, stat.cores)
	var p3 = (getProd(stat.level + 1, stat.ram, stat.cores) - base) / hacknet_costs[3](idx, 1);
	var p2 = (getProd(stat.level, stat.ram * 2, stat.cores) - base) / hacknet_costs[2](idx, 1);
	var p1 = (getProd(stat.level, stat.ram, stat.cores + 1) - base) / hacknet_costs[1](idx, 1);
	// ns.tprint(`${hacknet2.getNodeStats(hacknet2.numNodes() - 1).production / hacknet2.getPurchaseNodeCost()} ${p1} ${p2} ${p3} ${stat.level} ${stat.ram} ${stat.cores}`)
	if (p1 > p2 && p1 > p3) { return [1, p1 * calibrate] }
	return [p2 > p3 ? 2 : 3, Math.max(p2, p3) * calibrate]
}
// Newest analyze & hack version - Utils/prepare.js, part 3 编辑

/** @param {NS} ns  @param {Hacknet} hacknet2 **/
export function maintance_hacknet(ns, hacknet2) {
	// const getProd = (level, ram, cores) => (level * 1.5) * Math.pow(1.035, ram - 1) * ((cores + 5) / 6);
	const hacknet__buynode = (a, b) => hacknet2.purchaseNode();
	const hacknet__buynodecost = (a, b) => hacknet2.purchaseNode();
	const hacknet_buyings = [hacknet__buynode, hacknet2.upgradeCore, hacknet2.upgradeRam, hacknet2.upgradeLevel]
	const hacknet_costs = [hacknet__buynodecost, hacknet2.getCoreUpgradeCost, hacknet2.getRamUpgradeCost, hacknet2.getLevelUpgradeCost]
	const hacknet_types = ["New Node", "CPU", "RAM", "Level"]
	var num_nodes = hacknet2.numNodes()
	if (hacknet_purchased || hacknet_num_nodes < num_nodes) {
		hacknet_num_nodes = num_nodes
		hacknet_nodeNum = -1
		hacknet_itemType = 0
		hacknet_cost = hacknet2.getPurchaseNodeCost()
		if (hacknet_cost <= 0) {
			hacknet_cost = 1e300
		}
		var profit_ratio = 1e300;
		if (hacknet_num_nodes > 0) {
			profit_ratio = hacknet2.getNodeStats(hacknet_num_nodes - 1).production / hacknet_cost
		}
		// Iterate through all nodes and select lowest purchase/upgrade available
		// var max_profit = [0, 0, 0]
		for (var i = 0; i < hacknet_num_nodes; i++) {
			var node_stats = idxProd(hacknet2, i, hacknet_costs);
			if (node_stats[1] > profit_ratio) {
				hacknet_nodeNum = i
				hacknet_itemType = node_stats[0]
				profit_ratio = node_stats[1]
			}
			// for (var j = 0; j < 3; j++) {
			//   max_profit[j] = Math.max(node_stats[j + 2], max_profit[j])
			// }
		}
		hacknet_purchased = false
		if (hacknet_nodeNum >= 0) {
			hacknet_cost = hacknet_costs[hacknet_itemType](hacknet_nodeNum, 1)
		}
		hacknet_gain_back_seconds = 1 / profit_ratio
		hacknet_reserve_ratio = Math.max(1, hacknet_gain_back_seconds / hacknet_ratio_second)
		hacknet_cache_reserve_ratio = Math.max(1, hacknet_reserve_ratio * 0.9)
		ns.tprint(`profit is ${profit_ratio * hacknet_cost}, in ${hacknet_gain_back_seconds}s`)
	}
	if (IsHacknetServer) {
		for (var i = 0; i < hacknet_num_nodes; i++) {
			if (checkM(ns, hacknet2.getCacheUpgradeCost(i), hacknet_cache_reserve_ratio)) {
				hacknet2.upgradeCache(i)
				post = `buying cache for Node ${i}`
				return true;
			}
		}
	}
	var money = ns.getServerMoneyAvailable("home");
	var money_enough = money < hacknet_cost
	/*if (money < hacknet_cost * hacknet_reserve_ratio && money >= hacknet_cost) {
		post = ` reserve ${ns.formatPercent(hacknet_reserve_ratio)} since gain_back in ${Math.ceil(hacknet_gain_back_seconds)}s`
	} else */ if (checkM(ns, hacknet_cost, hacknet_reserve_ratio)) {
		post = `         Buying ${hacknet_types[hacknet_itemType]} ${hacknet_nodeNum >= 0 ? "for node " + hacknet_nodeNum : " "}`
		hacknet_buyings[hacknet_itemType](hacknet_nodeNum,
			checkM(ns, hacknet_costs[hacknet_itemType](hacknet_nodeNum, 10), hacknet_reserve_ratio)
				? 10
				: checkM(ns, hacknet_costs[hacknet_itemType](hacknet_nodeNum, 5), hacknet_reserve_ratio)
					? 5
					: 1
		)
		hacknet_purchased = true;
		return true
	} else {
		post = ` ${money_enough ? "Reserve" : "Waiting"} to buy ${hacknet_types[hacknet_itemType]} ${hacknet_nodeNum >= 0 ? "for node " + hacknet_nodeNum : " "
			} ${money_enough
				? ns.formatPercent(money / hacknet_cost)
				: ns.formatPercent(money / (hacknet_cost * hacknet_reserve_ratio)) + '(x' + ns.formatNumber(hacknet_reserve_ratio) + ')'
			}`
		return false
	}
}