import {files, refreshServer, scanExes, hosts} from 'Utils/prepare.js'
/** @param {NS} ns */
export async function main(ns) {
	ns.tail()
	scanExes(ns)
	refreshServer(ns)
	ns.clearLog()
	ns.print(`${hosts.map((x)=>x[1])}`)
	for(let host of hosts.map((x)=>x[1])){
		for(let file of files){
			// ns.print(`kill ${file} at ${host}`)
			ns.scriptKill(file,host)
		}
	}
}