const MIN_SUCCESS = 1
const general = "General"
const contract = "contract"
const operation = "Operation"
const blackop = "blackops"
const contracts = ["Tracking", "Bounty Hunter", "Retirement"].reverse()
const operations = ["Investigation", "Undercover Operation", "Sting Operation", "Raid", "Stealth Retirement Operation", "Assassination"].reverse()
const blackops = ["Operation Typhoon", "Operation Zero", "Operation X", "Operation Titan", "Operation Ares", "Operation Archangel", "Operation Juggernaut", "Operation Red Dragon", "Operation K", "Operation Deckard", "Operation Tyrell", "Operation Wallace", "Operation Shoulder of Orion", "Operation Hyron", "Operation Morpheus", "Operation Ion Storm", "Operation Annihilus", "Operation Ultron", "Operation Centurion", "Operation Vindictus"/*, "Operation Daedalus"*/]
// const generals=["Training","Field Analysis","Recruitment","Diplomacy","Hyperbolic Regeneration Chamber","Incite Violence"]
const generals = ["Diplomacy", "Incite Violence", "Training", "Field Analysis", "Recruitment", "Hyperbolic Regeneration Chamber"]
const skills = ["Blade's Intuition", "Cloak", "Short-Circuit", "Digital Observer", "Tracer", "Overclock", "Reaper", "Evasive System", "Datamancer", "Cyber's Edge", "Hands of Midas", "Hyperdrive"]

/** @param {Bladeburner} bladeburner @param {String} t @param {String} n*/
export function action(bladeburner, t, n, min_success = MIN_SUCCESS) {
	// let bladeburner=ns.bladeburner
	return (t == blackop
		? bladeburner.getBlackOpRank(n) < bladeburner.getRank()
		: bladeburner.getActionAutolevel(t, n)
	) ? (
		(t == general || bladeburner.getActionCountRemaining(t, n) > 0)
			? (
				(bladeburner.getActionEstimatedSuccessChance(t, n)[0] >= min_success)
					? (
						bladeburner.startAction(t, n)
							? bladeburner.getActionTime(t, n)
							: 0  // startAction failed
					)
					: n == "Raid"
						? -2
						: -1   // min EstimatedSuccessChance < min_success
			)
			: -2         // getActionCountRemaining == 0
	)
		: -3           // getActionAutolevel not set or black ops rank not enough
}

/** @param {NS} ns */
export async function main(ns) {
	ns.tail()
	ns.clearLog()
	var cont = 1
	var has_ops = false
	let bladeburner = ns.bladeburner
	/*
	var contracts=ns.bladeburner.getContractNames()
	var operations=ns.bladeburner.getOperationNames()
	var blackops=ns.bladeburner.getBlackOpNames()
	var generals=ns.bladeburner.getGeneralActionNames()
	var skills=ns.bladeburner.getSkillNames()
	ns.tprint(`\nconst contracts=[${contracts.map((x)=>'"'+x+'"')}]\nconst operations=[${operations.map((x)=>'"'+x+'"')}]\nconst blackops=[${blackops.map((x)=>'"'+x+'"')}]\nconst generals=[${generals.map((x)=>'"'+x+'"')}]\nconst skills=[${skills.map((x)=>'"'+x+'"')}]`)
	ns.exit()
	//*/
	while (cont > 0) {
		if (bladeburner.getCurrentAction().type != "Idle" && bladeburner.getCurrentAction().type != general) {
			var act = bladeburner.getCurrentAction()
			if (bladeburner.getActionEstimatedSuccessChance(act.type, act.name)[0] >= MIN_SUCCESS) {
				ns.print(`executing ${act.name} (${act.type})`)
				bladeburner.upgradeSkill(skills[5])
				await ns.asleep(bladeburner.getActionTime(act.type, act.name) - bladeburner.getActionCurrentTime())
			} else {
				bladeburner.stopBladeburnerAction()
			}
		} else {
			has_ops = false
			for (let [t, names] of [[blackop, blackops], [operation, operations], [contract, contracts]]) {
				if (names.some((n) => (cont = action(bladeburner, t, n)) > 0 ? true : (cont > -2 && (ns.print(`has "${t}" "${n}" to execute`), has_ops = true) && false))) {
					await ns.asleep(cont)
					bladeburner.upgradeSkill(skills[5])
					break
				}
			}
			if (cont <= 0) {
				if (has_ops) {
					ns.print(`Chaos is ${bladeburner.getCityChaos(bladeburner.getCity())}`)
				} else {
					ns.print(`No work to do`)
				}
				((cont = action(bladeburner, general, generals[has_ops ? 0 : 1])) > 0 && await ns.asleep(cont))
			}
		}
	}
}