/** @param {NS} ns */
export async function main(ns) {
	/*prevent arrays being passed by reference, 
		pass by value instead: array1.push( JSON.parse( JSON.stringify( array2 ) ) )*/

	ns.disableLog("ALL");
	ns.tail();

	var scannedServers;
	var server;
	var homePaths = [["home"]];
	var homePath = JSON.parse(JSON.stringify(homePaths[0]));
	var newNode;
	var path;
	var pathDuplicate;
	var pathNode;
	var nodeDuplicate;
	ns.atExit(() => {
		ns.singularity.connect("home")
		ns.tprint("Backdoors completed");
	})
	while (homePath.length !== 0) {
		scannedServers = ns.scan(homePath[homePath.length - 1]);
		newNode = false;
		for (server of scannedServers) {
			nodeDuplicate = false;
			for (pathNode of homePath) {
				if (JSON.stringify(pathNode) === JSON.stringify(server)) {
					nodeDuplicate = true;
					break;
				}
			}

			if (nodeDuplicate === false) {
				homePath.push(server);
				pathDuplicate = false;
				for (path of homePaths) {
					if (JSON.stringify(path) === JSON.stringify(homePath)) {
						pathDuplicate = true;
						break;
					}
				}

				if (pathDuplicate === false) {
					homePaths.push(JSON.parse(JSON.stringify(homePath)));
					newNode = true;
					break;
				}
				else {
					homePath.pop();
				}
			}
		}

		if (newNode === false) {
			homePath.pop();
		}
	}
	var changed = true;
	homePaths.sort((x,y)=>x.length-y.length)
	while (changed){
		changed=false
		for (path of homePaths) /*install backdoors on servers not owned which have root access*/ {
			server = ns.getServer(path[path.length - 1]);
			if ( !server.backdoorInstalled && !server.purchasedByPlayer &&
				server.hasAdminRights && (server.requiredHackingSkill <= ns.getHackingLevel())) {
				path.map(ns.singularity.connect).some((x)=>!x) || (changed=true),await ns.singularity.installBackdoor()
			}
		}
		await ns.sleep(1000)
	}
}