// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/System.Reflection.Primitives.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/System.Reflection.Emit.ILGeneration.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Runtime.dll" -unsafe -debug -optimize -deterministic Backend.cs *.CS -out:Backend.dll
// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Neutron's Taiwu Collections
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

 // Backend: ProcessMethodCall -> CallMethod -> ...
 #define BACKEND

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;
namespace ASmallCollectionsFromNeutron;
[TaiwuModdingLib.Core.Plugin.PluginConfig("NoToolLimit","Neutron3529","0.1.0")]
public class NoToolLimitImpl : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Initialize()=>this.HarmonyInstance = new RobustHarmonyInstance(this.GetGuid());
    public override void Dispose()=>this.HarmonyInstance.UnpatchSelf();
    public static void logger(string s)=>GameData.Utilities.AdaptableLog.Info(s);
    public static void logwarn(string s)=>GameData.Utilities.AdaptableLog.Warning("<color=#FFFF00>"+s+"</color>",true);
    public RobustHarmonyInstance HarmonyInstance;

    private bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logwarn("enable 出错，出错键值为\""+key+"\"，错误原因是"+ex.Message);
            return false;
        }
    }
    private bool _enable(string key){
        bool enabled=false;
        return GameData.Domains.DomainManager.Mod.GetSetting(this.ModIdStr, key, ref enabled) && enabled;
    }
    public override void OnModSettingUpdate(){
        this.HarmonyInstance.UnpatchSelf();
        if(enable("NForceOFF")){return;}
        this.HarmonyInstance.PatchAll(typeof(EnableNullTool));
    }
    public class EnableNullTool {
        static FieldInfo TemplateId=typeof(GameData.Domains.Item.ItemBase).GetField("TemplateId",(BindingFlags)(-1));
        static FieldInfo MaxDurability=typeof(GameData.Domains.Item.ItemBase).GetField("MaxDurability",(BindingFlags)(-1));
        static FieldInfo CurrDurability=typeof(GameData.Domains.Item.ItemBase).GetField("CurrDurability",(BindingFlags)(-1));
        static FieldInfo Id=typeof(GameData.Domains.Item.ItemBase).GetField("Id",(BindingFlags)(-1));
        public static GameData.Domains.Item.CraftTool tool;
        public static List<sbyte> sbl=new List<sbyte>(){6,7,8,9,10,11,14};
        public EnableNullTool(){
            tool=new GameData.Domains.Item.CraftTool();
            TemplateId.SetValue((GameData.Domains.Item.ItemBase)tool,(short)0);//改成-1可以debug
            MaxDurability.SetValue((GameData.Domains.Item.ItemBase)tool,(short)32000);//改成-1可以debug
            CurrDurability.SetValue((GameData.Domains.Item.ItemBase)tool,(short)32000);//改成-1可以debug
            Id.SetValue((GameData.Domains.Item.ItemBase)tool,(int)-1);//改成-1可以debug
        }



        [HarmonyPrefix]
        [HarmonyPatch(typeof(GameData.Domains.Building.BuildingDomain),"GetLifeSkillTotalAttainment")]
        public static bool Pre0(ref short __result, sbyte type, GameData.Domains.Item.ItemKey toolKey) {
            if(toolKey.Id==-1){
                __result=GameData.Domains.DomainManager.Taiwu.GetTaiwu().GetLifeSkillAttainment(type); // 造诣加成为0
                return false;
            }
            return true;
        }
        [HarmonyPrefix]
        [HarmonyPatch(typeof(GameData.Domains.Item.ItemDomain),"GetElement_CraftTools")]
        public static bool Pre1(GameData.Domains.Item.CraftTool __result, int objectId) {
            if(objectId==-1){
                CurrDurability.SetValue((GameData.Domains.Item.ItemBase)tool,(short)32000);//改成-1可以debug
                __result=tool;
                return false;
            }
            return true;
        }
        [HarmonyPrefix]
        [HarmonyPatch(typeof(GameData.Domains.Item.CraftTool),"GetRealAttainmentBonus")]
        public static bool Pre2(GameData.Domains.Item.CraftTool __instance, ref short __result) {
            if(__instance==null || (int)(Id.GetValue((GameData.Domains.Item.ItemBase)__instance))==-1){
                __result=0;
                return false;
            }
            return true;
        }
        [HarmonyPrefix]
        [HarmonyPatch(typeof(GameData.Domains.Item.CraftTool),"GetAttainmentBonus")]
        public static bool Pre3(GameData.Domains.Item.CraftTool __instance, ref short __result) {
            if(__instance==null || (int)(Id.GetValue((GameData.Domains.Item.ItemBase)__instance))==-1){
                __result=0;
                return false;
            }
            return true;
        }
        [HarmonyPrefix]
        [HarmonyPatch(typeof(GameData.Domains.Item.CraftTool),"GetRequiredLifeSkillTypes")]
        public static bool Pre4(GameData.Domains.Item.CraftTool __instance, List<sbyte> __result) {
            if((int)Id.GetValue((GameData.Domains.Item.ItemBase)__instance)==-1){
                __result=sbl;
                return false;
            }
            return true;
        }
    }
}
