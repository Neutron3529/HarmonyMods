// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Collections.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/mscorlib.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Private.CoreLib.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Runtime.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Threading.Tasks.Parallel.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Collections.Concurrent.dll" -unsafe -optimize -deterministic Backend.cs *.CS -out:Backend.dll
// -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;

namespace Optim;
[TaiwuModdingLib.Core.Plugin.PluginConfig("Optim","Neutron3529","0.2.0")]
public partial class Optim : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Initialize()=>this.HarmonyInstance = new RobustHarmonyInstance(this.GetGuid());
    public override void Dispose()=>this.HarmonyInstance.UnpatchSelf();
    public static void logger(string s)=>GameData.Utilities.AdaptableLog.Info(s);
    public RobustHarmonyInstance HarmonyInstance;
    public static int Remove_Gap=12;
    public static bool reverse=false;
    private bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logger("enable 出错，出错键值为\""+key+"\"，错误原因是"+ex.Message);
            return false;
        }
    }
    private bool _enable(string key){
        bool enabled=false;
        if(GameData.Domains.DomainManager.Mod.GetSetting(this.ModIdStr, key, ref enabled) && enabled){
            GameData.Domains.DomainManager.Mod.GetSetting(this.ModIdStr, "N01", ref reverse);
            return true;
        } else {
            return false;
        }
    }
    public override void OnModSettingUpdate(){
        this.HarmonyInstance.UnpatchSelf();
        if(enable("On"))this.HarmonyInstance.PatchAll(typeof(UpdateTeaHorseCaravan));
    }

    [HarmonyPatch(typeof(GameData.Domains.Building.BuildingDomain),"UpdateTeaHorseCaravan")]
    public class UpdateTeaHorseCaravan {
        public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
// 1107	0BA7	ldfld	class GameData.Domains.Building.TeaHorseCaravanData GameData.Domains.Building.BuildingDomain::_teaHorseCaravanData
// 1108	0BAC	ldfld	class [System.Collections]System.Collections.Generic.List`1<valuetype GameData.Domains.Item.ItemKey> GameData.Domains.Building.TeaHorseCaravanData::ExchangeGoodsList
// 1109	0BB1	ldloc.s	exchangeItem (62)
// 1110	0BB3	callvirt	instance void class [System.Collections]System.Collections.Generic.List`1<valuetype GameData.Domains.Item.ItemKey>::Add(!0)
// 1111	0BB8	nop
// 1112	0BB9	ldarg.0
// 1113	0BBA	ldfld	class GameData.Domains.Building.TeaHorseCaravanData GameData.Domains.Building.BuildingDomain::_teaHorseCaravanData
// 1114	0BBF	ldfld	class [System.Collections]System.Collections.Generic.List`1<valuetype [System.Runtime]System.ValueTuple`2<valuetype GameData.Domains.Item.ItemKey, int8>> GameData.Domains.Building.TeaHorseCaravanData::CarryGoodsList
// 1115	0BC4	ldloc.s	itemIndex (59)
// 1116	0BC6	callvirt	instance void class [System.Collections]System.Collections.Generic.List`1<valuetype [System.Runtime]System.ValueTuple`2<valuetype GameData.Domains.Item.ItemKey, int8>>::RemoveAt(int32)
            int counter=-10;
            logger("patch triggered");
            foreach(CodeInstruction ins in instructions) {
                if(counter==11 && (ins.opcode==OpCodes.Ldloc || ins.opcode==OpCodes.Ldloc_S)){
                    counter=0;
                    if(reverse){
                        logger("reverse triggered");
                        yield return new CodeInstruction(OpCodes.Dup,null);
                        yield return new CodeInstruction(OpCodes.Callvirt, typeof(List<ValueTuple<GameData.Domains.Item.ItemKey,char>>).GetProperty("Count").GetGetMethod());
                        yield return new CodeInstruction(OpCodes.Ldc_I4_1,null);
                        yield return new CodeInstruction(OpCodes.Sub,null);
                    } else {
                        logger("Ldc_I4_0 triggered");
                        yield return new CodeInstruction(OpCodes.Ldc_I4_0,null);
                    }
                    continue;
                }
                if(ins.opcode==OpCodes.Ldfld && ((FieldInfo)(ins.operand)).Name=="ExchangeGoodsList"){
                    logger("ExchangeGoodsList found");
                    counter=10;
                } else if(counter>0 && ins.opcode==OpCodes.Ldfld && ((FieldInfo)(ins.operand)).Name=="CarryGoodsList"){
                    logger("CarryGoodsList after ExchangeGoodsList found");
                    counter=12;
                }
                yield return ins;
                counter--;
            }
            logger("patch done");
        }
    }
}
