// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Runtime.dll" -r:"../../Backend/System.Linq.Expressions.dll" -unsafe -optimize -deterministic -debug Backend.cs ../UTILS/*.CS *.CS -out:Backend.dll -define:CharacterMod -define:Neili -define:Beautify -define:UnfairTrue -define:MapEditor
// -r:"../../Backend/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;

namespace GongfaEffect;
[TaiwuModdingLib.Core.Plugin.PluginConfig("GongfaEffect","Neutron3529","0.2.0")]
public partial class GongfaEffect : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Initialize()=>this.HarmonyInstance = new RobustHarmonyInstance(this.GetGuid());
    public override void Dispose()=>this.HarmonyInstance.UnpatchSelf();
    public static void logger(string s)=>GameData.Utilities.AdaptableLog.Info(s);
    public static void logwarn(string s)=>GameData.Utilities.AdaptableLog.Warning("<color=#FFFF00>"+s+"</color>",true);
    public RobustHarmonyInstance HarmonyInstance;
    private bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logwarn("enable 出错，出错键值为\""+key+"\"，错误原因是"+ex.Message);
            return false;
        }
    }
    private bool _enable(string key){
        bool enabled=false;
        GameData.Domains.DomainManager.Mod.GetSetting(this.ModIdStr, key, ref enabled);
        return enabled;
    }
    public static bool xn_beautify=true;
    public override void OnModSettingUpdate(){
        this.HarmonyInstance.UnpatchSelf();
        var param=new object[]{this.HarmonyInstance, this.ModIdStr};
        var types=new Type[]{typeof(RobustHarmonyInstance),typeof(string)};
        foreach(var type in typeof(GongfaEffect).Assembly.GetTypes()){
            var method=type.GetMethod("Patch", BindingFlags.Static | BindingFlags.Public, null, types, null);
            if(method!=null){
                if(enable(type.Name)){
                    method.Invoke(null,param);
                    logger("已开启: "+type.Name);
                }else{
                    logger("未开启: "+type.Name);
                }
            }
        }
    }
//     public override void Initialize() {
// //! 在Mod加载上，我建议将除了操作文件、制作全局变量这种只做一次的之外的全部逻辑，放在初始化之外，初始化和更新Mod配置共用一个逻辑，这可以减少错误发生概率。
// //!    值得注意的是，在这里，完全不需要在初始化数据时执行数据更新操作，因为太吾会帮你体面一下。
// //!    this.OnModSettingUpdate();
// //! 在初始化的时候，我们可以dump出一些需要的数据，就像这样：
// //! 值得说明的是，dotnet 5.0中，文件操作相关的类似乎被放在了"System.IO.FileSystem.dll"这里，所以需要加入引用-r:"../../Backend/System.IO.FileSystem.dll"
// //        foreach(var c in (List<Config.PersonalNeedItem>)(typeof(Config.PersonalNeed).GetField("_dataArray",(BindingFlags)(-1)).GetValue(Config.PersonalNeed.Instance)))
// //            System.IO.File.AppendAllText("PersonalNeed.txt",string.Format("TemplateId:{0}, Name:{1}, MatchType:{2}, Overwrite:{3}, Combine:{4}, Duration:{5}\n",c.TemplateId,c.Name,c.MatchType,c.Overwrite,c.Combine,c.Duration));
// // output:TemplateId:23, Name:共度春宵, MatchType:False, Overwrite:False, Combine:False, Duration:3
// //! 从上面的dump可以看到，共度春宵不能覆盖不能匹配，于是我们可以给一个人加许多次对同一个人的春宵需求（以实现人形自走榨汁姬这一AI行为）
//     }
}

