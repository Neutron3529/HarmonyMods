// echo partial file, should not compile along

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;
namespace GongfaEffect;

public partial class GongfaEffect {
    public static class TianCan {
        public const short Defkey_TianCan=294; // 目前这个方法并没有被暴露在defkey之外，因此只能如此使用。
        public static void Patch(RobustHarmonyInstance HarmonyInstance, string ModIdStr){
            CharacterMod.EarlyCheck();
            HarmonyInstance.PatchAll(System.Reflection.MethodBase.GetCurrentMethod().ReflectedType);
            // typeof(Config.MedicineItem).GetField("Duration").SetValue(med,(short)32000);
            // typeof(Config.MedicineItem).GetField("WugGrowthType").SetValue(med,(sbyte)4);
        }
        // static Config.MedicineItem med=new Config.MedicineItem();
        // static MethodInfo OfflineReplaceWug=typeof(GameData.Domains.Character.Character).GetMethod("OfflineReplaceWug",(BindingFlags)(-1));
        // static FieldInfo TemplateId=typeof(Config.MedicineItem).GetField("TemplateId"); // short
        [HarmonyPostfix]
        [HarmonyPatch(typeof(GameData.Domains.SpecialEffect.CombatSkill.Wuxianjiao.DefenseAndAssist.TianCanShiGu), "OnEnable")]
        public unsafe static void OnEnable(GameData.Domains.SpecialEffect.CombatSkill.Wuxianjiao.DefenseAndAssist.TianCanShiGu __instance, GameData.Common.DataContext context){
            if(GameData.Domains.DomainManager.Character.TryGetElement_Objects(__instance.CharacterId, out var character)){
                // logger($"TianCan.OnEnable called for {__instance.CharacterId}");
                var happiness=character.happiness();
                CharacterMod.Happiness.setter(character,0);
                var eatingItems = character.GetEatingItems();
                bool flag=false;
                for(sbyte wugType=0;wugType<8;wugType++){
                    var x=eatingItems.IndexOfWug(wugType);
                    if(x!=-1){
                        flag=true;
                        // TemplateId.SetValue(med, GameData.Domains.Item.ItemDomain.GetWugTemplateId(wugType, 1));
                        // OfflineReplaceWug.Invoke(character, new object[]{context, x, new GameData.Domains.Item.ItemKey(8, 0, GameData.Domains.Item.ItemDomain.GetWugTemplateId(wugType, 1), -1)});
                        character.AddWug(context, GameData.Domains.Item.ItemDomain.GetWugTemplateId(wugType, 1));
                    }
                }
                var items=character.GetEatingItems();
                for(int i=0;i<9;i++){
                    if(GameData.Domains.Character.EatingItems.IsWug((GameData.Domains.Item.ItemKey)(items.ItemKeys[i])) && items.Durations[i]!=32000){
                        items.Durations[i]=32000;
                    }
                }
                if(flag)character.SetEatingItems(ref items, context);
                CharacterMod.Happiness.setter(character,happiness);
            }
        }
    }
}
