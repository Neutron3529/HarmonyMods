-- CS.System.Diagnostics.Process.Start("notepad")
return {
    Title = "前后端通信demo",
--workshop_only    FileId = 2884606343,
--workshop_only    Source = 1,
    Version = 0, -- 2022<<32+0925,
    BackendPlugins = {"../Backend.dll"},
    --BackendPatches = {},
    FrontendPlugins = {"../DemoFrontend.dll"},
    --FrontendPatches = {},
    --EventPackages = {},
    Author = "Neutron3529",
    Description = "这是一个简单的通信示范，欢迎大家尝试。\n\n<color=#FF0000>订阅此Mod后，请务必删除Mod自带的Settings.Lua。Settings.Lua是我用来触发BUG的，会保证你在开始游戏时候打开一个记事本（哪怕你没有启用这个Mod！）</color>\n\n理论上源码是按照AGPL-V3发布的，但考虑到大家不会拿Mod卖钱玩，而C#的加密就跟没有一样强……\n使用AGPL可以最大地保存Mod制作者的权益（RedHat照样能在GPL协议下卖Linux赚钱就是最好的证据。）\n不过讲道理，这个Mod活不了太久。因为我希望螺舟尽快把这个Mod吃掉。\n所以用这个Mod时候，想写什么发布许可，就写什么发布许可吧。我只是强烈推荐AGPL-v3而已。\n\n<color=#FF0000>请跟我一起骂映超，映超提议使用lua写config的唯一好处就是，让Mod彻底失控。这里我只是打开了记事本，但……程序员完全有能力在后台打开一个脚本，下载并执行挖矿程序（或者勒索软件）。\n请记住，目前太吾Mod是不安全的，请务必按需订阅。</color>"
}
