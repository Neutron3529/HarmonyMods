// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../The Scroll of Taiwu_Data/Managed/System.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../The Scroll of Taiwu_Data/Managed/mscorlib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Assembly-CSharp.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Unity.TextMeshPro.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.CoreModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.UI.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.dll" -optimize -deterministic Frontend.cs -out:DemoFrontend.dll
// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\mscorlib.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Assembly-CSharp.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Unity.TextMeshPro.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\UnityEngine.CoreModule.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\UnityEngine.UI.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\UnityEngine.dll" -optimize -deterministic Frontend.cs -out:Frontend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace Communicate;
[TaiwuModdingLib.Core.Plugin.PluginConfig("Communicate","Neutron3529","0.3.1")]
public class Communicate : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
    static GameData.Domains.Mod.ModInfo modInfo;
    public override void OnModSettingUpdate(){
        modIdStr=this.ModIdStr;
        ((Dictionary<string, GameData.Domains.Mod.ModInfo>)(typeof(ModManager).GetField("_localMods",(BindingFlags)(-1))).GetValue(null)).TryGetValue(modIdStr, out modInfo);
    }
    public static string modIdStr;
    [HarmonyPatch(typeof(UI_Bottom), "DoAdvance")]
    public class FastSave {
        public static void Prefix(){
            GameData.GameDataBridge.GameDataBridge.AddMethodCall(-1,65535,65535,modInfo.ModId,"SendReturn"); // 直接调用
        }
    }
    public void Working(GameData.Utilities.RawDataPool dataPool, int offset){ // 后端调用Working
        int int2=0,int3=0;
        offset  +=  GameData.Serializer.Serializer.Deserialize(dataPool, offset, ref int2);
        /*offset+=*/GameData.Serializer.Serializer.Deserialize(dataPool, offset, ref int3); // 我们不需要最后一个offset的结果。
        SingletonObject.getInstance<AsynchMethodDispatcher>().AsynchMethodCall(65535,65535,modInfo.ModId,"AddReturnString",int2,int3,(int offset, GameData.Utilities.RawDataPool dataPool)=>{
            string ret=null;
            GameData.Serializer.Serializer.Deserialize(dataPool, offset, ref ret);

            DialogCmd dialogCmd4 = new DialogCmd {
                Title = "调用完成",
                Content = ret,
                Type = 1,
                Yes = delegate(){}
            };
            UIElement.Dialog.SetOnInitArgs(FrameWork.EasyPool.Get<FrameWork.ArgumentBox>().SetObject("Cmd", dialogCmd4));
            UIManager.Instance.ShowUI(UIElement.Dialog);
        });
    }
}
