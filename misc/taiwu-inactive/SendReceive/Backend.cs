// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Collections.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/mscorlib.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Private.CoreLib.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Runtime.dll" -unsafe -optimize -deterministic Backend.cs -out:Backend.dll
// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Collections.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\mscorlib.dll" -r:"..\..\..\Backend\GameData.dll" -r:"..\..\..\Backend\Redzen.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Private.CoreLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll" -unsafe -optimize -deterministic Backend.cs -out:Backend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

 // Backend: ProcessMethodCall -> CallMethod -> ...
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace ReceiveDemo;
[TaiwuModdingLib.Core.Plugin.PluginConfig("ReceiveDemo", "Neutron3529", "0.1.0")]
public class ReceiveDemo : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
    static FieldInfo ModInfoDict=typeof(GameData.Domains.Mod.ModDomain).GetField("ModInfoDict",(BindingFlags)(-1));
    string modIdStr;
    GameData.Domains.Mod.ModInfo modInfo;
    public override void OnModSettingUpdate(){
        modIdStr=this.ModIdStr;
        if(!((Dictionary<string, GameData.Domains.Mod.ModInfo>)(ModInfoDict.GetValue(null))).TryGetValue(modIdStr, out modInfo)){
            GameData.Utilities.AdaptableLog.Warning("<color=#FFFF00>找不到"+modIdStr+"</color>",true);
        }
    }
    public static int counter=0; // 我们可以定义一些类变量来辅助计算/存储中间结果
    public int SendReturn (GameData.GameDataBridge.Operation operation, GameData.Utilities.RawDataPool argDataPool, GameData.Utilities.RawDataPool returnDataPool, GameData.Common.DataContext context){
        GameData.Utilities.AdaptableLog.Info("mod ("+this.ModIdStr+")收到了SendReturn方法的调用请求，这是第"+(++counter)+"次调用\n这个方法马上会创建一个调用前端Working的事件，这不是一个合适的传参方式，这只是为了展示后端调用前端代码的技巧");
        GameData.GameDataBridge.GameDataBridge.AddDisplayEvent((GameData.GameDataBridge.DisplayEventType)65535,modInfo.ModId,"Working",(int)3,(int)4);
        return -1;
    }
    // 需保证调用的函数是类ReceiveDemo的方法。
    public int AddReturnString (GameData.GameDataBridge.Operation operation, GameData.Utilities.RawDataPool argDataPool, GameData.Utilities.RawDataPool returnDataPool, GameData.Common.DataContext context){
        ++counter;
        int argsOffset = operation.ArgsOffset;
        int a=default(int);
        int b=default(int); // 这是无脑的写法，但省事
        argsOffset += GameData.Serializer.Serializer.Deserialize(argDataPool, argsOffset, ref a);
        argsOffset += GameData.Serializer.Serializer.Deserialize(argDataPool, argsOffset, ref b);
        GameData.Utilities.AdaptableLog.Info("mod ("+this.ModIdStr+")收到了AddReturnString方法的调用请求，输入数据为"+a+"和"+b+"，答案为"+(a+b));

        return GameData.Serializer.Serializer.Serialize("答案是"+(a+b), returnDataPool);
//      理论上熟悉反序列化的或许可以在这里一次返回好几个东西出来，但我仍然需要强调：最好只返回一个返回值
    }
}
