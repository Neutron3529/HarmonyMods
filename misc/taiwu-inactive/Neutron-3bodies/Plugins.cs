// echo fine # $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Runtime.dll" -r:"../../Backend/System.Linq.dll" -r:"../../Backend/System.Linq.Expressions.dll" -r:"../../Backend/System.ComponentModel.Primitives.dll" -unsafe -optimize -deterministic Plugins.cs ../UTILS/*.CS -out:Plugins.dll -debug -define:BACKEND -define:NO_HARMONY -define:CharacterMod -define:NameId

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法
//! 这是一个示例Mod，用途是展示事件Mod的标准写法

/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.ComponentModel;
using Utils;
using static Utils.Logger;

namespace Neutron3529.脱水浸泡;
public static class 脱水浸泡 {
    static class 铭刻词典 {
        static class _inscribedCharacters;
        static Func<GameData.Domains.Global.GlobalDomain, Dictionary<GameData.Domains.Global.Inscription.InscribedCharacterKey, GameData.Domains.Global.Inscription.InscribedCharacter>> cdict=Utils.FastExpr.Met<_inscribedCharacters>.get(out cdict);
        public static Dictionary<GameData.Domains.Global.Inscription.InscribedCharacterKey, GameData.Domains.Global.Inscription.InscribedCharacter> get=>cdict(GameData.Domains.DomainManager.Global);
    }
    // [DisplayName("GlobalArgBox")]
    public static GameData.Domains.TaiwuEvent.EventArgBox GlobalArgBox=>GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetGlobalArgBox();
    [DisplayName("int_可浸泡人物人数")]
    public static int 铭刻人物人数()=>GlobalArgBox.Get("Neutron.Inscription-Recovered.Id", out GameData.Utilities.LongList ll)?铭刻词典.get.Count-(ll.Items?.Count??0):铭刻词典.get.Count;
    [DisplayName("InscribedCharacterKey_获取一个铭刻人物key")]
    public static GameData.Domains.Global.Inscription.InscribedCharacterKey 获取一个铭刻人物()=>GlobalArgBox.Get("Neutron.Inscription-Recovered.Id", out GameData.Utilities.LongList ll)?铭刻词典.get.Keys.Where(x=>!ll.Items.Contains((long)(ulong)x)).FirstOrDefault(GameData.Domains.Global.Inscription.InscribedCharacterKey.Invalid):铭刻词典.get.Keys.FirstOrDefault(GameData.Domains.Global.Inscription.InscribedCharacterKey.Invalid);
    [DisplayName("void_浸泡")]
    public static void bornchild_门派模板为太吾村__如果母亲已怀孕_以父亲2为接盘侠_孩子的真实父亲由母亲怀孕状态决定(GameData.Domains.Global.Inscription.InscribedCharacterKey chkey){
        if(!铭刻词典.get.TryGetValue(chkey, out var idata)){
            return;
        }
        var newInfo=idata.OrganizationInfo;

        var info=new GameData.Domains.Character.Creation.IntelligentCharacterCreationInfo(GameData.Domains.DomainManager.Taiwu.GetTaiwu().GetLocation(), newInfo, 10); // 10=太吾传人，按村民模板进行生成

        info.PregnantState = null;
        info.GrowingSectId = idata.OrganizationInfo.OrgTemplateId;
        info.GrowingSectGrade = idata.OrganizationInfo.Grade; // grade可以高于神一品，但似乎并无卵用……
        info.InitializeSectSkills = false;
        info.Gender=idata.Gender;
        info.Age=idata.CurrAge;
        info.BirthMonth=idata.BirthMonth;
        info.Transgender=idata.Avatar.Gender!=idata.Gender;
        info.CombatSkillQualificationGrowthType=idata.CombatSkillQualificationGrowthType;
        info.LifeSkillQualificationGrowthType=idata.LifeSkillQualificationGrowthType;
        info.MotherCharId = -1;
        info.Mother = null;
        info.FatherCharId = -1;
        info.ActualFatherCharId = -1;
        info.Father = null;
        info.ActualFather = null;
        info.DestinyType = -1;

        var rfn=info.ReferenceFullName;
        rfn.Type=1;

        NameId.refresh_ctext();
        var csid=NameId.c2i(idata.Surname);
        if(csid!=-1){
            rfn.Type|=4;
            rfn.CustomSurnameId=csid;
        }
        var cgid=NameId.c2i(idata.GivenName);
        if(cgid!=-1){
            rfn.Type|=8;
            rfn.CustomGivenNameId=cgid;
        }
        var sid=NameId.s2i(idata.Surname);
        if(sid!=-1){
            rfn.CustomSurnameId=sid; // CustomSurnameId = [Surname,_]=[Zangprefix,Zangsuffix]
            csid=sid;
        }
        var (gid,ty)=NameId.gg2i(idata.GivenName,idata.Gender);
        if(gid!=-1){
            rfn.CustomGivenNameId=gid; // CustomSurnameId = [Surname,_]=[Zangprefix,Zangsuffix]
            rfn.GivenNameType=ty;
            cgid=gid;
        }
        if(csid==-1 && cgid==-1){
            rfn=new();
        }
        info.ReferenceFullName=rfn;
        logger($"generate a new name:{rfn.GetName(idata.Gender,GameData.Domains.DomainManager.World.GetCustomTexts())}");

        var ctx = GameData.Domains.DomainManager.TaiwuEvent.MainThreadDataContext;
        var character= GameData.Domains.DomainManager.Character.ComplementCreateIntelligentCharacter(ctx, GameData.Domains.Character.CharacterDomain.ParallelCreateIntelligentCharacter(ctx, ref info, recordModification: false), autoComplement: false);

        int charId = character.GetId();
        int birthDate = character.GetBirthDate();
        GameData.Domains.DomainManager.Character.CompleteCreatingCharacter(charId);
        character.SetIdealSect(-1,ctx);

        character.actualAge(idata.ActualAge ,ctx);
        character.baseMaxHealth(idata.BaseMaxHealth,ctx);
        character.baseMorality(idata.Morality,ctx);
        character.SetAvatar(idata.Avatar,ctx);
        character.baseMainAttributes(idata.BaseMainAttributes,ctx);
        character.baseLifeSkillQualifications(idata.BaseLifeSkillQualifications,ctx);
        character.baseCombatSkillQualifications(idata.BaseCombatSkillQualifications,ctx);
        character.SetSkillQualificationBonuses(idata.InnateSkillQualificationBonuses.ToList(),ctx);
        character.SetFeatureIds(idata.FeatureIds,ctx);

        rfn=character.GetFullName();
        if(csid!=-1){
            rfn.CustomSurnameId=csid;
        }
        if(cgid!=-1){
            rfn.CustomGivenNameId=cgid;
            rfn.GivenNameType=ty;
        }
        character.SetFullName(rfn,ctx);

        GameData.Domains.DomainManager.Taiwu.JoinGroup(ctx, charId);
        // GameData.Domains.DomainManager.Global.RemoveInscribedCharacter(ctx,chkey);
        if(!GlobalArgBox.Get("Neutron.Inscription-Recovered.Id", out GameData.Utilities.LongList ll)){
            ll=GameData.Utilities.LongList.Create();
        }
        ll.Items.Add((long)(ulong)chkey);
        GlobalArgBox.Set("Neutron.Inscription-Recovered.Id", ll);

        var lst=new List<int>{charId};
        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ShowGetItemPageForCharacters(lst,isVillager: false);
    }
}
