// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Linq.dll" -r:"../../Backend/System.Runtime.dll" -unsafe -optimize -deterministic Taiwu_EventPackage_Test.cs -out:Taiwu_EventPackage_Test.dll -r:./NeutronBlackMarket.dll -debug
// -r:"../../Backend/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using static Utils.Logger;

using Config.EventConfig;
using GameData.Domains.TaiwuEvent;
using GameData.Domains.TaiwuEvent.Enum;

namespace Neutron3529.BlackEvents{
    public partial class BlackEvents : EventPackage {
        public BlackEvents() {
            // logger("into BlackEvents");
            this.NameSpace = "Taiwu.Event.Neutron3529.BlackMarket";
            this.Author = "Neutron3529";
            this.Group = "BlackMarket";
            this.EventList = null;
            this.EventList = new List<TaiwuEventItem>(){
                公用.公用.ECMN()
            };
            // check valid
            var hs=new HashSet<string>();
            foreach(var e in this.EventList){
                // logwarn(((EventContainer)e).guid);
                foreach(var o in e.EventOptions){
                    if(!hs.Contains(o.OptionKey)){
                        hs.Add(o.OptionKey);
                    } else {
                        logwarn($"OptionKey冲突：当前事件中已有{o.OptionKey}，这会导致部分选项失效");
                    }
                }
            }
        }
    }
    public class EventContainer : TaiwuEventItem {
        public string guid;
        public EventContainer(string name, string guid){
            this.guid = guid;
            this.Guid = Guid.Parse(guid);
            this.IsHeadEvent = false;
            this.EventGroup = name;
            this.ForceSingle = false;
            this.EventType = EEventType.ModEvent;
            this.TriggerType = EventTrigger.None;
            this.EventSortingOrder = 500;
            this.MainRoleKey = "RoleTaiwu";
            this.TargetRoleKey = "";
            this.EventBackground = "";
            this.MaskControl = 0;
            this.MaskTweenTime = 0f;
            this.EscOptionKey = "";
            this.EventOptions = null;
        }
        public virtual Func<string> Desc(int i){return ()=>string.Empty;}
        public override bool OnCheckEventCondition()=>true;
        public override void OnEventEnter(){}
        public override void OnEventExit(){}
        public override string GetReplacedContentString()=>string.Empty;
        public override List<string> GetExtraFormatLanguageKeys()=>null;
        public GameData.Common.DataContext context {
            get {
                return GameData.Domains.DomainManager.TaiwuEvent.MainThreadDataContext;
            }
        }
        public string Goto(string eventBody, string option, int abidx=-1, string jumpto=null){
            var ab=abidx==-1?this.ArgBox:this.EventOptions[abidx].ArgBox;
            ab.Set("NeutronEvent",eventBody);
            ab.Set("NeutronOption",option);
            ab.Set("NeutronJumpto",jumpto==null?string.Empty:jumpto);
            return "70b4d45d-ba15-423d-b94e-b40e348eb9bf";
        }
        public string Event(GameData.Domains.TaiwuEvent.EventArgBox argBox, int token){
            argBox.Set("NeutronIndex",token);
            return "4e657574-726f-423d-6e33-353239000000";
        }
        public string Event(int token){
            this.ArgBox.Set("NeutronIndex",token);
            return "4e657574-726f-423d-6e33-353239000000";
        }
        public static string Name(GameData.Domains.Character.Character ch, int color=0)=>$"<color=#{Colors(color)}FF>{GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetMonasticTitleOrDisplayName(ch,false)}</color>";
        public static string NameG(GameData.Domains.Character.Character ch)=>Name(ch, 9-ch.GetOrganizationInfo().Grade);
        public static string Grade(string str,int grade)=>$"<color=#{Colors(grade)}FF>{str}</color>";
        public static string Colors(int grade){ //1-9:品阶，0:普通人
            switch (grade) {
                case 0:return "B97D4B";
                case 1:return "E4504D";
                case 2:return "F28234";
                case 3:return "E3C66D";
                case 4:return "AE5AC8";
                case 5:return "63CED0";
                case 6:return "8FBAE7";
                case 7:return "6DB75F";
                case 8:return "FBFBFB";
                default:return "8E8E8E";
            }
        }
    }

    public class EventItem : EventContainer {
        public EventItem(string name, string guid, string content=null):base(name,guid){
            this.content=content==null?string.Empty:content;
        }
        public string content;
        public string display_content;
        public new virtual string EventContent()=>this.content;
        public override string GetReplacedContentString(){
            this.SetLanguage(new string[]{this.EventContent()});
            // this.EventContent=this.EventContent();
            return string.Empty;
        }
    }

    namespace 公用 {
        public class 公用 : EventItem {
            public static 公用 ECMN(){
                var o=new 公用();
                {
                    var taiwuEvent = GameData.Domains.DomainManager.TaiwuEvent.GetEvent("fb38f657-6ed0-41e4-a0c2-c82afb49762f"); // @人物互动-恩义互动
                    var lst=taiwuEvent.EventConfig.EventOptions.ToList();
                    lst.Insert(lst.Count-1, o.EventOptions[1]);
                    lst.Insert(lst.Count-1, o.EventOptions[2]);
                    taiwuEvent.EventConfig.EventOptions=lst.ToArray();
                }
                return o;
            }
            public override void OnEventEnter(){
                if(this.ArgBox.GetCharacter("NeutronCargo")!=null){
                    this.TargetRoleKey="NeutronCargo";
                } else {
                    this.TargetRoleKey=string.Empty;
                }
            }
            public override void OnEventExit(){
                this.TargetRoleKey=string.Empty;
                var ch=this.ArgBox.GetCharacter("NeutronCargo");
                Neutron3529.BlackMarket.Backend.RemoveChar(ch);
            }
            public 公用() :base("Prof.公用", "70b4d45d-ba15-423d-b94e-b40e348e19be"){
                this.EventOptions = new TaiwuEventOption[3];// 变化NPC为智能NPC 复制人物/退出预览
                base.content="预览当前人物";
                for(int i=0;i<this.EventOptions.Length;i++){
                    var op=new TaiwuEventOption {
                        OptionKey = "Option_3529009"+(i+1).ToString("01"),
                        OnOptionSelect = new Func<string>(this.OnOptionSelect(i)),
                        OnOptionVisibleCheck = new Func<bool>(this.OnCheckOptionCondition(i)),
                        OnOptionAvailableCheck = new Func<bool>(this.OnCheckEventCondition),
                        GetReplacedContent = new Func<string>(this.Desc(i)),
                        GetExtraFormatLanguageKeys = new Func<List<string>>(this.GetExtraFormatLanguageKeys)
                    };
                    this.EventOptions[i]=op;
                }
            }
            private Func<bool> OnCheckOptionCondition(int index){
                switch(index){
                    case 0:
                    case 1:return ()=>true;
                    case 2:return ()=>this.EventOptions[index].ArgBox.GetCharacter("NeutronCargo")==null;
                    default: return ()=>false;
                }
            }
            private unsafe Func<string> OnOptionSelect(int index){
                switch(index){
                    case 0:return ()=>{
                        var character=this.EventOptions[index].ArgBox.GetCharacter("NeutronCargo");
                        Neutron3529.BlackMarket.Backend.ConvertChar(character);
                        // GameData.Domains.TaiwuEvent.EventHelper.EventHelper.KeepTemporaryCharacterAfterAdventure(character);
                        return Goto("转化完成","（哭笑不得……）",index);
                    };
                    case 1:return ()=>{
                        var character=this.EventOptions[index].ArgBox.GetCharacter("NeutronCargo");
                        if(character!=null){
                            return Goto("临时人物已删除","（哭笑不得……）",index);
                        }
                        character=this.EventOptions[index].ArgBox.GetCharacter("CharacterId");
                        Neutron3529.BlackMarket.Backend.TestV1(character.GetId()); // 这里应该直接复制的，但我懒得写了，借用测试机制完成复制，将数据写入test_decode
                        this.EventOptions[index].ArgBox.Set("NeutronCargo", Neutron3529.BlackMarket.Backend.CreateChar(Neutron3529.BlackMarket.Backend.test_decode)); // 使用test_decode的数据，创建非智能NPC
                        character=this.EventOptions[index].ArgBox.GetCharacter("NeutronCargo");
                        Neutron3529.BlackMarket.Backend.TestV1_write(character.GetId()); // 完成复制任务
                        return "70b4d45d-ba15-423d-b94e-b40e348e19be";
                    };
                    case 2:return ()=>{
                        var character=this.EventOptions[index].ArgBox.GetCharacter("CharacterId");
                        if(Neutron3529.BlackMarket.Backend.Sell(character, context)){
                            return Goto("寄！","（哭笑不得……）",index);
                        } else {
                            return Goto("咦，人呢？","（哭笑不得……）",index);
                        }
                        // GameData.Domains.TaiwuEvent.EventHelper.EventHelper.KeepTemporaryCharacterAfterAdventure(character);

                    };
                    default: return ()=>string.Empty;
                }
            }
            public override Func<string> Desc(int index){
                switch(index){
                    case 0:
                        return ()=>"（转化NPC为智能人物……）";
                    case 1:
                        return ()=>this.EventOptions[index].ArgBox.GetCharacter("NeutronCargo")==null?"（复制Npc……）":"（退出预览……）";
                    case 2:
                        return ()=>"（抹杀人物……）";
                    default: return ()=>string.Empty;
                }
            }
        }
    }
}
