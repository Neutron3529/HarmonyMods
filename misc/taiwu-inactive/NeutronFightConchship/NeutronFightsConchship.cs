// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../The Scroll of Taiwu_Data/Managed/System.dll" -r:"../../The Scroll of Taiwu_Data/Managed/netstandard.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Reflection.Emit.ILGeneration.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../The Scroll of Taiwu_Data/Managed/mscorlib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Assembly-CSharp.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Unity.TextMeshPro.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.CoreModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.UI.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.dll" -optimize -deterministic NeutronFightsConchship.cs *.CS -out:NeutronFightsConchship.dll
// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll"
/**
 *  Neutron's Taiwu Collections
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#define FRONTEND
#define Fix_0_0_20

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;
[assembly: AssemblyVersion("0.0.0.3529")]
namespace NeutronFightsConchship;
[TaiwuModdingLib.Core.Plugin.PluginConfig("NeutronFightsConchship","Neutron3529","0.1.0")]
public class NeutronFightsConchship : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Initialize()=>this.HarmonyInstance = new RobustHarmonyInstance(this.GetGuid());
    public override void Dispose()=>this.HarmonyInstance.UnpatchSelf();
    public static void logger(string s)=>GameData.Utilities.AdaptableLog.Info(s);
    public static void logwarn(string s)=>GameData.Utilities.AdaptableLog.Warning("<color=#FFFF00>"+s+"</color>",true);
    public RobustHarmonyInstance HarmonyInstance;

    private bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logwarn("enable 出错，出错键值为\""+key+"\"，错误原因是"+ex.Message);
            return false;
        }
    }
    private bool _enable(string key){
        bool enable=false;
        return ModManager.GetSetting(this.ModIdStr, key,ref enable) && enable;
    }
    public override void OnModSettingUpdate(){
        this.HarmonyInstance.UnpatchSelf();
        // if(enable("VC"))this.HarmonyInstance.PatchAll(typeof(SteamManagerUploadItemUpdate));
        if(enable("VC"))this.HarmonyInstance.PatchAll(typeof(ModManagerWriteModInfo));
    }
    [HarmonyPatch(typeof(ModManager),"WriteModInfo")]
    public static class ModManagerWriteModInfo {
        public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            instructions = new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(OpCodes.Ldsfld,typeof(Game).GetField("Instance")),
                    new CodeMatch(OpCodes.Ldfld,typeof(Game).GetField("GameVersion"))
                ).Repeat( (matcher) => {// Do the following for each match
                    logger("GameVersion detected(should appear only once?)");
                    matcher.SetAndAdvance(
                        OpCodes.Ldstr,"99.99.99.99-Signed-by-Neutron"
                    ).SetAndAdvance(
                        OpCodes.Nop,null
                    ).Advance(1); // advance(1)结尾是美德
                }).InstructionEnumeration();
            return instructions;
        }
    }

    // public static class SteamManagerUploadItemUpdate {
    //     static IEnumerable<MethodInfo> TargetMethods(){
    //         yield return typeof(ModManager).Assembly.GetType("SteamManager").GetMethod("UploadItemUpdate");
    //     }
    //     public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
    //         instructions = new CodeMatcher(instructions)
    //             .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
    //                 new CodeMatch(OpCodes.Ldsfld,typeof(Game).GetField("Instance")),
    //                 new CodeMatch(OpCodes.Ldfld,typeof(Game).GetField("GameVersion"))
    //             ).Repeat( (matcher) => {// Do the following for each match
    //                 logger("GameVersion detected(should appear only once)");
    //                 matcher.SetAndAdvance(
    //                     OpCodes.Ldstr,"99.99.99.99"
    //                 ).SetAndAdvance(
    //                     OpCodes.Nop,null
    //                 ).Advance(1); // advance(1)结尾是美德
    //             }).InstructionEnumeration();
    //         return instructions;
    //     }
    // }
}
