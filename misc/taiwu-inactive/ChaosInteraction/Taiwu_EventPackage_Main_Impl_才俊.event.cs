// echo aux function only, should not compile alone.
// -r:"../../Backend/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using static Utils.Logger;
// using Utils; // equivlent to the following 3 lines.
using static Utils.Neili;
using static Utils.CharacterMod;
using static Utils.Beautify;

using Config.EventConfig;
using GameData.Domains.TaiwuEvent;
using GameData.Domains.TaiwuEvent.Enum;

namespace Neutron3529.ChaosEvents {
    public partial class ChaosEvents : EventPackage {
        public 才俊.才俊 E06(){
            var o=new 才俊.才俊();
            {
                var taiwuEvent = GameData.Domains.DomainManager.TaiwuEvent.GetEvent("fb38f657-6ed0-41e4-a0c2-c82afb49762f"); // @人物互动-恩义互动
                var lst=taiwuEvent.EventConfig.EventOptions.ToList();
                for(int i=0;i<o.EventOptions.Length;i++){
                    lst.Insert(lst.Count-1, o.EventOptions[i]);
                }
                taiwuEvent.EventConfig.EventOptions=lst.ToArray();
            }
            return o;
        }
    }
    namespace 才俊 {
        public static unsafe class 事件结算 {
            public static short DefKey_VirginityTrue = (short?)typeof(Config.CharacterFeature.DefKey).GetField("VirginityTrue",(BindingFlags)(-1))?.GetValue(null)??Config.CharacterFeature.DefKey.VirginityTrue;
            public static short DefKey_InfertileFemale = (short?)typeof(Config.CharacterFeature.DefKey).GetField("InfertileFemale",(BindingFlags)(-1))?.GetValue(null)??Config.CharacterFeature.DefKey.InfertileFemale;
            public static short DefKey_InfertileMale = (short?)typeof(Config.CharacterFeature.DefKey).GetField("InfertileMale",(BindingFlags)(-1))?.GetValue(null)??Config.CharacterFeature.DefKey.InfertileMale;
            public static short DefKey_Pregnant = (short?)typeof(Config.CharacterFeature.DefKey).GetField("Pregnant",(BindingFlags)(-1))?.GetValue(null)??Config.CharacterFeature.DefKey.Pregnant;
            public static ushort RelationType_Adored= (ushort?)typeof(GameData.Domains.Character.Relation.RelationType).GetField("Adored",(BindingFlags)(-1))?.GetValue(null) ?? GameData.Domains.Character.Relation.RelationType.Adored;
            public static MethodInfo SetBabyBonusMainAttributes=typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("SetBabyBonusMainAttributes",(BindingFlags)(-1));
            public static MethodInfo SetBabyBonusCombatSkillQualifications=typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("SetBabyBonusCombatSkillQualifications",(BindingFlags)(-1));
            public static MethodInfo SetBabyBonusLifeSkillQualifications=typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("SetBabyBonusLifeSkillQualifications",(BindingFlags)(-1));
            public static void EndEvent(ComplexTaiwuEvent self){
                GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(string.Empty);
            }
            public static void Adored(ComplexTaiwuEvent self){
                var character=self.ArgBox.GetCharacter("CharacterId");
                var taiwuChar=self.ArgBox.GetCharacter("RoleTaiwu");
                var currDate=GameData.Domains.DomainManager.World.GetCurrDate();
                GameData.Domains.DomainManager.Character.AddRelation(self.context, character.GetId(), taiwuChar.GetId(), RelationType_Adored, GameData.Domains.DomainManager.World.GetCurrDate());
            }
            public static void MakeLove(ComplexTaiwuEvent self){
                var character=self.ArgBox.GetCharacter("CharacterId");
                var taiwuChar=self.ArgBox.GetCharacter("RoleTaiwu");
                var currDate=GameData.Domains.DomainManager.World.GetCurrDate();
                var lifeRecordCollection = GameData.Domains.DomainManager.LifeRecord.GetLifeRecordCollection();
                var location=character.GetLocation();
                character.MakeLove(self.context,taiwuChar,false);
                lifeRecordCollection.AddMakeLoveIllegal(character.GetId(), currDate, taiwuChar.GetId(), location);
            }
            public static void CharDecHealth(ComplexTaiwuEvent self){
                var character=self.ArgBox.GetCharacter("CharacterId");
                character.baseMaxHealth((short)(character.baseMaxHealth()-120),self.context);
            }
            public static void TaiwuAddAge(ComplexTaiwuEvent self){
                var taiwuChar=self.ArgBox.GetCharacter("RoleTaiwu");
                taiwuChar.currAge((short)(taiwuChar.currAge()+10),self.context);
                taiwuChar.disorderOfQi(0,self.context);
            }
            public static void TaiwuRecover(ComplexTaiwuEvent self){
                var taiwuChar=self.ArgBox.GetCharacter("RoleTaiwu");
                taiwuChar.health(taiwuChar.GetLeftMaxHealth(false),self.context);
                taiwuChar.disorderOfQi(0,self.context);
                taiwuChar.currMainAttributes(taiwuChar.GetMaxMainAttributes(),self.context);
                if(taiwuChar.currAge()>16){
                    taiwuChar.currAge((short)(Math.Max(taiwuChar.currAge()*0.95,16)),self.context);
                }
            }
            public static void TaiwuRecoverInterrupted(ComplexTaiwuEvent self){
                var taiwuChar=self.ArgBox.GetCharacter("RoleTaiwu");
                taiwuChar.health((short)Math.Max(taiwuChar.health()+36,taiwuChar.GetLeftMaxHealth(false)),self.context);
                taiwuChar.disorderOfQi(8000,self.context);
            }
            public static void CharmFailed(ComplexTaiwuEvent self){
                var character=self.ArgBox.GetCharacter("CharacterId");
                var taiwuChar=self.ArgBox.GetCharacter("RoleTaiwu");
                var currDate=GameData.Domains.DomainManager.World.GetCurrDate();
                var lifeRecordCollection = GameData.Domains.DomainManager.LifeRecord.GetLifeRecordCollection();
                var location=character.GetLocation();
                if(GameData.Domains.DomainManager.Character.TryGetPregnantState(taiwuChar.GetId(), out var ps)) {
                    ps.ExpectedBirthDate=GameData.Domains.DomainManager.World.GetCurrDate()+1;
                    SetBabyBonusMainAttributes.Invoke(GameData.Domains.DomainManager.Taiwu,new object[]{(short)0, self.context});
                    SetBabyBonusCombatSkillQualifications.Invoke(GameData.Domains.DomainManager.Taiwu,new object[]{(short)0, self.context});
                    SetBabyBonusLifeSkillQualifications.Invoke(GameData.Domains.DomainManager.Taiwu,new object[]{(short)0, self.context});
                    ps.IsHuman=false;
                    taiwuChar.ChangeDisorderOfQi(self.context,8000);
                    taiwuChar.GetPoisoned().Items[4]=Math.Clamp(taiwuChar.GetPoisoned().Items[4] + 6561, 0, 25000);
                    taiwuChar.GetPoisoned().Items[5]=Math.Clamp(taiwuChar.GetPoisoned().Items[5] + 6561, 0, 25000);

                    character.SetHealth(character.GetLeftMaxHealth(false), self.context);
                    taiwuChar.SetHealth((short)((taiwuChar.GetHealth()+9)/10), self.context);
                    GameData.DomainEvents.Events.RegisterHandler_PostAdvanceMonthBegin(new RegAdvMonth(self.context, taiwuChar.GetId()).oamb);
                    GameData.DomainEvents.Events.RegisterHandler_PostAdvanceMonthBegin(new RegAdvMonth_Avatar(self.context, taiwuChar.GetId()).oamb);
                }
            }
            public static void BeingPregnanted(ComplexTaiwuEvent self){
                var character=self.ArgBox.GetCharacter("CharacterId");
                var taiwuChar=self.ArgBox.GetCharacter("RoleTaiwu");
                var currDate=GameData.Domains.DomainManager.World.GetCurrDate();
                var lifeRecordCollection = GameData.Domains.DomainManager.LifeRecord.GetLifeRecordCollection();
                var location=character.GetLocation();
                GameData.Domains.DomainManager.Character.CreatePregnantState(self.context, taiwuChar, character, false);
                taiwuChar.AddFeature(self.context, DefKey_Pregnant);
                character.MakeLove(self.context,taiwuChar,false);
                lifeRecordCollection.AddMakeLoveIllegal(character.GetId(), currDate, taiwuChar.GetId(), location);
                if(GameData.Domains.DomainManager.Character.TryGetPregnantState(taiwuChar.GetId(), out var ps)) {
                    ps.ExpectedBirthDate=GameData.Domains.DomainManager.World.GetCurrDate()+1;
                    GameData.Domains.DomainManager.Taiwu.PrenatalEducateForCombatSkill(self.context, 30);
                    GameData.Domains.DomainManager.Taiwu.PrenatalEducateForLifeSkill(self.context, 30);
                    GameData.Domains.DomainManager.Taiwu.PrenatalEducateForMainAttribute(self.context, 30);
                    character.SetBaseMaxHealth((short)Math.Max(character.GetBaseMaxHealth()-120,1),self.context);
                } else {
                    logwarn("<color=#FF0000>你可以认为是红字了……\n中子并没有处理这种明明创建了PregnantState但无法获取PregnantState的情况\n\n你大概需要备份一次存档，并在此之后原地过月检查是否有过月红字\n或许这也是徒劳，毕竟理论上你永远也不应该看见这行字\n……请自求多福好了……</color>");
                    GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(string.Empty);
                }
            }
            public static void UpdateActionPoint(ComplexTaiwuEvent self){
                GameData.Domains.DomainManager.Extra.UpdateActionPoint(self.context);
                GameData.Domains.DomainManager.Extra.UpdateActionPoint(self.context); // update previous month left days.
            }
            public static void CharacterLoseLife(ComplexTaiwuEvent self){
                var character=self.ArgBox.GetCharacter("CharacterId");
                character.SetBaseMaxHealth((short)Math.Max(character.GetBaseMaxHealth()-120,1),self.context);
            }
            public static void TaiwuFallInCharm(ComplexTaiwuEvent self){
                var character=self.ArgBox.GetCharacter("CharacterId");
                var taiwuChar=self.ArgBox.GetCharacter("RoleTaiwu");
                var currDate=GameData.Domains.DomainManager.World.GetCurrDate();
                var lifeRecordCollection = GameData.Domains.DomainManager.LifeRecord.GetLifeRecordCollection();
                var location=character.GetLocation();
                character.MakeLove(self.context,taiwuChar,false);
                lifeRecordCollection.AddMakeLoveIllegal(character.GetId(), currDate, taiwuChar.GetId(), location);
                var c=Math.Ceiling((taiwuChar.GetCurrMainAttributes().GetSum()*0.2+(taiwuChar.GetHealth()-((taiwuChar.GetHealth()+9)/10))*0.1+taiwuChar.DecNeili(self.context, taiwuChar.GetCurrNeili())*0.05));
                taiwuChar.SetHealth((short)((taiwuChar.GetHealth()+9)/10), self.context);
                taiwuChar.SetCurrMainAttributes(new GameData.Domains.Character.MainAttributes(0,0,0,0,0,0),self.context);
                character.SetBaseMaxHealth((short)Math.Max(character.GetBaseMaxHealth()+(short)c,1),self.context);
                character.beautify(self.context);
            }
            public static void BetterSettlement_Impl(ComplexTaiwuEvent self, GameData.Domains.Organization.Settlement set){
                if(set!=null){
                    set.SetMaxSafety(250,self.context);
                    set.SetMaxCulture(250,self.context);
                    set.SetSafety(250,self.context);
                    set.ChangeCulture(self.context,250);
                }
            }
            public static void BetterSettlement(ComplexTaiwuEvent self){
                var set=GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetSettlementByLocation(self.ArgBox.GetCharacter("CharacterId").GetLocation());
                BetterSettlement_Impl(self, set);
            }
            public static void SectApprove(ComplexTaiwuEvent self){
                var ch=self.ArgBox.GetCharacter("CharacterId");
                var oi=ch.GetOrganizationInfo();
                GameData.Domains.TaiwuEvent.EventHelper.EventHelper.SetSectCharApprovedTaiwu(ch.GetId(), true);
                var set=GameData.Domains.DomainManager.Organization.GetSettlementByOrgTemplateId(oi.OrgTemplateId);
                if(set!=null){
                    BetterSettlement_Impl(self, set);
                    GameData.Domains.TaiwuEvent.EventHelper.EventHelper.AddMaxApprovingRateBonus(set.GetId(), 100, 36);
                }
            }
            public static bool cond_太吾音律不足一百(ComplexTaiwuEvent self)=>self.ArgBox.GetCharacter("RoleTaiwu").GetLifeSkillQualification(0)<100;
            public static bool cond_太吾音律大于两百(ComplexTaiwuEvent self)=>self.ArgBox.GetCharacter("RoleTaiwu").GetLifeSkillQualification(0)>200;
            public static bool cond_性别相同(ComplexTaiwuEvent self)=>self.ArgBox.GetCharacter("CharacterId").GetGender()==self.ArgBox.GetCharacter("RoleTaiwu").GetGender();
            public static bool cond_太吾为男性(ComplexTaiwuEvent self)=>self.ArgBox.GetCharacter("RoleTaiwu").GetGender()==1;
            public static bool cond_太吾怀孕(ComplexTaiwuEvent self)=>GameData.Domains.DomainManager.Character.TryGetPregnantState(self.ArgBox.GetCharacter("RoleTaiwu").GetId(), out var _);
            public static bool cond_行动力不满(ComplexTaiwuEvent self)=>GameData.Domains.DomainManager.Extra.GetActionPointCurrMonth()<300 | GameData.Domains.DomainManager.Extra.GetActionPointPrevMonth()<300;
            public static bool cond_人物在门派(ComplexTaiwuEvent self)=>GameData.Domains.DomainManager.Organization.IsInAnySect(self.ArgBox.GetCharacter("CharacterId").GetId());
            public static bool cond_人物魅力为天人(ComplexTaiwuEvent self)=>self.ArgBox.GetCharacter("CharacterId").GetAvatar().GetCharm(self.ArgBox.GetCharacter("CharacterId").GetPhysiologicalAge(),0)>799;
            public static bool cond_人物还有寿命(ComplexTaiwuEvent self)=>self.ArgBox.GetCharacter("CharacterId").GetLeftMaxHealth(false)>0;
            public static bool cond_目前在定居点(ComplexTaiwuEvent self)=>GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetSettlementByLocation(self.ArgBox.GetCharacter("CharacterId").GetLocation())!=null;
            public static bool cond_太吾超过16岁(ComplexTaiwuEvent self)=>self.ArgBox.GetCharacter("CharacterId").currAge()>16;
            public class RegAdvMonth{
                int id;
                public RegAdvMonth(GameData.Common.DataContext context, int id){
                    this.id=id;
                    if(GameData.Domains.DomainManager.Character.TryGetElement_Objects(id, out var character)){
                        character.AddFeature(context, DefKey_InfertileFemale, true);
                    }
                }
                public void oamb(GameData.Common.DataContext context){
                    if(GameData.Domains.DomainManager.Character.TryGetElement_Objects(id, out var character)){
                        character.RemoveFeature(context, DefKey_InfertileFemale);
                        character.AddFeature(context, DefKey_VirginityTrue, true);
                    }
                    GameData.DomainEvents.Events.UnRegisterHandler_PostAdvanceMonthBegin(this.oamb);
                }
            }
            public class RegAdvMonth_Avatar{
                int id;
                public RegAdvMonth_Avatar(GameData.Common.DataContext context, int id){
                    this.id=id;
                    if(GameData.Domains.DomainManager.Character.TryGetElement_Objects(id, out var character)){
                        var avatar=character.GetAvatar();
                        avatar.ShowVeil=true;
                        avatar.SetGrowableElementShowingAbility(3,true);
                        avatar.SetGrowableElementShowingAbility(4,true);
                        avatar.SetGrowableElementShowingAbility(5,true);
                        character.SetAvatar(avatar, context);
                    }
                }
                public void oamb(GameData.Common.DataContext context){
                    if(GameData.Domains.DomainManager.Character.TryGetElement_Objects(id, out var character)){
                        var avatar=character.GetAvatar();
                        avatar.ShowVeil=false;
                        var age=character.GetPhysiologicalAge();
                        avatar.SetGrowableElementShowingAbility(3,GameData.Domains.Character.Character.IsAbleToGrowWrinkle1(age));
                        avatar.SetGrowableElementShowingAbility(4,GameData.Domains.Character.Character.IsAbleToGrowWrinkle2(age));
                        avatar.SetGrowableElementShowingAbility(5,GameData.Domains.Character.Character.IsAbleToGrowWrinkle3(age));
                        character.SetAvatar(avatar, context);
                    }
                    GameData.DomainEvents.Events.UnRegisterHandler_PostAdvanceMonthBegin(this.oamb);
                }
            }
        }
        public class 才俊 : EventContainer {
            public unsafe 才俊() :base("Prof.才俊", "70b4d45d-ba15-423d-b94e-b40e348eb9c6"){
                this.EventOptions = new TaiwuEventOption[2];//凤求凰 填鸭
                for(int i=0;i<this.EventOptions.Length;i++){
                    var op=new TaiwuEventOption {
                        OptionKey = "Option_352906"+(i+1).ToString("02"),
                        OnOptionSelect = new Func<string>(this.OnOptionSelect(i)),
                        OnOptionVisibleCheck = new Func<bool>(this.OnCheckOptionCondition(i)),
                        OnOptionAvailableCheck = new Func<bool>(this.OnCheckEventCondition),
                        GetReplacedContent = new Func<string>(this.Desc(i)),
                        GetExtraFormatLanguageKeys = new Func<List<string>>(this.GetExtraFormatLanguageKeys)
                    };
                    this.EventOptions[i]=op;
                }

                if(((Dictionary<string, GameData.Domains.Mod.ModInfo>)(typeof(GameData.Domains.Mod.ModDomain).GetField("ModInfoDict",(BindingFlags)(-1))).GetValue(null)).TryGetValue("1_2882942864", out var modInfo)){
                    foreach(var tsvline in System.IO.File.ReadAllLines(modInfo.DirectoryName+"/Events.python.tsv")){
                        if(!string.IsNullOrEmpty(tsvline.Trim())){
                            AddEvent(new AutoEvent(tsvline));
                        }
                    }
                }
            }
            private Func<bool> OnCheckOptionCondition(int index){
                switch(index){
                    case 1: return ()=>{
                        var pd=GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetCurrentProfessionData();
                        var character=this.EventOptions[index].ArgBox.GetCharacter("CharacterId");
                        var taiwuChar=this.EventOptions[index].ArgBox.GetCharacter("RoleTaiwu");
                        if(character!=null && taiwuChar!=null && pd!=null && pd.TemplateId == 4){
                            var lst=character.GetLearnedLifeSkills();
                            foreach(var x in taiwuChar.GetLearnedLifeSkills()){
                                var z=character.FindLearnedLifeSkillIndex(x.SkillTemplateId);
                                if(z==-1 || ((x.ReadingState&~lst[z].ReadingState)>0)){
                                    return true;
                                }
                            }
                            var loc=character.GetLocation();
                            if(loc.IsValid()){
                                var blck=GameData.Domains.DomainManager.Map.GetBlock(loc);
                                if(blck.CharacterSet!=null){
                                    foreach(var id in blck.CharacterSet){
                                        if(GameData.Domains.DomainManager.Character.TryGetElement_Objects(id, out var ch)){
                                            foreach(var x in taiwuChar.GetLearnedLifeSkills()){
                                                var z=ch.FindLearnedLifeSkillIndex(x.SkillTemplateId);
                                                if(z==-1 || ((x.ReadingState&~lst[z].ReadingState)>0)){
                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return false;
                    };
                    default: return ()=>{
                        var pd=GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetCurrentProfessionData();
                        var character=this.EventOptions[index].ArgBox.GetCharacter("CharacterId");
                        var taiwuChar=this.EventOptions[index].ArgBox.GetCharacter("RoleTaiwu");
                        return character!=null && taiwuChar!=null && pd!=null && character.GetGender()!=taiwuChar.GetGender() && pd.TemplateId == 4 && pd.IsSkillUnlocked(1);
                    };
                }
            }
            void learn_all(GameData.Domains.Character.Character character, GameData.Domains.Character.Character taiwuChar){
                var lst=character.GetLearnedLifeSkills();
                foreach(var x in taiwuChar.GetLearnedLifeSkills()){
                    var z=character.FindLearnedLifeSkillIndex(x.SkillTemplateId);
                    if(z==-1){
                        lst.Add(x);
                    } else {
                        var w=lst[z];
                        w.ReadingState|=x.ReadingState;
                        lst[z]=w;
                    }
                }
                character.SetLearnedLifeSkills(lst, this.context);
            }
            private unsafe Func<string> OnOptionSelect(int index){
                switch(index){
                    case 0:return ()=>Event(this.EventOptions[index].ArgBox,"CondJump.Neutron-才俊对话-凤求凰");
                    case 1: return ()=>{
                        var pd=GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetCurrentProfessionData();
                        var character = this.EventOptions[index].ArgBox.GetCharacter("CharacterId");
                        var taiwuChar=this.EventOptions[index].ArgBox.GetCharacter("RoleTaiwu");
                        var loc=character.GetLocation();
                        var tname=Name(taiwuChar);
                        if(loc.IsValid()){
                            var blck=GameData.Domains.DomainManager.Map.GetBlock(loc);
                            if(blck.CharacterSet!=null){
                                List<GameData.Domains.Character.Character> lst=new();
                                foreach(var id in blck.CharacterSet){
                                    if(GameData.Domains.DomainManager.Character.TryGetElement_Objects(id, out var ch)){
                                        lst.Add(ch);
                                    }
                                }
                                if(lst.Count>1){
                                    foreach(var ch in lst){
                                        learn_all(ch, taiwuChar);
                                    }
                                    return Goto($"宫商角徵羽，起于天元\n关关雎鸠，婉若游龙\n\n<color=#lightgrey>（{tname}的讲课声，不绝地灌入{string.Join("、",lst.Select(x=>Name(x)))}的脑海之中）</color>","（意犹未满……）",index);
                                }
                            }
                        }
                        var cname=Name(character);
                        learn_all(character, taiwuChar);
                        return Goto($"宫商角徵羽，起于天元\n关关雎鸠，婉若游龙\n\n<color=#lightgrey>（{cname}在{tname}的填鸭式教育之下，学到了不少东西）</color>","（意犹未满……）",index);
                    };
                    default: return ()=>string.Empty;
                }
            }
            public override Func<string> Desc(int index){
                switch(index){
                    case 0:
                        return ()=>"（一曲《凤求凰》……）"
                            // return this.EventOptions[index].ArgBox.Contains<bool>("NeutronFlag")?"（力保举荐……）":"（凤求凰……）";
                        ;
                    case 1: return ()=>"（填鸭式教学……）";
                    default: return ()=>string.Empty;
                }
            }
        }
    }
}
