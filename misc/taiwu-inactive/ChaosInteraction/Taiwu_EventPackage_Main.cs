// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Linq.dll" -r:"../../Backend/System.Linq.Expressions.dll" -r:"../../Backend/System.Runtime.dll" -unsafe -optimize -deterministic Taiwu_EventPackage_Main.cs ../UTILS/*.CS *.event.cs -out:Taiwu_EventPackage_Profession.dll -debug -define:CharacterMod -define:Neili -define:Beautify
// -r:"../../Backend/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using static Utils.Logger;
using static Utils.Event;

using Config.EventConfig;
using GameData.Domains.TaiwuEvent;
using GameData.Domains.TaiwuEvent.Enum;

using static Neutron3529.ChaosEvents.Const;
namespace Neutron3529.ChaosEvents;

public static class Const {
    public static readonly string BASE_EVENT="4e657574-726f-423d-6e33-353239203200";
    public static readonly string BYPASS_EVENT="4e657574-726f-423d-6e33-353239203300";
}

public partial class ChaosEvents : EventPackage {
    public ChaosEvents() {
        // logger("into ChaosEvents");
        this.NameSpace = "Taiwu.Event.Neutron3529.ChaosEvents";
        this.Author = "Neutron3529";
        this.Group = "ChaosEvents";
        this.EventList = null;
        bool need_init=true;
        foreach(var x in typeof(ChaosEvents).GetMethods()){
            if(x.Name[0]=='E'){
                if(enable(x.Name)){
                    if(need_init){
                        try{
                            ComplexTaiwuEvent.Instance = new ComplexTaiwuEvent("ChaosEvents.Complex", BASE_EVENT);
                            this.EventList = new List<TaiwuEventItem>(){
                                new MainTaiwuEvent("ChaosEvents.Base", "70b4d45d-ba15-423d-b94e-b40e348eb9bf"),
                                new BypassEvent("ChaosEvents.Bypass",BYPASS_EVENT),
                                ComplexTaiwuEvent.Instance
                            };
                            need_init=false;
                        }catch(Exception ex){
                            logwarn("中子的交互扰乱器：初始化事件有误，这个Mod多半是寄了。错误信息如下：");
                            logex(ex);
                            return;
                        }
                    }
                    // logwarn($"Invoke {x.Name}");
                    try{
                        var o=x.Invoke(this,null);
                        logger($"{o.GetType().Name}({x.Name}) 加载成功");
                        this.EventList.Add((TaiwuEventItem)o);
                    }catch(Exception ex){
                        logwarn($"中子的交互扰乱器：初始化事件{x.Name}有误，这个事件多半是寄了，但不会影响其他事件的正常patch。错误原因如下：");
                        logex(ex);
                    }
                }
            }
        }
        // check valid
        var hs=new HashSet<string>();
        foreach(var e in this.EventList){
            // logwarn(((EventContainer)e).guid);
            foreach(var o in e.EventOptions){
                if(!hs.Contains(o.OptionKey)){
                    hs.Add(o.OptionKey);
                } else {
                    logwarn($"OptionKey冲突：当前事件中已有{o.OptionKey}，这会导致部分选项失效");
                }
            }
        }
    }
}
public class EventContainer : TaiwuEventItem {
    public string guid;
    public EventContainer(string name, string guid){
        this.guid = guid;
        this.Guid = Guid.Parse(guid);
        this.IsHeadEvent = false;
        this.EventGroup = name;
        this.ForceSingle = false;
        this.EventType = EEventType.ModEvent;
        this.TriggerType = EventTrigger.None;
        this.EventSortingOrder = 500;
        this.MainRoleKey = "";
        this.TargetRoleKey = "";
        this.EventBackground = "";
        this.MaskControl = 0;
        this.MaskTweenTime = 0f;
        this.EscOptionKey = "";
        this.EventOptions = new TaiwuEventOption[0];
    }
    public virtual Func<string> Desc(int i){return ()=>string.Empty;}
    public override bool OnCheckEventCondition()=>true;
    public override void OnEventEnter(){}
    public override void OnEventExit(){}
    public override string GetReplacedContentString()=>string.Empty;
    public override List<string> GetExtraFormatLanguageKeys()=>null;
    public GameData.Common.DataContext context {
        get {
            return GameData.Domains.DomainManager.TaiwuEvent.MainThreadDataContext;
        }
    }
    public string Goto(string eventBody, string option, int abidx=-1, string jumpto=null){
        var ab=abidx==-1?this.ArgBox:this.EventOptions[abidx].ArgBox;
        ab.Set("NeutronEvent",eventBody);
        ab.Set("NeutronOption",option);
        ab.Set("NeutronJumpto",jumpto==null?string.Empty:jumpto);
        return "70b4d45d-ba15-423d-b94e-b40e348eb9bf";
    }
    public static Dictionary<string,int> event_token=new();
    public static List<AutoEvent> events=new();
    public static int AddEvent(AutoEvent e){
        var x=events.Count;
        event_token[e.token]=x;
        events.Add(e);
        return x;
    }
    public static string Event(GameData.Domains.TaiwuEvent.EventArgBox argBox, string token){
        if(event_token.TryGetValue(token, out var x))
            return Event(argBox, x);
        logwarn($"事件token {token} 不存在，当前事件将直接结束。请通知Mod作者检查事件链设置是否正确");
        token=string.Empty;
        argBox.Get("NeutronChain",ref token);
        logwarn($"事件链：{token} (this Error)");
        return string.Empty;
    }
    public string Event(string token){
        return Event(this.ArgBox,token);
    }
    public static string Event(GameData.Domains.TaiwuEvent.EventArgBox argBox, int token){
        argBox.Set("NeutronIndex",token);
        return BASE_EVENT;
    }
    public string Event(int token){
        return Event(this.ArgBox, token);
    }
    public static string Name(GameData.Domains.Character.Character ch, int color=0)=>$"<color=#{Colors(color)}FF>{GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetMonasticTitleOrDisplayName(ch,false)}</color>";
    public static string NameG(GameData.Domains.Character.Character ch)=>Name(ch, 9-ch.GetOrganizationInfo().Grade);
    public static string Grade(string str,int grade)=>$"<color=#{Colors(grade)}FF>{str}</color>";
    public static string Colors(int grade){ //1-9:品阶，0:普通人
        switch (grade) {
            case 0:return "B97D4B";
            case 1:return "E4504D";
            case 2:return "F28234";
            case 3:return "E3C66D";
            case 4:return "AE5AC8";
            case 5:return "63CED0";
            case 6:return "8FBAE7";
            case 7:return "6DB75F";
            case 8:return "FBFBFB";
            default:return "8E8E8E";
        }
    }
}

public class EventItem : EventContainer {
    public EventItem(string name, string guid, string content=null):base(name,guid){
        this.content=content==null?string.Empty:content;
    }
    public string content;
    public string display_content;
    public new virtual string EventContent()=>this.content;
    public override string GetReplacedContentString(){
        this.SetLanguage(new string[]{this.EventContent()});
        // this.EventContent=this.EventContent();
        return string.Empty;
    }
}
public class AutoEvent {
    public string token;
    public string content;
    public string[] option;
    public string[] jumpto;
    public int esc_index;
    public Action<ComplexTaiwuEvent> enter=(self)=>{};
    public Func<ComplexTaiwuEvent, bool> condition=(self)=>true;
    public Func<ComplexTaiwuEvent, bool> visible=(self)=>true;
    public string MainRoleKey="RoleTaiwu";
    public string TargetRoleKey="CharacterId";
    public Func<ComplexTaiwuEvent, bool>[] conditions=null;
    // public bool condition(int i)=>i<this.Count && check_condition(i, condition_code[i]);
    // public string[] effect_code=null;
    // public string effect(int i)=>i<this.Count?execute_effect(i, effect_code[i]):string.Empty;
    public int Count=>option.Length;
    public string EscOptionKey => this.esc_index>99||this.esc_index<0?string.Empty:EscKey(esc_index);
    public static string EscKey(int esc_index)=>"Option_-352900"+esc_index.ToString("02");
    //templates: token visible condition enter content option jumpto esc_index
    public static void dispatcher(ComplexTaiwuEvent self){
        logger($"dispatcher called, token={self.Event.token}");
        for(int i=0;i<self.Event.conditions.Length-1;i++){
            if(self.Event.conditions[i](self)){
                self.Event(self.Event.jumpto[i]);
                GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(BYPASS_EVENT);
                return;
            }
        }
        self.Event(self.Event.jumpto[self.Event.jumpto.Length-1]);
        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(BYPASS_EVENT);
    }
    public AutoEvent(string tsvline){
        try {
            var content=tsvline.Split('\t');
            if(content.Length<5){
                throw new Exception($"事件读取有误，预期长度至少为5，实际长度{content.Length}，原始事件行如下：\n{tsvline}");
            }
            this.token=content[0].Trim();
            this.content=content[1].Replace("\\n","\n");
            this.option=content[2].Split(',');
            this.jumpto=content[3].Split(',').Select((x)=>x.Trim()).ToArray();
            if(this.option.Length!=this.jumpto.Length){
                logwarn($"option.Length({this.option.Length})与jumpto.Length({this.jumpto.Length})不相等，将二者置空");
                this.option=new string[]{"option.Length({this.option.Length})与jumpto.Length({this.jumpto.Length})不相等，将二者置空"};
                this.jumpto=new string[]{string.Empty};
                this.esc_index=-1;
            }else{
                this.esc_index=int.Parse(content[4]);
            }
            if(content.Length>5){
                Type box=Type.GetType(content[5].Trim());
                if(box!=null){
                    if(content.Length>6&&!string.IsNullOrEmpty(content[6].Trim())){
                        if(content[6].Trim().ToLower()=="dispatcher"){
                            this.conditions=content[2].Split(',').Select((cond)=>process_condition(cond,box)).ToArray();
                            this.enter=AutoEvent.dispatcher;
                        } else {
                            this.enter=process_enter(content[6],box);
                        }
                    }
                    if(content.Length>7&&!string.IsNullOrEmpty(content[7].Trim())){
                        this.condition=process_condition(content[7],box); // 仿照SAT，先算or后算and，懒得支持括号了
                    }
                    if(content.Length>8&&!string.IsNullOrEmpty(content[8].Trim())){
                        this.visible=process_condition(content[8],box);
                    }
                } else if((content.Length>8&&!string.IsNullOrEmpty(content[8].Trim()))||(content.Length>7&&!string.IsNullOrEmpty(content[7].Trim()))||(content.Length>6&&!string.IsNullOrEmpty(content[6].Trim()))){
                    throw new Exception($"找不到类{content[5].Trim()}");
                }

                if(content.Length>9){
                    this.MainRoleKey=content[9].Trim();
                }
                if(content.Length>10){
                    this.TargetRoleKey=content[10].Trim();
                }
            }
        }catch(Exception ex){
            logwarn($"语句{tsvline}不能正常转化为tsv事件，错误如下：");
            logex(ex);
            throw ex;
        }
    }
    public static Action<ComplexTaiwuEvent> process_enter(string code, Type box){
        if(string.IsNullOrEmpty(code.Trim())){return (_)=>{};}
        ParameterExpression param = Expression.Parameter(typeof(ComplexTaiwuEvent));
        if(code.Split(',').Length>1){
            var result=Expression.Lambda<Action<ComplexTaiwuEvent>>(
                Expression.Block(code.Split(',').Select((x)=>{try{ return Expression.Call(box.GetMethod(x.Trim(),(BindingFlags)(-1)), param);}catch(Exception ex){logwarn($"类{box}找不到{x.Trim()}");logex(ex);throw ex;}})),
                param
            );
            logger($"OnEvent {code} is ({String.Join(",",result.Parameters)}) => {{ {String.Join(";",(result.Body as BlockExpression).Expressions.Select((x)=>x.ToString()))} }}");
            return result.Compile();
        } else {
            var method=box.GetMethod(code.Trim(),(BindingFlags)(-1));
            if(method==null){
                throw new Exception($"类{box}找不到{code.Trim()}");
            }
            var result=Expression.Lambda<Action<ComplexTaiwuEvent>>(
                Expression.Call(method, param),param
            );
            logger($"OnEvent {code} is {result}");
            return result.Compile();
        }
    }
    static Func<ComplexTaiwuEvent,bool> process_condition(string code, Type box){
        if(string.IsNullOrEmpty(code.Trim())){return (_)=>true;}
        ParameterExpression param = Expression.Parameter(typeof(ComplexTaiwuEvent));
        var result = Expression.Lambda<Func<ComplexTaiwuEvent, bool>>(
            code.Split('&',StringSplitOptions.RemoveEmptyEntries).Select(
                (ors)=>
                    ors.Split('|',StringSplitOptions.RemoveEmptyEntries).Select(
                        (func)=>{try {return func.Trim()[0] switch {
                            '!'=>Expression.Not(Expression.Call(box.GetMethod(func.Split('!')[1].Trim()), param)) as Expression,
                            _=>Expression.Call(box.GetMethod(func.Trim()), param) as Expression
                        };}catch (Exception ex){
                            logwarn($"cannot find {func}");
                            throw ex;
                        }}
                    ).Aggregate(
                        (first,second)=>Expression.OrElse(first,second) as Expression
                    )
            ).Aggregate(
                (first,second)=>Expression.AndAlso(first,second)
            ).Reduce(),param
        );
        logger($"condition {code} reduce to {result}");
        return result.Compile();
    }
    public AutoEvent(string token, string content, string[] option,int esc_index=-1){
        this.token=token;
        this.content=content;
        this.option=option;
        this.esc_index=esc_index;
        // 理论上jumpto和effect_code可以二选一，使用以null初始化，而后使用函数进行进一步初始化。
        this.jumpto=new string[this.Count];
        Array.Fill(this.jumpto,string.Empty);
    }
    public AutoEvent(string token, string content, string[] option, string[] jumpto, int esc_index=-1){
        if(option.Length!=jumpto.Length){
            throw new Exception($"输入长度不匹配,option.Length={option.Length},jumpto.Length={jumpto.Length}");
        }
        this.token=token;
        this.content=content;
        this.option=option;
        this.esc_index=esc_index;
        // 理论上jumpto和effect_code可以二选一，使用以null初始化，而后使用函数进行进一步初始化。
        this.jumpto=jumpto;
        // Array.Fill(this.jumpto,string.Empty);
    }
    public AutoEvent OnEnter(Action<ComplexTaiwuEvent> fn){
        this.enter=fn;
        return this;
    }
    public AutoEvent OnVisible(Func<ComplexTaiwuEvent, bool> fn){
        this.visible=fn;
        return this;
    }
    public AutoEvent OnCondition(Func<ComplexTaiwuEvent, bool> fn){
        this.condition=fn;
        return this;
    }
}
public class BypassEvent:EventContainer {
    public BypassEvent(string name, string guid):base(name,guid){}
    public override void OnEventEnter(){
        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(BASE_EVENT);
    }
}
public class ComplexTaiwuEvent : EventItem {
    public new AutoEvent Event=null;
    public static ComplexTaiwuEvent Instance;
    public ComplexTaiwuEvent(string name, string guid):base(name,guid){
        this.EscOptionKey = "Option_-35290000";
        this.EventOptions=new TaiwuEventOption[100];
        for(int j=0;j<100;j++){
            int i=j;
            this.EventOptions[i]=new TaiwuEventOption{
                OptionKey = "Option_-352900"+i.ToString("02"),
                OnOptionSelect = this.OnOptionSelect(i),
                OnOptionVisibleCheck = this.OnOptionVisibleCheck(i),
                OnOptionAvailableCheck = ()=>true,
                GetReplacedContent = this.Desc(i),
                GetExtraFormatLanguageKeys = this.GetExtraFormatLanguageKeys
            };
        };
        this.content=string.Empty;
    }
    public Func<bool> OnOptionVisibleCheck(int i)=>()=>{try{return string.IsNullOrEmpty(this.Event.jumpto[i])||EventContainer.events[EventContainer.event_token[this.Event.jumpto[i]]].visible(ComplexTaiwuEvent.Instance);}catch{return false;}};
    public Func<bool> OnOptionAvailableCheck(int i)=>()=>{try{return string.IsNullOrEmpty(this.Event.jumpto[i])||EventContainer.events[EventContainer.event_token[this.Event.jumpto[i]]].condition(ComplexTaiwuEvent.Instance);}catch{return false;}};
    public override void OnEventEnter(){
        int index=0;
        if(this.ArgBox.Get("NeutronIndex",ref index)){
            this.ArgBox.Remove<int>("NeutronIndex");
            this.Event=EventContainer.events[index];
        } else {
            string token=string.Empty;
            if(!this.ArgBox.Get("NeutronToken",ref token)){
                logwarn("OnEventEnter failed.");
            }
            this.ArgBox.Remove<int>("NeutronToken");
            this.Event=EventContainer.events[EventContainer.event_token[token]];
        }
        if(this.Event.esc_index<0){
            var x=this.Event.esc_index;
            for(int idx=this.Event.option.Length-1;idx>=0;idx--){
                try {
                    var to=EventContainer.events[EventContainer.event_token[this.Event.jumpto[idx]]];
                    if(to.visible(ComplexTaiwuEvent.Instance) && to.condition(ComplexTaiwuEvent.Instance)){
                        x++;
                        if(x==0){
                            this.EscOptionKey=AutoEvent.EscKey(idx);
                            break;
                        }
                    }
                } catch {this.EscOptionKey=string.Empty;}
            }
        } else {
            this.EscOptionKey=this.Event.EscOptionKey;
        }
#if VERBOSE
        // logger($"event token is {this.Event.token}");
        for(int idx=0;idx<this.Event.option.Length;idx++){
            try {
                if(string.IsNullOrEmpty(this.Event.jumpto[idx])){
                    // logger($"Option {idx}: end event.");
                } else {
                    var to=EventContainer.events[EventContainer.event_token[this.Event.jumpto[idx]]];
                    // logger($"Option {idx}: visible:{to.visible(ComplexTaiwuEvent.Instance)} available:{to.condition(ComplexTaiwuEvent.Instance)}");
                }
            } catch (Exception ex){
                logger($"logging Option {idx} ({this.Event.jumpto[idx]}) failed");
                logex(ex);
            }
        }
#endif
        if(!string.IsNullOrEmpty(this.Event.MainRoleKey)){this.MainRoleKey=this.Event.MainRoleKey;}
        if(!string.IsNullOrEmpty(this.Event.TargetRoleKey)){this.TargetRoleKey=this.Event.TargetRoleKey;}
        this.Event.enter(ComplexTaiwuEvent.Instance);
    }
    public override void OnEventExit(){}
    public override string EventContent(){
        return this.Event.content;
    }
    new Func<string> Desc(int i)=>()=>{
        this.EventOptions[i].SetContent(this.Event.option[i]);
        return string.Empty;
    };
    Func<string> OnOptionSelect(int i){
        return ()=>{
            var to=i<this.Event.jumpto.Length?this.Event.jumpto[i]:string.Empty;
            // NeutronChain: debug only, show the whole chain.
            string x=string.Empty;
            this.ArgBox.Get("NeutronChain",ref x);
            if(EventContainer.event_token.ContainsKey(to)){
                this.ArgBox.Set("NeutronChain",x + to + " -> ");
                to=Event(this.ArgBox, to);
            } else {
                logger("neutron chain finished: "+x+(to==string.Empty?"(finish)":to));
                this.ArgBox.Remove<string>("NeutronChain");
            }
            return to;
        };
    }
    // public override string GetReplacedContentString(){
    //     logwarn("replace check");
    //     logger(this.Event.token);
    //     logger(this.Event.content);
    //     logger(this.Event.option[0]);
    //     return base.GetReplacedContentString();
    // }
}
public class MainTaiwuEvent : EventItem {
    public MainTaiwuEvent(string name, string guid):base(name,guid){
        this.EscOptionKey = "Option_35290000";
        this.EventOptions=new TaiwuEventOption[1]{new TaiwuEventOption{
            OptionKey = "Option_35290000",
            OnOptionSelect = new Func<string>(this.OnOptionSelect),
            OnOptionVisibleCheck = new Func<bool>(this.OnCheckEventCondition),
            OnOptionAvailableCheck = new Func<bool>(this.OnCheckEventCondition),
            GetReplacedContent = new Func<string>(this.Desc),
            GetExtraFormatLanguageKeys = new Func<List<string>>(this.GetExtraFormatLanguageKeys)
        }};
        this.content=string.Empty;
    }
    bool erase=true;
    public override bool OnCheckEventCondition()=>true;
    public override void OnEventExit(){
        if(this.erase){
            this.ArgBox.Remove<string>("NeutronEvent");
            this.ArgBox.Remove<string>("NeutronOption");
            this.ArgBox.Remove<string>("NeutronJumpto");
        } else this.erase=true;
    }
    public override string EventContent(){
        this.ArgBox.Get("NeutronEvent",ref this.content);
        return this.content;
    }
    string Desc(){
        string x=string.Empty;
        this.ArgBox.Get("NeutronOption",ref x);
        return x;
    }
    string OnOptionSelect(){
        string x=string.Empty;
        this.ArgBox.Get("NeutronJumpto",ref x);
        if(x.Contains("\t")){
            this.erase=false;
            var z=x.Split("\t");
            if(z.Length>1){
                this.ArgBox.Set("NeutronEvent",z[0]);
                this.ArgBox.Set("NeutronOption",z[1]);
                this.ArgBox.Set("NeutronJumpto",string.Join("\t",z[2..]));
                return "70b4d45d-ba15-423d-b94e-b40e348eb9bf";
            }
        }
        return x;
    }
}
public class UnfairTrue : Redzen.Random.IRandomSource {
    public UnfairTrue(){}
    public void Reinitialise(ulong seed){}
    public int Next()=>0;
    public int Next(int maxValue)=>-2147483648;
    public int Next(int minValue, int maxValue)=>minValue;
    public double NextDouble()=>0;
    public void NextBytes(Span<byte> span){}
    public int NextInt()=>0;
    public uint NextUInt()=>0;
    public ulong NextULong()=>0;
    public bool NextBool()=>true;
    public byte NextByte()=>0;
    public float NextFloat()=>0f;
    public float NextFloatNonZero()=>1f;
    public double NextDoubleNonZero()=>1.0;
    public double NextDoubleHighRes()=>1.0;
}
