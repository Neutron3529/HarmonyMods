// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Collections.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/mscorlib.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Private.CoreLib.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Runtime.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Threading.Tasks.Parallel.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Collections.Concurrent.dll" -unsafe -optimize -deterministic Backend.cs *.CS -out:Backend.dll -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Linq.dll"
// -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../../../compatdata/838350/pfx/drive_c/Program Files/dotnet/shared/Microsoft.NETCore.App/5.0.17/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;
using System.Linq;

namespace Optim;
[TaiwuModdingLib.Core.Plugin.PluginConfig("Optim","Neutron3529","0.2.0")]
public partial class Optim : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Initialize()=>this.HarmonyInstance = new RobustHarmonyInstance(this.GetGuid());
    public override void Dispose()=>this.HarmonyInstance.UnpatchSelf();
    public static void logger(string s)=>GameData.Utilities.AdaptableLog.Info(s);
    public static void logwarn(string s)=>RobustHarmonyInstance.logwarn(s);
    public static void logex(Exception ex)=>RobustHarmonyInstance.logex(ex);
    public RobustHarmonyInstance HarmonyInstance;
    public static List<MethodInfo> methods=new List<MethodInfo>();
    private bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logger("enable 出错，出错键值为\""+key+"\"，错误原因是"+ex.Message);
            return false;
        }
    }
    HashSet<string> DomainName;
    HashSet<string> MethodName;
    private bool _enable(string key){
        bool enabled=false;
        if(key=="N01"){
            string s=string.Empty;
            if(GameData.Domains.DomainManager.Mod.GetSetting(this.ModIdStr, key, ref s) && s.Length>1){
                DomainName=new HashSet<string>(s.Split(','));
                return DomainName.Count>0;
            } else return false;
        } else if (key=="N02"){
            string s=string.Empty;
            if(GameData.Domains.DomainManager.Mod.GetSetting(this.ModIdStr, key, ref s) && s.Length>1){
                MethodName=new HashSet<string>(s.Split(','));
                return MethodName.Count>0;
            } else return false;
        }
        return GameData.Domains.DomainManager.Mod.GetSetting(this.ModIdStr, key, ref enabled) && enabled;
    }
    public override void OnModSettingUpdate(){
        this.HarmonyInstance.UnpatchSelf();
        methods.Clear();
        if(enable("N00"))try{methods.Add(typeof(GameData.Domains.Global.GlobalDomain).GetMethod("FixAllAbnormalDomainArchiveData",(BindingFlags)(-1)));}catch(Exception ex){logwarn("patch出错，可能是GameData.Domains.Global.GlobalDomain不存在FixAllAbnormalDomainArchiveData，具体报错信息如下：");logex(ex);}
        try{
            var q = from t in typeof(GameData.Domains.Global.GlobalDomain).Assembly.GetTypes()
            where t.IsClass && t.GetMethod("FixAbnormalDomainArchiveData",(BindingFlags)(-1)) != null
            select t;
            if(enable("N01"))try{
                // foreach(var x in DomainName){
                //     logger("Domain have "+x);
                // }
                foreach(var t in q){
                    // logger(t.Name);
                    if(DomainName.Contains(t.Name)){
                        methods.Add(t.GetMethod("FixAbnormalDomainArchiveData",(BindingFlags)(-1)));
                        // logger(t.Name+" Added, "+t.Name+".FixAbnormalDomainArchiveData Nullified.");
                    }
                }
            }catch(Exception ex){logwarn("禁用以下Domain的FixAbnormalDomainArchiveData出错，具体报错信息如下：");logex(ex);}
            if(enable("N02"))try{
                // foreach(var x in MethodName){
                //     logger("MethodName have "+x);
                // }
                foreach(var t in q){
                    foreach(var s in t.GetMethods((BindingFlags)(-1))){
                        // logger(s.Name);
                        if(MethodName.Contains(s.Name)){
                            methods.Add(s);
                            // logger(t.Name+"."+s.Name+" Nullified.");
                        }
                    }
                }
            }catch(Exception ex){logwarn("禁用带FixAbnormalDomainArchiveData的类中的函数出错，具体报错信息如下：");logex(ex);}
        }catch(Exception ex){logwarn("遍历type出错，具体报错信息如下：");logex(ex);}
        if(methods.Count>0)this.HarmonyInstance.PatchAll(typeof(NullifyAll));
        // logger("fine");
    }
    public class NullifyAll {
        public static IEnumerable<MethodBase> TargetMethods(){
            foreach(var i in methods){
                // logger("yield "+i.DeclaringType.Name+"."+i.Name);
                yield return i;
            }
        }
        public static bool Prefix(){
            return false;
        }
    }
}
