return {
	Title = "扩展突破盘与全黄点",
	--FileId = 24310421,
	Version = 20220921,
	BackendPlugins = {"Backend.dll"},
	--BackendPatches = {},
	--FrontendPlugins = {"Frontend.dll"},
	--FrontendPatches = {},
	--EventPackages = {},
	Author = "Neutron3529",
	Description = "现在突破时可以获取5总纲下全黄点（黄点数量取不同总纲中的最高值）加成，而非单总纲加成。鉴于不同总纲之间黄点数量各异，这里扩展了突破盘，以盛放多出的黄点"
	--[[
	,DefaultSettings = {
		{Key="maxV", DisplayName="maxV", Description="(toy only, not use for now) Max Value of a weapon could have (in order to prevent math overflow)", SettingType="Slider", DefaultValue=32000, MinValue=0, MaxValue=32767},
		{Key="mulF", DisplayName="mulF",  Description="(toy only, not use for now) multipiler of a weapon resource amount", SettingType="Slider", DefaultValue=30000, MinValue=0, MaxValue=32767},
		{Key="divN", DisplayName="divN",  Description="(toy only, not use for now) divided the resource amount after apply multipiler", SettingType="Slider", DefaultValue=100, MinValue=1, MaxValue=32767},
	}
	--]]--
}