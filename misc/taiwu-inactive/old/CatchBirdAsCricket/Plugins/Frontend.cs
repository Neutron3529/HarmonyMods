// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\mscorlib.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Assembly-CSharp.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" Frontend.cs -out:Frontend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace CatchBirdAsCricket;
[TaiwuModdingLib.Core.Plugin.PluginConfig("CatchBirdAsCricket","Neutron3529","0.1.1")]
public class CatchBirdAsCricket : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
	public override void Initialize() {
		this.HarmonyInstance.PatchAll(typeof(CatchAll));
	}
	[HarmonyPatch(typeof(UI_CatchCricket),"OnClickCatchPlace")]
	public static class CatchAll {
		//public static UI_CatchCricket.CricketPlaceInfo[] catchPlaceList=new UI_CatchCricket.CricketPlaceInfo[21];
		//public static void Prefix(UI_CatchCricket.CricketPlaceInfo[] ____catchPlaceList){catchPlaceList=____catchPlaceList;}
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			foreach(CodeInstruction i in instructions){
				if(i.opcode==OpCodes.Call && ((MethodInfo)(i.operand)).Name.Contains("AddMethodCall") ){
					i.operand=typeof(CatchAll).GetMethod("catch_all");
				}
				yield return i;
			}
		}
		public static void catch_all(int listenerId, ushort domainId, ushort methodId, short arg1, short arg2, short arg3, sbyte arg4) {
		//	foreach(var place in catchPlaceList){
		//		GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, place.CricketColorId, place.CricketPartsId, ��������, place.PlaceId);// ԭʼ����
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, arg1, arg2, 100, arg4); // ԭʼ���У������ĵ���100
				//GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 18, 0, 100, arg4);// ������
				//GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 19, 0, 100, arg4);// ���ν�
				//GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 21, 0, 100, arg4);// �˰�
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 21, 19, 100, arg4);// �˰����ν�
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 21, 19, 100, arg4);// �˰����ν�
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 21, 19, 100, arg4);// �˰����ν�
				//GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, arg1, arg2, 100, arg4);
		//	}
		}
	}
}