return {
	Title = "~我来抓鸟辣~",
	--FileId = 24310421,
	Version = 20220921,
	--BackendPlugins = {"Backend.dll"},
	--BackendPatches = {},
	FrontendPlugins = {"Frontend.dll"},
	--FrontendPatches = {},
	--EventPackages = {},
	Author = "Neutron3529",
	Description = "这个Mod可以在抓蛐蛐的同时，尝试捕捉隐藏生物，「八败三段锦」三次。\n「八败三段锦」，身如三段锦，叫声似鸟，是玩家对茄子咕咕咕的怨念之化身。"
	--[[
	,DefaultSettings = {
		{Key="maxV", DisplayName="maxV", Description="(toy only, not use for now) Max Value of a weapon could have (in order to prevent math overflow)", SettingType="Slider", DefaultValue=32000, MinValue=0, MaxValue=32767},
		{Key="mulF", DisplayName="mulF",  Description="(toy only, not use for now) multipiler of a weapon resource amount", SettingType="Slider", DefaultValue=30000, MinValue=0, MaxValue=32767},
		{Key="divN", DisplayName="divN",  Description="(toy only, not use for now) divided the resource amount after apply multipiler", SettingType="Slider", DefaultValue=100, MinValue=1, MaxValue=32767},
	}
	--]]--
}