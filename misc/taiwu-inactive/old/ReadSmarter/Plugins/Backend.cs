// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Collections.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\mscorlib.dll" -r:"..\..\..\Backend\GameData.dll" -r:"..\..\..\Backend\Redzen.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Private.CoreLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll" -unsafe -optimize -deterministic Backend.cs -out:Backend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll" 
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
 // Backend: ProcessMethodCall -> CallMethod -> ...
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace ReadSmarter_Backend;
[TaiwuModdingLib.Core.Plugin.PluginConfig("ReadSmarter_Backend","NobodyCares","0.1.1")]
public class ReadSmarter_Backend : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
	public override void Initialize() {
		this.HarmonyInstance.PatchAll(typeof(TaiwuDomainUpdateCombatSkillBookReadingProgress));
	}
	[HarmonyPatch(typeof(GameData.Domains.Taiwu.TaiwuDomain),"UpdateCombatSkillBookReadingProgress")]
	public class TaiwuDomainUpdateCombatSkillBookReadingProgress{
		public static void Postfix(GameData.Domains.Taiwu.TaiwuDomain __instance, GameData.Common.DataContext context, GameData.Domains.Item.SkillBook book){
			short skillTemplateId=book.GetCombatSkillTemplateId();
			GameData.Domains.Taiwu.TaiwuCombatSkill taiwuCombatSkill = (GameData.Domains.Taiwu.TaiwuCombatSkill) typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("GetTaiwuCombatSkill",(BindingFlags)(-1)).Invoke(__instance, new object[]{skillTemplateId});
			for(byte i=0;i<15;i++){
				typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("SetCombatSkillPageComplete",(BindingFlags)(-1)).Invoke(__instance, new object[]{context, book,i});
				typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("OfflineAddReadingProgress",(BindingFlags)(-1)).Invoke(__instance, new object[]{taiwuCombatSkill,i,1000});
			}
		}
	}
}