return {
	Title = "突破无忧",
	--FileId = 24310421,
	Version = 20220921,
	BackendPlugins = {"Backend.dll"},
	--BackendPatches = {},
	--FrontendPlugins = {"Frontend.dll"},
	--FrontendPatches = {},
	--EventPackages = {},
	Author = "Neutron3529",
	Description = "可用突破步数恒定为100"
	--[[
	,DefaultSettings = {
		{Key="maxV", DisplayName="maxV", Description="(toy only, not use for now) Max Value of a weapon could have (in order to prevent math overflow)", SettingType="Slider", DefaultValue=32000, MinValue=0, MaxValue=32767},
		{Key="mulF", DisplayName="mulF",  Description="(toy only, not use for now) multipiler of a weapon resource amount", SettingType="Slider", DefaultValue=30000, MinValue=0, MaxValue=32767},
		{Key="divN", DisplayName="divN",  Description="(toy only, not use for now) divided the resource amount after apply multipiler", SettingType="Slider", DefaultValue=100, MinValue=1, MaxValue=32767},
	}
	--]]--
}