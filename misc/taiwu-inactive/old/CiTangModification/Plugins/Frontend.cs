// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\mscorlib.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Assembly-CSharp.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" Frontend.cs -out:Frontend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace CiTangModification;
[TaiwuModdingLib.Core.Plugin.PluginConfig("CiTangModification","Neutron3529","0.1.1")]
public class CiTangModification : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
	public override void Initialize() {
		this.HarmonyInstance.PatchAll(typeof(CiTang_NoAgeRestrict));
		this.HarmonyInstance.PatchAll(typeof(CiTang_BetterEffect_NoAuthCost));
		
	}
	[HarmonyPatch(typeof(UI_BuildingTaiwuShrine),"InitTaiwuInfo")]
	public class CiTang_NoAgeRestrict {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldfld,typeof(GameData.Domains.Character.Display.GroupCharDisplayData).GetField("CurrAge"))
				).Repeat( matcher => // Do the following for each match
					matcher
					.SetAndAdvance(
						OpCodes.Pop,null
					).InsertAndAdvance(
						new CodeInstruction(OpCodes.Ldc_I4_S,100)
					)
				).InstructionEnumeration();
			return instructions;
		}
	}
	[HarmonyPatch(typeof(UI_BuildingTaiwuShrine),"OnSelectSkill")]
	public class CiTang_BetterEffect_NoAuthCost {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Conv_I1),
					new CodeMatch(OpCodes.Ldarg_2),
					new CodeMatch(OpCodes.Call,typeof(GameData.Domains.Character.SkillQualificationBonus).GetConstructor(new Type[]{typeof(sbyte),typeof(sbyte),typeof(sbyte),typeof(short)}))
				).Repeat( matcher => // Do the following for each match
					matcher
					.SetAndAdvance(
						OpCodes.Pop,null
					).InsertAndAdvance(
						new CodeInstruction(OpCodes.Ldc_I4_S,120)
					)
				).InstructionEnumeration();
			return instructions;
		}
		public static void Prefix(ref ushort ____shrineBuyTimes){
			____shrineBuyTimes=65535;
		}
	}
}