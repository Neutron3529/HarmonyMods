// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Collections.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\mscorlib.dll" -r:"..\..\..\Backend\GameData.dll" -r:"..\..\..\Backend\Redzen.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Private.CoreLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll" -unsafe -optimize -deterministic Backend.cs -out:Backend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll" 
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
 // Backend: ProcessMethodCall -> CallMethod -> ...
 
 #if EnableAll
	#define LifeSkill
	#define CombatSkill
	#define BaseMainAttributes
 #endif
 
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace AllFeature_Backend;
[TaiwuModdingLib.Core.Plugin.PluginConfig("AllFeature_Backend","NobodyCares","0.1.1")]
public class AllFeature_Backend : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
	public override void Initialize() {
		this.HarmonyInstance.PatchAll(typeof(AllFeature));
	}
	[HarmonyPatch(typeof(GameData.Domains.Character.Character),"GenerateRandomBasicFeatures")]
//	[HarmonyPatch(typeof(GameData.Domains.Character.Character),"OfflineCreateProtagonistRandomFeatures")]
	public class AllFeature {
//		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
//			return Transpilers.MethodReplacer(instructions,typeof(GameData.Domains.Character.Character).GetMethod("GenerateRandomBasicFeatures",(BindingFlags)(-1)),typeof(AllFeature).GetMethod("MG"));
//		}
		public static unsafe bool Prefix(Dictionary<short, short> featureGroup2Id, bool isProtagonist, bool allGoodBasicFeatures, ref GameData.Domains.Character.LifeSkillShorts ____baseLifeSkillQualifications, ref GameData.Domains.Character.CombatSkillShorts ____baseCombatSkillQualifications, ref GameData.Domains.Character.MainAttributes ____baseMainAttributes,  ref GameData.Domains.Character.LifeSkillShorts ____lifeSkillQualifications, ref GameData.Domains.Character.CombatSkillShorts ____combatSkillQualifications){
			if(!(isProtagonist || allGoodBasicFeatures)){
				return true;
			}
		#if LifeSkill
			fixed(short*ptr=____baseLifeSkillQualifications.Items)for (int i = 0; i < 16; i++){
				*(ptr+i)=900;
			}
		#endif
		#if CombatSkill
			fixed(short*ptr=____baseCombatSkillQualifications.Items)for (int i = 1; i < 14; i++){ // avoid finger check
				*(ptr+i)=500;
			}
		#endif
		#if BaseMainAttributes
			fixed(short*ptr=____baseMainAttributes.Items)for (int i = 0; i < 6; i++){
				*(ptr+i)=5000;
			}
		#endif
			____lifeSkillQualifications=____baseLifeSkillQualifications;
			____combatSkillQualifications=____baseCombatSkillQualifications;
			var alist=new List<short>();
			foreach (Config.CharacterFeatureItem item in ((IEnumerable<Config.CharacterFeatureItem>)Config.CharacterFeature.Instance)) {if (item != null) {
					if (item.Basic) {
						if (item.CandidateGroupId == 0) {
							alist.Add(item.TemplateId);
						} else if (item.CandidateGroupId == 1) {
							//for (int k = 0; k < (int)item.AppearProb; k++)
							//{
							//	CharacterDomain._normalNegativeBasicFeaturesPool.Add(item.TemplateId);
							//}
							//for (int l = 0; l < (int)item.ProtagonistAppearProb; l++)
							//{
							//	CharacterDomain._protagonistNegativeBasicFeaturesPool.Add(item.TemplateId);
							//}
						} else {
							// alist.Add(item.TemplateId); // 无根之人 石芯玉女 阴阳一体
						}
					}
				}
			}
			foreach (short featureId in alist) {
				Config.CharacterFeatureItem template = Config.CharacterFeature.Instance[featureId];
				if((!featureGroup2Id.ContainsKey(template.MutexGroupId))||featureId>featureGroup2Id[template.MutexGroupId]){
					featureGroup2Id[template.MutexGroupId]=featureId;
				}
			}
			return false;
		}
	}
}