// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\mscorlib.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Assembly-CSharp.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" Frontend.cs -out:Frontend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace MakeSpeedUp;
[TaiwuModdingLib.Core.Plugin.PluginConfig("MakeSpeedUp","Neutron3529","0.1.1")]
public class MakeSpeedUp : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
	public override void Initialize() {
		this.HarmonyInstance.PatchAll(typeof(MakeSpeedup)); // display only
	}
	[HarmonyPatch(typeof(UI_Make),"CheckMakeCondition")]
	public class MakeSpeedup{
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Stfld,typeof(UI_Make).GetField("_makeTime",(BindingFlags)(-1)))
				).Repeat( matcher => // Do the following for each match
					matcher
					.InsertAndAdvance(
						new CodeInstruction(OpCodes.Pop),
						new CodeInstruction(OpCodes.Ldc_I4_0)
//						new CodeInstruction(OpCodes.Ldc_I4_1),
//						new CodeInstruction(OpCodes.Call,typeof(Math).GetMethod("Min",new Type[]{typeof(short),typeof(short)}))
					).Advance(1)
				).InstructionEnumeration();
			return instructions;
		}
	}
}