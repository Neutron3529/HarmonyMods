// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\mscorlib.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Assembly-CSharp.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Unity.TextMeshPro.dll" -optimize -deterministic Frontend.cs -out:Frontend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace FastMake;
[TaiwuModdingLib.Core.Plugin.PluginConfig("FastMake","NoBodyCares","0.1.1")]
public class FastMake : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
	public override void Initialize() {
		this.HarmonyInstance.PatchAll(typeof(CatchAll));
//		this.HarmonyInstance.PatchAll(typeof(Speedup)); // display only
		this.HarmonyInstance.PatchAll(typeof(TraitCost));
		this.HarmonyInstance.PatchAll(typeof(RevealMap));
		this.HarmonyInstance.PatchAll(typeof(CricketAttackFirst));
		this.HarmonyInstance.PatchAll(typeof(CiTang_NoAgeRestrict));
		this.HarmonyInstance.PatchAll(typeof(CiTang_BetterEffect_NoAuthCost));
		this.HarmonyInstance.PatchAll(typeof(RevealHealth));
		
	}
	[HarmonyPatch(typeof(UI_BuildingTaiwuShrine),"InitTaiwuInfo")]
	public class CiTang_NoAgeRestrict {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldfld,typeof(GameData.Domains.Character.Display.GroupCharDisplayData).GetField("CurrAge"))
				).Repeat( matcher => // Do the following for each match
					matcher
					.SetAndAdvance(
						OpCodes.Pop,null
					).InsertAndAdvance(
						new CodeInstruction(OpCodes.Ldc_I4_S,100)
					)
				).InstructionEnumeration();
			return instructions;
		}
	}
	[HarmonyPatch(typeof(UI_BuildingTaiwuShrine),"OnSelectSkill")]
	public class CiTang_BetterEffect_NoAuthCost {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Conv_I1),
					new CodeMatch(OpCodes.Ldarg_2),
					new CodeMatch(OpCodes.Call,typeof(GameData.Domains.Character.SkillQualificationBonus).GetConstructor(new Type[]{typeof(sbyte),typeof(sbyte),typeof(sbyte),typeof(short)}))
				).Repeat( matcher => // Do the following for each match
					matcher
					.SetAndAdvance(
						OpCodes.Pop,null
					).InsertAndAdvance(
						new CodeInstruction(OpCodes.Ldc_I4_S,120)
					)
				).InstructionEnumeration();
			return instructions;
		}
		public static void Prefix(ref ushort ____shrineBuyTimes){
			____shrineBuyTimes=65535;
		}
	}
	[HarmonyPatch(typeof(WorldMapModel),"GetNeighborList")]
	public class RevealMap {
		public static void Prefix(ref int maxSteps){
			maxSteps=Math.Max(maxSteps,5);
		}
	}
	[HarmonyPatch(typeof(UI_CricketCombat),"OnListenerIdReady")]
	public static class CricketAttackFirst {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldc_I4_S,50),
					new CodeMatch(OpCodes.Ldc_I4_S,100)
				).Repeat( matcher => // Do the following for each match
					matcher
					.SetAndAdvance(
						OpCodes.Ldc_I4_S,100
					).Advance(1)
				).InstructionEnumeration();
			return instructions;
		}
	}
	[HarmonyPatch(typeof(UI_CatchCricket),"OnClickCatchPlace")]
	public static class CatchAll {
		//public static UI_CatchCricket.CricketPlaceInfo[] catchPlaceList=new UI_CatchCricket.CricketPlaceInfo[21];
		//public static void Prefix(UI_CatchCricket.CricketPlaceInfo[] ____catchPlaceList){catchPlaceList=____catchPlaceList;}
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			foreach(CodeInstruction i in instructions){
				if(i.opcode==OpCodes.Call && ((MethodInfo)(i.operand)).Name.Contains("AddMethodCall") ){
					i.operand=typeof(CatchAll).GetMethod("catch_all");
				}
				yield return i;
			}
		}
		public static void catch_all(int listenerId, ushort domainId, ushort methodId, short arg1, short arg2, short arg3, sbyte arg4) {
		//	foreach(var place in catchPlaceList){
		//		GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, place.CricketColorId, place.CricketPartsId, 100, place.PlaceId);
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 18, 0, 100, arg4);// ������
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 19, 0, 100, arg4);// ���ν�
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 21, 0, 100, arg4);// �˰�
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 21, 19, 100, arg4);// �˰����ν�
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 21, 19, 100, arg4);// �˰����ν�
				GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, 21, 19, 100, arg4);// �˰����ν�
				//GameData.GameDataBridge.GameDataBridge.AddMethodCall<short, short, short, sbyte>(listenerId, domainId, methodId, arg1, arg2, 100, arg4);
		//	}
		}
	}
	[HarmonyPatch(typeof(UI_Make),"CheckMakeCondition")]
	public class Speedup{
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Stfld,typeof(UI_Make).GetField("_makeTime",(BindingFlags)(-1)))
				).Repeat( matcher => // Do the following for each match
					matcher
					.InsertAndAdvance(
						new CodeInstruction(OpCodes.Pop),
						new CodeInstruction(OpCodes.Ldc_I4_0)
//						new CodeInstruction(OpCodes.Ldc_I4_1),
//						new CodeInstruction(OpCodes.Call,typeof(Math).GetMethod("Min",new Type[]{typeof(short),typeof(short)}))
					).Advance(1)
				).InstructionEnumeration();
			return instructions;
		}
	}
	public static class TraitCost{
		static IEnumerable<MethodBase> TargetMethods(){
			yield return typeof(UI_NewGame).GetMethod("OnAbilityClick",(BindingFlags)(-1));
			yield return typeof(UI_NewGame).GetMethod("OnCellItemRender",(BindingFlags)(-1));
			yield return typeof(UI_NewGame).GetMethod("UpdatePoints",(BindingFlags)(-1));
			yield return typeof(UI_NewGame).GetMethod("OnStartNewGame",(BindingFlags)(-1));
		}
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldfld,typeof(Config.ProtagonistFeatureItem).GetField("Cost"))
				).Repeat( matcher => // Do the following for each match
					matcher
					.InsertAndAdvance(
						new CodeInstruction(OpCodes.Pop)
					)
					.SetAndAdvance(
						OpCodes.Ldc_I4_0,null
					)
				).InstructionEnumeration();
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldfld,typeof(Config.ProtagonistFeatureItem).GetField("PrerequisiteCost"))
				).Repeat( matcher => // Do the following for each match
					matcher
					.InsertAndAdvance(
						new CodeInstruction(OpCodes.Pop)
					)
					.SetAndAdvance(
						OpCodes.Ldc_I4_0,null
					)
				).InstructionEnumeration();
			return instructions;
		}
	}
	[HarmonyPatch(typeof(CommonUtils),"GetCharacterHealthInfo")]
	public static class RevealHealth{
		public static void Postfix(ref ValueTuple<string, float> __result, short health, short leftHealth){
			__result.Item1+="<color=#aec9e3>("+health.ToString()+"/"+leftHealth.ToString()+"</color>)";
		}
	}
}