#!/bin/bash -e
#   * compile instructions: put this file in
#   *         `steamapps/common/Dyson Sphere Program/CustomMod`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x powerFull.cs
#   *     ./powerFull.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

if [ -z "$__DOTNET_CSC" ]; then
    export __DOTNET_CSC="`find /usr/share/dotnet -type f -name dotnet` `find /usr/share/dotnet -name csc.dll`"
    echo '$'"__DOTNET_CSC not set yet, you should execute"
    echo "export __DOTNET_CSC='$__DOTNET_CSC'"
    echo "manually, or this alert will occur each time you execute this script."
fi

__MODE_VERBOSE=77 # is the line number of "#define VERBOSE", may be modified
__MODE_DEBUG__=$((__MODE_VERBOSE+1))
__MODE_RELEASE=$((__MODE_DEBUG__+1))

case $1 in
    V)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    v)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    VERBOSE) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    verbose) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    D)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    d)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    DEBUG)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    debug)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    *)       _MODE__SELECT_=$__MODE_RELEASE     ;;
esac

export GAME_NAME="The Legend of Liāu Thiam Ting"
export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"
export MELON_DIR="common/The Legend of Liāu Thiam Ting/MelonLoader/"
export PLUGIN_ID="Neutron3529.Cheat"
export NAMESPACE_ID="Neutron3529.Cheat"
# echo $(for i in "${MELON_DIR}/Managed/$ASSEMBLY"*.dll ; do echo -r:\"$i\" ; done)
( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_ ) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $__DOTNET_CSC -nologo -t:library \
  -r:"${MELON_DIR}/0Harmony.dll" \
  -r:"${MELON_DIR}/MelonLoader.dll" \
  -r:"${MELON_DIR}/Managed/UnityEngine.dll" \
  -r:"${MELON_DIR}/Managed/mscorlib.dll" \
  -r:"${MELON_DIR}/Managed/Assembly-CSharp.dll" \
  -r:"${MELON_DIR}/Managed/Assembly-CSharp-firstpass.dll" \
  -r:"${MELON_DIR}/Managed/System.Core.dll" \
  -r:"${MELON_DIR}/Managed/UnityEngine.CoreModule.dll" \
  -out:"${MELON_DIR}/../Mods/${FILE_NAME%.*}".dll \
  -optimize -deterministic `[ "$_MODE__SELECT_" == "$__MODE_DEBUG__" ] && echo -debug` \
  - # && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit

#define VERBOSE // the line of __MODE_VERBOSE
#define DEBUG



using System;
using UnityEngine;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;


using HarmonyLib;
using MelonLoader;


[assembly: MelonInfo(typeof(Cheat.Cheat), "My Mod Name", "version", "Author Name")]
//[assembly: MelonGame(null,null)]
[assembly: HarmonyDontPatchAll]

namespace Cheat
{
    public class Cheat : MelonMod {
#if DEBUG
        public static Action<string> logger;
#else
        public static void logger(string s){}
#endif
        public override void OnApplicationStart() {
#if DEBUG
            logger=LoggerInstance.Msg;
#endif
            var harmony=new HarmonyLib.Harmony("Neutron3529.Cheat");
            var ourFirstCategory = MelonPreferences.CreateCategory("OurFirstCategory");

            if(ourFirstCategory.CreateEntry("perfect_dodge", true).Value){
                logger("perfect_dodge patched-0");
                harmony.PatchAll(typeof(CharacterDodgeIsPerfectTiming));
                logger("perfect_dodge patched-1");
                harmony.PatchAll(typeof(CharacterDodgeIsDamageDodged));
                logger("perfect_dodge patched-2");
                //harmony.PatchAll(typeof(CharacterDodge_perfectDodgeDuration));//failed to patch
                logger("perfect_dodge patched-3");
            }
        }
        [HarmonyPatch(typeof(Zebra.CharacterDodge), "IsPerfectTiming", MethodType.Getter)]
        class CharacterDodgeIsPerfectTiming {
            public static void Postfix(ref bool __result) {
                __result=true;
            }
        }
        [HarmonyPatch(typeof(Zebra.CharacterDodge), "IsDamageDodged")]
        class CharacterDodgeIsDamageDodged {
            public static void Postfix(ref bool __result) {
                __result=true;
            }
        }
        [HarmonyPatch(typeof(Zebra.CharacterDodge), "_perfectDodgeDuration", MethodType.Getter)]
        class CharacterDodge_perfectDodgeDuration {
            public static void Postfix(ref float __result) {
                __result*=10f;
            }
        }
    }
}

