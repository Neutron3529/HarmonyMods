#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022-2023 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="$GAME_NAME"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

case $1 in
V)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
v)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
VERBOSE) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
verbose) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
D)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
d)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
DEBUG)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
debug)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
*)       EXTRA_DEFINE=""                                               ;;
esac

_MODE__SELECT_=100
( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Core.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Unity.IL2CPP.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/Il2CppInterop.Runtime.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/interop/Il2Cppmscorlib.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/interop/Il2CppSystem.Core.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/interop/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/interop/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/interop/Unity.Mathematics.FixedPoint.dll" \
  $(for i in "${GAME_BASE_DIR}/BepInEx/interop/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
  `[ -e "${GAME_BASE_DIR}/dotnet/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/dotnet/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/dotnet/System.dll" \
  -r:"${GAME_BASE_DIR}/dotnet/System.Linq.dll" \
  -r:"${GAME_BASE_DIR}/dotnet/System.Runtime.dll" \
  -r:"${GAME_BASE_DIR}/dotnet/System.Private.CoreLib.dll" \
  -r:"${GAME_BASE_DIR}/dotnet/mscorlib.dll" \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize $EXTRA_DEFINE \
  - $UTILS -define:IL2CPP && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
    R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
*) git commit -am "$2" ;;
esac
git push
fi
exit



using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Unity.IL2CPP;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%;
using Neutron3529.Utils;

[BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
public class Cheat : ModEntry {
    public Cheat() : base("%%PLUGIN_ID%%") {}
    public override void Init() {
        base.Init();
        // 主要逻辑放在`utils.cs`中，这里的Awake2只是为了以防万一
        // 目前来说，这个函数的唯一用途是用来“叮”……
        // 就像这样：
        logger("叮~修改器启动，请安心游戏");
    }
    public static bool IsPlayer(GamePlay.ActorBase actor)=>actor.Faction==1;

    class godmode : Entry {
        [Desc("玩家受伤倍数（不影响显示，包括地形伤害）", "lt", 1)]
        public static double pmul=0;
        static Unity.Mathematics.FixedPoint.Fp pmul_fp=Unity.Mathematics.FixedPoint.Fp.Zero;
        public override void Enable(){
            pmul_fp=(Unity.Mathematics.FixedPoint.Fp)pmul;
            base.Enable();
        }
        // [HarmonyPatch(typeof(GamePlay.PlayerObj),"NotifyBeHit")]
        // static void Prefix(GamePlay.PlayerObj __instance, ref GamePlay.ActorHitResult hitResult){
        //     if(__instance!=null && IsPlayer(__instance) && hitResult.DamageTotal>Unity.Mathematics.FixedPoint.Fp.Zero){
        //         hitResult.IncomingDamageScale=pmul_fp;
        //     }
        // }
        [HarmonyPatch(typeof(GamePlay.PlayerObj),"DealDamage")]
        static void Prefix(GamePlay.PlayerObj __instance, ref GamePlay.ActorHpLoss hpLoss){
            if(__instance!=null){
                if(IsPlayer(__instance) && hpLoss.HPDelta<Unity.Mathematics.FixedPoint.Fp.Zero){
                    hpLoss.HPDelta*=pmul_fp;
                }
            }
        }
    }
    class godmode_2 : Entry {
        [Desc("玩家伤害倍数（影响显示，不包括地形伤害）","gt",1)]
        public static double pmul=100;
        [Desc("怪物伤害倍数（影响显示，不包括地形伤害）","lt",1)]
        public static double mmul=0;
        [Desc("反弹伤害")]
        public static bool reflect=true;
        static Unity.Mathematics.FixedPoint.Fp mmul_fp=Unity.Mathematics.FixedPoint.Fp.Zero;
        static Unity.Mathematics.FixedPoint.Fp pmul_fp=Unity.Mathematics.FixedPoint.Fp.Zero;
        public override void Enable(){
            pmul_fp=(Unity.Mathematics.FixedPoint.Fp)pmul;
            mmul_fp=(Unity.Mathematics.FixedPoint.Fp)mmul;
            base.Enable();
        }
        [HarmonyPatch(typeof(GamePlay.BattleDamage),"ActorHitActor")]
        static void Prefix(ref GamePlay.ActorBase attacker,ref GamePlay.ActorBase target, ref Unity.Mathematics.FixedPoint.Fp damageScale){
            if(IsPlayer(attacker) || !IsPlayer(target)){
                damageScale*=pmul_fp;
            } else {
                if(reflect){
                    (attacker,target)=(target,attacker);
                    damageScale*=pmul_fp;
                } else {
                    damageScale*=mmul_fp;
                }
            }
        }
    }

    // class refreshCnt : Entry {
    //     [Desc("商店刷新次数设为此值")]
    //     public static uint add=0;
    //     [Desc("商店刷新后刷新资源消耗","lt",0)]
    //     public static int cnt=0;
    //     [HarmonyPrefix, HarmonyPatch(typeof(Act91.ACE.World.Shop),"MaxRefreshCount",Desc.set)]
    //     static void Prefix0(ref uint value){
    //         value=add;
    //     }
    //     [HarmonyPrefix, HarmonyPatch(typeof(Act91.ACE.World.ShopRefreshCountCost),"RefreshCount",Desc.set)]
    //     static void Prefix1(Act91.ACE.World.ShopRefreshCountCost __instance, ref uint value){
    //         if(__instance!=null && __instance.CostResource!=null){
    //             __instance.CostResource.Count=cnt;
    //         }
    //         value=add;
    //     }
    //     [HarmonyPrefix, HarmonyPatch(typeof(Act91.ACE.World.ShopGoods),"BuyCount",Desc.set)]
    //     static void Prefix2(Act91.ACE.World.ShopRefreshCountCost __instance, ref uint value){
    //         value=0;
    //         if(__instance!=null){
    //             __instance.CostResource.Count=0;
    //         }
    //     }
    // }

    // class moremoney : Entry {
    //     [Desc("玩家金钱数值")]
    //     public static int min=10000;
    //     [HarmonyPrefix, HarmonyPatch(typeof(Act91.ACE.Player),"Money",Desc.set)]
    //     static void Prefix1(ref int value)=>value=Math.Max(min,value);
    //     [HarmonyPrefix, HarmonyPatch(typeof(Act91.ACE.Player),"Coin",Desc.set)]
    //     static void Prefix2(ref int value)=>value=Math.Max(min,value);
    // }

    class morerefresh : Entry {
        [Desc("玩家刷新次数")]
        public static int min=0;
        public override void Enable(){
            harmony.PatchAll(typeof(ref1));
            harmony.PatchAll(typeof(ref2));
        }
        // [HarmonyPatch(typeof(STExploreDungeon.Types.ShopGoodsInfo),"BuyCount",Desc.get)]
        // static uint Postfix(uint value)=>0;
        static class ref1 {
            [HarmonyPrefix, HarmonyPatch(typeof(STExploreDungeon.Types.RewardInfo),"BlessCanRefreshCount",Desc.set)]
            static void Prefix1(ref int value)=>value=Math.Max(min,value);
            [HarmonyPrefix, HarmonyPatch(typeof(STExploreDungeon.Types.RewardInfo),"BlessSelectRefreshCount",Desc.set)]
            static void Prefix2(ref int value)=>value=Math.Max(min,value);
            [HarmonyPrefix, HarmonyPatch(typeof(STExploreDungeon.Types.RewardInfo),"PotentialCanRefreshCount",Desc.set)]
            static void Prefix3(ref int value)=>value=Math.Max(min,value);
            [HarmonyPostfix, HarmonyPatch(typeof(STExploreDungeon.Types.RewardInfo),"PotentialCanRefreshCount",Desc.get)]
            static uint Postfix1(uint value)=>Math.Max((uint)min,value);
            // [HarmonyPostfix, HarmonyPatch(typeof(ApiExploreDungeonSelectReward.Types.post.Types.res),"PotentialCanRefreshCount",Desc.get)]
            // static uint Postfix3(uint value)=>Math.Max((uint)min,value);
            // [HarmonyPostfix, HarmonyPatch(typeof(ApiExploreDungeonSelectReward.Types.post.Types.res),"BlessCanRefreshCount",Desc.get)]
            // static uint Postfix4(uint value)=>Math.Max((uint)min,value);
        }
        static class ref2 {
            [HarmonyPrefix, HarmonyPatch(typeof(STBreedDungeon.Types.RewardInfo),"BlessCanRefreshCount",Desc.set)]
            static void Prefix1(ref int value)=>value=Math.Max(min,value);
            [HarmonyPrefix, HarmonyPatch(typeof(STBreedDungeon.Types.RewardInfo),"BlessSelectRefreshCount",Desc.set)]
            static void Prefix2(ref int value)=>value=Math.Max(min,value);
            [HarmonyPrefix, HarmonyPatch(typeof(STBreedDungeon.Types.RewardInfo),"PotentialCanRefreshCount",Desc.set)]
            static void Prefix3(ref int value)=>value=Math.Max(min,value);
            [HarmonyPostfix, HarmonyPatch(typeof(STBreedDungeon.Types.RewardInfo),"PotentialCanRefreshCount",Desc.get)]
            static uint Postfix1(uint value)=>Math.Max((uint)min,value);
            // [HarmonyPostfix, HarmonyPatch(typeof(ApiBreedDungeonSelectReward.Types.post.Types.res),"PotentialCanRefreshCount",Desc.get)]
            // static uint Postfix3(uint value)=>Math.Max((uint)min,value);
            // [HarmonyPostfix, HarmonyPatch(typeof(ApiBreedDungeonSelectReward.Types.post.Types.res),"BlessCanRefreshCount",Desc.get)]
            // static uint Postfix4(uint value)=>Math.Max((uint)min,value);
        }
    }
    [Desc("不锁潜能",-1)]
    class active_potential : Entry {
        [HarmonyPatch(typeof(STPotential),"Active",Desc.set)]
        static void Prefix(ref bool value)=>value=true;
        [HarmonyPatch(typeof(STPotential),"Active",Desc.get)]
        static bool Postfix()=>true;
    }
    [Desc("不锁潜能",-1)]
    class show_choice : Entry {
        public override void Enable(){
            harmony.PatchAll(typeof(ref1));
            harmony.PatchAll(typeof(ref2));
            harmony.PatchAll(typeof(ref3));
        }
        static class ref1 {
            [HarmonyPatch(typeof(STPotential),"Active",Desc.set)]
            static void Prefix(ref bool value)=>value=true;
            [HarmonyPatch(typeof(STPotential),"Active",Desc.get)]
            static bool Postfix()=>true;
        }
        static class ref2 {
            [HarmonyPatch(typeof(STPotentialFixedPoint),"Active",Desc.set)]
            static void Prefix(ref bool value)=>value=true;
            [HarmonyPatch(typeof(STPotentialFixedPoint),"Active",Desc.get)]
            static bool Postfix()=>true;
        }
        static class ref3 {
            [HarmonyPatch(typeof(STPotentialFixedPointWrap),"Active",Desc.set)]
            static void Prefix(ref bool value)=>value=true;
            [HarmonyPatch(typeof(STPotentialFixedPointWrap),"Active",Desc.get)]
            static bool Postfix()=>true;
        }
    }
    // class moremoney : Entry {
    //     [Desc("货币最小数值")]
    //     public static int min=10000;
    //     [HarmonyPatch(typeof(STCurrency),"Count",Desc.get)]
    //     static int Postfix(int res)=>Math.Max(min,res);
    // }
}

