#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022-2023 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="ReverseCollapse"                          # might modify if the name mismatch.
export GAME_DIR="Reverse Collapse Code Name Bakery/windows" # might be modified, but "$GAME_NAME" cover most of the cases.

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

case $1 in
V)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
v)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
VERBOSE) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
verbose) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
D)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
d)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
DEBUG)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
debug)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
*)       EXTRA_DEFINE=""                                               ;;
esac

_MODE__SELECT_=100
( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
-r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Core.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Unity.IL2CPP.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/core/Il2CppInterop.Runtime.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/Il2Cppmscorlib.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/Il2CppSystem.Core.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/UnityEngine.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/UnityEngine.UI.dll" \
$(for i in "${GAME_BASE_DIR}/BepInEx/interop/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
`[ -e "${GAME_BASE_DIR}/dotnet/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/dotnet/netstandard.dll\""` \
-r:"${GAME_BASE_DIR}/dotnet/System.dll" \
-r:"${GAME_BASE_DIR}/dotnet/System.Runtime.dll" \
-r:"${GAME_BASE_DIR}/dotnet/System.Private.CoreLib.dll" \
-r:"${GAME_BASE_DIR}/dotnet/mscorlib.dll" \
-out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
-optimize $EXTRA_DEFINE \
- $UTILS -define:IL2CPP && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
    R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
*) git commit -am "$2" ;;
esac
git push
fi
exit




using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Unity.IL2CPP;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%;

[BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
public class Cheat : Neutron3529.ModEntry {
    public Cheat() : base("%%PLUGIN_ID%%") {}
    public override void Init() {
        base.Init();
        // 主要逻辑放在`utils.cs`中，这里的Init只是为了以防万一
        // 目前来说，这个函数的唯一用途是用来“叮”……
        // 就像这样：
        logger("叮~修改器启动，请安心游戏");
    }


    // [Desc("可操控人物")]
    static class 可操控人物 {
        // [Desc("可操控人物，以竖线分割，尽可填写ID")]
        // string names="10000|10100|11310|11314|11000|11100"; // 11310:武器站 11314:侦查机
        // static int[] list=new int[0];
        // public override void Enable(){
        //     list=(from x in (names.Split('|')) select int.Parse(x)).ToArray();
        //     foreach(var x in list){
        //         logger($"可操控人物 got {x}");
        //     }
        // }
        public static bool Contains(RCGame.UnitBattle __instance){
            // logger($"check {__instance.tableRow.name} {__instance.tableRow.name_short} {__instance.tableRow.group} {__instance.unitId} tableId={__instance.tableId} {__instance.unitData.TableId} ret {__instance.isInPlayerControl}");
            // return (__instance.isPlayerControllable || __instance.isInPlayerControl) && __instance.tableRow.group==0;
            return __instance.GetGroup()==RCGame.RCGameDefine.Group_Player;
        } // 可操控人物.list.Contains(__instance.tableId) || 可操控人物.list.Contains(__instance.unitData.TableId);
    }

    // [Desc, HarmonyPatch(typeof(RCGame.UnitBattle),"GetWorkBuffList")]
    // class Buff持续时间无限 : Entry {
    //     // [Desc("穿甲加值")]
    //     // static int add=100;
    //     static void Postfix(IEnumerable<RCGame.BuffData> rList, RCGame.UnitBattle __instance){
    //         if(可操控人物.Contains(__instance)){
    //             foreach(var buff in rList){
    //                 logger($"{buff.buffId}({buff.buffRow.id}): name={buff.buffRow.name}, desc={buff.buffRow.desc} create turn/round={buff.createTurn}/{buff.createRound} lastBuffMasterId={buff.lastBuffMasterId}");
    //             }
    //         }
    //     }
    // }

    class 化减为增 : Entry {
        public static IEnumerable<MethodBase> TargetMethods(){
            if(AP) yield return typeof(RCGame.UnitBattle).GetMethod("ModifyAP",(BindingFlags)(-1), new Type[]{typeof(int)});
            if(SP) yield return typeof(RCGame.UnitBattle).GetMethod("ModifySP",(BindingFlags)(-1), new Type[]{typeof(int),typeof(bool),typeof(bool)});
        }
        [Desc("AP化减为增")]
        static bool AP=true;
        [Desc("SP化减为增")]
        static bool SP=true;
        static void Prefix(RCGame.UnitBattle __instance, ref int iChangeValue){
            if(可操控人物.Contains(__instance)){iChangeValue=Math.Abs(iChangeValue);}
        }
    }
    // [Desc, HarmonyPatch(typeof(RCGame.UnitBattle),"ModifySP", new Type[]{typeof(int),typeof(bool),typeof(bool)})]
    // class SP只增不减 : Entry {
    //     static void Prefix(RCGame.UnitBattle __instance, ref int iChangeValue){
    //         if(可操控人物.Contains(__instance)){iChangeValue=Math.Max(0,iChangeValue);}
    //     }
    // }
    [Desc, HarmonyPatch(typeof(RCGame.RCGamePlayManager),"ModifyGP")]
    class 科技点数只增不减 : Entry {
        static void Prefix(ref int iChangeValue)=>iChangeValue=Math.Max(0,iChangeValue);
    }

    // [Desc]
    // // [HarmonyPatch(typeof(RCGame.UnitBattle),"AddCooldown")]
    // [HarmonyPatch(typeof(RCGame.SkillData),"ModifyCooldown")]
    // class 取消CD : Entry {
    //     static void Prefix(RCGame.SkillData __instance, ref int changeValue){
    //         if(可操控人物.Contains(__instance.ownerUnitBattle)){
    //             changeValue=Math.Min(changeValue,0);
    //         }
    //     }
    // }
    [Desc("取消CD_或许会影响蒙德“执念”生效次数，进而阻止获取蒙德舍己为人成就")]
    [HarmonyPatch(typeof(RCGame.SkillData),"AddCooldown")]
    class 取消CD : Entry {
        [Desc("不取消以下技能id的CD，以逗号分割id（默认不取消1级执念CD，希望蒙德无敌的可以升级执念，或者把这个选项清空）","never")]
        static string ids="11302100";
        [Desc("技能id记录","never")]
        public static string _="执念lv1->4:11302100->11302400;输血lv1->6:11213100->11213600";
        static HashSet<int> sid=new();
        public override void Init(bool check){
            foreach(var seg in ids.Split(',')){
                if(int.TryParse(seg, out var id)){
                    sid.Add(id);
                }
            }
            base.Init(check);
        }
        static bool Prefix(RCGame.SkillData __instance, int skillId)/*{
            if(可操控人物.Contains(__instance.ownerUnitBattle) && !sid.Contains(skillId)){
                logger($"取消 {skillId} CD");
                return false;
            }
            return true;
        }//*/=>sid.Contains(skillId) || !可操控人物.Contains(__instance.ownerUnitBattle);
    }

    [HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseMaxAp")]
    class 最大AP加值 : Entry {
        [Desc] static int add=0;
        static int Postfix(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret;
    }
    // logger($"读取暴击: isInPlayerControl:{__instance.isInPlayerControl}({__instance.unitId} {__instance.tableId} {__instance.unitData.TableId}) val={ret}");
    [HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseCritical")]
    class 暴击加值 : Entry {
        [Desc] static int add=500;
        static int Postfix(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret;
    }
    // [HarmonyPatch(typeof(RCGame.UnitHero),"GetWeaponSilencerWork")]
    // [Desc] class 消音器 : Entry {
    //     static bool Prefix(ref bool __result, RCGame.UnitBattle __instance){
    //         if(可操控人物.Contains(__instance)){
    //             __result=true;
    //             return false;
    //         }
    //         else {
    //             return true;
    //         }
    //     }
    // }
    class 射程加值 : Entry {
        [Desc] static int 劣势射程=0;
        [Desc] static int 优势射程=5;
        static int[] adds=new int[]{0,0,0};
        public override void Enable(){
            adds[0]=Math.Max(劣势射程,0);
            adds[1]=Math.Max(优势射程,0);
            adds[2]=0;
            base.Enable();
        }
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetAttackRange", new Type[]{typeof(int)})]
        static int pfx1(int ret, RCGame.UnitBattle __instance, int weaponRangeType)=>可操控人物.Contains(__instance)?ret+adds[Math.Min(weaponRangeType,2)]:ret;
        // [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetWeaponRangeIndex")]
        // static int pfx2(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?(ret==1?1:(ret+add)):ret;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitHero),"GetAttackRange", new Type[]{typeof(int)})]
        static int pfx3(int ret, RCGame.UnitBattle __instance, int weaponRangeType)=>可操控人物.Contains(__instance)?ret+adds[Math.Min(weaponRangeType,2)]:ret;
        // [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitHero),"GetWeaponRangeIndex")]
        // static int pfx4(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?(ret==1?1:(ret+add)):ret;
    }
    class 护甲加值 : Entry {
        [Desc] static int add=500;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseArmor")]
        static int pfx1(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetArmor")]
        static int pfx2(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret;
    }
    class 穿甲加值 : Entry {
        [Desc] static int add=1000;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetArmorPenetration")]
        static int pfx1(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseArmorPentration")]
        static int pfx2(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret;
    }
    [HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseDodge")]
    class 闪避加值 : Entry {
        [Desc] static int add=500;
        static int Postfix(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret;
    }
    [HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseHit")]
    class 命中加值 : Entry {
        [Desc] static int add=500;
        static int Postfix(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret;
    }

    // [HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseSightRange")]
    // class 保证视野 : Entry {
    //     [Desc("视野最小为此数值")]
    //     static int min=10;
    //     static int Postfix(int ret, RCGame.UnitBattle __instance){
    //         if(可操控人物.Contains(__instance)){
    //             // logger($"读取视野: isInPlayerControl:{__instance.isInPlayerControl}({__instance.unitId} {__instance.tableId} {__instance.unitData.TableId}) val={ret} GetBaseStealthRange={__instance.GetBaseStealthRange()} GetSightType={__instance.GetSightType()} GetSelfStealthRange={__instance.GetSelfStealthRange()} GetDetectionRange={__instance.GetDetectionRange()} GetMineRange={__instance.GetMineRange()} GetTrueSightRange={__instance.GetTrueSightRange()} GetTrueSightHideRange={__instance.GetTrueSightHideRange()} GetRawStealthRange={__instance.GetRawStealthRange(true)} ");
    //             ret=Math.Max(ret,min);
    //         }
    //         return ret;
    //     }
    // }

    // [HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseSightRange")]
    // class 视野 : Entry {
    //     [Desc("视野加值")]
    //     static int add=-1;
    //     static int Postfix(int ret, RCGame.UnitBattle __instance){
    //         if(可操控人物.Contains(__instance)){
    //             // logger($"读取视野: isInPlayerControl:{__instance.isInPlayerControl}({__instance.unitId} {__instance.tableId} {__instance.unitData.TableId}) val={ret} GetBaseStealthRange={__instance.GetBaseStealthRange()} GetSightType={__instance.GetSightType()} GetSelfStealthRange={__instance.GetSelfStealthRange()} GetDetectionRange={__instance.GetDetectionRange()} GetMineRange={__instance.GetMineRange()} GetTrueSightRange={__instance.GetTrueSightRange()} GetTrueSightHideRange={__instance.GetTrueSightHideRange()} GetRawStealthRange={__instance.GetRawStealthRange(true)} ");
    //             ret+=add;
    //         }
    //         return ret;
    //     }
    // }
    class 视野加值 : Entry {
        [Desc("视野加值","ne",0)]
        static int add=100;
        [Desc("敌方视野减值","ne",0)]
        static int dec=0;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseSightRange")]
        static int pfx1(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret-dec;
        // [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetDetectionRange")]
        // static int pfx2(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret-dec;
    }
    class 反潜视野 : Entry {
        [Desc("反潜视野加值","ne",0)]
        static int add=100;
        [Desc("敌方反潜视野减值","ne",0)]
        static int dec=100;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetBaseStealthRange")]
        static int Postfix1(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret-dec;
        // [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetDetectionRange")]
        // static int Postfix2(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret;
    }
    class 反隐视野 : Entry {
        [Desc("反隐视野加值","ne",0)]
        static int add=100;
        [Desc("敌方反隐视野减值","ne",0)]
        static int dec=100;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetSightHideRange")]
        static int p1(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret-dec;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetMineRange")]
        static int p3(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret-dec;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetTrueSightRange")]
        static int p4(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret-dec;
        [HarmonyPostfix, HarmonyPatch(typeof(RCGame.UnitBattle),"GetTrueSightHideRange")]
        static int p5(int ret, RCGame.UnitBattle __instance)=>可操控人物.Contains(__instance)?ret+add:ret-dec;
    }

    [Desc, HarmonyPatch(typeof(RCGame.UI.UIDlg.UIuiweaponprepare),"CheckCanLevel")]
    class 升级武器无视图纸零件消耗限制 : Entry {
        static bool Prefix(ref bool __result){__result=true;return false;}
    }

    [Desc, HarmonyPatch(typeof(RCGame.UI.UIDlg.UIuiunitgene), "CheckCanUpgrade")]
    class 学习技能无视基因潜能消耗_对2092R无效 : Entry {
        internal static bool CheckCanUpgrade(RCGame.CharacterData cd, RCGame.Table.TableRowUnitGene rowGene){
            var next=RCGame.RCGamePlayManager.instance.GetNextLvGene(rowGene);
            if(next is null){
                return false;
            }
            var cost=cd.costGenePoint;
            cd.costGenePoint=-1000;
            var curlv=rowGene.player_level;
            var nextlv=next.player_level;
            rowGene.player_level=next.player_level=1;
            var ret=RCGame.RCGamePlayManager.instance.CheckCanUpgrade(rowGene, cd.unitId);
            cd.costGenePoint=cost;
            rowGene.player_level=curlv;
            next.player_level=nextlv;
            return ret;
        }
        static bool Prefix(ref bool __result, RCGame.UI.UIDlg.UIuiunitgene __instance, RCGame.Table.TableRowUnitGene rowGene)=>!(__result=CheckCanUpgrade(__instance.CurCharacter, rowGene));
    }

    [Desc, HarmonyPatch(typeof(RCGame.CharacterData),"DoGeneUpgrade")]
    class 开发基因时直接满级_对2092R无效 : Entry {
        static bool Prefix(RCGame.CharacterData __instance, int gene_id, int level){
            for(int i=0;i<6;i++){
                if(学习技能无视基因潜能消耗_对2092R无效.CheckCanUpgrade(__instance, __instance.GetGeneData(gene_id).rowData)){
                    var next=RCGame.RCGamePlayManager.instance.GetNextLvGene(__instance.GetGeneData(gene_id).rowData);
                    if(next is not null){
                        __instance.costGenePoint+=next.cost_genepoint;
                        __instance.SetGeneLevel(gene_id, __instance.GetGeneData(gene_id).level+1);
                    } else {
                        logwarn($"在执行i={i},level={__instance.GetGeneData(gene_id).level+1}的升级时，检查之后的next竟然返回null");
                        break;
                    }
                } else {
                    break;
                }
            }
            return false;
        }
    }
    [Desc("背包物品无AP与数量消耗_取消EndUseItem的调用_或许会影响军用食物成就获取，以及使用物品次数统计（导致无法完成2092R「放置4个绊雷」的任务，但不管你怎么扔漫画书都不影响最后的S胜）",-1), HarmonyPatch(typeof(RCGame.RCGamePlayManager),"EndUseItem")]
    class 背包物品无AP与数量消耗_取消EndUseItem的调用 : Entry {
        static bool Prefix(){
            return false;
        }
    }
    [Desc("背包物品无AP与数量消耗_调用EndUseItem时获取物品_不影响使用物品次数统计，但会在2092R中更改物品存储顺序"), HarmonyPatch(typeof(RCGame.RCGamePlayManager),"EndUseItem")]
    class 背包物品无AP与数量消耗 : Entry {
        [Desc("不获取召唤物","never")]
        static bool exclude_ItemType_Turret=true;
        static void Prefix(RCGame.RCGamePlayManager __instance, int itemUid){
            var itemdata=__instance.GetItem(itemUid);
            if(exclude_ItemType_Turret && itemdata.rowData.category==RCGame.RCGameDefine.ItemCategory_Summon){
                return;
            }
            __instance.AddItem(itemdata.itemId, 1, isNew : false, isConsole : false, battleprepare_compose : false);
        }
    }


    [Desc, HarmonyPatch(typeof(RCGame.RCMapBattleManager),"CheckIsBanItem")]
    class 取消潜行关物品使用限制 : Entry {
        static bool Prefix(ref bool __result)=>__result=false;
    }
    // [Desc, HarmonyPatch(typeof(RCGame.CharacterData),"GetItemUpgrade")]
    // class 物品升级时直接满级 : Entry {
    //     [HarmonyReversePatch]
    //     static RCGame.ItemUpgrade GetItemUpgrade(RCGame.CharacterData __instance, int itemId)=>null;
    //     static bool Prefix(RCGame.ItemUpgrade __result, RCGame.CharacterData __instance, ref int itemId){
    //         for(int i=0;i<12;i++){
    //             var item=GetItemUpgrade(__instance, int itemId);
    //             if(item is not null){
    //                 __result=item;
    //             } else {
    //                 if(i>0){return false;} else {return true;}
    //             }
    //         }
    //         return false;
    //     }
    // }
    // [Desc, HarmonyPatch(typeof(RCGame.RCDlcManager),"isDeluxe",Desc.get)]
    // class 假装这是豪华版游戏 {
    //     static bool Postfix(ref bool __result){
    //         __result=true;
    //         return false;
    //     }
    // }
}

