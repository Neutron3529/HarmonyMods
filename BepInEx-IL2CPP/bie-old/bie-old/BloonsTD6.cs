#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="BepInEx"                                # might be modified, but "$GAME_NAME" cover most of the cases.
# note : This game have a very weak mod detect algorithm. It does not detect whether `winhttp.dll` is normal, but just detect whether BepInEx is installed
# move BepInEx out of the folder will bypass the detection.
export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

case $1 in
V)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
v)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
VERBOSE) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
verbose) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
D)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
d)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
DEBUG)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
debug)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
*)       EXTRA_DEFINE=""                                               ;;
esac

_MODE__SELECT_=100
( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
-r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Core.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Unity.IL2CPP.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/core/Il2CppInterop.Runtime.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/Il2Cppmscorlib.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/Il2CppSystem.Core.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/NinjaKiwi.LiNK.Aot.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/UnityEngine.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/UnityEngine.UI.dll" \
-r:"${GAME_BASE_DIR}/BepInEx/interop/UnityEngine.CoreModule.dll" \
$(for i in "${GAME_BASE_DIR}/BepInEx/interop/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
`[ -e "${GAME_BASE_DIR}/dotnet/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/dotnet/netstandard.dll\""` \
-r:"${GAME_BASE_DIR}/dotnet/System.dll" \
-r:"${GAME_BASE_DIR}/dotnet/System.Runtime.dll" \
-r:"${GAME_BASE_DIR}/dotnet/System.Private.CoreLib.dll" \
-r:"${GAME_BASE_DIR}/dotnet/mscorlib.dll" \
-out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
-optimize $EXTRA_DEFINE \
- $UTILS && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
    R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
*) git commit -am "$2" ;;
esac
git push
fi
exit




using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Unity.IL2CPP;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%;

[BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
public class Cheat : Neutron3529.ModEntry {
    public Cheat() : base("%%PLUGIN_ID%%") {}
    public override void Awake() {
        base.Awake();
        // 主要逻辑放在`utils.cs`中，这里的Awake2只是为了以防万一
        // 目前来说，这个函数的唯一用途是用来“叮”……
        // 就像这样：
        logger("叮~修改器启动，请安心游戏");
    }
    [HarmonyPatch(typeof(Assets.Scripts.Models.GameModel), "ScaleRewardForTrackDifficulty")]
    public class GameModelScaleRewardForTrackDifficulty:Entry{
        [Desc("游戏收入乘数")]
        public static float val=10;
        public static void Prefix(ref float reward) {
            reward*=val;
        }
    }
    [Desc("造塔不扣钱")]
    [HarmonyPatch(typeof(Assets.Scripts.Simulation.Towers.TowerManager), "CreateTower")]
    public class TowerManagerCreateTower:Entry{
        [Desc("造塔时将知识增加到此数值")]
        public static int val=30;
        [Desc("造塔时增加塔经验")]
        public static float exp=0;
        public static void Prefix(Assets.Scripts.Models.Towers.TowerModel def, ref bool deductCash) {
            if(deductCash){
                deductCash=false;
                if(exp>0){
                    Assets.Scripts.Unity.Game.instance.playerService.Player.AddTowerXP(def.name, exp);
                }
                if(val>Assets.Scripts.Unity.Game.instance.playerService.Player.Data.KnowledgePoints){
                    Assets.Scripts.Unity.Game.instance.playerService.Player.Data.KnowledgePoints=val;
                }
            }
        }
        public static void Postfix(Assets.Scripts.Models.Towers.TowerModel def, Assets.Scripts.Simulation.Towers.Tower __result) {
            __result.worth=def.cost;
        }
    }

    [HarmonyPatch(typeof(Assets.Scripts.Simulation.Towers.TowerManager), "UpgradeTower")]
    public class TowerManagerUpgradeTower:Entry{
        [Desc("塔升级价格乘数","GE",0)]
        public static float val=0;
        public static void Prefix(Assets.Scripts.Simulation.Towers.Tower tower, ref float upgradeCost) {
            tower.worth+=upgradeCost*(1-val);
            upgradeCost*=val;
        }
    }
    [Desc("金钱不足时也可以升级塔")]
    [HarmonyPatch(typeof(Assets.Scripts.Simulation.Towers.TowerManager), "CanUpgradeTower")]
    public class TowerManagerCanUpgradeTower:Entry{
        public static void Prefix(Assets.Scripts.Simulation.Towers.TowerManager __instance, Assets.Scripts.Simulation.Towers.Tower tower, int path, int tier, int inputIndex, ref float cost) {
            cost=0;
        }
        // [HarmonyReversePatch]
        // public static bool orig(Assets.Scripts.Simulation.Towers.TowerManager __instance, Assets.Scripts.Simulation.Towers.Tower tower, int path, int tier, int inputIndex, ref float cost) {
        //     throw new Exception("Wrong!");
        // }
    }
    [HarmonyPatch(typeof(Assets.Scripts.Simulation.Simulation), "AddCash")]
    public class MultiplyCash : Entry{
        [Desc("现金乘数")]
        public static double mul=10;
        public static void Prefix(ref double c) {
            if(c>0){
                c*=mul;
            }
        }
    }
    // [Desc("能力无冷却")]
    // public class AbilityCooldown:Const{
    //     public static void Enable(){
    //         foreach (var abilityModel in GameModelExt.GetAllAbilityModels(model))
    //         {
    //             abilityModel.Cooldown = 0f;
    //             abilityModel.maxActivationsPerRound = 999999999;
    //             abilityModel.restrictAbilityAfterMaxRoundTimer = false;
    //         }
    //     }
    // }
}

