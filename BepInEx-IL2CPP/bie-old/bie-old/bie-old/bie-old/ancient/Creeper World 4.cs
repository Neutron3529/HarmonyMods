#!/bin/bash -e
#   * compile instructions: put this file in
#   *         `steamapps/common/Dyson Sphere Program/CustomMod`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x powerFull.cs
#   *     ./powerFull.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export IFS=$'\n' # to disable the annoying space.
export DOTNET=dotnet # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

__MODE_VERBOSE=78 # is the line number of "#define VERBOSE", may be modified
__MODE_DEBUG__=$((__MODE_VERBOSE+1))
__MODE_RELEASE=$((__MODE_DEBUG__+1))

case $1 in
    V)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    v)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    VERBOSE) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    verbose) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    D)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    d)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    DEBUG)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    debug)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    *)       _MODE__SELECT_=$__MODE_RELEASE     ;;
esac

export GAME_BASE_DIR="common/Creeper World 4"
export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"
export PLUGIN_ID="Neutron3529.Cheat"
export NAMESPACE_ID="Neutron3529.Cheat"

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_ ) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Core.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.IL2CPP.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/UnhollowerBaseLib.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/unhollowed/Il2Cppmscorlib.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/unhollowed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/unhollowed/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/unhollowed/UnityEngine.CoreModule.dll" \
  $(for i in "${GAME_BASE_DIR}/BepInEx/unhollowed/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
  `[ -e "${GAME_BASE_DIR}/mono/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/mono/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/mono/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/mono/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/mono/Managed/mscorlib.dll" \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize `[ x"$_MODE__SELECT_" == x"$__MODE_DEBUG__" ] && echo -debug` \
  - && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit

#define VERBOSE // the line of __MODE_VERBOSE
#define DEBUG




using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.IL2CPP;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%
{
    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
    public class Cheat : BasePlugin {
#if DEBUG
        public static Action<string> logger;
#else
        public static void logger(string s){}
#endif
        public override void Load() {
            var harmony=new Harmony("%%PLUGIN_ID%%");
#if DEBUG
            logger=Log.LogInfo;
#endif
            logger("叮~您的修改器已启动");
            if(Config.Bind("config", "cooldown", true, "cooldown全部改为1").Value){
                harmony.PatchAll(typeof(tower_coolDown));
                logger("%%PLUGIN_ID%%-cooldown全部改为1，且弹药量至少为10-注入完成");
            }
            if(Config.Bind("config", "nocost", true, "资源无消耗").Value){
                //harmony.PatchAll(typeof(MVerseStash_various));
                //harmony.PatchAll(typeof(UnitManager_Update));
                logger("%%PLUGIN_ID%%-资源无消耗-注入完成");
            }
            logger("叮~");
        }
        public class tower_coolDown {
            public static bool Prepare(MethodBase __originalMethod){
                if(__originalMethod==null){
                    logger("first prepare, should return true.");
                    return true;
                }
                bool non_generic=!__originalMethod.IsGenericMethod;
                bool not_getter_setter=!(__originalMethod.Name.Length>4 && ((__originalMethod.Name[0]=='g' || __originalMethod.Name[0]=='s') && __originalMethod.Name[1]=='e' && __originalMethod.Name[2]=='t' && __originalMethod.Name[3]=='_'));
                logger("prepare "+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法, non_generic="+non_generic+", not_getter_setter="+not_getter_setter);
                return non_generic && not_getter_setter;
            }
            static IEnumerable<MethodBase> TargetMethods() {
                foreach(Type ty in new Type[]{ // type name here
                        //typeof(Max),
                        typeof(Mortar),
                        typeof(ACBomber),
                        typeof(MissileLauncher),
                        typeof(Sniper),
                        typeof(Sprayer),
                        //typeof(Strafer),
                        typeof(Cannon),
                    }){
                    logger("%%NAMESPACE_ID%%-tower_coolDown-准备注入"+ty.Name);
                    MethodInfo[] m=ty.GetMethods();
                    foreach(MethodInfo i in m)if(i.Name=="Update"){ // method name here
                        logger("%%NAMESPACE_ID%%-tower_coolDown-准备注入"+ty.Name+"的"+i.Name+"方法");
                        yield return i;
                    }
                    logger("%%NAMESPACE_ID%%-tower_coolDown-顺手注入一下commandbase的Update方法");
                    yield return typeof(CommandBase).GetMethod("Update");
                }
            }
            static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
                logger("%%NAMESPACE_ID%%-正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
                if(__originalMethod.DeclaringType!=typeof(CommandBase)){
                    yield return new CodeInstruction(OpCodes.Ldarg_0,null);
                    yield return new CodeInstruction(OpCodes.Ldc_I4_0,null);
                    yield return new CodeInstruction(OpCodes.Callvirt,__originalMethod.DeclaringType.GetProperty("coolDown").SetMethod);
                }
                yield return new CodeInstruction(OpCodes.Ldarg_0,null);
                yield return new CodeInstruction(OpCodes.Call,typeof(tower_coolDown).GetMethod(__originalMethod.DeclaringType==typeof(Sprayer)?"modify_tank":"modify_ammo"));
                foreach(CodeInstruction i in instructions){yield return i;}
                logger("%%NAMESPACE_ID%%-类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法patch生成成功。");
            }
            public static void modify_ammo(CommandBase __instance) {
                __instance.ammo=Mathf.Max(__instance.ammo,10);
            }
            public static void modify_tank(Sprayer __instance) {
                __instance.tank=Mathf.Max(__instance.tank,10);
            }
        }
        class MVerseStash_various {
            public static bool Prepare(MethodBase __originalMethod){
                if(__originalMethod==null){
                    logger("first prepare, should return true.");
                    return true;
                }
                bool non_generic=!__originalMethod.IsGenericMethod;
                bool not_getter_setter=!(__originalMethod.Name.Length>4 && ((__originalMethod.Name[0]=='g' || __originalMethod.Name[0]=='s') && __originalMethod.Name[1]=='e' && __originalMethod.Name[2]=='t' && __originalMethod.Name[3]=='_'));
                logger("prepare "+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法, non_generic="+non_generic+", not_getter_setter="+not_getter_setter);
                return non_generic && not_getter_setter;
            }
            static IEnumerable<MethodBase> TargetMethods() {
                var ty=typeof(MVerseStash);
                logger("%%NAMESPACE_ID%%-tower_coolDown-准备注入"+ty.Name);
                foreach(var str in new string[]{"AddAC","AddArg","AddEnergy","AddLiftic"}){ // method name here
                    MethodInfo i=ty.GetMethod(str);
                    logger("%%NAMESPACE_ID%%-tower_coolDown-准备注入"+ty.Name+"的"+i.Name+"方法");
                    yield return i;
                }
            }
            static void Prefix(ref float val) {
                val=Mathf.Abs(val);
            }
        }
    }
}
